# GOOD PRACTICE GIT

## BRANCHES

---

`**master**` branch is readable on 10.0.0150. Master is production so we don't touch that branch. Just updated before each weekly sprint.

`**dev**` branch is the common branch for all developpers. This is the branch with all the commits under development and which will receive merges from other branches(child-branches)

`**feature-branches**` created from dev, it takes the name you want and concern one feature,

<br><br>

---

## FEATURE-BANCHES & DEV

---

/!\ Never work on master or dev /!\

Only merge you own branch into dev when feature is 100% ok, not before and not if there is a bugg, even if its little

<br><br>

---

## PROCESS

---

**From Dev, create a branch by feature. Once this feature is up and running, you can merge it into the dev branch.**

<br><br>

How:
(_(dev) = from branch named dev_)

**1. ...prelude**<br><br>

11. _update your local dev branch with distant dev branch version, recover all new features_

get version on dev: <br>

> (dev): git pull origin dev

get diff pushed/merged on Master:<br>

> (dev): git pull origin master

update/init dev branch: <br>

> (dev): git pull

<br><br>

12. _from dev, create your own branch to work your feature_
    > (dev): \$git checkout -b myBranch
    > <br>
13. make a first push<br>
    modify something <br>
    > git add file_name<br>
    > git commit -m "first commit"<br>
    > (myBranch): \$git push --set-upstream origin myBranch<br>

<br><br>

**2. ... work on my feature**<br><br>

21. _push on you own branch your FUNCTIONNALS features_
    > (myBranch): $git add *file_name*<br>
    > (myBranch): $git commit -m"[] lorem ipsum"<br>
    > (myBranch): \$git push <br>

<br>
if the feature works well and is completed, switch on 3.

<br><br>

**3. ... Merge into dev** <br><br>

/!\ Before precoding to the next commands be sure that the functionality developed on your branch works really well and does not cause any buggs<br>

If everything is ok...<br><br>

31. **In gitLab**: <br>
    In projectName/branches use the button "Merge Request"<br>
    _ex: ...>Platform Material>Repository>Branches_<br>
    [http://10.0.0.150:7070/Vlad/vision-material/-/branches]<br>

32. **In the mergeRequest view**: <br>
    321. Change branches -> check the source branch (_here myBranch for ex_) and the Target branch (_here dev for ex_) + Compare<br>
    322. You can update the Title, the description,etc...<br>
    323. You HAVE TO set the assignee, by your own name OR advise: naming an other dev to check before the merge<br>
    324. Check Source and Target branch a new time<br>
    325. Do not delete source branch for the moment, at the end of the sprint<br>
    326. Submit merge request<br>

The merge will run automatically, if it finds a conflict you will be informed and can correct it
If everything is ok, switch on 4.
<br><br>

_Advice: before 4., test dev branch in local to be sure the merge well integrate the new feature._

**4. ... Work on a new features**<br><br>

Please restart from step 1.<br>
_Updating your local dev branch with the remote dev branch, and starting from this version to create your own branches, reduces conflicts at the end of the sprint._<br>

<br><br><br><br>

---

## TIPS

**before to push on dev**<br>
In order to avoid that the phase of the unit tests on master is blocking, <br>
test your feature or your new component with the command: <br>

> ng test<br>

## you can also read README-UNIT_TEST<br>

**After a merge on master**<br>
For exemple, you allready are working on your own branch. during the "Sprint meeting" Master is updated. Yout branch will be behind... to that and avoid future merge pb:

save your actual work

> (mybranch) git add..., commit...
> (mybranch) git checkout dev
> (dev) git pull origin master
> (dev) git pull
> (dev) git checkout mybranch
> (mybranch) git pull origin dev
> (mybranch) git pull

now your branch is updated !

**misc**

- regularly, create little commits (reduce conflicts and complications)
- commit clear messages , you can start you message by: "..."
  - [ fix ] + step or bugg name
  - [ update ] + step or feature updated name
  - [ add ] + step or feature added name
  - [ in progress ], + step
  - [ ok ], + name of feature finished
  - [ misc ] + details

<br><br><br><br>

---

## MERGE ERROR

---

**DEV->MASTER (unit test with karma)**
**If error during dev with merge request, TEST ERROR**: <br>

test with: <br>

> git log --oneline --graph
> If merge seems ok and not in GitLab, it's certainly because a karma unit test failed<br><br>

To verify : <br>
on terminal, compil with test:<br>

> ng test

You will find the test wich failed and why.
Correct it.

some help: README-UNIT_TEST

**If error during a merge, COMMIT BEHIND**

1. push something, (a console log added for exemple) on your origin branch
2. from this branch :
   > git pull origin TargetBranch
3. Retry the merge
