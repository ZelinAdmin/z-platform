import { Directive, ElementRef, EventEmitter, HostListener, Input, Output, } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * A material design file upload queue component.
 */
var FileUploadInputFor = /** @class */ (function () {
    function FileUploadInputFor(element) {
        this.element = element;
        this._queue = null;
        this.onFileSelected = new EventEmitter();
        this._element = this.element.nativeElement;
    }
    Object.defineProperty(FileUploadInputFor.prototype, "fileUploadQueue", {
        set: function (value) {
            if (value) {
                this._queue = value;
            }
        },
        enumerable: false,
        configurable: true
    });
    FileUploadInputFor.prototype.onChange = function () {
        var files = this.element.nativeElement.files;
        this.onFileSelected.emit(files);
        for (var i = 0; i < files.length; i++) {
            this._queue.add(files[i]);
        }
        this.element.nativeElement.value = '';
    };
    FileUploadInputFor.prototype.onDrop = function (event) {
        var files = event.dataTransfer.files;
        this.onFileSelected.emit(files);
        for (var i = 0; i < files.length; i++) {
            this._queue.add(files[i]);
        }
        event.preventDefault();
        event.stopPropagation();
        this.element.nativeElement.value = '';
    };
    FileUploadInputFor.prototype.onDropOver = function (event) {
        event.preventDefault();
    };
    FileUploadInputFor.ɵfac = function FileUploadInputFor_Factory(t) { return new (t || FileUploadInputFor)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
    FileUploadInputFor.ɵdir = i0.ɵɵdefineDirective({ type: FileUploadInputFor, selectors: [["input", "fileUploadInputFor", ""], ["div", "fileUploadInputFor", ""]], hostBindings: function FileUploadInputFor_HostBindings(rf, ctx) { if (rf & 1) {
            i0.ɵɵlistener("change", function FileUploadInputFor_change_HostBindingHandler() { return ctx.onChange(); })("drop", function FileUploadInputFor_drop_HostBindingHandler($event) { return ctx.onDrop($event); })("dragover", function FileUploadInputFor_dragover_HostBindingHandler($event) { return ctx.onDropOver($event); });
        } }, inputs: { fileUploadQueue: ["fileUploadInputFor", "fileUploadQueue"] }, outputs: { onFileSelected: "onFileSelected" } });
    return FileUploadInputFor;
}());
export { FileUploadInputFor };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FileUploadInputFor, [{
        type: Directive,
        args: [{
                selector: 'input[fileUploadInputFor], div[fileUploadInputFor]',
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { onFileSelected: [{
            type: Output
        }], fileUploadQueue: [{
            type: Input,
            args: ['fileUploadInputFor']
        }], onChange: [{
            type: HostListener,
            args: ['change']
        }], onDrop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }], onDropOver: [{
            type: HostListener,
            args: ['dragover', ['$event']]
        }] }); })();
//# sourceMappingURL=fileUploadInputFor.directive.js.map