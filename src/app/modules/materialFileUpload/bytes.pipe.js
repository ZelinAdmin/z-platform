import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
var BytesPipe = /** @class */ (function () {
    function BytesPipe() {
    }
    BytesPipe.prototype.transform = function (bytes) {
        if (isNaN(parseFloat('' + bytes)) || !isFinite(bytes))
            return '-';
        if (bytes <= 0)
            return '0';
        var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(1) + ' ' + units[number];
    };
    BytesPipe.ɵfac = function BytesPipe_Factory(t) { return new (t || BytesPipe)(); };
    BytesPipe.ɵpipe = i0.ɵɵdefinePipe({ name: "bytes", type: BytesPipe, pure: true });
    return BytesPipe;
}());
export { BytesPipe };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(BytesPipe, [{
        type: Pipe,
        args: [{ name: 'bytes' }]
    }], null, null); })();
//# sourceMappingURL=bytes.pipe.js.map