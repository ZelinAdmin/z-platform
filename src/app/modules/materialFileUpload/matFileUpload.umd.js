(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('@angular/common'), require('@angular/cdk'), require('@angular/platform-browser'), require('@angular/animations')) :
    typeof define === 'function' && define.amd ? define(['exports', '@angular/core', 'rxjs', '@angular/common', '@angular/cdk', '@angular/platform-browser', '@angular/animations'], factory) :
    (global = global || self, factory(global.matFileUpload = {}, global.core, global.rxjs, global.common, global.cdk, global.platformBrowser, global.animations));
}(this, (function (exports, core, rxjs, common, cdk, platformBrowser, animations) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function isFunction(x) {
        return typeof x === 'function';
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var _enable_super_gross_mode_that_will_cause_bad_things = false;
    var config = {
        Promise: undefined,
        set useDeprecatedSynchronousErrorHandling(value) {
            if (value) {
                var error = /*@__PURE__*/ new Error();
                /*@__PURE__*/ console.warn('DEPRECATED! RxJS was set to use deprecated synchronous error handling behavior by code at: \n' + error.stack);
            }
            _enable_super_gross_mode_that_will_cause_bad_things = value;
        },
        get useDeprecatedSynchronousErrorHandling() {
            return _enable_super_gross_mode_that_will_cause_bad_things;
        },
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function hostReportError(err) {
        setTimeout(function () { throw err; }, 0);
    }

    /** PURE_IMPORTS_START _config,_util_hostReportError PURE_IMPORTS_END */
    var empty = {
        closed: true,
        next: function (value) { },
        error: function (err) {
            if (config.useDeprecatedSynchronousErrorHandling) {
                throw err;
            }
            else {
                hostReportError(err);
            }
        },
        complete: function () { }
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var isArray = /*@__PURE__*/ (function () { return Array.isArray || (function (x) { return x && typeof x.length === 'number'; }); })();

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function isObject(x) {
        return x !== null && typeof x === 'object';
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var UnsubscriptionErrorImpl = /*@__PURE__*/ (function () {
        function UnsubscriptionErrorImpl(errors) {
            Error.call(this);
            this.message = errors ?
                errors.length + " errors occurred during unsubscription:\n" + errors.map(function (err, i) { return i + 1 + ") " + err.toString(); }).join('\n  ') : '';
            this.name = 'UnsubscriptionError';
            this.errors = errors;
            return this;
        }
        UnsubscriptionErrorImpl.prototype = /*@__PURE__*/ Object.create(Error.prototype);
        return UnsubscriptionErrorImpl;
    })();
    var UnsubscriptionError = UnsubscriptionErrorImpl;

    /** PURE_IMPORTS_START _util_isArray,_util_isObject,_util_isFunction,_util_UnsubscriptionError PURE_IMPORTS_END */
    var Subscription = /*@__PURE__*/ (function () {
        function Subscription(unsubscribe) {
            this.closed = false;
            this._parentOrParents = null;
            this._subscriptions = null;
            if (unsubscribe) {
                this._unsubscribe = unsubscribe;
            }
        }
        Subscription.prototype.unsubscribe = function () {
            var errors;
            if (this.closed) {
                return;
            }
            var _a = this, _parentOrParents = _a._parentOrParents, _unsubscribe = _a._unsubscribe, _subscriptions = _a._subscriptions;
            this.closed = true;
            this._parentOrParents = null;
            this._subscriptions = null;
            if (_parentOrParents instanceof Subscription) {
                _parentOrParents.remove(this);
            }
            else if (_parentOrParents !== null) {
                for (var index = 0; index < _parentOrParents.length; ++index) {
                    var parent_1 = _parentOrParents[index];
                    parent_1.remove(this);
                }
            }
            if (isFunction(_unsubscribe)) {
                try {
                    _unsubscribe.call(this);
                }
                catch (e) {
                    errors = e instanceof UnsubscriptionError ? flattenUnsubscriptionErrors(e.errors) : [e];
                }
            }
            if (isArray(_subscriptions)) {
                var index = -1;
                var len = _subscriptions.length;
                while (++index < len) {
                    var sub = _subscriptions[index];
                    if (isObject(sub)) {
                        try {
                            sub.unsubscribe();
                        }
                        catch (e) {
                            errors = errors || [];
                            if (e instanceof UnsubscriptionError) {
                                errors = errors.concat(flattenUnsubscriptionErrors(e.errors));
                            }
                            else {
                                errors.push(e);
                            }
                        }
                    }
                }
            }
            if (errors) {
                throw new UnsubscriptionError(errors);
            }
        };
        Subscription.prototype.add = function (teardown) {
            var subscription = teardown;
            if (!teardown) {
                return Subscription.EMPTY;
            }
            switch (typeof teardown) {
                case 'function':
                    subscription = new Subscription(teardown);
                case 'object':
                    if (subscription === this || subscription.closed || typeof subscription.unsubscribe !== 'function') {
                        return subscription;
                    }
                    else if (this.closed) {
                        subscription.unsubscribe();
                        return subscription;
                    }
                    else if (!(subscription instanceof Subscription)) {
                        var tmp = subscription;
                        subscription = new Subscription();
                        subscription._subscriptions = [tmp];
                    }
                    break;
                default: {
                    throw new Error('unrecognized teardown ' + teardown + ' added to Subscription.');
                }
            }
            var _parentOrParents = subscription._parentOrParents;
            if (_parentOrParents === null) {
                subscription._parentOrParents = this;
            }
            else if (_parentOrParents instanceof Subscription) {
                if (_parentOrParents === this) {
                    return subscription;
                }
                subscription._parentOrParents = [_parentOrParents, this];
            }
            else if (_parentOrParents.indexOf(this) === -1) {
                _parentOrParents.push(this);
            }
            else {
                return subscription;
            }
            var subscriptions = this._subscriptions;
            if (subscriptions === null) {
                this._subscriptions = [subscription];
            }
            else {
                subscriptions.push(subscription);
            }
            return subscription;
        };
        Subscription.prototype.remove = function (subscription) {
            var subscriptions = this._subscriptions;
            if (subscriptions) {
                var subscriptionIndex = subscriptions.indexOf(subscription);
                if (subscriptionIndex !== -1) {
                    subscriptions.splice(subscriptionIndex, 1);
                }
            }
        };
        Subscription.EMPTY = (function (empty) {
            empty.closed = true;
            return empty;
        }(new Subscription()));
        return Subscription;
    }());
    function flattenUnsubscriptionErrors(errors) {
        return errors.reduce(function (errs, err) { return errs.concat((err instanceof UnsubscriptionError) ? err.errors : err); }, []);
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var rxSubscriber = /*@__PURE__*/ (function () {
        return typeof Symbol === 'function'
            ? /*@__PURE__*/ Symbol('rxSubscriber')
            : '@@rxSubscriber_' + /*@__PURE__*/ Math.random();
    })();

    /** PURE_IMPORTS_START tslib,_util_isFunction,_Observer,_Subscription,_internal_symbol_rxSubscriber,_config,_util_hostReportError PURE_IMPORTS_END */
    var Subscriber = /*@__PURE__*/ (function (_super) {
        __extends(Subscriber, _super);
        function Subscriber(destinationOrNext, error, complete) {
            var _this = _super.call(this) || this;
            _this.syncErrorValue = null;
            _this.syncErrorThrown = false;
            _this.syncErrorThrowable = false;
            _this.isStopped = false;
            switch (arguments.length) {
                case 0:
                    _this.destination = empty;
                    break;
                case 1:
                    if (!destinationOrNext) {
                        _this.destination = empty;
                        break;
                    }
                    if (typeof destinationOrNext === 'object') {
                        if (destinationOrNext instanceof Subscriber) {
                            _this.syncErrorThrowable = destinationOrNext.syncErrorThrowable;
                            _this.destination = destinationOrNext;
                            destinationOrNext.add(_this);
                        }
                        else {
                            _this.syncErrorThrowable = true;
                            _this.destination = new SafeSubscriber(_this, destinationOrNext);
                        }
                        break;
                    }
                default:
                    _this.syncErrorThrowable = true;
                    _this.destination = new SafeSubscriber(_this, destinationOrNext, error, complete);
                    break;
            }
            return _this;
        }
        Subscriber.prototype[rxSubscriber] = function () { return this; };
        Subscriber.create = function (next, error, complete) {
            var subscriber = new Subscriber(next, error, complete);
            subscriber.syncErrorThrowable = false;
            return subscriber;
        };
        Subscriber.prototype.next = function (value) {
            if (!this.isStopped) {
                this._next(value);
            }
        };
        Subscriber.prototype.error = function (err) {
            if (!this.isStopped) {
                this.isStopped = true;
                this._error(err);
            }
        };
        Subscriber.prototype.complete = function () {
            if (!this.isStopped) {
                this.isStopped = true;
                this._complete();
            }
        };
        Subscriber.prototype.unsubscribe = function () {
            if (this.closed) {
                return;
            }
            this.isStopped = true;
            _super.prototype.unsubscribe.call(this);
        };
        Subscriber.prototype._next = function (value) {
            this.destination.next(value);
        };
        Subscriber.prototype._error = function (err) {
            this.destination.error(err);
            this.unsubscribe();
        };
        Subscriber.prototype._complete = function () {
            this.destination.complete();
            this.unsubscribe();
        };
        Subscriber.prototype._unsubscribeAndRecycle = function () {
            var _parentOrParents = this._parentOrParents;
            this._parentOrParents = null;
            this.unsubscribe();
            this.closed = false;
            this.isStopped = false;
            this._parentOrParents = _parentOrParents;
            return this;
        };
        return Subscriber;
    }(Subscription));
    var SafeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SafeSubscriber, _super);
        function SafeSubscriber(_parentSubscriber, observerOrNext, error, complete) {
            var _this = _super.call(this) || this;
            _this._parentSubscriber = _parentSubscriber;
            var next;
            var context = _this;
            if (isFunction(observerOrNext)) {
                next = observerOrNext;
            }
            else if (observerOrNext) {
                next = observerOrNext.next;
                error = observerOrNext.error;
                complete = observerOrNext.complete;
                if (observerOrNext !== empty) {
                    context = Object.create(observerOrNext);
                    if (isFunction(context.unsubscribe)) {
                        _this.add(context.unsubscribe.bind(context));
                    }
                    context.unsubscribe = _this.unsubscribe.bind(_this);
                }
            }
            _this._context = context;
            _this._next = next;
            _this._error = error;
            _this._complete = complete;
            return _this;
        }
        SafeSubscriber.prototype.next = function (value) {
            if (!this.isStopped && this._next) {
                var _parentSubscriber = this._parentSubscriber;
                if (!config.useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
                    this.__tryOrUnsub(this._next, value);
                }
                else if (this.__tryOrSetError(_parentSubscriber, this._next, value)) {
                    this.unsubscribe();
                }
            }
        };
        SafeSubscriber.prototype.error = function (err) {
            if (!this.isStopped) {
                var _parentSubscriber = this._parentSubscriber;
                var useDeprecatedSynchronousErrorHandling = config.useDeprecatedSynchronousErrorHandling;
                if (this._error) {
                    if (!useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
                        this.__tryOrUnsub(this._error, err);
                        this.unsubscribe();
                    }
                    else {
                        this.__tryOrSetError(_parentSubscriber, this._error, err);
                        this.unsubscribe();
                    }
                }
                else if (!_parentSubscriber.syncErrorThrowable) {
                    this.unsubscribe();
                    if (useDeprecatedSynchronousErrorHandling) {
                        throw err;
                    }
                    hostReportError(err);
                }
                else {
                    if (useDeprecatedSynchronousErrorHandling) {
                        _parentSubscriber.syncErrorValue = err;
                        _parentSubscriber.syncErrorThrown = true;
                    }
                    else {
                        hostReportError(err);
                    }
                    this.unsubscribe();
                }
            }
        };
        SafeSubscriber.prototype.complete = function () {
            var _this = this;
            if (!this.isStopped) {
                var _parentSubscriber = this._parentSubscriber;
                if (this._complete) {
                    var wrappedComplete = function () { return _this._complete.call(_this._context); };
                    if (!config.useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
                        this.__tryOrUnsub(wrappedComplete);
                        this.unsubscribe();
                    }
                    else {
                        this.__tryOrSetError(_parentSubscriber, wrappedComplete);
                        this.unsubscribe();
                    }
                }
                else {
                    this.unsubscribe();
                }
            }
        };
        SafeSubscriber.prototype.__tryOrUnsub = function (fn, value) {
            try {
                fn.call(this._context, value);
            }
            catch (err) {
                this.unsubscribe();
                if (config.useDeprecatedSynchronousErrorHandling) {
                    throw err;
                }
                else {
                    hostReportError(err);
                }
            }
        };
        SafeSubscriber.prototype.__tryOrSetError = function (parent, fn, value) {
            if (!config.useDeprecatedSynchronousErrorHandling) {
                throw new Error('bad call');
            }
            try {
                fn.call(this._context, value);
            }
            catch (err) {
                if (config.useDeprecatedSynchronousErrorHandling) {
                    parent.syncErrorValue = err;
                    parent.syncErrorThrown = true;
                    return true;
                }
                else {
                    hostReportError(err);
                    return true;
                }
            }
            return false;
        };
        SafeSubscriber.prototype._unsubscribe = function () {
            var _parentSubscriber = this._parentSubscriber;
            this._context = null;
            this._parentSubscriber = null;
            _parentSubscriber.unsubscribe();
        };
        return SafeSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var OuterSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(OuterSubscriber, _super);
        function OuterSubscriber() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        OuterSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.destination.next(innerValue);
        };
        OuterSubscriber.prototype.notifyError = function (error, innerSub) {
            this.destination.error(error);
        };
        OuterSubscriber.prototype.notifyComplete = function (innerSub) {
            this.destination.complete();
        };
        return OuterSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var InnerSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(InnerSubscriber, _super);
        function InnerSubscriber(parent, outerValue, outerIndex) {
            var _this = _super.call(this) || this;
            _this.parent = parent;
            _this.outerValue = outerValue;
            _this.outerIndex = outerIndex;
            _this.index = 0;
            return _this;
        }
        InnerSubscriber.prototype._next = function (value) {
            this.parent.notifyNext(this.outerValue, value, this.outerIndex, this.index++, this);
        };
        InnerSubscriber.prototype._error = function (error) {
            this.parent.notifyError(error, this);
            this.unsubscribe();
        };
        InnerSubscriber.prototype._complete = function () {
            this.parent.notifyComplete(this);
            this.unsubscribe();
        };
        return InnerSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var subscribeToArray = function (array) {
        return function (subscriber) {
            for (var i = 0, len = array.length; i < len && !subscriber.closed; i++) {
                subscriber.next(array[i]);
            }
            subscriber.complete();
        };
    };

    /** PURE_IMPORTS_START _hostReportError PURE_IMPORTS_END */
    var subscribeToPromise = function (promise) {
        return function (subscriber) {
            promise.then(function (value) {
                if (!subscriber.closed) {
                    subscriber.next(value);
                    subscriber.complete();
                }
            }, function (err) { return subscriber.error(err); })
                .then(null, hostReportError);
            return subscriber;
        };
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function getSymbolIterator() {
        if (typeof Symbol !== 'function' || !Symbol.iterator) {
            return '@@iterator';
        }
        return Symbol.iterator;
    }
    var iterator = /*@__PURE__*/ getSymbolIterator();

    /** PURE_IMPORTS_START _symbol_iterator PURE_IMPORTS_END */
    var subscribeToIterable = function (iterable) {
        return function (subscriber) {
            var iterator$1 = iterable[iterator]();
            do {
                var item = iterator$1.next();
                if (item.done) {
                    subscriber.complete();
                    break;
                }
                subscriber.next(item.value);
                if (subscriber.closed) {
                    break;
                }
            } while (true);
            if (typeof iterator$1.return === 'function') {
                subscriber.add(function () {
                    if (iterator$1.return) {
                        iterator$1.return();
                    }
                });
            }
            return subscriber;
        };
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var observable = /*@__PURE__*/ (function () { return typeof Symbol === 'function' && Symbol.observable || '@@observable'; })();

    /** PURE_IMPORTS_START _symbol_observable PURE_IMPORTS_END */
    var subscribeToObservable = function (obj) {
        return function (subscriber) {
            var obs = obj[observable]();
            if (typeof obs.subscribe !== 'function') {
                throw new TypeError('Provided object does not correctly implement Symbol.observable');
            }
            else {
                return obs.subscribe(subscriber);
            }
        };
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var isArrayLike = (function (x) { return x && typeof x.length === 'number' && typeof x !== 'function'; });

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function isPromise(value) {
        return !!value && typeof value.subscribe !== 'function' && typeof value.then === 'function';
    }

    /** PURE_IMPORTS_START _subscribeToArray,_subscribeToPromise,_subscribeToIterable,_subscribeToObservable,_isArrayLike,_isPromise,_isObject,_symbol_iterator,_symbol_observable PURE_IMPORTS_END */
    var subscribeTo = function (result) {
        if (!!result && typeof result[observable] === 'function') {
            return subscribeToObservable(result);
        }
        else if (isArrayLike(result)) {
            return subscribeToArray(result);
        }
        else if (isPromise(result)) {
            return subscribeToPromise(result);
        }
        else if (!!result && typeof result[iterator] === 'function') {
            return subscribeToIterable(result);
        }
        else {
            var value = isObject(result) ? 'an invalid object' : "'" + result + "'";
            var msg = "You provided " + value + " where a stream was expected."
                + ' You can provide an Observable, Promise, Array, or Iterable.';
            throw new TypeError(msg);
        }
    };

    /** PURE_IMPORTS_START _Subscriber PURE_IMPORTS_END */
    function canReportError(observer) {
        while (observer) {
            var _a = observer, closed_1 = _a.closed, destination = _a.destination, isStopped = _a.isStopped;
            if (closed_1 || isStopped) {
                return false;
            }
            else if (destination && destination instanceof Subscriber) {
                observer = destination;
            }
            else {
                observer = null;
            }
        }
        return true;
    }

    /** PURE_IMPORTS_START _Subscriber,_symbol_rxSubscriber,_Observer PURE_IMPORTS_END */
    function toSubscriber(nextOrObserver, error, complete) {
        if (nextOrObserver) {
            if (nextOrObserver instanceof Subscriber) {
                return nextOrObserver;
            }
            if (nextOrObserver[rxSubscriber]) {
                return nextOrObserver[rxSubscriber]();
            }
        }
        if (!nextOrObserver && !error && !complete) {
            return new Subscriber(empty);
        }
        return new Subscriber(nextOrObserver, error, complete);
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function identity(x) {
        return x;
    }

    /** PURE_IMPORTS_START _identity PURE_IMPORTS_END */
    function pipeFromArray(fns) {
        if (fns.length === 0) {
            return identity;
        }
        if (fns.length === 1) {
            return fns[0];
        }
        return function piped(input) {
            return fns.reduce(function (prev, fn) { return fn(prev); }, input);
        };
    }

    /** PURE_IMPORTS_START _util_canReportError,_util_toSubscriber,_symbol_observable,_util_pipe,_config PURE_IMPORTS_END */
    var Observable = /*@__PURE__*/ (function () {
        function Observable(subscribe) {
            this._isScalar = false;
            if (subscribe) {
                this._subscribe = subscribe;
            }
        }
        Observable.prototype.lift = function (operator) {
            var observable = new Observable();
            observable.source = this;
            observable.operator = operator;
            return observable;
        };
        Observable.prototype.subscribe = function (observerOrNext, error, complete) {
            var operator = this.operator;
            var sink = toSubscriber(observerOrNext, error, complete);
            if (operator) {
                sink.add(operator.call(sink, this.source));
            }
            else {
                sink.add(this.source || (config.useDeprecatedSynchronousErrorHandling && !sink.syncErrorThrowable) ?
                    this._subscribe(sink) :
                    this._trySubscribe(sink));
            }
            if (config.useDeprecatedSynchronousErrorHandling) {
                if (sink.syncErrorThrowable) {
                    sink.syncErrorThrowable = false;
                    if (sink.syncErrorThrown) {
                        throw sink.syncErrorValue;
                    }
                }
            }
            return sink;
        };
        Observable.prototype._trySubscribe = function (sink) {
            try {
                return this._subscribe(sink);
            }
            catch (err) {
                if (config.useDeprecatedSynchronousErrorHandling) {
                    sink.syncErrorThrown = true;
                    sink.syncErrorValue = err;
                }
                if (canReportError(sink)) {
                    sink.error(err);
                }
                else {
                    console.warn(err);
                }
            }
        };
        Observable.prototype.forEach = function (next, promiseCtor) {
            var _this = this;
            promiseCtor = getPromiseCtor(promiseCtor);
            return new promiseCtor(function (resolve, reject) {
                var subscription;
                subscription = _this.subscribe(function (value) {
                    try {
                        next(value);
                    }
                    catch (err) {
                        reject(err);
                        if (subscription) {
                            subscription.unsubscribe();
                        }
                    }
                }, reject, resolve);
            });
        };
        Observable.prototype._subscribe = function (subscriber) {
            var source = this.source;
            return source && source.subscribe(subscriber);
        };
        Observable.prototype[observable] = function () {
            return this;
        };
        Observable.prototype.pipe = function () {
            var operations = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                operations[_i] = arguments[_i];
            }
            if (operations.length === 0) {
                return this;
            }
            return pipeFromArray(operations)(this);
        };
        Observable.prototype.toPromise = function (promiseCtor) {
            var _this = this;
            promiseCtor = getPromiseCtor(promiseCtor);
            return new promiseCtor(function (resolve, reject) {
                var value;
                _this.subscribe(function (x) { return value = x; }, function (err) { return reject(err); }, function () { return resolve(value); });
            });
        };
        Observable.create = function (subscribe) {
            return new Observable(subscribe);
        };
        return Observable;
    }());
    function getPromiseCtor(promiseCtor) {
        if (!promiseCtor) {
            promiseCtor =  Promise;
        }
        if (!promiseCtor) {
            throw new Error('no Promise impl found');
        }
        return promiseCtor;
    }

    /** PURE_IMPORTS_START _InnerSubscriber,_subscribeTo,_Observable PURE_IMPORTS_END */
    function subscribeToResult(outerSubscriber, result, outerValue, outerIndex, innerSubscriber) {
        if (innerSubscriber === void 0) {
            innerSubscriber = new InnerSubscriber(outerSubscriber, outerValue, outerIndex);
        }
        if (innerSubscriber.closed) {
            return undefined;
        }
        if (result instanceof Observable) {
            return result.subscribe(innerSubscriber);
        }
        return subscribeTo(result)(innerSubscriber);
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function isScheduler(value) {
        return value && typeof value.schedule === 'function';
    }

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_InnerSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    function catchError(selector) {
        return function catchErrorOperatorFunction(source) {
            var operator = new CatchOperator(selector);
            var caught = source.lift(operator);
            return (operator.caught = caught);
        };
    }
    var CatchOperator = /*@__PURE__*/ (function () {
        function CatchOperator(selector) {
            this.selector = selector;
        }
        CatchOperator.prototype.call = function (subscriber, source) {
            return source.subscribe(new CatchSubscriber(subscriber, this.selector, this.caught));
        };
        return CatchOperator;
    }());
    var CatchSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(CatchSubscriber, _super);
        function CatchSubscriber(destination, selector, caught) {
            var _this = _super.call(this, destination) || this;
            _this.selector = selector;
            _this.caught = caught;
            return _this;
        }
        CatchSubscriber.prototype.error = function (err) {
            if (!this.isStopped) {
                var result = void 0;
                try {
                    result = this.selector(err, this.caught);
                }
                catch (err2) {
                    _super.prototype.error.call(this, err2);
                    return;
                }
                this._unsubscribeAndRecycle();
                var innerSubscriber = new InnerSubscriber(this, undefined, undefined);
                this.add(innerSubscriber);
                var innerSubscription = subscribeToResult(this, result, undefined, undefined, innerSubscriber);
                if (innerSubscription !== innerSubscriber) {
                    this.add(innerSubscription);
                }
            }
        };
        return CatchSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _Observable,_Subscription PURE_IMPORTS_END */
    function scheduleArray(input, scheduler) {
        return new Observable(function (subscriber) {
            var sub = new Subscription();
            var i = 0;
            sub.add(scheduler.schedule(function () {
                if (i === input.length) {
                    subscriber.complete();
                    return;
                }
                subscriber.next(input[i++]);
                if (!subscriber.closed) {
                    sub.add(this.schedule());
                }
            }));
            return sub;
        });
    }

    /** PURE_IMPORTS_START _Observable,_util_subscribeToArray,_scheduled_scheduleArray PURE_IMPORTS_END */
    function fromArray(input, scheduler) {
        if (!scheduler) {
            return new Observable(subscribeToArray(input));
        }
        else {
            return scheduleArray(input, scheduler);
        }
    }

    /** PURE_IMPORTS_START _Observable,_Subscription,_symbol_observable PURE_IMPORTS_END */
    function scheduleObservable(input, scheduler) {
        return new Observable(function (subscriber) {
            var sub = new Subscription();
            sub.add(scheduler.schedule(function () {
                var observable$1 = input[observable]();
                sub.add(observable$1.subscribe({
                    next: function (value) { sub.add(scheduler.schedule(function () { return subscriber.next(value); })); },
                    error: function (err) { sub.add(scheduler.schedule(function () { return subscriber.error(err); })); },
                    complete: function () { sub.add(scheduler.schedule(function () { return subscriber.complete(); })); },
                }));
            }));
            return sub;
        });
    }

    /** PURE_IMPORTS_START _Observable,_Subscription PURE_IMPORTS_END */
    function schedulePromise(input, scheduler) {
        return new Observable(function (subscriber) {
            var sub = new Subscription();
            sub.add(scheduler.schedule(function () {
                return input.then(function (value) {
                    sub.add(scheduler.schedule(function () {
                        subscriber.next(value);
                        sub.add(scheduler.schedule(function () { return subscriber.complete(); }));
                    }));
                }, function (err) {
                    sub.add(scheduler.schedule(function () { return subscriber.error(err); }));
                });
            }));
            return sub;
        });
    }

    /** PURE_IMPORTS_START _Observable,_Subscription,_symbol_iterator PURE_IMPORTS_END */
    function scheduleIterable(input, scheduler) {
        if (!input) {
            throw new Error('Iterable cannot be null');
        }
        return new Observable(function (subscriber) {
            var sub = new Subscription();
            var iterator$1;
            sub.add(function () {
                if (iterator$1 && typeof iterator$1.return === 'function') {
                    iterator$1.return();
                }
            });
            sub.add(scheduler.schedule(function () {
                iterator$1 = input[iterator]();
                sub.add(scheduler.schedule(function () {
                    if (subscriber.closed) {
                        return;
                    }
                    var value;
                    var done;
                    try {
                        var result = iterator$1.next();
                        value = result.value;
                        done = result.done;
                    }
                    catch (err) {
                        subscriber.error(err);
                        return;
                    }
                    if (done) {
                        subscriber.complete();
                    }
                    else {
                        subscriber.next(value);
                        this.schedule();
                    }
                }));
            }));
            return sub;
        });
    }

    /** PURE_IMPORTS_START _symbol_observable PURE_IMPORTS_END */
    function isInteropObservable(input) {
        return input && typeof input[observable] === 'function';
    }

    /** PURE_IMPORTS_START _symbol_iterator PURE_IMPORTS_END */
    function isIterable(input) {
        return input && typeof input[iterator] === 'function';
    }

    /** PURE_IMPORTS_START _scheduleObservable,_schedulePromise,_scheduleArray,_scheduleIterable,_util_isInteropObservable,_util_isPromise,_util_isArrayLike,_util_isIterable PURE_IMPORTS_END */
    function scheduled(input, scheduler) {
        if (input != null) {
            if (isInteropObservable(input)) {
                return scheduleObservable(input, scheduler);
            }
            else if (isPromise(input)) {
                return schedulePromise(input, scheduler);
            }
            else if (isArrayLike(input)) {
                return scheduleArray(input, scheduler);
            }
            else if (isIterable(input) || typeof input === 'string') {
                return scheduleIterable(input, scheduler);
            }
        }
        throw new TypeError((input !== null && typeof input || input) + ' is not observable');
    }

    /** PURE_IMPORTS_START _Observable,_util_subscribeTo,_scheduled_scheduled PURE_IMPORTS_END */
    function from(input, scheduler) {
        if (!scheduler) {
            if (input instanceof Observable) {
                return input;
            }
            return new Observable(subscribeTo(input));
        }
        else {
            return scheduled(input, scheduler);
        }
    }

    /** PURE_IMPORTS_START _util_isScheduler,_fromArray,_scheduled_scheduleArray PURE_IMPORTS_END */
    function of() {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var scheduler = args[args.length - 1];
        if (isScheduler(scheduler)) {
            args.pop();
            return scheduleArray(args, scheduler);
        }
        else {
            return fromArray(args);
        }
    }

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    function map(project, thisArg) {
        return function mapOperation(source) {
            if (typeof project !== 'function') {
                throw new TypeError('argument is not a function. Are you looking for `mapTo()`?');
            }
            return source.lift(new MapOperator(project, thisArg));
        };
    }
    var MapOperator = /*@__PURE__*/ (function () {
        function MapOperator(project, thisArg) {
            this.project = project;
            this.thisArg = thisArg;
        }
        MapOperator.prototype.call = function (subscriber, source) {
            return source.subscribe(new MapSubscriber(subscriber, this.project, this.thisArg));
        };
        return MapOperator;
    }());
    var MapSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(MapSubscriber, _super);
        function MapSubscriber(destination, project, thisArg) {
            var _this = _super.call(this, destination) || this;
            _this.project = project;
            _this.count = 0;
            _this.thisArg = thisArg || _this;
            return _this;
        }
        MapSubscriber.prototype._next = function (value) {
            var result;
            try {
                result = this.project.call(this.thisArg, value, this.count++);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.next(result);
        };
        return MapSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_util_subscribeToResult,_OuterSubscriber,_InnerSubscriber,_map,_observable_from PURE_IMPORTS_END */
    function mergeMap(project, resultSelector, concurrent) {
        if (concurrent === void 0) {
            concurrent = Number.POSITIVE_INFINITY;
        }
        if (typeof resultSelector === 'function') {
            return function (source) { return source.pipe(mergeMap(function (a, i) { return from(project(a, i)).pipe(map(function (b, ii) { return resultSelector(a, b, i, ii); })); }, concurrent)); };
        }
        else if (typeof resultSelector === 'number') {
            concurrent = resultSelector;
        }
        return function (source) { return source.lift(new MergeMapOperator(project, concurrent)); };
    }
    var MergeMapOperator = /*@__PURE__*/ (function () {
        function MergeMapOperator(project, concurrent) {
            if (concurrent === void 0) {
                concurrent = Number.POSITIVE_INFINITY;
            }
            this.project = project;
            this.concurrent = concurrent;
        }
        MergeMapOperator.prototype.call = function (observer, source) {
            return source.subscribe(new MergeMapSubscriber(observer, this.project, this.concurrent));
        };
        return MergeMapOperator;
    }());
    var MergeMapSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(MergeMapSubscriber, _super);
        function MergeMapSubscriber(destination, project, concurrent) {
            if (concurrent === void 0) {
                concurrent = Number.POSITIVE_INFINITY;
            }
            var _this = _super.call(this, destination) || this;
            _this.project = project;
            _this.concurrent = concurrent;
            _this.hasCompleted = false;
            _this.buffer = [];
            _this.active = 0;
            _this.index = 0;
            return _this;
        }
        MergeMapSubscriber.prototype._next = function (value) {
            if (this.active < this.concurrent) {
                this._tryNext(value);
            }
            else {
                this.buffer.push(value);
            }
        };
        MergeMapSubscriber.prototype._tryNext = function (value) {
            var result;
            var index = this.index++;
            try {
                result = this.project(value, index);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.active++;
            this._innerSub(result, value, index);
        };
        MergeMapSubscriber.prototype._innerSub = function (ish, value, index) {
            var innerSubscriber = new InnerSubscriber(this, value, index);
            var destination = this.destination;
            destination.add(innerSubscriber);
            var innerSubscription = subscribeToResult(this, ish, undefined, undefined, innerSubscriber);
            if (innerSubscription !== innerSubscriber) {
                destination.add(innerSubscription);
            }
        };
        MergeMapSubscriber.prototype._complete = function () {
            this.hasCompleted = true;
            if (this.active === 0 && this.buffer.length === 0) {
                this.destination.complete();
            }
            this.unsubscribe();
        };
        MergeMapSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.destination.next(innerValue);
        };
        MergeMapSubscriber.prototype.notifyComplete = function (innerSub) {
            var buffer = this.buffer;
            this.remove(innerSub);
            this.active--;
            if (buffer.length > 0) {
                this._next(buffer.shift());
            }
            else if (this.active === 0 && this.hasCompleted) {
                this.destination.complete();
            }
        };
        return MergeMapSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _mergeMap,_util_identity PURE_IMPORTS_END */
    function mergeAll(concurrent) {
        if (concurrent === void 0) {
            concurrent = Number.POSITIVE_INFINITY;
        }
        return mergeMap(identity, concurrent);
    }

    /** PURE_IMPORTS_START _mergeAll PURE_IMPORTS_END */
    function concatAll() {
        return mergeAll(1);
    }

    /** PURE_IMPORTS_START _of,_operators_concatAll PURE_IMPORTS_END */
    function concat() {
        var observables = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            observables[_i] = arguments[_i];
        }
        return concatAll()(of.apply(void 0, observables));
    }

    /** PURE_IMPORTS_START _mergeMap PURE_IMPORTS_END */
    function concatMap(project, resultSelector) {
        return mergeMap(project, resultSelector, 1);
    }

    /** PURE_IMPORTS_START _Observable PURE_IMPORTS_END */
    var EMPTY = /*@__PURE__*/ new Observable(function (subscriber) { return subscriber.complete(); });
    function empty$1(scheduler) {
        return scheduler ? emptyScheduled(scheduler) : EMPTY;
    }
    function emptyScheduled(scheduler) {
        return new Observable(function (subscriber) { return scheduler.schedule(function () { return subscriber.complete(); }); });
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var ArgumentOutOfRangeErrorImpl = /*@__PURE__*/ (function () {
        function ArgumentOutOfRangeErrorImpl() {
            Error.call(this);
            this.message = 'argument out of range';
            this.name = 'ArgumentOutOfRangeError';
            return this;
        }
        ArgumentOutOfRangeErrorImpl.prototype = /*@__PURE__*/ Object.create(Error.prototype);
        return ArgumentOutOfRangeErrorImpl;
    })();
    var ArgumentOutOfRangeError = ArgumentOutOfRangeErrorImpl;

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    function filter(predicate, thisArg) {
        return function filterOperatorFunction(source) {
            return source.lift(new FilterOperator(predicate, thisArg));
        };
    }
    var FilterOperator = /*@__PURE__*/ (function () {
        function FilterOperator(predicate, thisArg) {
            this.predicate = predicate;
            this.thisArg = thisArg;
        }
        FilterOperator.prototype.call = function (subscriber, source) {
            return source.subscribe(new FilterSubscriber(subscriber, this.predicate, this.thisArg));
        };
        return FilterOperator;
    }());
    var FilterSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(FilterSubscriber, _super);
        function FilterSubscriber(destination, predicate, thisArg) {
            var _this = _super.call(this, destination) || this;
            _this.predicate = predicate;
            _this.thisArg = thisArg;
            _this.count = 0;
            return _this;
        }
        FilterSubscriber.prototype._next = function (value) {
            var result;
            try {
                result = this.predicate.call(this.thisArg, value, this.count++);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            if (result) {
                this.destination.next(value);
            }
        };
        return FilterSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_ArgumentOutOfRangeError,_observable_empty PURE_IMPORTS_END */
    function take(count) {
        return function (source) {
            if (count === 0) {
                return empty$1();
            }
            else {
                return source.lift(new TakeOperator(count));
            }
        };
    }
    var TakeOperator = /*@__PURE__*/ (function () {
        function TakeOperator(total) {
            this.total = total;
            if (this.total < 0) {
                throw new ArgumentOutOfRangeError;
            }
        }
        TakeOperator.prototype.call = function (subscriber, source) {
            return source.subscribe(new TakeSubscriber(subscriber, this.total));
        };
        return TakeOperator;
    }());
    var TakeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(TakeSubscriber, _super);
        function TakeSubscriber(destination, total) {
            var _this = _super.call(this, destination) || this;
            _this.total = total;
            _this.count = 0;
            return _this;
        }
        TakeSubscriber.prototype._next = function (value) {
            var total = this.total;
            var count = ++this.count;
            if (count <= total) {
                this.destination.next(value);
                if (count === total) {
                    this.destination.complete();
                    this.unsubscribe();
                }
            }
        };
        return TakeSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_Subscription PURE_IMPORTS_END */
    function finalize(callback) {
        return function (source) { return source.lift(new FinallyOperator(callback)); };
    }
    var FinallyOperator = /*@__PURE__*/ (function () {
        function FinallyOperator(callback) {
            this.callback = callback;
        }
        FinallyOperator.prototype.call = function (subscriber, source) {
            return source.subscribe(new FinallySubscriber(subscriber, this.callback));
        };
        return FinallyOperator;
    }());
    var FinallySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(FinallySubscriber, _super);
        function FinallySubscriber(destination, callback) {
            var _this = _super.call(this, destination) || this;
            _this.add(new Subscription(callback));
            return _this;
        }
        return FinallySubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var ObjectUnsubscribedErrorImpl = /*@__PURE__*/ (function () {
        function ObjectUnsubscribedErrorImpl() {
            Error.call(this);
            this.message = 'object unsubscribed';
            this.name = 'ObjectUnsubscribedError';
            return this;
        }
        ObjectUnsubscribedErrorImpl.prototype = /*@__PURE__*/ Object.create(Error.prototype);
        return ObjectUnsubscribedErrorImpl;
    })();
    var ObjectUnsubscribedError = ObjectUnsubscribedErrorImpl;

    /** PURE_IMPORTS_START tslib,_Subscription PURE_IMPORTS_END */
    var SubjectSubscription = /*@__PURE__*/ (function (_super) {
        __extends(SubjectSubscription, _super);
        function SubjectSubscription(subject, subscriber) {
            var _this = _super.call(this) || this;
            _this.subject = subject;
            _this.subscriber = subscriber;
            _this.closed = false;
            return _this;
        }
        SubjectSubscription.prototype.unsubscribe = function () {
            if (this.closed) {
                return;
            }
            this.closed = true;
            var subject = this.subject;
            var observers = subject.observers;
            this.subject = null;
            if (!observers || observers.length === 0 || subject.isStopped || subject.closed) {
                return;
            }
            var subscriberIndex = observers.indexOf(this.subscriber);
            if (subscriberIndex !== -1) {
                observers.splice(subscriberIndex, 1);
            }
        };
        return SubjectSubscription;
    }(Subscription));

    /** PURE_IMPORTS_START tslib,_Observable,_Subscriber,_Subscription,_util_ObjectUnsubscribedError,_SubjectSubscription,_internal_symbol_rxSubscriber PURE_IMPORTS_END */
    var SubjectSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SubjectSubscriber, _super);
        function SubjectSubscriber(destination) {
            var _this = _super.call(this, destination) || this;
            _this.destination = destination;
            return _this;
        }
        return SubjectSubscriber;
    }(Subscriber));
    var Subject = /*@__PURE__*/ (function (_super) {
        __extends(Subject, _super);
        function Subject() {
            var _this = _super.call(this) || this;
            _this.observers = [];
            _this.closed = false;
            _this.isStopped = false;
            _this.hasError = false;
            _this.thrownError = null;
            return _this;
        }
        Subject.prototype[rxSubscriber] = function () {
            return new SubjectSubscriber(this);
        };
        Subject.prototype.lift = function (operator) {
            var subject = new AnonymousSubject(this, this);
            subject.operator = operator;
            return subject;
        };
        Subject.prototype.next = function (value) {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            if (!this.isStopped) {
                var observers = this.observers;
                var len = observers.length;
                var copy = observers.slice();
                for (var i = 0; i < len; i++) {
                    copy[i].next(value);
                }
            }
        };
        Subject.prototype.error = function (err) {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            this.hasError = true;
            this.thrownError = err;
            this.isStopped = true;
            var observers = this.observers;
            var len = observers.length;
            var copy = observers.slice();
            for (var i = 0; i < len; i++) {
                copy[i].error(err);
            }
            this.observers.length = 0;
        };
        Subject.prototype.complete = function () {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            this.isStopped = true;
            var observers = this.observers;
            var len = observers.length;
            var copy = observers.slice();
            for (var i = 0; i < len; i++) {
                copy[i].complete();
            }
            this.observers.length = 0;
        };
        Subject.prototype.unsubscribe = function () {
            this.isStopped = true;
            this.closed = true;
            this.observers = null;
        };
        Subject.prototype._trySubscribe = function (subscriber) {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            else {
                return _super.prototype._trySubscribe.call(this, subscriber);
            }
        };
        Subject.prototype._subscribe = function (subscriber) {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            else if (this.hasError) {
                subscriber.error(this.thrownError);
                return Subscription.EMPTY;
            }
            else if (this.isStopped) {
                subscriber.complete();
                return Subscription.EMPTY;
            }
            else {
                this.observers.push(subscriber);
                return new SubjectSubscription(this, subscriber);
            }
        };
        Subject.prototype.asObservable = function () {
            var observable = new Observable();
            observable.source = this;
            return observable;
        };
        Subject.create = function (destination, source) {
            return new AnonymousSubject(destination, source);
        };
        return Subject;
    }(Observable));
    var AnonymousSubject = /*@__PURE__*/ (function (_super) {
        __extends(AnonymousSubject, _super);
        function AnonymousSubject(destination, source) {
            var _this = _super.call(this) || this;
            _this.destination = destination;
            _this.source = source;
            return _this;
        }
        AnonymousSubject.prototype.next = function (value) {
            var destination = this.destination;
            if (destination && destination.next) {
                destination.next(value);
            }
        };
        AnonymousSubject.prototype.error = function (err) {
            var destination = this.destination;
            if (destination && destination.error) {
                this.destination.error(err);
            }
        };
        AnonymousSubject.prototype.complete = function () {
            var destination = this.destination;
            if (destination && destination.complete) {
                this.destination.complete();
            }
        };
        AnonymousSubject.prototype._subscribe = function (subscriber) {
            var source = this.source;
            if (source) {
                return this.source.subscribe(subscriber);
            }
            else {
                return Subscription.EMPTY;
            }
        };
        return AnonymousSubject;
    }(Subject));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    function refCount() {
        return function refCountOperatorFunction(source) {
            return source.lift(new RefCountOperator(source));
        };
    }
    var RefCountOperator = /*@__PURE__*/ (function () {
        function RefCountOperator(connectable) {
            this.connectable = connectable;
        }
        RefCountOperator.prototype.call = function (subscriber, source) {
            var connectable = this.connectable;
            connectable._refCount++;
            var refCounter = new RefCountSubscriber(subscriber, connectable);
            var subscription = source.subscribe(refCounter);
            if (!refCounter.closed) {
                refCounter.connection = connectable.connect();
            }
            return subscription;
        };
        return RefCountOperator;
    }());
    var RefCountSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(RefCountSubscriber, _super);
        function RefCountSubscriber(destination, connectable) {
            var _this = _super.call(this, destination) || this;
            _this.connectable = connectable;
            return _this;
        }
        RefCountSubscriber.prototype._unsubscribe = function () {
            var connectable = this.connectable;
            if (!connectable) {
                this.connection = null;
                return;
            }
            this.connectable = null;
            var refCount = connectable._refCount;
            if (refCount <= 0) {
                this.connection = null;
                return;
            }
            connectable._refCount = refCount - 1;
            if (refCount > 1) {
                this.connection = null;
                return;
            }
            var connection = this.connection;
            var sharedConnection = connectable._connection;
            this.connection = null;
            if (sharedConnection && (!connection || sharedConnection === connection)) {
                sharedConnection.unsubscribe();
            }
        };
        return RefCountSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subject,_Observable,_Subscriber,_Subscription,_operators_refCount PURE_IMPORTS_END */
    var ConnectableObservable = /*@__PURE__*/ (function (_super) {
        __extends(ConnectableObservable, _super);
        function ConnectableObservable(source, subjectFactory) {
            var _this = _super.call(this) || this;
            _this.source = source;
            _this.subjectFactory = subjectFactory;
            _this._refCount = 0;
            _this._isComplete = false;
            return _this;
        }
        ConnectableObservable.prototype._subscribe = function (subscriber) {
            return this.getSubject().subscribe(subscriber);
        };
        ConnectableObservable.prototype.getSubject = function () {
            var subject = this._subject;
            if (!subject || subject.isStopped) {
                this._subject = this.subjectFactory();
            }
            return this._subject;
        };
        ConnectableObservable.prototype.connect = function () {
            var connection = this._connection;
            if (!connection) {
                this._isComplete = false;
                connection = this._connection = new Subscription();
                connection.add(this.source
                    .subscribe(new ConnectableSubscriber(this.getSubject(), this)));
                if (connection.closed) {
                    this._connection = null;
                    connection = Subscription.EMPTY;
                }
            }
            return connection;
        };
        ConnectableObservable.prototype.refCount = function () {
            return refCount()(this);
        };
        return ConnectableObservable;
    }(Observable));
    var connectableObservableDescriptor = /*@__PURE__*/ (function () {
        var connectableProto = ConnectableObservable.prototype;
        return {
            operator: { value: null },
            _refCount: { value: 0, writable: true },
            _subject: { value: null, writable: true },
            _connection: { value: null, writable: true },
            _subscribe: { value: connectableProto._subscribe },
            _isComplete: { value: connectableProto._isComplete, writable: true },
            getSubject: { value: connectableProto.getSubject },
            connect: { value: connectableProto.connect },
            refCount: { value: connectableProto.refCount }
        };
    })();
    var ConnectableSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ConnectableSubscriber, _super);
        function ConnectableSubscriber(destination, connectable) {
            var _this = _super.call(this, destination) || this;
            _this.connectable = connectable;
            return _this;
        }
        ConnectableSubscriber.prototype._error = function (err) {
            this._unsubscribe();
            _super.prototype._error.call(this, err);
        };
        ConnectableSubscriber.prototype._complete = function () {
            this.connectable._isComplete = true;
            this._unsubscribe();
            _super.prototype._complete.call(this);
        };
        ConnectableSubscriber.prototype._unsubscribe = function () {
            var connectable = this.connectable;
            if (connectable) {
                this.connectable = null;
                var connection = connectable._connection;
                connectable._refCount = 0;
                connectable._subject = null;
                connectable._connection = null;
                if (connection) {
                    connection.unsubscribe();
                }
            }
        };
        return ConnectableSubscriber;
    }(SubjectSubscriber));

    /** PURE_IMPORTS_START _observable_ConnectableObservable PURE_IMPORTS_END */
    function multicast(subjectOrSubjectFactory, selector) {
        return function multicastOperatorFunction(source) {
            var subjectFactory;
            if (typeof subjectOrSubjectFactory === 'function') {
                subjectFactory = subjectOrSubjectFactory;
            }
            else {
                subjectFactory = function subjectFactory() {
                    return subjectOrSubjectFactory;
                };
            }
            if (typeof selector === 'function') {
                return source.lift(new MulticastOperator(subjectFactory, selector));
            }
            var connectable = Object.create(source, connectableObservableDescriptor);
            connectable.source = source;
            connectable.subjectFactory = subjectFactory;
            return connectable;
        };
    }
    var MulticastOperator = /*@__PURE__*/ (function () {
        function MulticastOperator(subjectFactory, selector) {
            this.subjectFactory = subjectFactory;
            this.selector = selector;
        }
        MulticastOperator.prototype.call = function (subscriber, source) {
            var selector = this.selector;
            var subject = this.subjectFactory();
            var subscription = selector(subject).subscribe(subscriber);
            subscription.add(source.subscribe(subject));
            return subscription;
        };
        return MulticastOperator;
    }());

    /** PURE_IMPORTS_START _multicast,_refCount,_Subject PURE_IMPORTS_END */
    function shareSubjectFactory() {
        return new Subject();
    }
    function share() {
        return function (source) { return refCount()(multicast(shareSubjectFactory)(source)); };
    }

    /** PURE_IMPORTS_START _observable_concat,_util_isScheduler PURE_IMPORTS_END */
    function startWith() {
        var array = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            array[_i] = arguments[_i];
        }
        var scheduler = array[array.length - 1];
        if (isScheduler(scheduler)) {
            array.pop();
            return function (source) { return concat(array, source, scheduler); };
        }
        else {
            return function (source) { return concat(array, source); };
        }
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function noop() { }

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_noop,_util_isFunction PURE_IMPORTS_END */
    function tap(nextOrObserver, error, complete) {
        return function tapOperatorFunction(source) {
            return source.lift(new DoOperator(nextOrObserver, error, complete));
        };
    }
    var DoOperator = /*@__PURE__*/ (function () {
        function DoOperator(nextOrObserver, error, complete) {
            this.nextOrObserver = nextOrObserver;
            this.error = error;
            this.complete = complete;
        }
        DoOperator.prototype.call = function (subscriber, source) {
            return source.subscribe(new TapSubscriber(subscriber, this.nextOrObserver, this.error, this.complete));
        };
        return DoOperator;
    }());
    var TapSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(TapSubscriber, _super);
        function TapSubscriber(destination, observerOrNext, error, complete) {
            var _this = _super.call(this, destination) || this;
            _this._tapNext = noop;
            _this._tapError = noop;
            _this._tapComplete = noop;
            _this._tapError = error || noop;
            _this._tapComplete = complete || noop;
            if (isFunction(observerOrNext)) {
                _this._context = _this;
                _this._tapNext = observerOrNext;
            }
            else if (observerOrNext) {
                _this._context = observerOrNext;
                _this._tapNext = observerOrNext.next || noop;
                _this._tapError = observerOrNext.error || noop;
                _this._tapComplete = observerOrNext.complete || noop;
            }
            return _this;
        }
        TapSubscriber.prototype._next = function (value) {
            try {
                this._tapNext.call(this._context, value);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.next(value);
        };
        TapSubscriber.prototype._error = function (err) {
            try {
                this._tapError.call(this._context, err);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.error(err);
        };
        TapSubscriber.prototype._complete = function () {
            try {
                this._tapComplete.call(this._context);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            return this.destination.complete();
        };
        return TapSubscriber;
    }(Subscriber));

    /**
     * @license Angular v10.0.0
     * (c) 2010-2020 Google LLC. https://angular.io/
     * License: MIT
     */

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Transforms an `HttpRequest` into a stream of `HttpEvent`s, one of which will likely be a
     * `HttpResponse`.
     *
     * `HttpHandler` is injectable. When injected, the handler instance dispatches requests to the
     * first interceptor in the chain, which dispatches to the second, etc, eventually reaching the
     * `HttpBackend`.
     *
     * In an `HttpInterceptor`, the `HttpHandler` parameter is the next interceptor in the chain.
     *
     * @publicApi
     */
    class HttpHandler {
    }
    /**
     * A final `HttpHandler` which will dispatch the request via browser HTTP APIs to a backend.
     *
     * Interceptors sit between the `HttpClient` interface and the `HttpBackend`.
     *
     * When injected, `HttpBackend` dispatches requests directly to the backend, without going
     * through the interceptor chain.
     *
     * @publicApi
     */
    class HttpBackend {
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Represents the header configuration options for an HTTP request.
     * Instances are immutable. Modifying methods return a cloned
     * instance with the change. The original object is never changed.
     *
     * @publicApi
     */
    class HttpHeaders {
        /**  Constructs a new HTTP header object with the given values.*/
        constructor(headers) {
            /**
             * Internal map of lowercased header names to the normalized
             * form of the name (the form seen first).
             */
            this.normalizedNames = new Map();
            /**
             * Queued updates to be materialized the next initialization.
             */
            this.lazyUpdate = null;
            if (!headers) {
                this.headers = new Map();
            }
            else if (typeof headers === 'string') {
                this.lazyInit = () => {
                    this.headers = new Map();
                    headers.split('\n').forEach(line => {
                        const index = line.indexOf(':');
                        if (index > 0) {
                            const name = line.slice(0, index);
                            const key = name.toLowerCase();
                            const value = line.slice(index + 1).trim();
                            this.maybeSetNormalizedName(name, key);
                            if (this.headers.has(key)) {
                                this.headers.get(key).push(value);
                            }
                            else {
                                this.headers.set(key, [value]);
                            }
                        }
                    });
                };
            }
            else {
                this.lazyInit = () => {
                    this.headers = new Map();
                    Object.keys(headers).forEach(name => {
                        let values = headers[name];
                        const key = name.toLowerCase();
                        if (typeof values === 'string') {
                            values = [values];
                        }
                        if (values.length > 0) {
                            this.headers.set(key, values);
                            this.maybeSetNormalizedName(name, key);
                        }
                    });
                };
            }
        }
        /**
         * Checks for existence of a given header.
         *
         * @param name The header name to check for existence.
         *
         * @returns True if the header exists, false otherwise.
         */
        has(name) {
            this.init();
            return this.headers.has(name.toLowerCase());
        }
        /**
         * Retrieves the first value of a given header.
         *
         * @param name The header name.
         *
         * @returns The value string if the header exists, null otherwise
         */
        get(name) {
            this.init();
            const values = this.headers.get(name.toLowerCase());
            return values && values.length > 0 ? values[0] : null;
        }
        /**
         * Retrieves the names of the headers.
         *
         * @returns A list of header names.
         */
        keys() {
            this.init();
            return Array.from(this.normalizedNames.values());
        }
        /**
         * Retrieves a list of values for a given header.
         *
         * @param name The header name from which to retrieve values.
         *
         * @returns A string of values if the header exists, null otherwise.
         */
        getAll(name) {
            this.init();
            return this.headers.get(name.toLowerCase()) || null;
        }
        /**
         * Appends a new value to the existing set of values for a header
         * and returns them in a clone of the original instance.
         *
         * @param name The header name for which to append the values.
         * @param value The value to append.
         *
         * @returns A clone of the HTTP headers object with the value appended to the given header.
         */
        append(name, value) {
            return this.clone({ name, value, op: 'a' });
        }
        /**
         * Sets or modifies a value for a given header in a clone of the original instance.
         * If the header already exists, its value is replaced with the given value
         * in the returned object.
         *
         * @param name The header name.
         * @param value The value or values to set or overide for the given header.
         *
         * @returns A clone of the HTTP headers object with the newly set header value.
         */
        set(name, value) {
            return this.clone({ name, value, op: 's' });
        }
        /**
         * Deletes values for a given header in a clone of the original instance.
         *
         * @param name The header name.
         * @param value The value or values to delete for the given header.
         *
         * @returns A clone of the HTTP headers object with the given value deleted.
         */
        delete(name, value) {
            return this.clone({ name, value, op: 'd' });
        }
        maybeSetNormalizedName(name, lcName) {
            if (!this.normalizedNames.has(lcName)) {
                this.normalizedNames.set(lcName, name);
            }
        }
        init() {
            if (!!this.lazyInit) {
                if (this.lazyInit instanceof HttpHeaders) {
                    this.copyFrom(this.lazyInit);
                }
                else {
                    this.lazyInit();
                }
                this.lazyInit = null;
                if (!!this.lazyUpdate) {
                    this.lazyUpdate.forEach(update => this.applyUpdate(update));
                    this.lazyUpdate = null;
                }
            }
        }
        copyFrom(other) {
            other.init();
            Array.from(other.headers.keys()).forEach(key => {
                this.headers.set(key, other.headers.get(key));
                this.normalizedNames.set(key, other.normalizedNames.get(key));
            });
        }
        clone(update) {
            const clone = new HttpHeaders();
            clone.lazyInit =
                (!!this.lazyInit && this.lazyInit instanceof HttpHeaders) ? this.lazyInit : this;
            clone.lazyUpdate = (this.lazyUpdate || []).concat([update]);
            return clone;
        }
        applyUpdate(update) {
            const key = update.name.toLowerCase();
            switch (update.op) {
                case 'a':
                case 's':
                    let value = update.value;
                    if (typeof value === 'string') {
                        value = [value];
                    }
                    if (value.length === 0) {
                        return;
                    }
                    this.maybeSetNormalizedName(update.name, key);
                    const base = (update.op === 'a' ? this.headers.get(key) : undefined) || [];
                    base.push(...value);
                    this.headers.set(key, base);
                    break;
                case 'd':
                    const toDelete = update.value;
                    if (!toDelete) {
                        this.headers.delete(key);
                        this.normalizedNames.delete(key);
                    }
                    else {
                        let existing = this.headers.get(key);
                        if (!existing) {
                            return;
                        }
                        existing = existing.filter(value => toDelete.indexOf(value) === -1);
                        if (existing.length === 0) {
                            this.headers.delete(key);
                            this.normalizedNames.delete(key);
                        }
                        else {
                            this.headers.set(key, existing);
                        }
                    }
                    break;
            }
        }
        /**
         * @internal
         */
        forEach(fn) {
            this.init();
            Array.from(this.normalizedNames.keys())
                .forEach(key => fn(this.normalizedNames.get(key), this.headers.get(key)));
        }
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Provides encoding and decoding of URL parameter and query-string values.
     *
     * Serializes and parses URL parameter keys and values to encode and decode them.
     * If you pass URL query parameters without encoding,
     * the query parameters can be misinterpreted at the receiving end.
     *
     *
     * @publicApi
     */
    class HttpUrlEncodingCodec {
        /**
         * Encodes a key name for a URL parameter or query-string.
         * @param key The key name.
         * @returns The encoded key name.
         */
        encodeKey(key) {
            return standardEncoding(key);
        }
        /**
         * Encodes the value of a URL parameter or query-string.
         * @param value The value.
         * @returns The encoded value.
         */
        encodeValue(value) {
            return standardEncoding(value);
        }
        /**
         * Decodes an encoded URL parameter or query-string key.
         * @param key The encoded key name.
         * @returns The decoded key name.
         */
        decodeKey(key) {
            return decodeURIComponent(key);
        }
        /**
         * Decodes an encoded URL parameter or query-string value.
         * @param value The encoded value.
         * @returns The decoded value.
         */
        decodeValue(value) {
            return decodeURIComponent(value);
        }
    }
    function paramParser(rawParams, codec) {
        const map = new Map();
        if (rawParams.length > 0) {
            const params = rawParams.split('&');
            params.forEach((param) => {
                const eqIdx = param.indexOf('=');
                const [key, val] = eqIdx == -1 ?
                    [codec.decodeKey(param), ''] :
                    [codec.decodeKey(param.slice(0, eqIdx)), codec.decodeValue(param.slice(eqIdx + 1))];
                const list = map.get(key) || [];
                list.push(val);
                map.set(key, list);
            });
        }
        return map;
    }
    function standardEncoding(v) {
        return encodeURIComponent(v)
            .replace(/%40/gi, '@')
            .replace(/%3A/gi, ':')
            .replace(/%24/gi, '$')
            .replace(/%2C/gi, ',')
            .replace(/%3B/gi, ';')
            .replace(/%2B/gi, '+')
            .replace(/%3D/gi, '=')
            .replace(/%3F/gi, '?')
            .replace(/%2F/gi, '/');
    }
    /**
     * An HTTP request/response body that represents serialized parameters,
     * per the MIME type `application/x-www-form-urlencoded`.
     *
     * This class is immutable; all mutation operations return a new instance.
     *
     * @publicApi
     */
    class HttpParams {
        constructor(options = {}) {
            this.updates = null;
            this.cloneFrom = null;
            this.encoder = options.encoder || new HttpUrlEncodingCodec();
            if (!!options.fromString) {
                if (!!options.fromObject) {
                    throw new Error(`Cannot specify both fromString and fromObject.`);
                }
                this.map = paramParser(options.fromString, this.encoder);
            }
            else if (!!options.fromObject) {
                this.map = new Map();
                Object.keys(options.fromObject).forEach(key => {
                    const value = options.fromObject[key];
                    this.map.set(key, Array.isArray(value) ? value : [value]);
                });
            }
            else {
                this.map = null;
            }
        }
        /**
         * Reports whether the body includes one or more values for a given parameter.
         * @param param The parameter name.
         * @returns True if the parameter has one or more values,
         * false if it has no value or is not present.
         */
        has(param) {
            this.init();
            return this.map.has(param);
        }
        /**
         * Retrieves the first value for a parameter.
         * @param param The parameter name.
         * @returns The first value of the given parameter,
         * or `null` if the parameter is not present.
         */
        get(param) {
            this.init();
            const res = this.map.get(param);
            return !!res ? res[0] : null;
        }
        /**
         * Retrieves all values for a  parameter.
         * @param param The parameter name.
         * @returns All values in a string array,
         * or `null` if the parameter not present.
         */
        getAll(param) {
            this.init();
            return this.map.get(param) || null;
        }
        /**
         * Retrieves all the parameters for this body.
         * @returns The parameter names in a string array.
         */
        keys() {
            this.init();
            return Array.from(this.map.keys());
        }
        /**
         * Appends a new value to existing values for a parameter.
         * @param param The parameter name.
         * @param value The new value to add.
         * @return A new body with the appended value.
         */
        append(param, value) {
            return this.clone({ param, value, op: 'a' });
        }
        /**
         * Replaces the value for a parameter.
         * @param param The parameter name.
         * @param value The new value.
         * @return A new body with the new value.
         */
        set(param, value) {
            return this.clone({ param, value, op: 's' });
        }
        /**
         * Removes a given value or all values from a parameter.
         * @param param The parameter name.
         * @param value The value to remove, if provided.
         * @return A new body with the given value removed, or with all values
         * removed if no value is specified.
         */
        delete(param, value) {
            return this.clone({ param, value, op: 'd' });
        }
        /**
         * Serializes the body to an encoded string, where key-value pairs (separated by `=`) are
         * separated by `&`s.
         */
        toString() {
            this.init();
            return this.keys()
                .map(key => {
                const eKey = this.encoder.encodeKey(key);
                // `a: ['1']` produces `'a=1'`
                // `b: []` produces `''`
                // `c: ['1', '2']` produces `'c=1&c=2'`
                return this.map.get(key).map(value => eKey + '=' + this.encoder.encodeValue(value))
                    .join('&');
            })
                // filter out empty values because `b: []` produces `''`
                // which results in `a=1&&c=1&c=2` instead of `a=1&c=1&c=2` if we don't
                .filter(param => param !== '')
                .join('&');
        }
        clone(update) {
            const clone = new HttpParams({ encoder: this.encoder });
            clone.cloneFrom = this.cloneFrom || this;
            clone.updates = (this.updates || []).concat([update]);
            return clone;
        }
        init() {
            if (this.map === null) {
                this.map = new Map();
            }
            if (this.cloneFrom !== null) {
                this.cloneFrom.init();
                this.cloneFrom.keys().forEach(key => this.map.set(key, this.cloneFrom.map.get(key)));
                this.updates.forEach(update => {
                    switch (update.op) {
                        case 'a':
                        case 's':
                            const base = (update.op === 'a' ? this.map.get(update.param) : undefined) || [];
                            base.push(update.value);
                            this.map.set(update.param, base);
                            break;
                        case 'd':
                            if (update.value !== undefined) {
                                let base = this.map.get(update.param) || [];
                                const idx = base.indexOf(update.value);
                                if (idx !== -1) {
                                    base.splice(idx, 1);
                                }
                                if (base.length > 0) {
                                    this.map.set(update.param, base);
                                }
                                else {
                                    this.map.delete(update.param);
                                }
                            }
                            else {
                                this.map.delete(update.param);
                                break;
                            }
                    }
                });
                this.cloneFrom = this.updates = null;
            }
        }
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Determine whether the given HTTP method may include a body.
     */
    function mightHaveBody(method) {
        switch (method) {
            case 'DELETE':
            case 'GET':
            case 'HEAD':
            case 'OPTIONS':
            case 'JSONP':
                return false;
            default:
                return true;
        }
    }
    /**
     * Safely assert whether the given value is an ArrayBuffer.
     *
     * In some execution environments ArrayBuffer is not defined.
     */
    function isArrayBuffer(value) {
        return typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer;
    }
    /**
     * Safely assert whether the given value is a Blob.
     *
     * In some execution environments Blob is not defined.
     */
    function isBlob(value) {
        return typeof Blob !== 'undefined' && value instanceof Blob;
    }
    /**
     * Safely assert whether the given value is a FormData instance.
     *
     * In some execution environments FormData is not defined.
     */
    function isFormData(value) {
        return typeof FormData !== 'undefined' && value instanceof FormData;
    }
    /**
     * An outgoing HTTP request with an optional typed body.
     *
     * `HttpRequest` represents an outgoing request, including URL, method,
     * headers, body, and other request configuration options. Instances should be
     * assumed to be immutable. To modify a `HttpRequest`, the `clone`
     * method should be used.
     *
     * @publicApi
     */
    class HttpRequest {
        constructor(method, url, third, fourth) {
            this.url = url;
            /**
             * The request body, or `null` if one isn't set.
             *
             * Bodies are not enforced to be immutable, as they can include a reference to any
             * user-defined data type. However, interceptors should take care to preserve
             * idempotence by treating them as such.
             */
            this.body = null;
            /**
             * Whether this request should be made in a way that exposes progress events.
             *
             * Progress events are expensive (change detection runs on each event) and so
             * they should only be requested if the consumer intends to monitor them.
             */
            this.reportProgress = false;
            /**
             * Whether this request should be sent with outgoing credentials (cookies).
             */
            this.withCredentials = false;
            /**
             * The expected response type of the server.
             *
             * This is used to parse the response appropriately before returning it to
             * the requestee.
             */
            this.responseType = 'json';
            this.method = method.toUpperCase();
            // Next, need to figure out which argument holds the HttpRequestInit
            // options, if any.
            let options;
            // Check whether a body argument is expected. The only valid way to omit
            // the body argument is to use a known no-body method like GET.
            if (mightHaveBody(this.method) || !!fourth) {
                // Body is the third argument, options are the fourth.
                this.body = (third !== undefined) ? third : null;
                options = fourth;
            }
            else {
                // No body required, options are the third argument. The body stays null.
                options = third;
            }
            // If options have been passed, interpret them.
            if (options) {
                // Normalize reportProgress and withCredentials.
                this.reportProgress = !!options.reportProgress;
                this.withCredentials = !!options.withCredentials;
                // Override default response type of 'json' if one is provided.
                if (!!options.responseType) {
                    this.responseType = options.responseType;
                }
                // Override headers if they're provided.
                if (!!options.headers) {
                    this.headers = options.headers;
                }
                if (!!options.params) {
                    this.params = options.params;
                }
            }
            // If no headers have been passed in, construct a new HttpHeaders instance.
            if (!this.headers) {
                this.headers = new HttpHeaders();
            }
            // If no parameters have been passed in, construct a new HttpUrlEncodedParams instance.
            if (!this.params) {
                this.params = new HttpParams();
                this.urlWithParams = url;
            }
            else {
                // Encode the parameters to a string in preparation for inclusion in the URL.
                const params = this.params.toString();
                if (params.length === 0) {
                    // No parameters, the visible URL is just the URL given at creation time.
                    this.urlWithParams = url;
                }
                else {
                    // Does the URL already have query parameters? Look for '?'.
                    const qIdx = url.indexOf('?');
                    // There are 3 cases to handle:
                    // 1) No existing parameters -> append '?' followed by params.
                    // 2) '?' exists and is followed by existing query string ->
                    //    append '&' followed by params.
                    // 3) '?' exists at the end of the url -> append params directly.
                    // This basically amounts to determining the character, if any, with
                    // which to join the URL and parameters.
                    const sep = qIdx === -1 ? '?' : (qIdx < url.length - 1 ? '&' : '');
                    this.urlWithParams = url + sep + params;
                }
            }
        }
        /**
         * Transform the free-form body into a serialized format suitable for
         * transmission to the server.
         */
        serializeBody() {
            // If no body is present, no need to serialize it.
            if (this.body === null) {
                return null;
            }
            // Check whether the body is already in a serialized form. If so,
            // it can just be returned directly.
            if (isArrayBuffer(this.body) || isBlob(this.body) || isFormData(this.body) ||
                typeof this.body === 'string') {
                return this.body;
            }
            // Check whether the body is an instance of HttpUrlEncodedParams.
            if (this.body instanceof HttpParams) {
                return this.body.toString();
            }
            // Check whether the body is an object or array, and serialize with JSON if so.
            if (typeof this.body === 'object' || typeof this.body === 'boolean' ||
                Array.isArray(this.body)) {
                return JSON.stringify(this.body);
            }
            // Fall back on toString() for everything else.
            return this.body.toString();
        }
        /**
         * Examine the body and attempt to infer an appropriate MIME type
         * for it.
         *
         * If no such type can be inferred, this method will return `null`.
         */
        detectContentTypeHeader() {
            // An empty body has no content type.
            if (this.body === null) {
                return null;
            }
            // FormData bodies rely on the browser's content type assignment.
            if (isFormData(this.body)) {
                return null;
            }
            // Blobs usually have their own content type. If it doesn't, then
            // no type can be inferred.
            if (isBlob(this.body)) {
                return this.body.type || null;
            }
            // Array buffers have unknown contents and thus no type can be inferred.
            if (isArrayBuffer(this.body)) {
                return null;
            }
            // Technically, strings could be a form of JSON data, but it's safe enough
            // to assume they're plain strings.
            if (typeof this.body === 'string') {
                return 'text/plain';
            }
            // `HttpUrlEncodedParams` has its own content-type.
            if (this.body instanceof HttpParams) {
                return 'application/x-www-form-urlencoded;charset=UTF-8';
            }
            // Arrays, objects, and numbers will be encoded as JSON.
            if (typeof this.body === 'object' || typeof this.body === 'number' ||
                Array.isArray(this.body)) {
                return 'application/json';
            }
            // No type could be inferred.
            return null;
        }
        clone(update = {}) {
            // For method, url, and responseType, take the current value unless
            // it is overridden in the update hash.
            const method = update.method || this.method;
            const url = update.url || this.url;
            const responseType = update.responseType || this.responseType;
            // The body is somewhat special - a `null` value in update.body means
            // whatever current body is present is being overridden with an empty
            // body, whereas an `undefined` value in update.body implies no
            // override.
            const body = (update.body !== undefined) ? update.body : this.body;
            // Carefully handle the boolean options to differentiate between
            // `false` and `undefined` in the update args.
            const withCredentials = (update.withCredentials !== undefined) ? update.withCredentials : this.withCredentials;
            const reportProgress = (update.reportProgress !== undefined) ? update.reportProgress : this.reportProgress;
            // Headers and params may be appended to if `setHeaders` or
            // `setParams` are used.
            let headers = update.headers || this.headers;
            let params = update.params || this.params;
            // Check whether the caller has asked to add headers.
            if (update.setHeaders !== undefined) {
                // Set every requested header.
                headers =
                    Object.keys(update.setHeaders)
                        .reduce((headers, name) => headers.set(name, update.setHeaders[name]), headers);
            }
            // Check whether the caller has asked to set params.
            if (update.setParams) {
                // Set every requested param.
                params = Object.keys(update.setParams)
                    .reduce((params, param) => params.set(param, update.setParams[param]), params);
            }
            // Finally, construct the new HttpRequest using the pieces from above.
            return new HttpRequest(method, url, body, {
                params,
                headers,
                reportProgress,
                responseType,
                withCredentials,
            });
        }
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Type enumeration for the different kinds of `HttpEvent`.
     *
     * @publicApi
     */
    var HttpEventType;
    (function (HttpEventType) {
        /**
         * The request was sent out over the wire.
         */
        HttpEventType[HttpEventType["Sent"] = 0] = "Sent";
        /**
         * An upload progress event was received.
         */
        HttpEventType[HttpEventType["UploadProgress"] = 1] = "UploadProgress";
        /**
         * The response status code and headers were received.
         */
        HttpEventType[HttpEventType["ResponseHeader"] = 2] = "ResponseHeader";
        /**
         * A download progress event was received.
         */
        HttpEventType[HttpEventType["DownloadProgress"] = 3] = "DownloadProgress";
        /**
         * The full response including the body was received.
         */
        HttpEventType[HttpEventType["Response"] = 4] = "Response";
        /**
         * A custom event from an interceptor or a backend.
         */
        HttpEventType[HttpEventType["User"] = 5] = "User";
    })(HttpEventType || (HttpEventType = {}));
    /**
     * Base class for both `HttpResponse` and `HttpHeaderResponse`.
     *
     * @publicApi
     */
    class HttpResponseBase {
        /**
         * Super-constructor for all responses.
         *
         * The single parameter accepted is an initialization hash. Any properties
         * of the response passed there will override the default values.
         */
        constructor(init, defaultStatus = 200, defaultStatusText = 'OK') {
            // If the hash has values passed, use them to initialize the response.
            // Otherwise use the default values.
            this.headers = init.headers || new HttpHeaders();
            this.status = init.status !== undefined ? init.status : defaultStatus;
            this.statusText = init.statusText || defaultStatusText;
            this.url = init.url || null;
            // Cache the ok value to avoid defining a getter.
            this.ok = this.status >= 200 && this.status < 300;
        }
    }
    /**
     * A partial HTTP response which only includes the status and header data,
     * but no response body.
     *
     * `HttpHeaderResponse` is a `HttpEvent` available on the response
     * event stream, only when progress events are requested.
     *
     * @publicApi
     */
    class HttpHeaderResponse extends HttpResponseBase {
        /**
         * Create a new `HttpHeaderResponse` with the given parameters.
         */
        constructor(init = {}) {
            super(init);
            this.type = HttpEventType.ResponseHeader;
        }
        /**
         * Copy this `HttpHeaderResponse`, overriding its contents with the
         * given parameter hash.
         */
        clone(update = {}) {
            // Perform a straightforward initialization of the new HttpHeaderResponse,
            // overriding the current parameters with new ones if given.
            return new HttpHeaderResponse({
                headers: update.headers || this.headers,
                status: update.status !== undefined ? update.status : this.status,
                statusText: update.statusText || this.statusText,
                url: update.url || this.url || undefined,
            });
        }
    }
    /**
     * A full HTTP response, including a typed response body (which may be `null`
     * if one was not returned).
     *
     * `HttpResponse` is a `HttpEvent` available on the response event
     * stream.
     *
     * @publicApi
     */
    class HttpResponse extends HttpResponseBase {
        /**
         * Construct a new `HttpResponse`.
         */
        constructor(init = {}) {
            super(init);
            this.type = HttpEventType.Response;
            this.body = init.body !== undefined ? init.body : null;
        }
        clone(update = {}) {
            return new HttpResponse({
                body: (update.body !== undefined) ? update.body : this.body,
                headers: update.headers || this.headers,
                status: (update.status !== undefined) ? update.status : this.status,
                statusText: update.statusText || this.statusText,
                url: update.url || this.url || undefined,
            });
        }
    }
    /**
     * A response that represents an error or failure, either from a
     * non-successful HTTP status, an error while executing the request,
     * or some other failure which occurred during the parsing of the response.
     *
     * Any error returned on the `Observable` response stream will be
     * wrapped in an `HttpErrorResponse` to provide additional context about
     * the state of the HTTP layer when the error occurred. The error property
     * will contain either a wrapped Error object or the error response returned
     * from the server.
     *
     * @publicApi
     */
    class HttpErrorResponse extends HttpResponseBase {
        constructor(init) {
            // Initialize with a default status of 0 / Unknown Error.
            super(init, 0, 'Unknown Error');
            this.name = 'HttpErrorResponse';
            /**
             * Errors are never okay, even when the status code is in the 2xx success range.
             */
            this.ok = false;
            // If the response was successful, then this was a parse error. Otherwise, it was
            // a protocol-level failure of some sort. Either the request failed in transit
            // or the server returned an unsuccessful status code.
            if (this.status >= 200 && this.status < 300) {
                this.message = `Http failure during parsing for ${init.url || '(unknown url)'}`;
            }
            else {
                this.message = `Http failure response for ${init.url || '(unknown url)'}: ${init.status} ${init.statusText}`;
            }
            this.error = init.error || null;
        }
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Constructs an instance of `HttpRequestOptions<T>` from a source `HttpMethodOptions` and
     * the given `body`. This function clones the object and adds the body.
     *
     * Note that the `responseType` *options* value is a String that identifies the
     * single data type of the response.
     * A single overload version of the method handles each response type.
     * The value of `responseType` cannot be a union, as the combined signature could imply.
     *
     */
    function addBody(options, body) {
        return {
            body,
            headers: options.headers,
            observe: options.observe,
            params: options.params,
            reportProgress: options.reportProgress,
            responseType: options.responseType,
            withCredentials: options.withCredentials,
        };
    }
    /**
     * Performs HTTP requests.
     * This service is available as an injectable class, with methods to perform HTTP requests.
     * Each request method has multiple signatures, and the return type varies based on
     * the signature that is called (mainly the values of `observe` and `responseType`).
     *
     * Note that the `responseType` *options* value is a String that identifies the
     * single data type of the response.
     * A single overload version of the method handles each response type.
     * The value of `responseType` cannot be a union, as the combined signature could imply.

     *
     * @usageNotes
     * Sample HTTP requests for the [Tour of Heroes](/tutorial/toh-pt0) application.
     *
     * ### HTTP Request Example
     *
     * ```
     *  // GET heroes whose name contains search term
     * searchHeroes(term: string): observable<Hero[]>{
     *
     *  const params = new HttpParams({fromString: 'name=term'});
     *    return this.httpClient.request('GET', this.heroesUrl, {responseType:'json', params});
     * }
     * ```
     * ### JSONP Example
     * ```
     * requestJsonp(url, callback = 'callback') {
     *  return this.httpClient.jsonp(this.heroesURL, callback);
     * }
     * ```
     *
     * ### PATCH Example
     * ```
     * // PATCH one of the heroes' name
     * patchHero (id: number, heroName: string): Observable<{}> {
     * const url = `${this.heroesUrl}/${id}`;   // PATCH api/heroes/42
     *  return this.httpClient.patch(url, {name: heroName}, httpOptions)
     *    .pipe(catchError(this.handleError('patchHero')));
     * }
     * ```
     *
     * @see [HTTP Guide](guide/http)
     *
     * @publicApi
     */
    class HttpClient {
        constructor(handler) {
            this.handler = handler;
        }
        /**
         * Constructs an observable for a generic HTTP request that, when subscribed,
         * fires the request through the chain of registered interceptors and on to the
         * server.
         *
         * You can pass an `HttpRequest` directly as the only parameter. In this case,
         * the call returns an observable of the raw `HttpEvent` stream.
         *
         * Alternatively you can pass an HTTP method as the first parameter,
         * a URL string as the second, and an options hash containing the request body as the third.
         * See `addBody()`. In this case, the specified `responseType` and `observe` options determine the
         * type of returned observable.
         *   * The `responseType` value determines how a successful response body is parsed.
         *   * If `responseType` is the default `json`, you can pass a type interface for the resulting
         * object as a type parameter to the call.
         *
         * The `observe` value determines the return type, according to what you are interested in
         * observing.
         *   * An `observe` value of events returns an observable of the raw `HttpEvent` stream, including
         * progress events by default.
         *   * An `observe` value of response returns an observable of `HttpResponse<T>`,
         * where the `T` parameter depends on the `responseType` and any optionally provided type
         * parameter.
         *   * An `observe` value of body returns an observable of `<T>` with the same `T` body type.
         *
         */
        request(first, url, options = {}) {
            let req;
            // First, check whether the primary argument is an instance of `HttpRequest`.
            if (first instanceof HttpRequest) {
                // It is. The other arguments must be undefined (per the signatures) and can be
                // ignored.
                req = first;
            }
            else {
                // It's a string, so it represents a URL. Construct a request based on it,
                // and incorporate the remaining arguments (assuming `GET` unless a method is
                // provided.
                // Figure out the headers.
                let headers = undefined;
                if (options.headers instanceof HttpHeaders) {
                    headers = options.headers;
                }
                else {
                    headers = new HttpHeaders(options.headers);
                }
                // Sort out parameters.
                let params = undefined;
                if (!!options.params) {
                    if (options.params instanceof HttpParams) {
                        params = options.params;
                    }
                    else {
                        params = new HttpParams({ fromObject: options.params });
                    }
                }
                // Construct the request.
                req = new HttpRequest(first, url, (options.body !== undefined ? options.body : null), {
                    headers,
                    params,
                    reportProgress: options.reportProgress,
                    // By default, JSON is assumed to be returned for all calls.
                    responseType: options.responseType || 'json',
                    withCredentials: options.withCredentials,
                });
            }
            // Start with an Observable.of() the initial request, and run the handler (which
            // includes all interceptors) inside a concatMap(). This way, the handler runs
            // inside an Observable chain, which causes interceptors to be re-run on every
            // subscription (this also makes retries re-run the handler, including interceptors).
            const events$ = rxjs.of(req).pipe(concatMap((req) => this.handler.handle(req)));
            // If coming via the API signature which accepts a previously constructed HttpRequest,
            // the only option is to get the event stream. Otherwise, return the event stream if
            // that is what was requested.
            if (first instanceof HttpRequest || options.observe === 'events') {
                return events$;
            }
            // The requested stream contains either the full response or the body. In either
            // case, the first step is to filter the event stream to extract a stream of
            // responses(s).
            const res$ = events$.pipe(filter((event) => event instanceof HttpResponse));
            // Decide which stream to return.
            switch (options.observe || 'body') {
                case 'body':
                    // The requested stream is the body. Map the response stream to the response
                    // body. This could be done more simply, but a misbehaving interceptor might
                    // transform the response body into a different format and ignore the requested
                    // responseType. Guard against this by validating that the response is of the
                    // requested type.
                    switch (req.responseType) {
                        case 'arraybuffer':
                            return res$.pipe(map((res) => {
                                // Validate that the body is an ArrayBuffer.
                                if (res.body !== null && !(res.body instanceof ArrayBuffer)) {
                                    throw new Error('Response is not an ArrayBuffer.');
                                }
                                return res.body;
                            }));
                        case 'blob':
                            return res$.pipe(map((res) => {
                                // Validate that the body is a Blob.
                                if (res.body !== null && !(res.body instanceof Blob)) {
                                    throw new Error('Response is not a Blob.');
                                }
                                return res.body;
                            }));
                        case 'text':
                            return res$.pipe(map((res) => {
                                // Validate that the body is a string.
                                if (res.body !== null && typeof res.body !== 'string') {
                                    throw new Error('Response is not a string.');
                                }
                                return res.body;
                            }));
                        case 'json':
                        default:
                            // No validation needed for JSON responses, as they can be of any type.
                            return res$.pipe(map((res) => res.body));
                    }
                case 'response':
                    // The response stream was requested directly, so return it.
                    return res$;
                default:
                    // Guard against new future observe types being added.
                    throw new Error(`Unreachable: unhandled observe type ${options.observe}}`);
            }
        }
        /**
         * Constructs an observable that, when subscribed, causes the configured
         * `DELETE` request to execute on the server. See the individual overloads for
         * details on the return type.
         *
         * @param url     The endpoint URL.
         * @param options The HTTP options to send with the request.
         *
         */
        delete(url, options = {}) {
            return this.request('DELETE', url, options);
        }
        /**
         * Constructs an observable that, when subscribed, causes the configured
         * `GET` request to execute on the server. See the individual overloads for
         * details on the return type.
         */
        get(url, options = {}) {
            return this.request('GET', url, options);
        }
        /**
         * Constructs an observable that, when subscribed, causes the configured
         * `HEAD` request to execute on the server. The `HEAD` method returns
         * meta information about the resource without transferring the
         * resource itself. See the individual overloads for
         * details on the return type.
         */
        head(url, options = {}) {
            return this.request('HEAD', url, options);
        }
        /**
         * Constructs an `Observable` that, when subscribed, causes a request with the special method
         * `JSONP` to be dispatched via the interceptor pipeline.
         * The [JSONP pattern](https://en.wikipedia.org/wiki/JSONP) works around limitations of certain
         * API endpoints that don't support newer,
         * and preferable [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) protocol.
         * JSONP treats the endpoint API as a JavaScript file and tricks the browser to process the
         * requests even if the API endpoint is not located on the same domain (origin) as the client-side
         * application making the request.
         * The endpoint API must support JSONP callback for JSONP requests to work.
         * The resource API returns the JSON response wrapped in a callback function.
         * You can pass the callback function name as one of the query parameters.
         * Note that JSONP requests can only be used with `GET` requests.
         *
         * @param url The resource URL.
         * @param callbackParam The callback function name.
         *
         */
        jsonp(url, callbackParam) {
            return this.request('JSONP', url, {
                params: new HttpParams().append(callbackParam, 'JSONP_CALLBACK'),
                observe: 'body',
                responseType: 'json',
            });
        }
        /**
         * Constructs an `Observable` that, when subscribed, causes the configured
         * `OPTIONS` request to execute on the server. This method allows the client
         * to determine the supported HTTP methods and other capabilites of an endpoint,
         * without implying a resource action. See the individual overloads for
         * details on the return type.
         */
        options(url, options = {}) {
            return this.request('OPTIONS', url, options);
        }
        /**
         * Constructs an observable that, when subscribed, causes the configured
         * `PATCH` request to execute on the server. See the individual overloads for
         * details on the return type.
         */
        patch(url, body, options = {}) {
            return this.request('PATCH', url, addBody(options, body));
        }
        /**
         * Constructs an observable that, when subscribed, causes the configured
         * `POST` request to execute on the server. The server responds with the location of
         * the replaced resource. See the individual overloads for
         * details on the return type.
         */
        post(url, body, options = {}) {
            return this.request('POST', url, addBody(options, body));
        }
        /**
         * Constructs an observable that, when subscribed, causes the configured
         * `PUT` request to execute on the server. The `PUT` method replaces an existing resource
         * with a new set of values.
         * See the individual overloads for details on the return type.
         */
        put(url, body, options = {}) {
            return this.request('PUT', url, addBody(options, body));
        }
    }
    HttpClient.decorators = [
        { type: core.Injectable }
    ];
    HttpClient.ctorParameters = () => [
        { type: HttpHandler }
    ];

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * `HttpHandler` which applies an `HttpInterceptor` to an `HttpRequest`.
     *
     *
     */
    class HttpInterceptorHandler {
        constructor(next, interceptor) {
            this.next = next;
            this.interceptor = interceptor;
        }
        handle(req) {
            return this.interceptor.intercept(req, this.next);
        }
    }
    /**
     * A multi-provider token that represents the array of registered
     * `HttpInterceptor` objects.
     *
     * @publicApi
     */
    const HTTP_INTERCEPTORS = new core.InjectionToken('HTTP_INTERCEPTORS');
    class NoopInterceptor {
        intercept(req, next) {
            return next.handle(req);
        }
    }
    NoopInterceptor.decorators = [
        { type: core.Injectable }
    ];

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    const XSSI_PREFIX = /^\)\]\}',?\n/;
    /**
     * Determine an appropriate URL for the response, by checking either
     * XMLHttpRequest.responseURL or the X-Request-URL header.
     */
    function getResponseUrl(xhr) {
        if ('responseURL' in xhr && xhr.responseURL) {
            return xhr.responseURL;
        }
        if (/^X-Request-URL:/m.test(xhr.getAllResponseHeaders())) {
            return xhr.getResponseHeader('X-Request-URL');
        }
        return null;
    }
    /**
     * A wrapper around the `XMLHttpRequest` constructor.
     *
     * @publicApi
     */
    class XhrFactory {
    }
    /**
     * A factory for `HttpXhrBackend` that uses the `XMLHttpRequest` browser API.
     *
     */
    class BrowserXhr {
        constructor() { }
        build() {
            return (new XMLHttpRequest());
        }
    }
    BrowserXhr.decorators = [
        { type: core.Injectable }
    ];
    BrowserXhr.ctorParameters = () => [];
    /**
     * Uses `XMLHttpRequest` to send requests to a backend server.
     * @see `HttpHandler`
     * @see `JsonpClientBackend`
     *
     * @publicApi
     */
    class HttpXhrBackend {
        constructor(xhrFactory) {
            this.xhrFactory = xhrFactory;
        }
        /**
         * Processes a request and returns a stream of response events.
         * @param req The request object.
         * @returns An observable of the response events.
         */
        handle(req) {
            // Quick check to give a better error message when a user attempts to use
            // HttpClient.jsonp() without installing the JsonpClientModule
            if (req.method === 'JSONP') {
                throw new Error(`Attempted to construct Jsonp request without JsonpClientModule installed.`);
            }
            // Everything happens on Observable subscription.
            return new rxjs.Observable((observer) => {
                // Start by setting up the XHR object with request method, URL, and withCredentials flag.
                const xhr = this.xhrFactory.build();
                xhr.open(req.method, req.urlWithParams);
                if (!!req.withCredentials) {
                    xhr.withCredentials = true;
                }
                // Add all the requested headers.
                req.headers.forEach((name, values) => xhr.setRequestHeader(name, values.join(',')));
                // Add an Accept header if one isn't present already.
                if (!req.headers.has('Accept')) {
                    xhr.setRequestHeader('Accept', 'application/json, text/plain, */*');
                }
                // Auto-detect the Content-Type header if one isn't present already.
                if (!req.headers.has('Content-Type')) {
                    const detectedType = req.detectContentTypeHeader();
                    // Sometimes Content-Type detection fails.
                    if (detectedType !== null) {
                        xhr.setRequestHeader('Content-Type', detectedType);
                    }
                }
                // Set the responseType if one was requested.
                if (req.responseType) {
                    const responseType = req.responseType.toLowerCase();
                    // JSON responses need to be processed as text. This is because if the server
                    // returns an XSSI-prefixed JSON response, the browser will fail to parse it,
                    // xhr.response will be null, and xhr.responseText cannot be accessed to
                    // retrieve the prefixed JSON data in order to strip the prefix. Thus, all JSON
                    // is parsed by first requesting text and then applying JSON.parse.
                    xhr.responseType = ((responseType !== 'json') ? responseType : 'text');
                }
                // Serialize the request body if one is present. If not, this will be set to null.
                const reqBody = req.serializeBody();
                // If progress events are enabled, response headers will be delivered
                // in two events - the HttpHeaderResponse event and the full HttpResponse
                // event. However, since response headers don't change in between these
                // two events, it doesn't make sense to parse them twice. So headerResponse
                // caches the data extracted from the response whenever it's first parsed,
                // to ensure parsing isn't duplicated.
                let headerResponse = null;
                // partialFromXhr extracts the HttpHeaderResponse from the current XMLHttpRequest
                // state, and memoizes it into headerResponse.
                const partialFromXhr = () => {
                    if (headerResponse !== null) {
                        return headerResponse;
                    }
                    // Read status and normalize an IE9 bug (http://bugs.jquery.com/ticket/1450).
                    const status = xhr.status === 1223 ? 204 : xhr.status;
                    const statusText = xhr.statusText || 'OK';
                    // Parse headers from XMLHttpRequest - this step is lazy.
                    const headers = new HttpHeaders(xhr.getAllResponseHeaders());
                    // Read the response URL from the XMLHttpResponse instance and fall back on the
                    // request URL.
                    const url = getResponseUrl(xhr) || req.url;
                    // Construct the HttpHeaderResponse and memoize it.
                    headerResponse = new HttpHeaderResponse({ headers, status, statusText, url });
                    return headerResponse;
                };
                // Next, a few closures are defined for the various events which XMLHttpRequest can
                // emit. This allows them to be unregistered as event listeners later.
                // First up is the load event, which represents a response being fully available.
                const onLoad = () => {
                    // Read response state from the memoized partial data.
                    let { headers, status, statusText, url } = partialFromXhr();
                    // The body will be read out if present.
                    let body = null;
                    if (status !== 204) {
                        // Use XMLHttpRequest.response if set, responseText otherwise.
                        body = (typeof xhr.response === 'undefined') ? xhr.responseText : xhr.response;
                    }
                    // Normalize another potential bug (this one comes from CORS).
                    if (status === 0) {
                        status = !!body ? 200 : 0;
                    }
                    // ok determines whether the response will be transmitted on the event or
                    // error channel. Unsuccessful status codes (not 2xx) will always be errors,
                    // but a successful status code can still result in an error if the user
                    // asked for JSON data and the body cannot be parsed as such.
                    let ok = status >= 200 && status < 300;
                    // Check whether the body needs to be parsed as JSON (in many cases the browser
                    // will have done that already).
                    if (req.responseType === 'json' && typeof body === 'string') {
                        // Save the original body, before attempting XSSI prefix stripping.
                        const originalBody = body;
                        body = body.replace(XSSI_PREFIX, '');
                        try {
                            // Attempt the parse. If it fails, a parse error should be delivered to the user.
                            body = body !== '' ? JSON.parse(body) : null;
                        }
                        catch (error) {
                            // Since the JSON.parse failed, it's reasonable to assume this might not have been a
                            // JSON response. Restore the original body (including any XSSI prefix) to deliver
                            // a better error response.
                            body = originalBody;
                            // If this was an error request to begin with, leave it as a string, it probably
                            // just isn't JSON. Otherwise, deliver the parsing error to the user.
                            if (ok) {
                                // Even though the response status was 2xx, this is still an error.
                                ok = false;
                                // The parse error contains the text of the body that failed to parse.
                                body = { error, text: body };
                            }
                        }
                    }
                    if (ok) {
                        // A successful response is delivered on the event stream.
                        observer.next(new HttpResponse({
                            body,
                            headers,
                            status,
                            statusText,
                            url: url || undefined,
                        }));
                        // The full body has been received and delivered, no further events
                        // are possible. This request is complete.
                        observer.complete();
                    }
                    else {
                        // An unsuccessful request is delivered on the error channel.
                        observer.error(new HttpErrorResponse({
                            // The error in this case is the response body (error from the server).
                            error: body,
                            headers,
                            status,
                            statusText,
                            url: url || undefined,
                        }));
                    }
                };
                // The onError callback is called when something goes wrong at the network level.
                // Connection timeout, DNS error, offline, etc. These are actual errors, and are
                // transmitted on the error channel.
                const onError = (error) => {
                    const { url } = partialFromXhr();
                    const res = new HttpErrorResponse({
                        error,
                        status: xhr.status || 0,
                        statusText: xhr.statusText || 'Unknown Error',
                        url: url || undefined,
                    });
                    observer.error(res);
                };
                // The sentHeaders flag tracks whether the HttpResponseHeaders event
                // has been sent on the stream. This is necessary to track if progress
                // is enabled since the event will be sent on only the first download
                // progerss event.
                let sentHeaders = false;
                // The download progress event handler, which is only registered if
                // progress events are enabled.
                const onDownProgress = (event) => {
                    // Send the HttpResponseHeaders event if it hasn't been sent already.
                    if (!sentHeaders) {
                        observer.next(partialFromXhr());
                        sentHeaders = true;
                    }
                    // Start building the download progress event to deliver on the response
                    // event stream.
                    let progressEvent = {
                        type: HttpEventType.DownloadProgress,
                        loaded: event.loaded,
                    };
                    // Set the total number of bytes in the event if it's available.
                    if (event.lengthComputable) {
                        progressEvent.total = event.total;
                    }
                    // If the request was for text content and a partial response is
                    // available on XMLHttpRequest, include it in the progress event
                    // to allow for streaming reads.
                    if (req.responseType === 'text' && !!xhr.responseText) {
                        progressEvent.partialText = xhr.responseText;
                    }
                    // Finally, fire the event.
                    observer.next(progressEvent);
                };
                // The upload progress event handler, which is only registered if
                // progress events are enabled.
                const onUpProgress = (event) => {
                    // Upload progress events are simpler. Begin building the progress
                    // event.
                    let progress = {
                        type: HttpEventType.UploadProgress,
                        loaded: event.loaded,
                    };
                    // If the total number of bytes being uploaded is available, include
                    // it.
                    if (event.lengthComputable) {
                        progress.total = event.total;
                    }
                    // Send the event.
                    observer.next(progress);
                };
                // By default, register for load and error events.
                xhr.addEventListener('load', onLoad);
                xhr.addEventListener('error', onError);
                // Progress events are only enabled if requested.
                if (req.reportProgress) {
                    // Download progress is always enabled if requested.
                    xhr.addEventListener('progress', onDownProgress);
                    // Upload progress depends on whether there is a body to upload.
                    if (reqBody !== null && xhr.upload) {
                        xhr.upload.addEventListener('progress', onUpProgress);
                    }
                }
                // Fire the request, and notify the event stream that it was fired.
                xhr.send(reqBody);
                observer.next({ type: HttpEventType.Sent });
                // This is the return from the Observable function, which is the
                // request cancellation handler.
                return () => {
                    // On a cancellation, remove all registered event listeners.
                    xhr.removeEventListener('error', onError);
                    xhr.removeEventListener('load', onLoad);
                    if (req.reportProgress) {
                        xhr.removeEventListener('progress', onDownProgress);
                        if (reqBody !== null && xhr.upload) {
                            xhr.upload.removeEventListener('progress', onUpProgress);
                        }
                    }
                    // Finally, abort the in-flight request.
                    xhr.abort();
                };
            });
        }
    }
    HttpXhrBackend.decorators = [
        { type: core.Injectable }
    ];
    HttpXhrBackend.ctorParameters = () => [
        { type: XhrFactory }
    ];

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    const XSRF_COOKIE_NAME = new core.InjectionToken('XSRF_COOKIE_NAME');
    const XSRF_HEADER_NAME = new core.InjectionToken('XSRF_HEADER_NAME');
    /**
     * Retrieves the current XSRF token to use with the next outgoing request.
     *
     * @publicApi
     */
    class HttpXsrfTokenExtractor {
    }
    /**
     * `HttpXsrfTokenExtractor` which retrieves the token from a cookie.
     */
    class HttpXsrfCookieExtractor {
        constructor(doc, platform, cookieName) {
            this.doc = doc;
            this.platform = platform;
            this.cookieName = cookieName;
            this.lastCookieString = '';
            this.lastToken = null;
            /**
             * @internal for testing
             */
            this.parseCount = 0;
        }
        getToken() {
            if (this.platform === 'server') {
                return null;
            }
            const cookieString = this.doc.cookie || '';
            if (cookieString !== this.lastCookieString) {
                this.parseCount++;
                this.lastToken = common.ɵparseCookieValue(cookieString, this.cookieName);
                this.lastCookieString = cookieString;
            }
            return this.lastToken;
        }
    }
    HttpXsrfCookieExtractor.decorators = [
        { type: core.Injectable }
    ];
    HttpXsrfCookieExtractor.ctorParameters = () => [
        { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
        { type: String, decorators: [{ type: core.Inject, args: [core.PLATFORM_ID,] }] },
        { type: String, decorators: [{ type: core.Inject, args: [XSRF_COOKIE_NAME,] }] }
    ];
    /**
     * `HttpInterceptor` which adds an XSRF token to eligible outgoing requests.
     */
    class HttpXsrfInterceptor {
        constructor(tokenService, headerName) {
            this.tokenService = tokenService;
            this.headerName = headerName;
        }
        intercept(req, next) {
            const lcUrl = req.url.toLowerCase();
            // Skip both non-mutating requests and absolute URLs.
            // Non-mutating requests don't require a token, and absolute URLs require special handling
            // anyway as the cookie set
            // on our origin is not the same as the token expected by another origin.
            if (req.method === 'GET' || req.method === 'HEAD' || lcUrl.startsWith('http://') ||
                lcUrl.startsWith('https://')) {
                return next.handle(req);
            }
            const token = this.tokenService.getToken();
            // Be careful not to overwrite an existing header of the same name.
            if (token !== null && !req.headers.has(this.headerName)) {
                req = req.clone({ headers: req.headers.set(this.headerName, token) });
            }
            return next.handle(req);
        }
    }
    HttpXsrfInterceptor.decorators = [
        { type: core.Injectable }
    ];
    HttpXsrfInterceptor.ctorParameters = () => [
        { type: HttpXsrfTokenExtractor },
        { type: String, decorators: [{ type: core.Inject, args: [XSRF_HEADER_NAME,] }] }
    ];

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * An injectable `HttpHandler` that applies multiple interceptors
     * to a request before passing it to the given `HttpBackend`.
     *
     * The interceptors are loaded lazily from the injector, to allow
     * interceptors to themselves inject classes depending indirectly
     * on `HttpInterceptingHandler` itself.
     * @see `HttpInterceptor`
     */
    class HttpInterceptingHandler {
        constructor(backend, injector) {
            this.backend = backend;
            this.injector = injector;
            this.chain = null;
        }
        handle(req) {
            if (this.chain === null) {
                const interceptors = this.injector.get(HTTP_INTERCEPTORS, []);
                this.chain = interceptors.reduceRight((next, interceptor) => new HttpInterceptorHandler(next, interceptor), this.backend);
            }
            return this.chain.handle(req);
        }
    }
    HttpInterceptingHandler.decorators = [
        { type: core.Injectable }
    ];
    HttpInterceptingHandler.ctorParameters = () => [
        { type: HttpBackend },
        { type: core.Injector }
    ];
    /**
     * Configures XSRF protection support for outgoing requests.
     *
     * For a server that supports a cookie-based XSRF protection system,
     * use directly to configure XSRF protection with the correct
     * cookie and header names.
     *
     * If no names are supplied, the default cookie name is `XSRF-TOKEN`
     * and the default header name is `X-XSRF-TOKEN`.
     *
     * @publicApi
     */
    class HttpClientXsrfModule {
        /**
         * Disable the default XSRF protection.
         */
        static disable() {
            return {
                ngModule: HttpClientXsrfModule,
                providers: [
                    { provide: HttpXsrfInterceptor, useClass: NoopInterceptor },
                ],
            };
        }
        /**
         * Configure XSRF protection.
         * @param options An object that can specify either or both
         * cookie name or header name.
         * - Cookie name default is `XSRF-TOKEN`.
         * - Header name default is `X-XSRF-TOKEN`.
         *
         */
        static withOptions(options = {}) {
            return {
                ngModule: HttpClientXsrfModule,
                providers: [
                    options.cookieName ? { provide: XSRF_COOKIE_NAME, useValue: options.cookieName } : [],
                    options.headerName ? { provide: XSRF_HEADER_NAME, useValue: options.headerName } : [],
                ],
            };
        }
    }
    HttpClientXsrfModule.decorators = [
        { type: core.NgModule, args: [{
                    providers: [
                        HttpXsrfInterceptor,
                        { provide: HTTP_INTERCEPTORS, useExisting: HttpXsrfInterceptor, multi: true },
                        { provide: HttpXsrfTokenExtractor, useClass: HttpXsrfCookieExtractor },
                        { provide: XSRF_COOKIE_NAME, useValue: 'XSRF-TOKEN' },
                        { provide: XSRF_HEADER_NAME, useValue: 'X-XSRF-TOKEN' },
                    ],
                },] }
    ];
    /**
     * Configures the [dependency injector](guide/glossary#injector) for `HttpClient`
     * with supporting services for XSRF. Automatically imported by `HttpClientModule`.
     *
     * You can add interceptors to the chain behind `HttpClient` by binding them to the
     * multiprovider for built-in [DI token](guide/glossary#di-token) `HTTP_INTERCEPTORS`.
     *
     * @publicApi
     */
    class HttpClientModule {
    }
    HttpClientModule.decorators = [
        { type: core.NgModule, args: [{
                    /**
                     * Optional configuration for XSRF protection.
                     */
                    imports: [
                        HttpClientXsrfModule.withOptions({
                            cookieName: 'XSRF-TOKEN',
                            headerName: 'X-XSRF-TOKEN',
                        }),
                    ],
                    /**
                     * Configures the [dependency injector](guide/glossary#injector) where it is imported
                     * with supporting services for HTTP communications.
                     */
                    providers: [
                        HttpClient,
                        { provide: HttpHandler, useClass: HttpInterceptingHandler },
                        HttpXhrBackend,
                        { provide: HttpBackend, useExisting: HttpXhrBackend },
                        BrowserXhr,
                        { provide: XhrFactory, useExisting: BrowserXhr },
                    ],
                },] }
    ];

    var MatFileUploadQueue = /** @class */ (function () {
        function MatFileUploadQueue() {
            this.files = [];
            this.httpRequestHeaders = new HttpHeaders();
            this.httpRequestParams = new HttpParams();
            this.fileAlias = "file";
        }
        Object.defineProperty(MatFileUploadQueue.prototype, "fileUploadRemoveEvents", {
            get: function () {
                return rxjs.merge.apply(void 0, this.fileUploads.map(function (fileUpload) { return fileUpload.removeEvent; }));
            },
            enumerable: false,
            configurable: true
        });
        MatFileUploadQueue.prototype.ngAfterViewInit = function () {
            var _this = this;
            this._changeSubscription = this.fileUploads.changes.pipe(startWith(null)).subscribe(function () {
                if (_this._fileRemoveSubscription) {
                    _this._fileRemoveSubscription.unsubscribe();
                }
                _this._listenTofileRemoved();
            });
        };
        MatFileUploadQueue.prototype._listenTofileRemoved = function () {
            var _this = this;
            this._fileRemoveSubscription = this.fileUploadRemoveEvents.subscribe(function (event) {
                _this.files.splice(event.id, 1);
            });
        };
        MatFileUploadQueue.prototype.add = function (file) {
            this.files.push(file);
        };
        MatFileUploadQueue.prototype.uploadAll = function () {
            this.fileUploads.forEach(function (fileUpload) { fileUpload.upload(); });
        };
        MatFileUploadQueue.prototype.removeAll = function () {
            this.files.splice(0, this.files.length);
        };
        MatFileUploadQueue.prototype.ngOnDestroy = function () {
            if (this.files) {
                this.removeAll();
            }
        };
        var _a;
        __decorate([
            core.ContentChildren(core.forwardRef(function () { return MatFileUpload; })),
            __metadata("design:type", typeof (_a = typeof core.QueryList !== "undefined" && core.QueryList) === "function" ? _a : Object)
        ], MatFileUploadQueue.prototype, "fileUploads", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", String)
        ], MatFileUploadQueue.prototype, "httpUrl", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], MatFileUploadQueue.prototype, "httpRequestHeaders", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], MatFileUploadQueue.prototype, "httpRequestParams", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", String)
        ], MatFileUploadQueue.prototype, "fileAlias", void 0);
        MatFileUploadQueue = __decorate([
            core.Component({
                selector: 'mat-file-upload-queue',
                template: "<ng-content></ng-content>\n<br>\n<button mat-raised-button color=\"primary\" *ngIf=\"files.length > 0\" (click)=\"uploadAll()\">Upload All</button>\n<button mat-raised-button color=\"primary\" *ngIf=\"files.length > 0\" (click)=\"removeAll()\">Remove All</button>\n",
                exportAs: 'matFileUploadQueue',
            })
        ], MatFileUploadQueue);
        return MatFileUploadQueue;
    }());

    var MatFileUpload = /** @class */ (function () {
        function MatFileUpload(HttpClient, matFileUploadQueue) {
            this.HttpClient = HttpClient;
            this.matFileUploadQueue = matFileUploadQueue;
            this.isUploading = false;
            this.httpUrl = 'http://localhost:8080';
            this.httpRequestHeaders = new HttpHeaders();
            this.httpRequestParams = new HttpParams();
            this.fileAlias = "file";
            this.removeEvent = new core.EventEmitter();
            this.onUpload = new core.EventEmitter();
            this.progressPercentage = 0;
            this.loaded = 0;
            this.total = 0;
            if (matFileUploadQueue) {
                this.httpUrl = matFileUploadQueue.httpUrl || this.httpUrl;
                this.httpRequestHeaders = matFileUploadQueue.httpRequestHeaders || this.httpRequestHeaders;
                this.httpRequestParams = matFileUploadQueue.httpRequestParams || this.httpRequestParams;
                this.fileAlias = matFileUploadQueue.fileAlias || this.fileAlias;
            }
        }
        Object.defineProperty(MatFileUpload.prototype, "file", {
            get: function () {
                return this._file;
            },
            set: function (file) {
                this._file = file;
                this.total = this._file.size;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MatFileUpload.prototype, "id", {
            get: function () {
                return this._id;
            },
            set: function (id) {
                this._id = id;
            },
            enumerable: false,
            configurable: true
        });
        MatFileUpload.prototype.upload = function () {
            var _this = this;
            this.isUploading = true;
            var formData = new FormData();
            formData.set(this.fileAlias, this._file, this._file.name);
            this.fileUploadSubscription = this.HttpClient.post(this.httpUrl, formData, {
                headers: this.httpRequestHeaders,
                observe: "events",
                params: this.httpRequestParams,
                reportProgress: true,
                responseType: "json"
            }).subscribe(function (event) {
                if (event.type === HttpEventType.UploadProgress) {
                    _this.progressPercentage = Math.floor(event.loaded * 100 / event.total);
                    _this.loaded = event.loaded;
                    _this.total = event.total;
                }
                _this.onUpload.emit({ file: _this._file, event: event });
            }, function (error) {
                if (_this.fileUploadSubscription) {
                    _this.fileUploadSubscription.unsubscribe();
                }
                _this.isUploading = false;
                _this.onUpload.emit({ file: _this._file, event: event });
            });
        };
        MatFileUpload.prototype.remove = function () {
            if (this.fileUploadSubscription) {
                this.fileUploadSubscription.unsubscribe();
            }
            this.removeEvent.emit(this);
        };
        MatFileUpload.prototype.ngOnDestroy = function () {
            console.log('file ' + this._file.name + ' destroyed...');
        };
        var _c, _d;
        __decorate([
            core.Input(),
            __metadata("design:type", String)
        ], MatFileUpload.prototype, "httpUrl", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], MatFileUpload.prototype, "httpRequestHeaders", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], MatFileUpload.prototype, "httpRequestParams", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", String)
        ], MatFileUpload.prototype, "fileAlias", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object),
            __metadata("design:paramtypes", [Object])
        ], MatFileUpload.prototype, "file", null);
        __decorate([
            core.Input(),
            __metadata("design:type", Number),
            __metadata("design:paramtypes", [Number])
        ], MatFileUpload.prototype, "id", null);
        __decorate([
            core.Output(),
            __metadata("design:type", Object)
        ], MatFileUpload.prototype, "removeEvent", void 0);
        __decorate([
            core.Output(),
            __metadata("design:type", Object)
        ], MatFileUpload.prototype, "onUpload", void 0);
        MatFileUpload = __decorate([
            core.Component({
                selector: 'mat-file-upload',
                template: "<mat-card>\n    <span class=\"file-info\">{{file.name}}({{file.size | bytes}})</span>\n    <section class=\"example-section\">\n        <mat-progress-bar class=\"example-margin\" [value]=\"progressPercentage\"></mat-progress-bar>\n        <a [ngClass]=\"{'disabled' : isUploading}\"><mat-icon class=\"action\" (click)=\"upload()\">file_upload</mat-icon></a>\n        <mat-icon class=\"action\" (click)=\"remove()\">cancel</mat-icon>\n    </section>\n    <span class=\"file-info\">{{progressPercentage}}%</span><span> {{loaded | bytes}} of {{total | bytes}}</span>\n</mat-card>",
                exportAs: 'matFileUpload',
                host: {
                    'class': 'mat-file-upload',
                },
                styles: [".dropzone {\n  background-color: brown;\n  width: 100px;\n  height: 100px; }\n\n.example-section {\n  display: flex;\n  align-content: center;\n  align-items: center;\n  height: 10px; }\n\n.file-info {\n  font-size: .85rem; }\n\n#drop_zone {\n  border: 5px solid blue;\n  width: 200px;\n  height: 100px; }\n\n.action {\n  cursor: pointer;\n  outline: none; }\n\na.disabled {\n  pointer-events: none; }\n\n.upload-drop-zone {\n  height: 200px;\n  border-width: 2px;\n  margin-bottom: 20px; }\n\n/* skin.css Style*/\n.upload-drop-zone {\n  color: #ccc;\n  border-style: dashed;\n  border-color: #ccc;\n  line-height: 200px;\n  text-align: center; }\n\n.upload-drop-zone.drop {\n  color: #222;\n  border-color: #222; }\n"],
            }),
            __param(1, core.Inject(core.forwardRef(function () { return MatFileUploadQueue; }))),
            __metadata("design:paramtypes", [typeof (_c = typeof HttpClient !== "undefined" && HttpClient) === "function" ? _c : Object, typeof (_d = typeof MatFileUploadQueue !== "undefined" && MatFileUploadQueue) === "function" ? _d : Object])
        ], MatFileUpload);
        return MatFileUpload;
    }());

    var BytesPipe = /** @class */ (function () {
        function BytesPipe() {
        }
        BytesPipe.prototype.transform = function (bytes) {
            if (isNaN(parseFloat('' + bytes)) || !isFinite(bytes))
                return '-';
            if (bytes <= 0)
                return '0';
            var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes) / Math.log(1024));
            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(1) + ' ' + units[number];
        };
        BytesPipe = __decorate([
            core.Pipe({ name: 'bytes' })
        ], BytesPipe);
        return BytesPipe;
    }());

    /**
     * A material design file upload queue component.
     */
    var FileUploadInputFor = /** @class */ (function () {
        function FileUploadInputFor(element) {
            this.element = element;
            this._queue = null;
            this.onFileSelected = new core.EventEmitter();
            this._element = this.element.nativeElement;
        }
        Object.defineProperty(FileUploadInputFor.prototype, "fileUploadQueue", {
            set: function (value) {
                if (value) {
                    this._queue = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        FileUploadInputFor.prototype.onChange = function () {
            var files = this.element.nativeElement.files;
            this.onFileSelected.emit(files);
            for (var i = 0; i < files.length; i++) {
                this._queue.add(files[i]);
            }
            this.element.nativeElement.value = '';
        };
        FileUploadInputFor.prototype.onDrop = function (event) {
            var files = event.dataTransfer.files;
            this.onFileSelected.emit(files);
            for (var i = 0; i < files.length; i++) {
                this._queue.add(files[i]);
            }
            event.preventDefault();
            event.stopPropagation();
            this.element.nativeElement.value = '';
        };
        FileUploadInputFor.prototype.onDropOver = function (event) {
            event.preventDefault();
        };
        var _a, _b;
        __decorate([
            core.Output(),
            __metadata("design:type", typeof (_a = typeof core.EventEmitter !== "undefined" && core.EventEmitter) === "function" ? _a : Object)
        ], FileUploadInputFor.prototype, "onFileSelected", void 0);
        __decorate([
            core.Input('fileUploadInputFor'),
            __metadata("design:type", Object),
            __metadata("design:paramtypes", [Object])
        ], FileUploadInputFor.prototype, "fileUploadQueue", null);
        __decorate([
            core.HostListener('change'),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", Object)
        ], FileUploadInputFor.prototype, "onChange", null);
        __decorate([
            core.HostListener('drop', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Object]),
            __metadata("design:returntype", Object)
        ], FileUploadInputFor.prototype, "onDrop", null);
        __decorate([
            core.HostListener('dragover', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Object]),
            __metadata("design:returntype", Object)
        ], FileUploadInputFor.prototype, "onDropOver", null);
        FileUploadInputFor = __decorate([
            core.Directive({
                selector: 'input[fileUploadInputFor], div[fileUploadInputFor]',
            }),
            __metadata("design:paramtypes", [typeof (_b = typeof core.ElementRef !== "undefined" && core.ElementRef) === "function" ? _b : Object])
        ], FileUploadInputFor);
        return FileUploadInputFor;
    }());

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    const ENTER = 13;
    const SPACE = 32;

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Checks whether a modifier key is pressed.
     * @param event Event to be checked.
     */
    function hasModifierKey(event, ...modifiers) {
        if (modifiers.length) {
            return modifiers.some(modifier => event[modifier]);
        }
        return event.altKey || event.shiftKey || event.ctrlKey || event.metaKey;
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Coerces a data-bound value (typically a string) to a boolean. */
    function coerceBooleanProperty(value) {
        return value != null && `${value}` !== 'false';
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    function coerceNumberProperty(value, fallbackValue = 0) {
        return _isNumberValue(value) ? Number(value) : fallbackValue;
    }
    /**
     * Whether the provided value is considered a number.
     * @docs-private
     */
    function _isNumberValue(value) {
        // parseFloat(value) handles most of the cases we're interested in (it treats null, empty string,
        // and other non-number values as NaN, where Number just uses 0) but it considers the string
        // '123hello' to be a valid number. Therefore we also check if Number(value) is NaN.
        return !isNaN(parseFloat(value)) && !isNaN(Number(value));
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Coerces an ElementRef or an Element into an element.
     * Useful for APIs that can accept either a ref or the native element itself.
     */
    function coerceElement(elementOrRef) {
        return elementOrRef instanceof core.ElementRef ? elementOrRef.nativeElement : elementOrRef;
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // Whether the current platform supports the V8 Break Iterator. The V8 check
    // is necessary to detect all Blink based browsers.
    let hasV8BreakIterator;
    // We need a try/catch around the reference to `Intl`, because accessing it in some cases can
    // cause IE to throw. These cases are tied to particular versions of Windows and can happen if
    // the consumer is providing a polyfilled `Map`. See:
    // https://github.com/Microsoft/ChakraCore/issues/3189
    // https://github.com/angular/components/issues/15687
    try {
        hasV8BreakIterator = (typeof Intl !== 'undefined' && Intl.v8BreakIterator);
    }
    catch (_a) {
        hasV8BreakIterator = false;
    }
    /**
     * Service to detect the current platform by comparing the userAgent strings and
     * checking browser-specific global properties.
     */
    let Platform = /** @class */ (() => {
        class Platform {
            constructor(_platformId) {
                this._platformId = _platformId;
                // We want to use the Angular platform check because if the Document is shimmed
                // without the navigator, the following checks will fail. This is preferred because
                // sometimes the Document may be shimmed without the user's knowledge or intention
                /** Whether the Angular application is being rendered in the browser. */
                this.isBrowser = this._platformId ?
                    common.isPlatformBrowser(this._platformId) : typeof document === 'object' && !!document;
                /** Whether the current browser is Microsoft Edge. */
                this.EDGE = this.isBrowser && /(edge)/i.test(navigator.userAgent);
                /** Whether the current rendering engine is Microsoft Trident. */
                this.TRIDENT = this.isBrowser && /(msie|trident)/i.test(navigator.userAgent);
                // EdgeHTML and Trident mock Blink specific things and need to be excluded from this check.
                /** Whether the current rendering engine is Blink. */
                this.BLINK = this.isBrowser && (!!(window.chrome || hasV8BreakIterator) &&
                    typeof CSS !== 'undefined' && !this.EDGE && !this.TRIDENT);
                // Webkit is part of the userAgent in EdgeHTML, Blink and Trident. Therefore we need to
                // ensure that Webkit runs standalone and is not used as another engine's base.
                /** Whether the current rendering engine is WebKit. */
                this.WEBKIT = this.isBrowser &&
                    /AppleWebKit/i.test(navigator.userAgent) && !this.BLINK && !this.EDGE && !this.TRIDENT;
                /** Whether the current platform is Apple iOS. */
                this.IOS = this.isBrowser && /iPad|iPhone|iPod/.test(navigator.userAgent) &&
                    !('MSStream' in window);
                // It's difficult to detect the plain Gecko engine, because most of the browsers identify
                // them self as Gecko-like browsers and modify the userAgent's according to that.
                // Since we only cover one explicit Firefox case, we can simply check for Firefox
                // instead of having an unstable check for Gecko.
                /** Whether the current browser is Firefox. */
                this.FIREFOX = this.isBrowser && /(firefox|minefield)/i.test(navigator.userAgent);
                /** Whether the current platform is Android. */
                // Trident on mobile adds the android platform to the userAgent to trick detections.
                this.ANDROID = this.isBrowser && /android/i.test(navigator.userAgent) && !this.TRIDENT;
                // Safari browsers will include the Safari keyword in their userAgent. Some browsers may fake
                // this and just place the Safari keyword in the userAgent. To be more safe about Safari every
                // Safari browser should also use Webkit as its layout engine.
                /** Whether the current browser is Safari. */
                this.SAFARI = this.isBrowser && /safari/i.test(navigator.userAgent) && this.WEBKIT;
            }
        }
        Platform.ɵprov = core.ɵɵdefineInjectable({ factory: function Platform_Factory() { return new Platform(core.ɵɵinject(core.PLATFORM_ID)); }, token: Platform, providedIn: "root" });
        Platform.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        Platform.ctorParameters = () => [
            { type: Object, decorators: [{ type: core.Inject, args: [core.PLATFORM_ID,] }] }
        ];
        return Platform;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let PlatformModule = /** @class */ (() => {
        class PlatformModule {
        }
        PlatformModule.decorators = [
            { type: core.NgModule, args: [{},] }
        ];
        return PlatformModule;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Cached result of whether the user's browser supports passive event listeners. */
    let supportsPassiveEvents;
    /**
     * Checks whether the user's browser supports passive event listeners.
     * See: https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md
     */
    function supportsPassiveEventListeners() {
        if (supportsPassiveEvents == null && typeof window !== 'undefined') {
            try {
                window.addEventListener('test', null, Object.defineProperty({}, 'passive', {
                    get: () => supportsPassiveEvents = true
                }));
            }
            finally {
                supportsPassiveEvents = supportsPassiveEvents || false;
            }
        }
        return supportsPassiveEvents;
    }
    /**
     * Normalizes an `AddEventListener` object to something that can be passed
     * to `addEventListener` on any browser, no matter whether it supports the
     * `options` parameter.
     * @param options Object to be normalized.
     */
    function normalizePassiveListenerOptions(options) {
        return supportsPassiveEventListeners() ? options : !!options.capture;
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let shadowDomIsSupported;
    /** Checks whether the user's browser support Shadow DOM. */
    function _supportsShadowDom() {
        if (shadowDomIsSupported == null) {
            const head = typeof document !== 'undefined' ? document.head : null;
            shadowDomIsSupported = !!(head && (head.createShadowRoot || head.attachShadow));
        }
        return shadowDomIsSupported;
    }
    /** Gets the shadow root of an element, if supported and the element is inside the Shadow DOM. */
    function _getShadowRoot(element) {
        if (_supportsShadowDom()) {
            const rootNode = element.getRootNode ? element.getRootNode() : null;
            // Note that this should be caught by `_supportsShadowDom`, but some
            // teams have been able to hit this code path on unsupported browsers.
            if (typeof ShadowRoot !== 'undefined' && ShadowRoot && rootNode instanceof ShadowRoot) {
                return rootNode;
            }
        }
        return null;
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Factory that creates a new MutationObserver and allows us to stub it out in unit tests.
     * @docs-private
     */
    let MutationObserverFactory = /** @class */ (() => {
        class MutationObserverFactory {
            create(callback) {
                return typeof MutationObserver === 'undefined' ? null : new MutationObserver(callback);
            }
        }
        MutationObserverFactory.ɵprov = core.ɵɵdefineInjectable({ factory: function MutationObserverFactory_Factory() { return new MutationObserverFactory(); }, token: MutationObserverFactory, providedIn: "root" });
        MutationObserverFactory.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        return MutationObserverFactory;
    })();
    /** An injectable service that allows watching elements for changes to their content. */
    let ContentObserver = /** @class */ (() => {
        class ContentObserver {
            constructor(_mutationObserverFactory) {
                this._mutationObserverFactory = _mutationObserverFactory;
                /** Keeps track of the existing MutationObservers so they can be reused. */
                this._observedElements = new Map();
            }
            ngOnDestroy() {
                this._observedElements.forEach((_, element) => this._cleanupObserver(element));
            }
            observe(elementOrRef) {
                const element = coerceElement(elementOrRef);
                return new rxjs.Observable((observer) => {
                    const stream = this._observeElement(element);
                    const subscription = stream.subscribe(observer);
                    return () => {
                        subscription.unsubscribe();
                        this._unobserveElement(element);
                    };
                });
            }
            /**
             * Observes the given element by using the existing MutationObserver if available, or creating a
             * new one if not.
             */
            _observeElement(element) {
                if (!this._observedElements.has(element)) {
                    const stream = new rxjs.Subject();
                    const observer = this._mutationObserverFactory.create(mutations => stream.next(mutations));
                    if (observer) {
                        observer.observe(element, {
                            characterData: true,
                            childList: true,
                            subtree: true
                        });
                    }
                    this._observedElements.set(element, { observer, stream, count: 1 });
                }
                else {
                    this._observedElements.get(element).count++;
                }
                return this._observedElements.get(element).stream;
            }
            /**
             * Un-observes the given element and cleans up the underlying MutationObserver if nobody else is
             * observing this element.
             */
            _unobserveElement(element) {
                if (this._observedElements.has(element)) {
                    this._observedElements.get(element).count--;
                    if (!this._observedElements.get(element).count) {
                        this._cleanupObserver(element);
                    }
                }
            }
            /** Clean up the underlying MutationObserver for the specified element. */
            _cleanupObserver(element) {
                if (this._observedElements.has(element)) {
                    const { observer, stream } = this._observedElements.get(element);
                    if (observer) {
                        observer.disconnect();
                    }
                    stream.complete();
                    this._observedElements.delete(element);
                }
            }
        }
        ContentObserver.ɵprov = core.ɵɵdefineInjectable({ factory: function ContentObserver_Factory() { return new ContentObserver(core.ɵɵinject(MutationObserverFactory)); }, token: ContentObserver, providedIn: "root" });
        ContentObserver.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        ContentObserver.ctorParameters = () => [
            { type: MutationObserverFactory }
        ];
        return ContentObserver;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** IDs are delimited by an empty space, as per the spec. */
    const ID_DELIMITER = ' ';
    /**
     * Adds the given ID to the specified ARIA attribute on an element.
     * Used for attributes such as aria-labelledby, aria-owns, etc.
     */
    function addAriaReferencedId(el, attr, id) {
        const ids = getAriaReferenceIds(el, attr);
        if (ids.some(existingId => existingId.trim() == id.trim())) {
            return;
        }
        ids.push(id.trim());
        el.setAttribute(attr, ids.join(ID_DELIMITER));
    }
    /**
     * Removes the given ID from the specified ARIA attribute on an element.
     * Used for attributes such as aria-labelledby, aria-owns, etc.
     */
    function removeAriaReferencedId(el, attr, id) {
        const ids = getAriaReferenceIds(el, attr);
        const filteredIds = ids.filter(val => val != id.trim());
        if (filteredIds.length) {
            el.setAttribute(attr, filteredIds.join(ID_DELIMITER));
        }
        else {
            el.removeAttribute(attr);
        }
    }
    /**
     * Gets the list of IDs referenced by the given ARIA attribute on an element.
     * Used for attributes such as aria-labelledby, aria-owns, etc.
     */
    function getAriaReferenceIds(el, attr) {
        // Get string array of all individual ids (whitespace delimited) in the attribute value
        return (el.getAttribute(attr) || '').match(/\S+/g) || [];
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** ID used for the body container where all messages are appended. */
    const MESSAGES_CONTAINER_ID = 'cdk-describedby-message-container';
    /** ID prefix used for each created message element. */
    const CDK_DESCRIBEDBY_ID_PREFIX = 'cdk-describedby-message';
    /** Attribute given to each host element that is described by a message element. */
    const CDK_DESCRIBEDBY_HOST_ATTRIBUTE = 'cdk-describedby-host';
    /** Global incremental identifier for each registered message element. */
    let nextId = 0;
    /** Global map of all registered message elements that have been placed into the document. */
    const messageRegistry = new Map();
    /** Container for all registered messages. */
    let messagesContainer = null;
    /**
     * Utility that creates visually hidden elements with a message content. Useful for elements that
     * want to use aria-describedby to further describe themselves without adding additional visual
     * content.
     */
    let AriaDescriber = /** @class */ (() => {
        class AriaDescriber {
            constructor(_document) {
                this._document = _document;
            }
            /**
             * Adds to the host element an aria-describedby reference to a hidden element that contains
             * the message. If the same message has already been registered, then it will reuse the created
             * message element.
             */
            describe(hostElement, message) {
                if (!this._canBeDescribed(hostElement, message)) {
                    return;
                }
                if (typeof message !== 'string') {
                    // We need to ensure that the element has an ID.
                    this._setMessageId(message);
                    messageRegistry.set(message, { messageElement: message, referenceCount: 0 });
                }
                else if (!messageRegistry.has(message)) {
                    this._createMessageElement(message);
                }
                if (!this._isElementDescribedByMessage(hostElement, message)) {
                    this._addMessageReference(hostElement, message);
                }
            }
            /** Removes the host element's aria-describedby reference to the message element. */
            removeDescription(hostElement, message) {
                if (!this._isElementNode(hostElement)) {
                    return;
                }
                if (this._isElementDescribedByMessage(hostElement, message)) {
                    this._removeMessageReference(hostElement, message);
                }
                // If the message is a string, it means that it's one that we created for the
                // consumer so we can remove it safely, otherwise we should leave it in place.
                if (typeof message === 'string') {
                    const registeredMessage = messageRegistry.get(message);
                    if (registeredMessage && registeredMessage.referenceCount === 0) {
                        this._deleteMessageElement(message);
                    }
                }
                if (messagesContainer && messagesContainer.childNodes.length === 0) {
                    this._deleteMessagesContainer();
                }
            }
            /** Unregisters all created message elements and removes the message container. */
            ngOnDestroy() {
                const describedElements = this._document.querySelectorAll(`[${CDK_DESCRIBEDBY_HOST_ATTRIBUTE}]`);
                for (let i = 0; i < describedElements.length; i++) {
                    this._removeCdkDescribedByReferenceIds(describedElements[i]);
                    describedElements[i].removeAttribute(CDK_DESCRIBEDBY_HOST_ATTRIBUTE);
                }
                if (messagesContainer) {
                    this._deleteMessagesContainer();
                }
                messageRegistry.clear();
            }
            /**
             * Creates a new element in the visually hidden message container element with the message
             * as its content and adds it to the message registry.
             */
            _createMessageElement(message) {
                const messageElement = this._document.createElement('div');
                this._setMessageId(messageElement);
                messageElement.textContent = message;
                this._createMessagesContainer();
                messagesContainer.appendChild(messageElement);
                messageRegistry.set(message, { messageElement, referenceCount: 0 });
            }
            /** Assigns a unique ID to an element, if it doesn't have one already. */
            _setMessageId(element) {
                if (!element.id) {
                    element.id = `${CDK_DESCRIBEDBY_ID_PREFIX}-${nextId++}`;
                }
            }
            /** Deletes the message element from the global messages container. */
            _deleteMessageElement(message) {
                const registeredMessage = messageRegistry.get(message);
                const messageElement = registeredMessage && registeredMessage.messageElement;
                if (messagesContainer && messageElement) {
                    messagesContainer.removeChild(messageElement);
                }
                messageRegistry.delete(message);
            }
            /** Creates the global container for all aria-describedby messages. */
            _createMessagesContainer() {
                if (!messagesContainer) {
                    const preExistingContainer = this._document.getElementById(MESSAGES_CONTAINER_ID);
                    // When going from the server to the client, we may end up in a situation where there's
                    // already a container on the page, but we don't have a reference to it. Clear the
                    // old container so we don't get duplicates. Doing this, instead of emptying the previous
                    // container, should be slightly faster.
                    if (preExistingContainer) {
                        preExistingContainer.parentNode.removeChild(preExistingContainer);
                    }
                    messagesContainer = this._document.createElement('div');
                    messagesContainer.id = MESSAGES_CONTAINER_ID;
                    messagesContainer.setAttribute('aria-hidden', 'true');
                    messagesContainer.style.display = 'none';
                    this._document.body.appendChild(messagesContainer);
                }
            }
            /** Deletes the global messages container. */
            _deleteMessagesContainer() {
                if (messagesContainer && messagesContainer.parentNode) {
                    messagesContainer.parentNode.removeChild(messagesContainer);
                    messagesContainer = null;
                }
            }
            /** Removes all cdk-describedby messages that are hosted through the element. */
            _removeCdkDescribedByReferenceIds(element) {
                // Remove all aria-describedby reference IDs that are prefixed by CDK_DESCRIBEDBY_ID_PREFIX
                const originalReferenceIds = getAriaReferenceIds(element, 'aria-describedby')
                    .filter(id => id.indexOf(CDK_DESCRIBEDBY_ID_PREFIX) != 0);
                element.setAttribute('aria-describedby', originalReferenceIds.join(' '));
            }
            /**
             * Adds a message reference to the element using aria-describedby and increments the registered
             * message's reference count.
             */
            _addMessageReference(element, message) {
                const registeredMessage = messageRegistry.get(message);
                // Add the aria-describedby reference and set the
                // describedby_host attribute to mark the element.
                addAriaReferencedId(element, 'aria-describedby', registeredMessage.messageElement.id);
                element.setAttribute(CDK_DESCRIBEDBY_HOST_ATTRIBUTE, '');
                registeredMessage.referenceCount++;
            }
            /**
             * Removes a message reference from the element using aria-describedby
             * and decrements the registered message's reference count.
             */
            _removeMessageReference(element, message) {
                const registeredMessage = messageRegistry.get(message);
                registeredMessage.referenceCount--;
                removeAriaReferencedId(element, 'aria-describedby', registeredMessage.messageElement.id);
                element.removeAttribute(CDK_DESCRIBEDBY_HOST_ATTRIBUTE);
            }
            /** Returns true if the element has been described by the provided message ID. */
            _isElementDescribedByMessage(element, message) {
                const referenceIds = getAriaReferenceIds(element, 'aria-describedby');
                const registeredMessage = messageRegistry.get(message);
                const messageId = registeredMessage && registeredMessage.messageElement.id;
                return !!messageId && referenceIds.indexOf(messageId) != -1;
            }
            /** Determines whether a message can be described on a particular element. */
            _canBeDescribed(element, message) {
                if (!this._isElementNode(element)) {
                    return false;
                }
                if (message && typeof message === 'object') {
                    // We'd have to make some assumptions about the description element's text, if the consumer
                    // passed in an element. Assume that if an element is passed in, the consumer has verified
                    // that it can be used as a description.
                    return true;
                }
                const trimmedMessage = message == null ? '' : `${message}`.trim();
                const ariaLabel = element.getAttribute('aria-label');
                // We shouldn't set descriptions if they're exactly the same as the `aria-label` of the
                // element, because screen readers will end up reading out the same text twice in a row.
                return trimmedMessage ? (!ariaLabel || ariaLabel.trim() !== trimmedMessage) : false;
            }
            /** Checks whether a node is an Element node. */
            _isElementNode(element) {
                return element.nodeType === this._document.ELEMENT_NODE;
            }
        }
        AriaDescriber.ɵprov = core.ɵɵdefineInjectable({ factory: function AriaDescriber_Factory() { return new AriaDescriber(core.ɵɵinject(common.DOCUMENT)); }, token: AriaDescriber, providedIn: "root" });
        AriaDescriber.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        AriaDescriber.ctorParameters = () => [
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] }
        ];
        return AriaDescriber;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // The InteractivityChecker leans heavily on the ally.js accessibility utilities.
    // Methods like `isTabbable` are only covering specific edge-cases for the browsers which are
    // supported.
    /**
     * Utility for checking the interactivity of an element, such as whether is is focusable or
     * tabbable.
     */
    let InteractivityChecker = /** @class */ (() => {
        class InteractivityChecker {
            constructor(_platform) {
                this._platform = _platform;
            }
            /**
             * Gets whether an element is disabled.
             *
             * @param element Element to be checked.
             * @returns Whether the element is disabled.
             */
            isDisabled(element) {
                // This does not capture some cases, such as a non-form control with a disabled attribute or
                // a form control inside of a disabled form, but should capture the most common cases.
                return element.hasAttribute('disabled');
            }
            /**
             * Gets whether an element is visible for the purposes of interactivity.
             *
             * This will capture states like `display: none` and `visibility: hidden`, but not things like
             * being clipped by an `overflow: hidden` parent or being outside the viewport.
             *
             * @returns Whether the element is visible.
             */
            isVisible(element) {
                return hasGeometry(element) && getComputedStyle(element).visibility === 'visible';
            }
            /**
             * Gets whether an element can be reached via Tab key.
             * Assumes that the element has already been checked with isFocusable.
             *
             * @param element Element to be checked.
             * @returns Whether the element is tabbable.
             */
            isTabbable(element) {
                // Nothing is tabbable on the server 😎
                if (!this._platform.isBrowser) {
                    return false;
                }
                const frameElement = getFrameElement(getWindow(element));
                if (frameElement) {
                    const frameType = frameElement && frameElement.nodeName.toLowerCase();
                    // Frame elements inherit their tabindex onto all child elements.
                    if (getTabIndexValue(frameElement) === -1) {
                        return false;
                    }
                    // Webkit and Blink consider anything inside of an <object> element as non-tabbable.
                    if ((this._platform.BLINK || this._platform.WEBKIT) && frameType === 'object') {
                        return false;
                    }
                    // Webkit and Blink disable tabbing to an element inside of an invisible frame.
                    if ((this._platform.BLINK || this._platform.WEBKIT) && !this.isVisible(frameElement)) {
                        return false;
                    }
                }
                let nodeName = element.nodeName.toLowerCase();
                let tabIndexValue = getTabIndexValue(element);
                if (element.hasAttribute('contenteditable')) {
                    return tabIndexValue !== -1;
                }
                if (nodeName === 'iframe') {
                    // The frames may be tabbable depending on content, but it's not possibly to reliably
                    // investigate the content of the frames.
                    return false;
                }
                if (nodeName === 'audio') {
                    if (!element.hasAttribute('controls')) {
                        // By default an <audio> element without the controls enabled is not tabbable.
                        return false;
                    }
                    else if (this._platform.BLINK) {
                        // In Blink <audio controls> elements are always tabbable.
                        return true;
                    }
                }
                if (nodeName === 'video') {
                    if (!element.hasAttribute('controls') && this._platform.TRIDENT) {
                        // In Trident a <video> element without the controls enabled is not tabbable.
                        return false;
                    }
                    else if (this._platform.BLINK || this._platform.FIREFOX) {
                        // In Chrome and Firefox <video controls> elements are always tabbable.
                        return true;
                    }
                }
                if (nodeName === 'object' && (this._platform.BLINK || this._platform.WEBKIT)) {
                    // In all Blink and WebKit based browsers <object> elements are never tabbable.
                    return false;
                }
                // In iOS the browser only considers some specific elements as tabbable.
                if (this._platform.WEBKIT && this._platform.IOS && !isPotentiallyTabbableIOS(element)) {
                    return false;
                }
                return element.tabIndex >= 0;
            }
            /**
             * Gets whether an element can be focused by the user.
             *
             * @param element Element to be checked.
             * @returns Whether the element is focusable.
             */
            isFocusable(element) {
                // Perform checks in order of left to most expensive.
                // Again, naive approach that does not capture many edge cases and browser quirks.
                return isPotentiallyFocusable(element) && !this.isDisabled(element) && this.isVisible(element);
            }
        }
        InteractivityChecker.ɵprov = core.ɵɵdefineInjectable({ factory: function InteractivityChecker_Factory() { return new InteractivityChecker(core.ɵɵinject(Platform)); }, token: InteractivityChecker, providedIn: "root" });
        InteractivityChecker.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        InteractivityChecker.ctorParameters = () => [
            { type: Platform }
        ];
        return InteractivityChecker;
    })();
    /**
     * Returns the frame element from a window object. Since browsers like MS Edge throw errors if
     * the frameElement property is being accessed from a different host address, this property
     * should be accessed carefully.
     */
    function getFrameElement(window) {
        try {
            return window.frameElement;
        }
        catch (_a) {
            return null;
        }
    }
    /** Checks whether the specified element has any geometry / rectangles. */
    function hasGeometry(element) {
        // Use logic from jQuery to check for an invisible element.
        // See https://github.com/jquery/jquery/blob/master/src/css/hiddenVisibleSelectors.js#L12
        return !!(element.offsetWidth || element.offsetHeight ||
            (typeof element.getClientRects === 'function' && element.getClientRects().length));
    }
    /** Gets whether an element's  */
    function isNativeFormElement(element) {
        let nodeName = element.nodeName.toLowerCase();
        return nodeName === 'input' ||
            nodeName === 'select' ||
            nodeName === 'button' ||
            nodeName === 'textarea';
    }
    /** Gets whether an element is an `<input type="hidden">`. */
    function isHiddenInput(element) {
        return isInputElement(element) && element.type == 'hidden';
    }
    /** Gets whether an element is an anchor that has an href attribute. */
    function isAnchorWithHref(element) {
        return isAnchorElement(element) && element.hasAttribute('href');
    }
    /** Gets whether an element is an input element. */
    function isInputElement(element) {
        return element.nodeName.toLowerCase() == 'input';
    }
    /** Gets whether an element is an anchor element. */
    function isAnchorElement(element) {
        return element.nodeName.toLowerCase() == 'a';
    }
    /** Gets whether an element has a valid tabindex. */
    function hasValidTabIndex(element) {
        if (!element.hasAttribute('tabindex') || element.tabIndex === undefined) {
            return false;
        }
        let tabIndex = element.getAttribute('tabindex');
        // IE11 parses tabindex="" as the value "-32768"
        if (tabIndex == '-32768') {
            return false;
        }
        return !!(tabIndex && !isNaN(parseInt(tabIndex, 10)));
    }
    /**
     * Returns the parsed tabindex from the element attributes instead of returning the
     * evaluated tabindex from the browsers defaults.
     */
    function getTabIndexValue(element) {
        if (!hasValidTabIndex(element)) {
            return null;
        }
        // See browser issue in Gecko https://bugzilla.mozilla.org/show_bug.cgi?id=1128054
        const tabIndex = parseInt(element.getAttribute('tabindex') || '', 10);
        return isNaN(tabIndex) ? -1 : tabIndex;
    }
    /** Checks whether the specified element is potentially tabbable on iOS */
    function isPotentiallyTabbableIOS(element) {
        let nodeName = element.nodeName.toLowerCase();
        let inputType = nodeName === 'input' && element.type;
        return inputType === 'text'
            || inputType === 'password'
            || nodeName === 'select'
            || nodeName === 'textarea';
    }
    /**
     * Gets whether an element is potentially focusable without taking current visible/disabled state
     * into account.
     */
    function isPotentiallyFocusable(element) {
        // Inputs are potentially focusable *unless* they're type="hidden".
        if (isHiddenInput(element)) {
            return false;
        }
        return isNativeFormElement(element) ||
            isAnchorWithHref(element) ||
            element.hasAttribute('contenteditable') ||
            hasValidTabIndex(element);
    }
    /** Gets the parent window of a DOM node with regards of being inside of an iframe. */
    function getWindow(node) {
        // ownerDocument is null if `node` itself *is* a document.
        return node.ownerDocument && node.ownerDocument.defaultView || window;
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Class that allows for trapping focus within a DOM element.
     *
     * This class currently uses a relatively simple approach to focus trapping.
     * It assumes that the tab order is the same as DOM order, which is not necessarily true.
     * Things like `tabIndex > 0`, flex `order`, and shadow roots can cause the two to misalign.
     *
     * @deprecated Use `ConfigurableFocusTrap` instead.
     * @breaking-change for 11.0.0 Remove this class.
     */
    class FocusTrap {
        constructor(_element, _checker, _ngZone, _document, deferAnchors = false) {
            this._element = _element;
            this._checker = _checker;
            this._ngZone = _ngZone;
            this._document = _document;
            this._hasAttached = false;
            // Event listeners for the anchors. Need to be regular functions so that we can unbind them later.
            this.startAnchorListener = () => this.focusLastTabbableElement();
            this.endAnchorListener = () => this.focusFirstTabbableElement();
            this._enabled = true;
            if (!deferAnchors) {
                this.attachAnchors();
            }
        }
        /** Whether the focus trap is active. */
        get enabled() { return this._enabled; }
        set enabled(value) {
            this._enabled = value;
            if (this._startAnchor && this._endAnchor) {
                this._toggleAnchorTabIndex(value, this._startAnchor);
                this._toggleAnchorTabIndex(value, this._endAnchor);
            }
        }
        /** Destroys the focus trap by cleaning up the anchors. */
        destroy() {
            const startAnchor = this._startAnchor;
            const endAnchor = this._endAnchor;
            if (startAnchor) {
                startAnchor.removeEventListener('focus', this.startAnchorListener);
                if (startAnchor.parentNode) {
                    startAnchor.parentNode.removeChild(startAnchor);
                }
            }
            if (endAnchor) {
                endAnchor.removeEventListener('focus', this.endAnchorListener);
                if (endAnchor.parentNode) {
                    endAnchor.parentNode.removeChild(endAnchor);
                }
            }
            this._startAnchor = this._endAnchor = null;
        }
        /**
         * Inserts the anchors into the DOM. This is usually done automatically
         * in the constructor, but can be deferred for cases like directives with `*ngIf`.
         * @returns Whether the focus trap managed to attach successfuly. This may not be the case
         * if the target element isn't currently in the DOM.
         */
        attachAnchors() {
            // If we're not on the browser, there can be no focus to trap.
            if (this._hasAttached) {
                return true;
            }
            this._ngZone.runOutsideAngular(() => {
                if (!this._startAnchor) {
                    this._startAnchor = this._createAnchor();
                    this._startAnchor.addEventListener('focus', this.startAnchorListener);
                }
                if (!this._endAnchor) {
                    this._endAnchor = this._createAnchor();
                    this._endAnchor.addEventListener('focus', this.endAnchorListener);
                }
            });
            if (this._element.parentNode) {
                this._element.parentNode.insertBefore(this._startAnchor, this._element);
                this._element.parentNode.insertBefore(this._endAnchor, this._element.nextSibling);
                this._hasAttached = true;
            }
            return this._hasAttached;
        }
        /**
         * Waits for the zone to stabilize, then either focuses the first element that the
         * user specified, or the first tabbable element.
         * @returns Returns a promise that resolves with a boolean, depending
         * on whether focus was moved successfuly.
         */
        focusInitialElementWhenReady() {
            return new Promise(resolve => {
                this._executeOnStable(() => resolve(this.focusInitialElement()));
            });
        }
        /**
         * Waits for the zone to stabilize, then focuses
         * the first tabbable element within the focus trap region.
         * @returns Returns a promise that resolves with a boolean, depending
         * on whether focus was moved successfuly.
         */
        focusFirstTabbableElementWhenReady() {
            return new Promise(resolve => {
                this._executeOnStable(() => resolve(this.focusFirstTabbableElement()));
            });
        }
        /**
         * Waits for the zone to stabilize, then focuses
         * the last tabbable element within the focus trap region.
         * @returns Returns a promise that resolves with a boolean, depending
         * on whether focus was moved successfuly.
         */
        focusLastTabbableElementWhenReady() {
            return new Promise(resolve => {
                this._executeOnStable(() => resolve(this.focusLastTabbableElement()));
            });
        }
        /**
         * Get the specified boundary element of the trapped region.
         * @param bound The boundary to get (start or end of trapped region).
         * @returns The boundary element.
         */
        _getRegionBoundary(bound) {
            // Contains the deprecated version of selector, for temporary backwards comparability.
            let markers = this._element.querySelectorAll(`[cdk-focus-region-${bound}], ` +
                `[cdkFocusRegion${bound}], ` +
                `[cdk-focus-${bound}]`);
            for (let i = 0; i < markers.length; i++) {
                // @breaking-change 8.0.0
                if (markers[i].hasAttribute(`cdk-focus-${bound}`)) {
                    console.warn(`Found use of deprecated attribute 'cdk-focus-${bound}', ` +
                        `use 'cdkFocusRegion${bound}' instead. The deprecated ` +
                        `attribute will be removed in 8.0.0.`, markers[i]);
                }
                else if (markers[i].hasAttribute(`cdk-focus-region-${bound}`)) {
                    console.warn(`Found use of deprecated attribute 'cdk-focus-region-${bound}', ` +
                        `use 'cdkFocusRegion${bound}' instead. The deprecated attribute ` +
                        `will be removed in 8.0.0.`, markers[i]);
                }
            }
            if (bound == 'start') {
                return markers.length ? markers[0] : this._getFirstTabbableElement(this._element);
            }
            return markers.length ?
                markers[markers.length - 1] : this._getLastTabbableElement(this._element);
        }
        /**
         * Focuses the element that should be focused when the focus trap is initialized.
         * @returns Whether focus was moved successfuly.
         */
        focusInitialElement() {
            // Contains the deprecated version of selector, for temporary backwards comparability.
            const redirectToElement = this._element.querySelector(`[cdk-focus-initial], ` +
                `[cdkFocusInitial]`);
            if (redirectToElement) {
                // @breaking-change 8.0.0
                if (redirectToElement.hasAttribute(`cdk-focus-initial`)) {
                    console.warn(`Found use of deprecated attribute 'cdk-focus-initial', ` +
                        `use 'cdkFocusInitial' instead. The deprecated attribute ` +
                        `will be removed in 8.0.0`, redirectToElement);
                }
                // Warn the consumer if the element they've pointed to
                // isn't focusable, when not in production mode.
                if (core.isDevMode() && !this._checker.isFocusable(redirectToElement)) {
                    console.warn(`Element matching '[cdkFocusInitial]' is not focusable.`, redirectToElement);
                }
                redirectToElement.focus();
                return true;
            }
            return this.focusFirstTabbableElement();
        }
        /**
         * Focuses the first tabbable element within the focus trap region.
         * @returns Whether focus was moved successfuly.
         */
        focusFirstTabbableElement() {
            const redirectToElement = this._getRegionBoundary('start');
            if (redirectToElement) {
                redirectToElement.focus();
            }
            return !!redirectToElement;
        }
        /**
         * Focuses the last tabbable element within the focus trap region.
         * @returns Whether focus was moved successfuly.
         */
        focusLastTabbableElement() {
            const redirectToElement = this._getRegionBoundary('end');
            if (redirectToElement) {
                redirectToElement.focus();
            }
            return !!redirectToElement;
        }
        /**
         * Checks whether the focus trap has successfuly been attached.
         */
        hasAttached() {
            return this._hasAttached;
        }
        /** Get the first tabbable element from a DOM subtree (inclusive). */
        _getFirstTabbableElement(root) {
            if (this._checker.isFocusable(root) && this._checker.isTabbable(root)) {
                return root;
            }
            // Iterate in DOM order. Note that IE doesn't have `children` for SVG so we fall
            // back to `childNodes` which includes text nodes, comments etc.
            let children = root.children || root.childNodes;
            for (let i = 0; i < children.length; i++) {
                let tabbableChild = children[i].nodeType === this._document.ELEMENT_NODE ?
                    this._getFirstTabbableElement(children[i]) :
                    null;
                if (tabbableChild) {
                    return tabbableChild;
                }
            }
            return null;
        }
        /** Get the last tabbable element from a DOM subtree (inclusive). */
        _getLastTabbableElement(root) {
            if (this._checker.isFocusable(root) && this._checker.isTabbable(root)) {
                return root;
            }
            // Iterate in reverse DOM order.
            let children = root.children || root.childNodes;
            for (let i = children.length - 1; i >= 0; i--) {
                let tabbableChild = children[i].nodeType === this._document.ELEMENT_NODE ?
                    this._getLastTabbableElement(children[i]) :
                    null;
                if (tabbableChild) {
                    return tabbableChild;
                }
            }
            return null;
        }
        /** Creates an anchor element. */
        _createAnchor() {
            const anchor = this._document.createElement('div');
            this._toggleAnchorTabIndex(this._enabled, anchor);
            anchor.classList.add('cdk-visually-hidden');
            anchor.classList.add('cdk-focus-trap-anchor');
            anchor.setAttribute('aria-hidden', 'true');
            return anchor;
        }
        /**
         * Toggles the `tabindex` of an anchor, based on the enabled state of the focus trap.
         * @param isEnabled Whether the focus trap is enabled.
         * @param anchor Anchor on which to toggle the tabindex.
         */
        _toggleAnchorTabIndex(isEnabled, anchor) {
            // Remove the tabindex completely, rather than setting it to -1, because if the
            // element has a tabindex, the user might still hit it when navigating with the arrow keys.
            isEnabled ? anchor.setAttribute('tabindex', '0') : anchor.removeAttribute('tabindex');
        }
        /**
         * Toggles the`tabindex` of both anchors to either trap Tab focus or allow it to escape.
         * @param enabled: Whether the anchors should trap Tab.
         */
        toggleAnchors(enabled) {
            if (this._startAnchor && this._endAnchor) {
                this._toggleAnchorTabIndex(enabled, this._startAnchor);
                this._toggleAnchorTabIndex(enabled, this._endAnchor);
            }
        }
        /** Executes a function when the zone is stable. */
        _executeOnStable(fn) {
            if (this._ngZone.isStable) {
                fn();
            }
            else {
                this._ngZone.onStable.asObservable().pipe(take(1)).subscribe(fn);
            }
        }
    }
    /**
     * Factory that allows easy instantiation of focus traps.
     * @deprecated Use `ConfigurableFocusTrapFactory` instead.
     * @breaking-change for 11.0.0 Remove this class.
     */
    let FocusTrapFactory = /** @class */ (() => {
        class FocusTrapFactory {
            constructor(_checker, _ngZone, _document) {
                this._checker = _checker;
                this._ngZone = _ngZone;
                this._document = _document;
            }
            /**
             * Creates a focus-trapped region around the given element.
             * @param element The element around which focus will be trapped.
             * @param deferCaptureElements Defers the creation of focus-capturing elements to be done
             *     manually by the user.
             * @returns The created focus trap instance.
             */
            create(element, deferCaptureElements = false) {
                return new FocusTrap(element, this._checker, this._ngZone, this._document, deferCaptureElements);
            }
        }
        FocusTrapFactory.ɵprov = core.ɵɵdefineInjectable({ factory: function FocusTrapFactory_Factory() { return new FocusTrapFactory(core.ɵɵinject(InteractivityChecker), core.ɵɵinject(core.NgZone), core.ɵɵinject(common.DOCUMENT)); }, token: FocusTrapFactory, providedIn: "root" });
        FocusTrapFactory.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        FocusTrapFactory.ctorParameters = () => [
            { type: InteractivityChecker },
            { type: core.NgZone },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] }
        ];
        return FocusTrapFactory;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Class that allows for trapping focus within a DOM element.
     *
     * This class uses a strategy pattern that determines how it traps focus.
     * See FocusTrapInertStrategy.
     */
    class ConfigurableFocusTrap extends FocusTrap {
        constructor(_element, _checker, _ngZone, _document, _focusTrapManager, _inertStrategy, config) {
            super(_element, _checker, _ngZone, _document, config.defer);
            this._focusTrapManager = _focusTrapManager;
            this._inertStrategy = _inertStrategy;
            this._focusTrapManager.register(this);
        }
        /** Whether the FocusTrap is enabled. */
        get enabled() { return this._enabled; }
        set enabled(value) {
            this._enabled = value;
            if (this._enabled) {
                this._focusTrapManager.register(this);
            }
            else {
                this._focusTrapManager.deregister(this);
            }
        }
        /** Notifies the FocusTrapManager that this FocusTrap will be destroyed. */
        destroy() {
            this._focusTrapManager.deregister(this);
            super.destroy();
        }
        /** @docs-private Implemented as part of ManagedFocusTrap. */
        _enable() {
            this._inertStrategy.preventFocus(this);
            this.toggleAnchors(true);
        }
        /** @docs-private Implemented as part of ManagedFocusTrap. */
        _disable() {
            this._inertStrategy.allowFocus(this);
            this.toggleAnchors(false);
        }
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** IE 11 compatible closest implementation that is able to start from non-Element Nodes. */
    function closest(element, selector) {
        if (!(element instanceof Node)) {
            return null;
        }
        let curr = element;
        while (curr != null && !(curr instanceof Element)) {
            curr = curr.parentNode;
        }
        return curr && (hasNativeClosest ?
            curr.closest(selector) : polyfillClosest(curr, selector));
    }
    /** Polyfill for browsers without Element.closest. */
    function polyfillClosest(element, selector) {
        let curr = element;
        while (curr != null && !(curr instanceof Element && matches(curr, selector))) {
            curr = curr.parentNode;
        }
        return (curr || null);
    }
    const hasNativeClosest = typeof Element != 'undefined' && !!Element.prototype.closest;
    /** IE 11 compatible matches implementation. */
    function matches(element, selector) {
        return element.matches ?
            element.matches(selector) :
            element['msMatchesSelector'](selector);
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Lightweight FocusTrapInertStrategy that adds a document focus event
     * listener to redirect focus back inside the FocusTrap.
     */
    class EventListenerFocusTrapInertStrategy {
        constructor() {
            /** Focus event handler. */
            this._listener = null;
        }
        /** Adds a document event listener that keeps focus inside the FocusTrap. */
        preventFocus(focusTrap) {
            // Ensure there's only one listener per document
            if (this._listener) {
                focusTrap._document.removeEventListener('focus', this._listener, true);
            }
            this._listener = (e) => this._trapFocus(focusTrap, e);
            focusTrap._ngZone.runOutsideAngular(() => {
                focusTrap._document.addEventListener('focus', this._listener, true);
            });
        }
        /** Removes the event listener added in preventFocus. */
        allowFocus(focusTrap) {
            if (!this._listener) {
                return;
            }
            focusTrap._document.removeEventListener('focus', this._listener, true);
            this._listener = null;
        }
        /**
         * Refocuses the first element in the FocusTrap if the focus event target was outside
         * the FocusTrap.
         *
         * This is an event listener callback. The event listener is added in runOutsideAngular,
         * so all this code runs outside Angular as well.
         */
        _trapFocus(focusTrap, event) {
            const target = event.target;
            const focusTrapRoot = focusTrap._element;
            // Don't refocus if target was in an overlay, because the overlay might be associated
            // with an element inside the FocusTrap, ex. mat-select.
            if (!focusTrapRoot.contains(target) && closest(target, 'div.cdk-overlay-pane') === null) {
                // Some legacy FocusTrap usages have logic that focuses some element on the page
                // just before FocusTrap is destroyed. For backwards compatibility, wait
                // to be sure FocusTrap is still enabled before refocusing.
                setTimeout(() => {
                    // Check whether focus wasn't put back into the focus trap while the timeout was pending.
                    if (focusTrap.enabled && !focusTrapRoot.contains(focusTrap._document.activeElement)) {
                        focusTrap.focusFirstTabbableElement();
                    }
                });
            }
        }
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Configuration for creating a ConfigurableFocusTrap.
     */
    class ConfigurableFocusTrapConfig {
        constructor() {
            /**
             * Whether to defer the creation of FocusTrap elements to be
             * done manually by the user. Default is to create them
             * automatically.
             */
            this.defer = false;
        }
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** The injection token used to specify the inert strategy. */
    const FOCUS_TRAP_INERT_STRATEGY = new core.InjectionToken('FOCUS_TRAP_INERT_STRATEGY');

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Injectable that ensures only the most recently enabled FocusTrap is active. */
    let FocusTrapManager = /** @class */ (() => {
        class FocusTrapManager {
            constructor() {
                // A stack of the FocusTraps on the page. Only the FocusTrap at the
                // top of the stack is active.
                this._focusTrapStack = [];
            }
            /**
             * Disables the FocusTrap at the top of the stack, and then pushes
             * the new FocusTrap onto the stack.
             */
            register(focusTrap) {
                // Dedupe focusTraps that register multiple times.
                this._focusTrapStack = this._focusTrapStack.filter((ft) => ft !== focusTrap);
                let stack = this._focusTrapStack;
                if (stack.length) {
                    stack[stack.length - 1]._disable();
                }
                stack.push(focusTrap);
                focusTrap._enable();
            }
            /**
             * Removes the FocusTrap from the stack, and activates the
             * FocusTrap that is the new top of the stack.
             */
            deregister(focusTrap) {
                focusTrap._disable();
                const stack = this._focusTrapStack;
                const i = stack.indexOf(focusTrap);
                if (i !== -1) {
                    stack.splice(i, 1);
                    if (stack.length) {
                        stack[stack.length - 1]._enable();
                    }
                }
            }
        }
        FocusTrapManager.ɵprov = core.ɵɵdefineInjectable({ factory: function FocusTrapManager_Factory() { return new FocusTrapManager(); }, token: FocusTrapManager, providedIn: "root" });
        FocusTrapManager.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        return FocusTrapManager;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Factory that allows easy instantiation of configurable focus traps. */
    let ConfigurableFocusTrapFactory = /** @class */ (() => {
        class ConfigurableFocusTrapFactory {
            constructor(_checker, _ngZone, _focusTrapManager, _document, _inertStrategy) {
                this._checker = _checker;
                this._ngZone = _ngZone;
                this._focusTrapManager = _focusTrapManager;
                this._document = _document;
                // TODO split up the strategies into different modules, similar to DateAdapter.
                this._inertStrategy = _inertStrategy || new EventListenerFocusTrapInertStrategy();
            }
            create(element, config = new ConfigurableFocusTrapConfig()) {
                let configObject;
                if (typeof config === 'boolean') {
                    configObject = new ConfigurableFocusTrapConfig();
                    configObject.defer = config;
                }
                else {
                    configObject = config;
                }
                return new ConfigurableFocusTrap(element, this._checker, this._ngZone, this._document, this._focusTrapManager, this._inertStrategy, configObject);
            }
        }
        ConfigurableFocusTrapFactory.ɵprov = core.ɵɵdefineInjectable({ factory: function ConfigurableFocusTrapFactory_Factory() { return new ConfigurableFocusTrapFactory(core.ɵɵinject(InteractivityChecker), core.ɵɵinject(core.NgZone), core.ɵɵinject(FocusTrapManager), core.ɵɵinject(common.DOCUMENT), core.ɵɵinject(FOCUS_TRAP_INERT_STRATEGY, 8)); }, token: ConfigurableFocusTrapFactory, providedIn: "root" });
        ConfigurableFocusTrapFactory.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        ConfigurableFocusTrapFactory.ctorParameters = () => [
            { type: InteractivityChecker },
            { type: core.NgZone },
            { type: FocusTrapManager },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [FOCUS_TRAP_INERT_STRATEGY,] }] }
        ];
        return ConfigurableFocusTrapFactory;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    const LIVE_ANNOUNCER_ELEMENT_TOKEN = new core.InjectionToken('liveAnnouncerElement', {
        providedIn: 'root',
        factory: LIVE_ANNOUNCER_ELEMENT_TOKEN_FACTORY,
    });
    /** @docs-private */
    function LIVE_ANNOUNCER_ELEMENT_TOKEN_FACTORY() {
        return null;
    }
    /** Injection token that can be used to configure the default options for the LiveAnnouncer. */
    const LIVE_ANNOUNCER_DEFAULT_OPTIONS = new core.InjectionToken('LIVE_ANNOUNCER_DEFAULT_OPTIONS');

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let LiveAnnouncer = /** @class */ (() => {
        class LiveAnnouncer {
            constructor(elementToken, _ngZone, _document, _defaultOptions) {
                this._ngZone = _ngZone;
                this._defaultOptions = _defaultOptions;
                // We inject the live element and document as `any` because the constructor signature cannot
                // reference browser globals (HTMLElement, Document) on non-browser environments, since having
                // a class decorator causes TypeScript to preserve the constructor signature types.
                this._document = _document;
                this._liveElement = elementToken || this._createLiveElement();
            }
            announce(message, ...args) {
                const defaultOptions = this._defaultOptions;
                let politeness;
                let duration;
                if (args.length === 1 && typeof args[0] === 'number') {
                    duration = args[0];
                }
                else {
                    [politeness, duration] = args;
                }
                this.clear();
                clearTimeout(this._previousTimeout);
                if (!politeness) {
                    politeness =
                        (defaultOptions && defaultOptions.politeness) ? defaultOptions.politeness : 'polite';
                }
                if (duration == null && defaultOptions) {
                    duration = defaultOptions.duration;
                }
                // TODO: ensure changing the politeness works on all environments we support.
                this._liveElement.setAttribute('aria-live', politeness);
                // This 100ms timeout is necessary for some browser + screen-reader combinations:
                // - Both JAWS and NVDA over IE11 will not announce anything without a non-zero timeout.
                // - With Chrome and IE11 with NVDA or JAWS, a repeated (identical) message won't be read a
                //   second time without clearing and then using a non-zero delay.
                // (using JAWS 17 at time of this writing).
                return this._ngZone.runOutsideAngular(() => {
                    return new Promise(resolve => {
                        clearTimeout(this._previousTimeout);
                        this._previousTimeout = setTimeout(() => {
                            this._liveElement.textContent = message;
                            resolve();
                            if (typeof duration === 'number') {
                                this._previousTimeout = setTimeout(() => this.clear(), duration);
                            }
                        }, 100);
                    });
                });
            }
            /**
             * Clears the current text from the announcer element. Can be used to prevent
             * screen readers from reading the text out again while the user is going
             * through the page landmarks.
             */
            clear() {
                if (this._liveElement) {
                    this._liveElement.textContent = '';
                }
            }
            ngOnDestroy() {
                clearTimeout(this._previousTimeout);
                if (this._liveElement && this._liveElement.parentNode) {
                    this._liveElement.parentNode.removeChild(this._liveElement);
                    this._liveElement = null;
                }
            }
            _createLiveElement() {
                const elementClass = 'cdk-live-announcer-element';
                const previousElements = this._document.getElementsByClassName(elementClass);
                const liveEl = this._document.createElement('div');
                // Remove any old containers. This can happen when coming in from a server-side-rendered page.
                for (let i = 0; i < previousElements.length; i++) {
                    previousElements[i].parentNode.removeChild(previousElements[i]);
                }
                liveEl.classList.add(elementClass);
                liveEl.classList.add('cdk-visually-hidden');
                liveEl.setAttribute('aria-atomic', 'true');
                liveEl.setAttribute('aria-live', 'polite');
                this._document.body.appendChild(liveEl);
                return liveEl;
            }
        }
        LiveAnnouncer.ɵprov = core.ɵɵdefineInjectable({ factory: function LiveAnnouncer_Factory() { return new LiveAnnouncer(core.ɵɵinject(LIVE_ANNOUNCER_ELEMENT_TOKEN, 8), core.ɵɵinject(core.NgZone), core.ɵɵinject(common.DOCUMENT), core.ɵɵinject(LIVE_ANNOUNCER_DEFAULT_OPTIONS, 8)); }, token: LiveAnnouncer, providedIn: "root" });
        LiveAnnouncer.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        LiveAnnouncer.ctorParameters = () => [
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [LIVE_ANNOUNCER_ELEMENT_TOKEN,] }] },
            { type: core.NgZone },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [LIVE_ANNOUNCER_DEFAULT_OPTIONS,] }] }
        ];
        return LiveAnnouncer;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Screenreaders will often fire fake mousedown events when a focusable element
     * is activated using the keyboard. We can typically distinguish between these faked
     * mousedown events and real mousedown events using the "buttons" property. While
     * real mousedowns will indicate the mouse button that was pressed (e.g. "1" for
     * the left mouse button), faked mousedowns will usually set the property value to 0.
     */
    function isFakeMousedownFromScreenReader(event) {
        return event.buttons === 0;
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // This is the value used by AngularJS Material. Through trial and error (on iPhone 6S) they found
    // that a value of around 650ms seems appropriate.
    const TOUCH_BUFFER_MS = 650;
    /** InjectionToken for FocusMonitorOptions. */
    const FOCUS_MONITOR_DEFAULT_OPTIONS = new core.InjectionToken('cdk-focus-monitor-default-options');
    /**
     * Event listener options that enable capturing and also
     * mark the listener as passive if the browser supports it.
     */
    const captureEventListenerOptions = normalizePassiveListenerOptions({
        passive: true,
        capture: true
    });
    /** Monitors mouse and keyboard events to determine the cause of focus events. */
    let FocusMonitor = /** @class */ (() => {
        class FocusMonitor {
            constructor(_ngZone, _platform, 
            /** @breaking-change 11.0.0 make document required */
            document, options) {
                this._ngZone = _ngZone;
                this._platform = _platform;
                /** The focus origin that the next focus event is a result of. */
                this._origin = null;
                /** Whether the window has just been focused. */
                this._windowFocused = false;
                /** Map of elements being monitored to their info. */
                this._elementInfo = new Map();
                /** The number of elements currently being monitored. */
                this._monitoredElementCount = 0;
                /**
                 * Keeps track of the root nodes to which we've currently bound a focus/blur handler,
                 * as well as the number of monitored elements that they contain. We have to treat focus/blur
                 * handlers differently from the rest of the events, because the browser won't emit events
                 * to the document when focus moves inside of a shadow root.
                 */
                this._rootNodeFocusListenerCount = new Map();
                /**
                 * Event listener for `keydown` events on the document.
                 * Needs to be an arrow function in order to preserve the context when it gets bound.
                 */
                this._documentKeydownListener = () => {
                    // On keydown record the origin and clear any touch event that may be in progress.
                    this._lastTouchTarget = null;
                    this._setOriginForCurrentEventQueue('keyboard');
                };
                /**
                 * Event listener for `mousedown` events on the document.
                 * Needs to be an arrow function in order to preserve the context when it gets bound.
                 */
                this._documentMousedownListener = (event) => {
                    // On mousedown record the origin only if there is not touch
                    // target, since a mousedown can happen as a result of a touch event.
                    if (!this._lastTouchTarget) {
                        // In some cases screen readers fire fake `mousedown` events instead of `keydown`.
                        // Resolve the focus source to `keyboard` if we detect one of them.
                        const source = isFakeMousedownFromScreenReader(event) ? 'keyboard' : 'mouse';
                        this._setOriginForCurrentEventQueue(source);
                    }
                };
                /**
                 * Event listener for `touchstart` events on the document.
                 * Needs to be an arrow function in order to preserve the context when it gets bound.
                 */
                this._documentTouchstartListener = (event) => {
                    // When the touchstart event fires the focus event is not yet in the event queue. This means
                    // we can't rely on the trick used above (setting timeout of 1ms). Instead we wait 650ms to
                    // see if a focus happens.
                    if (this._touchTimeoutId != null) {
                        clearTimeout(this._touchTimeoutId);
                    }
                    this._lastTouchTarget = getTarget(event);
                    this._touchTimeoutId = setTimeout(() => this._lastTouchTarget = null, TOUCH_BUFFER_MS);
                };
                /**
                 * Event listener for `focus` events on the window.
                 * Needs to be an arrow function in order to preserve the context when it gets bound.
                 */
                this._windowFocusListener = () => {
                    // Make a note of when the window regains focus, so we can
                    // restore the origin info for the focused element.
                    this._windowFocused = true;
                    this._windowFocusTimeoutId = setTimeout(() => this._windowFocused = false);
                };
                /**
                 * Event listener for `focus` and 'blur' events on the document.
                 * Needs to be an arrow function in order to preserve the context when it gets bound.
                 */
                this._rootNodeFocusAndBlurListener = (event) => {
                    const target = getTarget(event);
                    const handler = event.type === 'focus' ? this._onFocus : this._onBlur;
                    // We need to walk up the ancestor chain in order to support `checkChildren`.
                    for (let element = target; element; element = element.parentElement) {
                        handler.call(this, event, element);
                    }
                };
                this._document = document;
                this._detectionMode = (options === null || options === void 0 ? void 0 : options.detectionMode) || 0 /* IMMEDIATE */;
            }
            monitor(element, checkChildren = false) {
                // Do nothing if we're not on the browser platform.
                if (!this._platform.isBrowser) {
                    return rxjs.of(null);
                }
                const nativeElement = coerceElement(element);
                // If the element is inside the shadow DOM, we need to bind our focus/blur listeners to
                // the shadow root, rather than the `document`, because the browser won't emit focus events
                // to the `document`, if focus is moving within the same shadow root.
                const rootNode = _getShadowRoot(nativeElement) || this._getDocument();
                const cachedInfo = this._elementInfo.get(nativeElement);
                // Check if we're already monitoring this element.
                if (cachedInfo) {
                    if (checkChildren) {
                        // TODO(COMP-318): this can be problematic, because it'll turn all non-checkChildren
                        // observers into ones that behave as if `checkChildren` was turned on. We need a more
                        // robust solution.
                        cachedInfo.checkChildren = true;
                    }
                    return cachedInfo.subject.asObservable();
                }
                // Create monitored element info.
                const info = {
                    checkChildren: checkChildren,
                    subject: new rxjs.Subject(),
                    rootNode
                };
                this._elementInfo.set(nativeElement, info);
                this._registerGlobalListeners(info);
                return info.subject.asObservable();
            }
            stopMonitoring(element) {
                const nativeElement = coerceElement(element);
                const elementInfo = this._elementInfo.get(nativeElement);
                if (elementInfo) {
                    elementInfo.subject.complete();
                    this._setClasses(nativeElement);
                    this._elementInfo.delete(nativeElement);
                    this._removeGlobalListeners(elementInfo);
                }
            }
            focusVia(element, origin, options) {
                const nativeElement = coerceElement(element);
                this._setOriginForCurrentEventQueue(origin);
                // `focus` isn't available on the server
                if (typeof nativeElement.focus === 'function') {
                    // Cast the element to `any`, because the TS typings don't have the `options` parameter yet.
                    nativeElement.focus(options);
                }
            }
            ngOnDestroy() {
                this._elementInfo.forEach((_info, element) => this.stopMonitoring(element));
            }
            /** Access injected document if available or fallback to global document reference */
            _getDocument() {
                return this._document || document;
            }
            /** Use defaultView of injected document if available or fallback to global window reference */
            _getWindow() {
                const doc = this._getDocument();
                return doc.defaultView || window;
            }
            _toggleClass(element, className, shouldSet) {
                if (shouldSet) {
                    element.classList.add(className);
                }
                else {
                    element.classList.remove(className);
                }
            }
            _getFocusOrigin(event) {
                // If we couldn't detect a cause for the focus event, it's due to one of three reasons:
                // 1) The window has just regained focus, in which case we want to restore the focused state of
                //    the element from before the window blurred.
                // 2) It was caused by a touch event, in which case we mark the origin as 'touch'.
                // 3) The element was programmatically focused, in which case we should mark the origin as
                //    'program'.
                if (this._origin) {
                    return this._origin;
                }
                if (this._windowFocused && this._lastFocusOrigin) {
                    return this._lastFocusOrigin;
                }
                else if (this._wasCausedByTouch(event)) {
                    return 'touch';
                }
                else {
                    return 'program';
                }
            }
            /**
             * Sets the focus classes on the element based on the given focus origin.
             * @param element The element to update the classes on.
             * @param origin The focus origin.
             */
            _setClasses(element, origin) {
                this._toggleClass(element, 'cdk-focused', !!origin);
                this._toggleClass(element, 'cdk-touch-focused', origin === 'touch');
                this._toggleClass(element, 'cdk-keyboard-focused', origin === 'keyboard');
                this._toggleClass(element, 'cdk-mouse-focused', origin === 'mouse');
                this._toggleClass(element, 'cdk-program-focused', origin === 'program');
            }
            /**
             * Sets the origin and schedules an async function to clear it at the end of the event queue.
             * If the detection mode is 'eventual', the origin is never cleared.
             * @param origin The origin to set.
             */
            _setOriginForCurrentEventQueue(origin) {
                this._ngZone.runOutsideAngular(() => {
                    this._origin = origin;
                    if (this._detectionMode === 0 /* IMMEDIATE */) {
                        // Sometimes the focus origin won't be valid in Firefox because Firefox seems to focus *one*
                        // tick after the interaction event fired. To ensure the focus origin is always correct,
                        // the focus origin will be determined at the beginning of the next tick.
                        this._originTimeoutId = setTimeout(() => this._origin = null, 1);
                    }
                });
            }
            /**
             * Checks whether the given focus event was caused by a touchstart event.
             * @param event The focus event to check.
             * @returns Whether the event was caused by a touch.
             */
            _wasCausedByTouch(event) {
                // Note(mmalerba): This implementation is not quite perfect, there is a small edge case.
                // Consider the following dom structure:
                //
                // <div #parent tabindex="0" cdkFocusClasses>
                //   <div #child (click)="#parent.focus()"></div>
                // </div>
                //
                // If the user touches the #child element and the #parent is programmatically focused as a
                // result, this code will still consider it to have been caused by the touch event and will
                // apply the cdk-touch-focused class rather than the cdk-program-focused class. This is a
                // relatively small edge-case that can be worked around by using
                // focusVia(parentEl, 'program') to focus the parent element.
                //
                // If we decide that we absolutely must handle this case correctly, we can do so by listening
                // for the first focus event after the touchstart, and then the first blur event after that
                // focus event. When that blur event fires we know that whatever follows is not a result of the
                // touchstart.
                const focusTarget = getTarget(event);
                return this._lastTouchTarget instanceof Node && focusTarget instanceof Node &&
                    (focusTarget === this._lastTouchTarget || focusTarget.contains(this._lastTouchTarget));
            }
            /**
             * Handles focus events on a registered element.
             * @param event The focus event.
             * @param element The monitored element.
             */
            _onFocus(event, element) {
                // NOTE(mmalerba): We currently set the classes based on the focus origin of the most recent
                // focus event affecting the monitored element. If we want to use the origin of the first event
                // instead we should check for the cdk-focused class here and return if the element already has
                // it. (This only matters for elements that have includesChildren = true).
                // If we are not counting child-element-focus as focused, make sure that the event target is the
                // monitored element itself.
                const elementInfo = this._elementInfo.get(element);
                if (!elementInfo || (!elementInfo.checkChildren && element !== getTarget(event))) {
                    return;
                }
                const origin = this._getFocusOrigin(event);
                this._setClasses(element, origin);
                this._emitOrigin(elementInfo.subject, origin);
                this._lastFocusOrigin = origin;
            }
            /**
             * Handles blur events on a registered element.
             * @param event The blur event.
             * @param element The monitored element.
             */
            _onBlur(event, element) {
                // If we are counting child-element-focus as focused, make sure that we aren't just blurring in
                // order to focus another child of the monitored element.
                const elementInfo = this._elementInfo.get(element);
                if (!elementInfo || (elementInfo.checkChildren && event.relatedTarget instanceof Node &&
                    element.contains(event.relatedTarget))) {
                    return;
                }
                this._setClasses(element);
                this._emitOrigin(elementInfo.subject, null);
            }
            _emitOrigin(subject, origin) {
                this._ngZone.run(() => subject.next(origin));
            }
            _registerGlobalListeners(elementInfo) {
                if (!this._platform.isBrowser) {
                    return;
                }
                const rootNode = elementInfo.rootNode;
                const rootNodeFocusListeners = this._rootNodeFocusListenerCount.get(rootNode) || 0;
                if (!rootNodeFocusListeners) {
                    this._ngZone.runOutsideAngular(() => {
                        rootNode.addEventListener('focus', this._rootNodeFocusAndBlurListener, captureEventListenerOptions);
                        rootNode.addEventListener('blur', this._rootNodeFocusAndBlurListener, captureEventListenerOptions);
                    });
                }
                this._rootNodeFocusListenerCount.set(rootNode, rootNodeFocusListeners + 1);
                // Register global listeners when first element is monitored.
                if (++this._monitoredElementCount === 1) {
                    // Note: we listen to events in the capture phase so we
                    // can detect them even if the user stops propagation.
                    this._ngZone.runOutsideAngular(() => {
                        const document = this._getDocument();
                        const window = this._getWindow();
                        document.addEventListener('keydown', this._documentKeydownListener, captureEventListenerOptions);
                        document.addEventListener('mousedown', this._documentMousedownListener, captureEventListenerOptions);
                        document.addEventListener('touchstart', this._documentTouchstartListener, captureEventListenerOptions);
                        window.addEventListener('focus', this._windowFocusListener);
                    });
                }
            }
            _removeGlobalListeners(elementInfo) {
                const rootNode = elementInfo.rootNode;
                if (this._rootNodeFocusListenerCount.has(rootNode)) {
                    const rootNodeFocusListeners = this._rootNodeFocusListenerCount.get(rootNode);
                    if (rootNodeFocusListeners > 1) {
                        this._rootNodeFocusListenerCount.set(rootNode, rootNodeFocusListeners - 1);
                    }
                    else {
                        rootNode.removeEventListener('focus', this._rootNodeFocusAndBlurListener, captureEventListenerOptions);
                        rootNode.removeEventListener('blur', this._rootNodeFocusAndBlurListener, captureEventListenerOptions);
                        this._rootNodeFocusListenerCount.delete(rootNode);
                    }
                }
                // Unregister global listeners when last element is unmonitored.
                if (!--this._monitoredElementCount) {
                    const document = this._getDocument();
                    const window = this._getWindow();
                    document.removeEventListener('keydown', this._documentKeydownListener, captureEventListenerOptions);
                    document.removeEventListener('mousedown', this._documentMousedownListener, captureEventListenerOptions);
                    document.removeEventListener('touchstart', this._documentTouchstartListener, captureEventListenerOptions);
                    window.removeEventListener('focus', this._windowFocusListener);
                    // Clear timeouts for all potentially pending timeouts to prevent the leaks.
                    clearTimeout(this._windowFocusTimeoutId);
                    clearTimeout(this._touchTimeoutId);
                    clearTimeout(this._originTimeoutId);
                }
            }
        }
        FocusMonitor.ɵprov = core.ɵɵdefineInjectable({ factory: function FocusMonitor_Factory() { return new FocusMonitor(core.ɵɵinject(core.NgZone), core.ɵɵinject(Platform), core.ɵɵinject(common.DOCUMENT, 8), core.ɵɵinject(FOCUS_MONITOR_DEFAULT_OPTIONS, 8)); }, token: FocusMonitor, providedIn: "root" });
        FocusMonitor.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        FocusMonitor.ctorParameters = () => [
            { type: core.NgZone },
            { type: Platform },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [FOCUS_MONITOR_DEFAULT_OPTIONS,] }] }
        ];
        return FocusMonitor;
    })();
    /** Gets the target of an event, accounting for Shadow DOM. */
    function getTarget(event) {
        // If an event is bound outside the Shadow DOM, the `event.target` will
        // point to the shadow root so we have to use `composedPath` instead.
        return (event.composedPath ? event.composedPath()[0] : event.target);
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** CSS class applied to the document body when in black-on-white high-contrast mode. */
    const BLACK_ON_WHITE_CSS_CLASS = 'cdk-high-contrast-black-on-white';
    /** CSS class applied to the document body when in white-on-black high-contrast mode. */
    const WHITE_ON_BLACK_CSS_CLASS = 'cdk-high-contrast-white-on-black';
    /** CSS class applied to the document body when in high-contrast mode. */
    const HIGH_CONTRAST_MODE_ACTIVE_CSS_CLASS = 'cdk-high-contrast-active';
    /**
     * Service to determine whether the browser is currently in a high-contrast-mode environment.
     *
     * Microsoft Windows supports an accessibility feature called "High Contrast Mode". This mode
     * changes the appearance of all applications, including web applications, to dramatically increase
     * contrast.
     *
     * IE, Edge, and Firefox currently support this mode. Chrome does not support Windows High Contrast
     * Mode. This service does not detect high-contrast mode as added by the Chrome "High Contrast"
     * browser extension.
     */
    let HighContrastModeDetector = /** @class */ (() => {
        class HighContrastModeDetector {
            constructor(_platform, document) {
                this._platform = _platform;
                this._document = document;
            }
            /** Gets the current high-contrast-mode for the page. */
            getHighContrastMode() {
                if (!this._platform.isBrowser) {
                    return 0 /* NONE */;
                }
                // Create a test element with an arbitrary background-color that is neither black nor
                // white; high-contrast mode will coerce the color to either black or white. Also ensure that
                // appending the test element to the DOM does not affect layout by absolutely positioning it
                const testElement = this._document.createElement('div');
                testElement.style.backgroundColor = 'rgb(1,2,3)';
                testElement.style.position = 'absolute';
                this._document.body.appendChild(testElement);
                // Get the computed style for the background color, collapsing spaces to normalize between
                // browsers. Once we get this color, we no longer need the test element. Access the `window`
                // via the document so we can fake it in tests. Note that we have extra null checks, because
                // this logic will likely run during app bootstrap and throwing can break the entire app.
                const documentWindow = this._document.defaultView || window;
                const computedStyle = (documentWindow && documentWindow.getComputedStyle) ?
                    documentWindow.getComputedStyle(testElement) : null;
                const computedColor = (computedStyle && computedStyle.backgroundColor || '').replace(/ /g, '');
                this._document.body.removeChild(testElement);
                switch (computedColor) {
                    case 'rgb(0,0,0)': return 2 /* WHITE_ON_BLACK */;
                    case 'rgb(255,255,255)': return 1 /* BLACK_ON_WHITE */;
                }
                return 0 /* NONE */;
            }
            /** Applies CSS classes indicating high-contrast mode to document body (browser-only). */
            _applyBodyHighContrastModeCssClasses() {
                if (this._platform.isBrowser && this._document.body) {
                    const bodyClasses = this._document.body.classList;
                    // IE11 doesn't support `classList` operations with multiple arguments
                    bodyClasses.remove(HIGH_CONTRAST_MODE_ACTIVE_CSS_CLASS);
                    bodyClasses.remove(BLACK_ON_WHITE_CSS_CLASS);
                    bodyClasses.remove(WHITE_ON_BLACK_CSS_CLASS);
                    const mode = this.getHighContrastMode();
                    if (mode === 1 /* BLACK_ON_WHITE */) {
                        bodyClasses.add(HIGH_CONTRAST_MODE_ACTIVE_CSS_CLASS);
                        bodyClasses.add(BLACK_ON_WHITE_CSS_CLASS);
                    }
                    else if (mode === 2 /* WHITE_ON_BLACK */) {
                        bodyClasses.add(HIGH_CONTRAST_MODE_ACTIVE_CSS_CLASS);
                        bodyClasses.add(WHITE_ON_BLACK_CSS_CLASS);
                    }
                }
            }
        }
        HighContrastModeDetector.ɵprov = core.ɵɵdefineInjectable({ factory: function HighContrastModeDetector_Factory() { return new HighContrastModeDetector(core.ɵɵinject(Platform), core.ɵɵinject(common.DOCUMENT)); }, token: HighContrastModeDetector, providedIn: "root" });
        HighContrastModeDetector.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        HighContrastModeDetector.ctorParameters = () => [
            { type: Platform },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] }
        ];
        return HighContrastModeDetector;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Injection token used to inject the document into Directionality.
     * This is used so that the value can be faked in tests.
     *
     * We can't use the real document in tests because changing the real `dir` causes geometry-based
     * tests in Safari to fail.
     *
     * We also can't re-provide the DOCUMENT token from platform-brower because the unit tests
     * themselves use things like `querySelector` in test code.
     *
     * This token is defined in a separate file from Directionality as a workaround for
     * https://github.com/angular/angular/issues/22559
     *
     * @docs-private
     */
    const DIR_DOCUMENT = new core.InjectionToken('cdk-dir-doc', {
        providedIn: 'root',
        factory: DIR_DOCUMENT_FACTORY,
    });
    /** @docs-private */
    function DIR_DOCUMENT_FACTORY() {
        return core.inject(common.DOCUMENT);
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * The directionality (LTR / RTL) context for the application (or a subtree of it).
     * Exposes the current direction and a stream of direction changes.
     */
    let Directionality = /** @class */ (() => {
        class Directionality {
            constructor(_document) {
                /** The current 'ltr' or 'rtl' value. */
                this.value = 'ltr';
                /** Stream that emits whenever the 'ltr' / 'rtl' state changes. */
                this.change = new core.EventEmitter();
                if (_document) {
                    // TODO: handle 'auto' value -
                    // We still need to account for dir="auto".
                    // It looks like HTMLElemenet.dir is also "auto" when that's set to the attribute,
                    // but getComputedStyle return either "ltr" or "rtl". avoiding getComputedStyle for now
                    const bodyDir = _document.body ? _document.body.dir : null;
                    const htmlDir = _document.documentElement ? _document.documentElement.dir : null;
                    const value = bodyDir || htmlDir;
                    this.value = (value === 'ltr' || value === 'rtl') ? value : 'ltr';
                }
            }
            ngOnDestroy() {
                this.change.complete();
            }
        }
        Directionality.ɵprov = core.ɵɵdefineInjectable({ factory: function Directionality_Factory() { return new Directionality(core.ɵɵinject(DIR_DOCUMENT, 8)); }, token: Directionality, providedIn: "root" });
        Directionality.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        Directionality.ctorParameters = () => [
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [DIR_DOCUMENT,] }] }
        ];
        return Directionality;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Directive to listen for changes of direction of part of the DOM.
     *
     * Provides itself as Directionality such that descendant directives only need to ever inject
     * Directionality to get the closest direction.
     */
    let Dir = /** @class */ (() => {
        class Dir {
            constructor() {
                /** Normalized direction that accounts for invalid/unsupported values. */
                this._dir = 'ltr';
                /** Whether the `value` has been set to its initial value. */
                this._isInitialized = false;
                /** Event emitted when the direction changes. */
                this.change = new core.EventEmitter();
            }
            /** @docs-private */
            get dir() { return this._dir; }
            set dir(value) {
                const old = this._dir;
                const normalizedValue = value ? value.toLowerCase() : value;
                this._rawDir = value;
                this._dir = (normalizedValue === 'ltr' || normalizedValue === 'rtl') ? normalizedValue : 'ltr';
                if (old !== this._dir && this._isInitialized) {
                    this.change.emit(this._dir);
                }
            }
            /** Current layout direction of the element. */
            get value() { return this.dir; }
            /** Initialize once default value has been set. */
            ngAfterContentInit() {
                this._isInitialized = true;
            }
            ngOnDestroy() {
                this.change.complete();
            }
        }
        Dir.decorators = [
            { type: core.Directive, args: [{
                        selector: '[dir]',
                        providers: [{ provide: Directionality, useExisting: Dir }],
                        host: { '[attr.dir]': '_rawDir' },
                        exportAs: 'dir',
                    },] }
        ];
        Dir.propDecorators = {
            change: [{ type: core.Output, args: ['dirChange',] }],
            dir: [{ type: core.Input }]
        };
        return Dir;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let BidiModule = /** @class */ (() => {
        class BidiModule {
        }
        BidiModule.decorators = [
            { type: core.NgModule, args: [{
                        exports: [Dir],
                        declarations: [Dir],
                    },] }
        ];
        return BidiModule;
    })();

    /**
     * @license Angular v10.0.0
     * (c) 2010-2020 Google LLC. https://angular.io/
     * License: MIT
     */
    /**
     * @publicApi
     */
    const ANIMATION_MODULE_TYPE = new core.InjectionToken('AnimationModuleType');

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Current version of Angular Material. */
    const VERSION = new core.Version('10.0.0');

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // Private version constant to circumvent test/build issues,
    // i.e. avoid core to depend on the @angular/material primary entry-point
    // Can be removed once the Material primary entry-point no longer
    // re-exports all secondary entry-points
    const VERSION$1 = new core.Version('10.0.0');
    /** @docs-private */
    function MATERIAL_SANITY_CHECKS_FACTORY() {
        return true;
    }
    /** Injection token that configures whether the Material sanity checks are enabled. */
    const MATERIAL_SANITY_CHECKS = new core.InjectionToken('mat-sanity-checks', {
        providedIn: 'root',
        factory: MATERIAL_SANITY_CHECKS_FACTORY,
    });
    /**
     * Module that captures anything that should be loaded and/or run for *all* Angular Material
     * components. This includes Bidi, etc.
     *
     * This module should be imported to each top-level component module (e.g., MatTabsModule).
     */
    let MatCommonModule = /** @class */ (() => {
        class MatCommonModule {
            constructor(highContrastModeDetector, sanityChecks, 
            /** @breaking-change 11.0.0 make document required */
            document) {
                /** Whether we've done the global sanity checks (e.g. a theme is loaded, there is a doctype). */
                this._hasDoneGlobalChecks = false;
                this._document = document;
                // While A11yModule also does this, we repeat it here to avoid importing A11yModule
                // in MatCommonModule.
                highContrastModeDetector._applyBodyHighContrastModeCssClasses();
                // Note that `_sanityChecks` is typed to `any`, because AoT
                // throws an error if we use the `SanityChecks` type directly.
                this._sanityChecks = sanityChecks;
                if (!this._hasDoneGlobalChecks) {
                    this._checkDoctypeIsDefined();
                    this._checkThemeIsPresent();
                    this._checkCdkVersionMatch();
                    this._hasDoneGlobalChecks = true;
                }
            }
            /** Access injected document if available or fallback to global document reference */
            _getDocument() {
                const doc = this._document || document;
                return typeof doc === 'object' && doc ? doc : null;
            }
            /** Use defaultView of injected document if available or fallback to global window reference */
            _getWindow() {
                const doc = this._getDocument();
                const win = (doc === null || doc === void 0 ? void 0 : doc.defaultView) || window;
                return typeof win === 'object' && win ? win : null;
            }
            /** Whether any sanity checks are enabled. */
            _checksAreEnabled() {
                return core.isDevMode() && !this._isTestEnv();
            }
            /** Whether the code is running in tests. */
            _isTestEnv() {
                const window = this._getWindow();
                return window && (window.__karma__ || window.jasmine);
            }
            _checkDoctypeIsDefined() {
                const isEnabled = this._checksAreEnabled() &&
                    (this._sanityChecks === true || this._sanityChecks.doctype);
                const document = this._getDocument();
                if (isEnabled && document && !document.doctype) {
                    console.warn('Current document does not have a doctype. This may cause ' +
                        'some Angular Material components not to behave as expected.');
                }
            }
            _checkThemeIsPresent() {
                // We need to assert that the `body` is defined, because these checks run very early
                // and the `body` won't be defined if the consumer put their scripts in the `head`.
                const isDisabled = !this._checksAreEnabled() ||
                    (this._sanityChecks === false || !this._sanityChecks.theme);
                const document = this._getDocument();
                if (isDisabled || !document || !document.body ||
                    typeof getComputedStyle !== 'function') {
                    return;
                }
                const testElement = document.createElement('div');
                testElement.classList.add('mat-theme-loaded-marker');
                document.body.appendChild(testElement);
                const computedStyle = getComputedStyle(testElement);
                // In some situations the computed style of the test element can be null. For example in
                // Firefox, the computed style is null if an application is running inside of a hidden iframe.
                // See: https://bugzilla.mozilla.org/show_bug.cgi?id=548397
                if (computedStyle && computedStyle.display !== 'none') {
                    console.warn('Could not find Angular Material core theme. Most Material ' +
                        'components may not work as expected. For more info refer ' +
                        'to the theming guide: https://material.angular.io/guide/theming');
                }
                document.body.removeChild(testElement);
            }
            /** Checks whether the material version matches the cdk version */
            _checkCdkVersionMatch() {
                const isEnabled = this._checksAreEnabled() &&
                    (this._sanityChecks === true || this._sanityChecks.version);
                if (isEnabled && VERSION$1.full !== cdk.VERSION.full) {
                    console.warn('The Angular Material version (' + VERSION$1.full + ') does not match ' +
                        'the Angular CDK version (' + cdk.VERSION.full + ').\n' +
                        'Please ensure the versions of these two packages exactly match.');
                }
            }
        }
        MatCommonModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [BidiModule],
                        exports: [BidiModule],
                    },] }
        ];
        MatCommonModule.ctorParameters = () => [
            { type: HighContrastModeDetector },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [MATERIAL_SANITY_CHECKS,] }] },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [common.DOCUMENT,] }] }
        ];
        return MatCommonModule;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Mixin to augment a directive with a `disabled` property. */
    function mixinDisabled(base) {
        return class extends base {
            constructor(...args) {
                super(...args);
                this._disabled = false;
            }
            get disabled() { return this._disabled; }
            set disabled(value) { this._disabled = coerceBooleanProperty(value); }
        };
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Mixin to augment a directive with a `color` property. */
    function mixinColor(base, defaultColor) {
        return class extends base {
            constructor(...args) {
                super(...args);
                // Set the default color that can be specified from the mixin.
                this.color = defaultColor;
            }
            get color() { return this._color; }
            set color(value) {
                const colorPalette = value || defaultColor;
                if (colorPalette !== this._color) {
                    if (this._color) {
                        this._elementRef.nativeElement.classList.remove(`mat-${this._color}`);
                    }
                    if (colorPalette) {
                        this._elementRef.nativeElement.classList.add(`mat-${colorPalette}`);
                    }
                    this._color = colorPalette;
                }
            }
        };
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Mixin to augment a directive with a `disableRipple` property. */
    function mixinDisableRipple(base) {
        return class extends base {
            constructor(...args) {
                super(...args);
                this._disableRipple = false;
            }
            /** Whether the ripple effect is disabled or not. */
            get disableRipple() { return this._disableRipple; }
            set disableRipple(value) { this._disableRipple = coerceBooleanProperty(value); }
        };
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** InjectionToken for datepicker that can be used to override default locale code. */
    const MAT_DATE_LOCALE = new core.InjectionToken('MAT_DATE_LOCALE', {
        providedIn: 'root',
        factory: MAT_DATE_LOCALE_FACTORY,
    });
    /** @docs-private */
    function MAT_DATE_LOCALE_FACTORY() {
        return core.inject(core.LOCALE_ID);
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    const MAT_DATE_FORMATS = new core.InjectionToken('mat-date-formats');

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // TODO(mmalerba): Remove when we no longer support safari 9.
    /** Whether the browser supports the Intl API. */
    let SUPPORTS_INTL_API;
    // We need a try/catch around the reference to `Intl`, because accessing it in some cases can
    // cause IE to throw. These cases are tied to particular versions of Windows and can happen if
    // the consumer is providing a polyfilled `Map`. See:
    // https://github.com/Microsoft/ChakraCore/issues/3189
    // https://github.com/angular/components/issues/15687
    try {
        SUPPORTS_INTL_API = typeof Intl != 'undefined';
    }
    catch (_a) {
        SUPPORTS_INTL_API = false;
    }
    const ɵ0 = i => String(i + 1);
    /** The default date names to use if Intl API is not available. */
    const DEFAULT_DATE_NAMES = range(31, ɵ0);
    /** Creates an array and fills it with values. */
    function range(length, valueFunction) {
        const valuesArray = Array(length);
        for (let i = 0; i < length; i++) {
            valuesArray[i] = valueFunction(i);
        }
        return valuesArray;
    }
    /** Provider that defines how form controls behave with regards to displaying error messages. */
    let ErrorStateMatcher = /** @class */ (() => {
        class ErrorStateMatcher {
            isErrorState(control, form) {
                return !!(control && control.invalid && (control.touched || (form && form.submitted)));
            }
        }
        ErrorStateMatcher.ɵprov = core.ɵɵdefineInjectable({ factory: function ErrorStateMatcher_Factory() { return new ErrorStateMatcher(); }, token: ErrorStateMatcher, providedIn: "root" });
        ErrorStateMatcher.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        return ErrorStateMatcher;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Reference to a previously launched ripple element.
     */
    class RippleRef {
        constructor(_renderer, 
        /** Reference to the ripple HTML element. */
        element, 
        /** Ripple configuration used for the ripple. */
        config) {
            this._renderer = _renderer;
            this.element = element;
            this.config = config;
            /** Current state of the ripple. */
            this.state = 3 /* HIDDEN */;
        }
        /** Fades out the ripple element. */
        fadeOut() {
            this._renderer.fadeOutRipple(this);
        }
    }

    /**
     * Default ripple animation configuration for ripples without an explicit
     * animation config specified.
     */
    const defaultRippleAnimationConfig = {
        enterDuration: 450,
        exitDuration: 400
    };
    /**
     * Timeout for ignoring mouse events. Mouse events will be temporary ignored after touch
     * events to avoid synthetic mouse events.
     */
    const ignoreMouseEventsTimeout = 800;
    /** Options that apply to all the event listeners that are bound by the ripple renderer. */
    const passiveEventOptions = normalizePassiveListenerOptions({ passive: true });
    /** Events that signal that the pointer is down. */
    const pointerDownEvents = ['mousedown', 'touchstart'];
    /** Events that signal that the pointer is up. */
    const pointerUpEvents = ['mouseup', 'mouseleave', 'touchend', 'touchcancel'];
    /**
     * Helper service that performs DOM manipulations. Not intended to be used outside this module.
     * The constructor takes a reference to the ripple directive's host element and a map of DOM
     * event handlers to be installed on the element that triggers ripple animations.
     * This will eventually become a custom renderer once Angular support exists.
     * @docs-private
     */
    class RippleRenderer {
        constructor(_target, _ngZone, elementOrElementRef, platform) {
            this._target = _target;
            this._ngZone = _ngZone;
            /** Whether the pointer is currently down or not. */
            this._isPointerDown = false;
            /** Set of currently active ripple references. */
            this._activeRipples = new Set();
            /** Whether pointer-up event listeners have been registered. */
            this._pointerUpEventsRegistered = false;
            // Only do anything if we're on the browser.
            if (platform.isBrowser) {
                this._containerElement = coerceElement(elementOrElementRef);
            }
        }
        /**
         * Fades in a ripple at the given coordinates.
         * @param x Coordinate within the element, along the X axis at which to start the ripple.
         * @param y Coordinate within the element, along the Y axis at which to start the ripple.
         * @param config Extra ripple options.
         */
        fadeInRipple(x, y, config = {}) {
            const containerRect = this._containerRect =
                this._containerRect || this._containerElement.getBoundingClientRect();
            const animationConfig = Object.assign(Object.assign({}, defaultRippleAnimationConfig), config.animation);
            if (config.centered) {
                x = containerRect.left + containerRect.width / 2;
                y = containerRect.top + containerRect.height / 2;
            }
            const radius = config.radius || distanceToFurthestCorner(x, y, containerRect);
            const offsetX = x - containerRect.left;
            const offsetY = y - containerRect.top;
            const duration = animationConfig.enterDuration;
            const ripple = document.createElement('div');
            ripple.classList.add('mat-ripple-element');
            ripple.style.left = `${offsetX - radius}px`;
            ripple.style.top = `${offsetY - radius}px`;
            ripple.style.height = `${radius * 2}px`;
            ripple.style.width = `${radius * 2}px`;
            // If a custom color has been specified, set it as inline style. If no color is
            // set, the default color will be applied through the ripple theme styles.
            if (config.color != null) {
                ripple.style.backgroundColor = config.color;
            }
            ripple.style.transitionDuration = `${duration}ms`;
            this._containerElement.appendChild(ripple);
            // By default the browser does not recalculate the styles of dynamically created
            // ripple elements. This is critical because then the `scale` would not animate properly.
            enforceStyleRecalculation(ripple);
            ripple.style.transform = 'scale(1)';
            // Exposed reference to the ripple that will be returned.
            const rippleRef = new RippleRef(this, ripple, config);
            rippleRef.state = 0 /* FADING_IN */;
            // Add the ripple reference to the list of all active ripples.
            this._activeRipples.add(rippleRef);
            if (!config.persistent) {
                this._mostRecentTransientRipple = rippleRef;
            }
            // Wait for the ripple element to be completely faded in.
            // Once it's faded in, the ripple can be hidden immediately if the mouse is released.
            this._runTimeoutOutsideZone(() => {
                const isMostRecentTransientRipple = rippleRef === this._mostRecentTransientRipple;
                rippleRef.state = 1 /* VISIBLE */;
                // When the timer runs out while the user has kept their pointer down, we want to
                // keep only the persistent ripples and the latest transient ripple. We do this,
                // because we don't want stacked transient ripples to appear after their enter
                // animation has finished.
                if (!config.persistent && (!isMostRecentTransientRipple || !this._isPointerDown)) {
                    rippleRef.fadeOut();
                }
            }, duration);
            return rippleRef;
        }
        /** Fades out a ripple reference. */
        fadeOutRipple(rippleRef) {
            const wasActive = this._activeRipples.delete(rippleRef);
            if (rippleRef === this._mostRecentTransientRipple) {
                this._mostRecentTransientRipple = null;
            }
            // Clear out the cached bounding rect if we have no more ripples.
            if (!this._activeRipples.size) {
                this._containerRect = null;
            }
            // For ripples that are not active anymore, don't re-run the fade-out animation.
            if (!wasActive) {
                return;
            }
            const rippleEl = rippleRef.element;
            const animationConfig = Object.assign(Object.assign({}, defaultRippleAnimationConfig), rippleRef.config.animation);
            rippleEl.style.transitionDuration = `${animationConfig.exitDuration}ms`;
            rippleEl.style.opacity = '0';
            rippleRef.state = 2 /* FADING_OUT */;
            // Once the ripple faded out, the ripple can be safely removed from the DOM.
            this._runTimeoutOutsideZone(() => {
                rippleRef.state = 3 /* HIDDEN */;
                rippleEl.parentNode.removeChild(rippleEl);
            }, animationConfig.exitDuration);
        }
        /** Fades out all currently active ripples. */
        fadeOutAll() {
            this._activeRipples.forEach(ripple => ripple.fadeOut());
        }
        /** Sets up the trigger event listeners */
        setupTriggerEvents(elementOrElementRef) {
            const element = coerceElement(elementOrElementRef);
            if (!element || element === this._triggerElement) {
                return;
            }
            // Remove all previously registered event listeners from the trigger element.
            this._removeTriggerEvents();
            this._triggerElement = element;
            this._registerEvents(pointerDownEvents);
        }
        /**
         * Handles all registered events.
         * @docs-private
         */
        handleEvent(event) {
            if (event.type === 'mousedown') {
                this._onMousedown(event);
            }
            else if (event.type === 'touchstart') {
                this._onTouchStart(event);
            }
            else {
                this._onPointerUp();
            }
            // If pointer-up events haven't been registered yet, do so now.
            // We do this on-demand in order to reduce the total number of event listeners
            // registered by the ripples, which speeds up the rendering time for large UIs.
            if (!this._pointerUpEventsRegistered) {
                this._registerEvents(pointerUpEvents);
                this._pointerUpEventsRegistered = true;
            }
        }
        /** Function being called whenever the trigger is being pressed using mouse. */
        _onMousedown(event) {
            // Screen readers will fire fake mouse events for space/enter. Skip launching a
            // ripple in this case for consistency with the non-screen-reader experience.
            const isFakeMousedown = isFakeMousedownFromScreenReader(event);
            const isSyntheticEvent = this._lastTouchStartEvent &&
                Date.now() < this._lastTouchStartEvent + ignoreMouseEventsTimeout;
            if (!this._target.rippleDisabled && !isFakeMousedown && !isSyntheticEvent) {
                this._isPointerDown = true;
                this.fadeInRipple(event.clientX, event.clientY, this._target.rippleConfig);
            }
        }
        /** Function being called whenever the trigger is being pressed using touch. */
        _onTouchStart(event) {
            if (!this._target.rippleDisabled) {
                // Some browsers fire mouse events after a `touchstart` event. Those synthetic mouse
                // events will launch a second ripple if we don't ignore mouse events for a specific
                // time after a touchstart event.
                this._lastTouchStartEvent = Date.now();
                this._isPointerDown = true;
                // Use `changedTouches` so we skip any touches where the user put
                // their finger down, but used another finger to tap the element again.
                const touches = event.changedTouches;
                for (let i = 0; i < touches.length; i++) {
                    this.fadeInRipple(touches[i].clientX, touches[i].clientY, this._target.rippleConfig);
                }
            }
        }
        /** Function being called whenever the trigger is being released. */
        _onPointerUp() {
            if (!this._isPointerDown) {
                return;
            }
            this._isPointerDown = false;
            // Fade-out all ripples that are visible and not persistent.
            this._activeRipples.forEach(ripple => {
                // By default, only ripples that are completely visible will fade out on pointer release.
                // If the `terminateOnPointerUp` option is set, ripples that still fade in will also fade out.
                const isVisible = ripple.state === 1 /* VISIBLE */ ||
                    ripple.config.terminateOnPointerUp && ripple.state === 0 /* FADING_IN */;
                if (!ripple.config.persistent && isVisible) {
                    ripple.fadeOut();
                }
            });
        }
        /** Runs a timeout outside of the Angular zone to avoid triggering the change detection. */
        _runTimeoutOutsideZone(fn, delay = 0) {
            this._ngZone.runOutsideAngular(() => setTimeout(fn, delay));
        }
        /** Registers event listeners for a given list of events. */
        _registerEvents(eventTypes) {
            this._ngZone.runOutsideAngular(() => {
                eventTypes.forEach((type) => {
                    this._triggerElement.addEventListener(type, this, passiveEventOptions);
                });
            });
        }
        /** Removes previously registered event listeners from the trigger element. */
        _removeTriggerEvents() {
            if (this._triggerElement) {
                pointerDownEvents.forEach((type) => {
                    this._triggerElement.removeEventListener(type, this, passiveEventOptions);
                });
                if (this._pointerUpEventsRegistered) {
                    pointerUpEvents.forEach((type) => {
                        this._triggerElement.removeEventListener(type, this, passiveEventOptions);
                    });
                }
            }
        }
    }
    /** Enforces a style recalculation of a DOM element by computing its styles. */
    function enforceStyleRecalculation(element) {
        // Enforce a style recalculation by calling `getComputedStyle` and accessing any property.
        // Calling `getPropertyValue` is important to let optimizers know that this is not a noop.
        // See: https://gist.github.com/paulirish/5d52fb081b3570c81e3a
        window.getComputedStyle(element).getPropertyValue('opacity');
    }
    /**
     * Returns the distance from the point (x, y) to the furthest corner of a rectangle.
     */
    function distanceToFurthestCorner(x, y, rect) {
        const distX = Math.max(Math.abs(x - rect.left), Math.abs(x - rect.right));
        const distY = Math.max(Math.abs(y - rect.top), Math.abs(y - rect.bottom));
        return Math.sqrt(distX * distX + distY * distY);
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Injection token that can be used to specify the global ripple options. */
    const MAT_RIPPLE_GLOBAL_OPTIONS = new core.InjectionToken('mat-ripple-global-options');
    let MatRipple = /** @class */ (() => {
        class MatRipple {
            constructor(_elementRef, ngZone, platform, globalOptions, _animationMode) {
                this._elementRef = _elementRef;
                this._animationMode = _animationMode;
                /**
                 * If set, the radius in pixels of foreground ripples when fully expanded. If unset, the radius
                 * will be the distance from the center of the ripple to the furthest corner of the host element's
                 * bounding rectangle.
                 */
                this.radius = 0;
                this._disabled = false;
                /** Whether ripple directive is initialized and the input bindings are set. */
                this._isInitialized = false;
                this._globalOptions = globalOptions || {};
                this._rippleRenderer = new RippleRenderer(this, ngZone, _elementRef, platform);
            }
            /**
             * Whether click events will not trigger the ripple. Ripples can be still launched manually
             * by using the `launch()` method.
             */
            get disabled() { return this._disabled; }
            set disabled(value) {
                this._disabled = value;
                this._setupTriggerEventsIfEnabled();
            }
            /**
             * The element that triggers the ripple when click events are received.
             * Defaults to the directive's host element.
             */
            get trigger() { return this._trigger || this._elementRef.nativeElement; }
            set trigger(trigger) {
                this._trigger = trigger;
                this._setupTriggerEventsIfEnabled();
            }
            ngOnInit() {
                this._isInitialized = true;
                this._setupTriggerEventsIfEnabled();
            }
            ngOnDestroy() {
                this._rippleRenderer._removeTriggerEvents();
            }
            /** Fades out all currently showing ripple elements. */
            fadeOutAll() {
                this._rippleRenderer.fadeOutAll();
            }
            /**
             * Ripple configuration from the directive's input values.
             * @docs-private Implemented as part of RippleTarget
             */
            get rippleConfig() {
                return {
                    centered: this.centered,
                    radius: this.radius,
                    color: this.color,
                    animation: Object.assign(Object.assign(Object.assign({}, this._globalOptions.animation), (this._animationMode === 'NoopAnimations' ? { enterDuration: 0, exitDuration: 0 } : {})), this.animation),
                    terminateOnPointerUp: this._globalOptions.terminateOnPointerUp,
                };
            }
            /**
             * Whether ripples on pointer-down are disabled or not.
             * @docs-private Implemented as part of RippleTarget
             */
            get rippleDisabled() {
                return this.disabled || !!this._globalOptions.disabled;
            }
            /** Sets up the trigger event listeners if ripples are enabled. */
            _setupTriggerEventsIfEnabled() {
                if (!this.disabled && this._isInitialized) {
                    this._rippleRenderer.setupTriggerEvents(this.trigger);
                }
            }
            /** Launches a manual ripple at the specified coordinated or just by the ripple config. */
            launch(configOrX, y = 0, config) {
                if (typeof configOrX === 'number') {
                    return this._rippleRenderer.fadeInRipple(configOrX, y, Object.assign(Object.assign({}, this.rippleConfig), config));
                }
                else {
                    return this._rippleRenderer.fadeInRipple(0, 0, Object.assign(Object.assign({}, this.rippleConfig), configOrX));
                }
            }
        }
        MatRipple.decorators = [
            { type: core.Directive, args: [{
                        selector: '[mat-ripple], [matRipple]',
                        exportAs: 'matRipple',
                        host: {
                            'class': 'mat-ripple',
                            '[class.mat-ripple-unbounded]': 'unbounded'
                        }
                    },] }
        ];
        MatRipple.ctorParameters = () => [
            { type: core.ElementRef },
            { type: core.NgZone },
            { type: Platform },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [MAT_RIPPLE_GLOBAL_OPTIONS,] }] },
            { type: String, decorators: [{ type: core.Optional }, { type: core.Inject, args: [ANIMATION_MODULE_TYPE,] }] }
        ];
        MatRipple.propDecorators = {
            color: [{ type: core.Input, args: ['matRippleColor',] }],
            unbounded: [{ type: core.Input, args: ['matRippleUnbounded',] }],
            centered: [{ type: core.Input, args: ['matRippleCentered',] }],
            radius: [{ type: core.Input, args: ['matRippleRadius',] }],
            animation: [{ type: core.Input, args: ['matRippleAnimation',] }],
            disabled: [{ type: core.Input, args: ['matRippleDisabled',] }],
            trigger: [{ type: core.Input, args: ['matRippleTrigger',] }]
        };
        return MatRipple;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let MatRippleModule = /** @class */ (() => {
        class MatRippleModule {
        }
        MatRippleModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [MatCommonModule, PlatformModule],
                        exports: [MatRipple, MatCommonModule],
                        declarations: [MatRipple],
                    },] }
        ];
        return MatRippleModule;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Component that shows a simplified checkbox without including any kind of "real" checkbox.
     * Meant to be used when the checkbox is purely decorative and a large number of them will be
     * included, such as for the options in a multi-select. Uses no SVGs or complex animations.
     * Note that theming is meant to be handled by the parent element, e.g.
     * `mat-primary .mat-pseudo-checkbox`.
     *
     * Note that this component will be completely invisible to screen-reader users. This is *not*
     * interchangeable with `<mat-checkbox>` and should *not* be used if the user would directly
     * interact with the checkbox. The pseudo-checkbox should only be used as an implementation detail
     * of more complex components that appropriately handle selected / checked state.
     * @docs-private
     */
    let MatPseudoCheckbox = /** @class */ (() => {
        class MatPseudoCheckbox {
            constructor(_animationMode) {
                this._animationMode = _animationMode;
                /** Display state of the checkbox. */
                this.state = 'unchecked';
                /** Whether the checkbox is disabled. */
                this.disabled = false;
            }
        }
        MatPseudoCheckbox.decorators = [
            { type: core.Component, args: [{
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        selector: 'mat-pseudo-checkbox',
                        template: '',
                        host: {
                            'class': 'mat-pseudo-checkbox',
                            '[class.mat-pseudo-checkbox-indeterminate]': 'state === "indeterminate"',
                            '[class.mat-pseudo-checkbox-checked]': 'state === "checked"',
                            '[class.mat-pseudo-checkbox-disabled]': 'disabled',
                            '[class._mat-animation-noopable]': '_animationMode === "NoopAnimations"',
                        },
                        styles: [".mat-pseudo-checkbox{width:16px;height:16px;border:2px solid;border-radius:2px;cursor:pointer;display:inline-block;vertical-align:middle;box-sizing:border-box;position:relative;flex-shrink:0;transition:border-color 90ms cubic-bezier(0, 0, 0.2, 0.1),background-color 90ms cubic-bezier(0, 0, 0.2, 0.1)}.mat-pseudo-checkbox::after{position:absolute;opacity:0;content:\"\";border-bottom:2px solid currentColor;transition:opacity 90ms cubic-bezier(0, 0, 0.2, 0.1)}.mat-pseudo-checkbox.mat-pseudo-checkbox-checked,.mat-pseudo-checkbox.mat-pseudo-checkbox-indeterminate{border-color:transparent}._mat-animation-noopable.mat-pseudo-checkbox{transition:none;animation:none}._mat-animation-noopable.mat-pseudo-checkbox::after{transition:none}.mat-pseudo-checkbox-disabled{cursor:default}.mat-pseudo-checkbox-indeterminate::after{top:5px;left:1px;width:10px;opacity:1;border-radius:2px}.mat-pseudo-checkbox-checked::after{top:2.4px;left:1px;width:8px;height:3px;border-left:2px solid currentColor;transform:rotate(-45deg);opacity:1;box-sizing:content-box}\n"]
                    },] }
        ];
        MatPseudoCheckbox.ctorParameters = () => [
            { type: String, decorators: [{ type: core.Optional }, { type: core.Inject, args: [ANIMATION_MODULE_TYPE,] }] }
        ];
        MatPseudoCheckbox.propDecorators = {
            state: [{ type: core.Input }],
            disabled: [{ type: core.Input }]
        };
        return MatPseudoCheckbox;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // Boilerplate for applying mixins to MatOptgroup.
    /** @docs-private */
    class MatOptgroupBase {
    }
    const _MatOptgroupMixinBase = mixinDisabled(MatOptgroupBase);
    // Counter for unique group ids.
    let _uniqueOptgroupIdCounter = 0;
    /**
     * Component that is used to group instances of `mat-option`.
     */
    let MatOptgroup = /** @class */ (() => {
        class MatOptgroup extends _MatOptgroupMixinBase {
            constructor() {
                super(...arguments);
                /** Unique id for the underlying label. */
                this._labelId = `mat-optgroup-label-${_uniqueOptgroupIdCounter++}`;
            }
        }
        MatOptgroup.decorators = [
            { type: core.Component, args: [{
                        selector: 'mat-optgroup',
                        exportAs: 'matOptgroup',
                        template: "<label class=\"mat-optgroup-label\" [id]=\"_labelId\">{{ label }} <ng-content></ng-content></label>\n<ng-content select=\"mat-option, ng-container\"></ng-content>\n",
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        inputs: ['disabled'],
                        host: {
                            'class': 'mat-optgroup',
                            'role': 'group',
                            '[class.mat-optgroup-disabled]': 'disabled',
                            '[attr.aria-disabled]': 'disabled.toString()',
                            '[attr.aria-labelledby]': '_labelId',
                        },
                        styles: [".mat-optgroup-label{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;line-height:48px;height:48px;padding:0 16px;text-align:left;text-decoration:none;max-width:100%;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:default}.mat-optgroup-label[disabled]{cursor:default}[dir=rtl] .mat-optgroup-label{text-align:right}.mat-optgroup-label .mat-icon{margin-right:16px;vertical-align:middle}.mat-optgroup-label .mat-icon svg{vertical-align:top}[dir=rtl] .mat-optgroup-label .mat-icon{margin-left:16px;margin-right:0}\n"]
                    },] }
        ];
        MatOptgroup.propDecorators = {
            label: [{ type: core.Input }]
        };
        return MatOptgroup;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Option IDs need to be unique across components, so this counter exists outside of
     * the component definition.
     */
    let _uniqueIdCounter = 0;
    /** Event object emitted by MatOption when selected or deselected. */
    class MatOptionSelectionChange {
        constructor(
        /** Reference to the option that emitted the event. */
        source, 
        /** Whether the change in the option's value was a result of a user action. */
        isUserInput = false) {
            this.source = source;
            this.isUserInput = isUserInput;
        }
    }
    /**
     * Injection token used to provide the parent component to options.
     */
    const MAT_OPTION_PARENT_COMPONENT = new core.InjectionToken('MAT_OPTION_PARENT_COMPONENT');
    /**
     * Single option inside of a `<mat-select>` element.
     */
    let MatOption = /** @class */ (() => {
        class MatOption {
            constructor(_element, _changeDetectorRef, _parent, group) {
                this._element = _element;
                this._changeDetectorRef = _changeDetectorRef;
                this._parent = _parent;
                this.group = group;
                this._selected = false;
                this._active = false;
                this._disabled = false;
                this._mostRecentViewValue = '';
                /** The unique ID of the option. */
                this.id = `mat-option-${_uniqueIdCounter++}`;
                /** Event emitted when the option is selected or deselected. */
                // tslint:disable-next-line:no-output-on-prefix
                this.onSelectionChange = new core.EventEmitter();
                /** Emits when the state of the option changes and any parents have to be notified. */
                this._stateChanges = new rxjs.Subject();
            }
            /** Whether the wrapping component is in multiple selection mode. */
            get multiple() { return this._parent && this._parent.multiple; }
            /** Whether or not the option is currently selected. */
            get selected() { return this._selected; }
            /** Whether the option is disabled. */
            get disabled() { return (this.group && this.group.disabled) || this._disabled; }
            set disabled(value) { this._disabled = coerceBooleanProperty(value); }
            /** Whether ripples for the option are disabled. */
            get disableRipple() { return this._parent && this._parent.disableRipple; }
            /**
             * Whether or not the option is currently active and ready to be selected.
             * An active option displays styles as if it is focused, but the
             * focus is actually retained somewhere else. This comes in handy
             * for components like autocomplete where focus must remain on the input.
             */
            get active() {
                return this._active;
            }
            /**
             * The displayed value of the option. It is necessary to show the selected option in the
             * select's trigger.
             */
            get viewValue() {
                // TODO(kara): Add input property alternative for node envs.
                return (this._getHostElement().textContent || '').trim();
            }
            /** Selects the option. */
            select() {
                if (!this._selected) {
                    this._selected = true;
                    this._changeDetectorRef.markForCheck();
                    this._emitSelectionChangeEvent();
                }
            }
            /** Deselects the option. */
            deselect() {
                if (this._selected) {
                    this._selected = false;
                    this._changeDetectorRef.markForCheck();
                    this._emitSelectionChangeEvent();
                }
            }
            /** Sets focus onto this option. */
            focus(_origin, options) {
                // Note that we aren't using `_origin`, but we need to keep it because some internal consumers
                // use `MatOption` in a `FocusKeyManager` and we need it to match `FocusableOption`.
                const element = this._getHostElement();
                if (typeof element.focus === 'function') {
                    element.focus(options);
                }
            }
            /**
             * This method sets display styles on the option to make it appear
             * active. This is used by the ActiveDescendantKeyManager so key
             * events will display the proper options as active on arrow key events.
             */
            setActiveStyles() {
                if (!this._active) {
                    this._active = true;
                    this._changeDetectorRef.markForCheck();
                }
            }
            /**
             * This method removes display styles on the option that made it appear
             * active. This is used by the ActiveDescendantKeyManager so key
             * events will display the proper options as active on arrow key events.
             */
            setInactiveStyles() {
                if (this._active) {
                    this._active = false;
                    this._changeDetectorRef.markForCheck();
                }
            }
            /** Gets the label to be used when determining whether the option should be focused. */
            getLabel() {
                return this.viewValue;
            }
            /** Ensures the option is selected when activated from the keyboard. */
            _handleKeydown(event) {
                if ((event.keyCode === ENTER || event.keyCode === SPACE) && !hasModifierKey(event)) {
                    this._selectViaInteraction();
                    // Prevent the page from scrolling down and form submits.
                    event.preventDefault();
                }
            }
            /**
             * `Selects the option while indicating the selection came from the user. Used to
             * determine if the select's view -> model callback should be invoked.`
             */
            _selectViaInteraction() {
                if (!this.disabled) {
                    this._selected = this.multiple ? !this._selected : true;
                    this._changeDetectorRef.markForCheck();
                    this._emitSelectionChangeEvent(true);
                }
            }
            /**
             * Gets the `aria-selected` value for the option. We explicitly omit the `aria-selected`
             * attribute from single-selection, unselected options. Including the `aria-selected="false"`
             * attributes adds a significant amount of noise to screen-reader users without providing useful
             * information.
             */
            _getAriaSelected() {
                return this.selected || (this.multiple ? false : null);
            }
            /** Returns the correct tabindex for the option depending on disabled state. */
            _getTabIndex() {
                return this.disabled ? '-1' : '0';
            }
            /** Gets the host DOM element. */
            _getHostElement() {
                return this._element.nativeElement;
            }
            ngAfterViewChecked() {
                // Since parent components could be using the option's label to display the selected values
                // (e.g. `mat-select`) and they don't have a way of knowing if the option's label has changed
                // we have to check for changes in the DOM ourselves and dispatch an event. These checks are
                // relatively cheap, however we still limit them only to selected options in order to avoid
                // hitting the DOM too often.
                if (this._selected) {
                    const viewValue = this.viewValue;
                    if (viewValue !== this._mostRecentViewValue) {
                        this._mostRecentViewValue = viewValue;
                        this._stateChanges.next();
                    }
                }
            }
            ngOnDestroy() {
                this._stateChanges.complete();
            }
            /** Emits the selection change event. */
            _emitSelectionChangeEvent(isUserInput = false) {
                this.onSelectionChange.emit(new MatOptionSelectionChange(this, isUserInput));
            }
        }
        MatOption.decorators = [
            { type: core.Component, args: [{
                        selector: 'mat-option',
                        exportAs: 'matOption',
                        host: {
                            'role': 'option',
                            '[attr.tabindex]': '_getTabIndex()',
                            '[class.mat-selected]': 'selected',
                            '[class.mat-option-multiple]': 'multiple',
                            '[class.mat-active]': 'active',
                            '[id]': 'id',
                            '[attr.aria-selected]': '_getAriaSelected()',
                            '[attr.aria-disabled]': 'disabled.toString()',
                            '[class.mat-option-disabled]': 'disabled',
                            '(click)': '_selectViaInteraction()',
                            '(keydown)': '_handleKeydown($event)',
                            'class': 'mat-option mat-focus-indicator',
                        },
                        template: "<mat-pseudo-checkbox *ngIf=\"multiple\" class=\"mat-option-pseudo-checkbox\"\n    [state]=\"selected ? 'checked' : 'unchecked'\" [disabled]=\"disabled\"></mat-pseudo-checkbox>\n\n<span class=\"mat-option-text\"><ng-content></ng-content></span>\n\n<div class=\"mat-option-ripple\" mat-ripple\n     [matRippleTrigger]=\"_getHostElement()\"\n     [matRippleDisabled]=\"disabled || disableRipple\">\n</div>\n",
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        styles: [".mat-option{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;line-height:48px;height:48px;padding:0 16px;text-align:left;text-decoration:none;max-width:100%;position:relative;cursor:pointer;outline:none;display:flex;flex-direction:row;max-width:100%;box-sizing:border-box;align-items:center;-webkit-tap-highlight-color:transparent}.mat-option[disabled]{cursor:default}[dir=rtl] .mat-option{text-align:right}.mat-option .mat-icon{margin-right:16px;vertical-align:middle}.mat-option .mat-icon svg{vertical-align:top}[dir=rtl] .mat-option .mat-icon{margin-left:16px;margin-right:0}.mat-option[aria-disabled=true]{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:default}.mat-optgroup .mat-option:not(.mat-option-multiple){padding-left:32px}[dir=rtl] .mat-optgroup .mat-option:not(.mat-option-multiple){padding-left:16px;padding-right:32px}.cdk-high-contrast-active .mat-option{margin:0 1px}.cdk-high-contrast-active .mat-option.mat-active{border:solid 1px currentColor;margin:0}.mat-option-text{display:inline-block;flex-grow:1;overflow:hidden;text-overflow:ellipsis}.mat-option .mat-option-ripple{top:0;left:0;right:0;bottom:0;position:absolute;pointer-events:none}.cdk-high-contrast-active .mat-option .mat-option-ripple{opacity:.5}.mat-option-pseudo-checkbox{margin-right:8px}[dir=rtl] .mat-option-pseudo-checkbox{margin-left:8px;margin-right:0}\n"]
                    },] }
        ];
        MatOption.ctorParameters = () => [
            { type: core.ElementRef },
            { type: core.ChangeDetectorRef },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [MAT_OPTION_PARENT_COMPONENT,] }] },
            { type: MatOptgroup, decorators: [{ type: core.Optional }] }
        ];
        MatOption.propDecorators = {
            value: [{ type: core.Input }],
            id: [{ type: core.Input }],
            disabled: [{ type: core.Input }],
            onSelectionChange: [{ type: core.Output }]
        };
        return MatOption;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * InjectionToken that can be used to specify the global label options.
     * @deprecated Use `MAT_FORM_FIELD_DEFAULT_OPTIONS` injection token from
     *     `@angular/material/form-field` instead.
     * @breaking-change 11.0.0
     */
    const MAT_LABEL_GLOBAL_OPTIONS = new core.InjectionToken('mat-label-global-options');

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // Boilerplate for applying mixins to MatProgressBar.
    /** @docs-private */
    class MatProgressBarBase {
        constructor(_elementRef) {
            this._elementRef = _elementRef;
        }
    }
    const _MatProgressBarMixinBase = mixinColor(MatProgressBarBase, 'primary');
    /**
     * Injection token used to provide the current location to `MatProgressBar`.
     * Used to handle server-side rendering and to stub out during unit tests.
     * @docs-private
     */
    const MAT_PROGRESS_BAR_LOCATION = new core.InjectionToken('mat-progress-bar-location', { providedIn: 'root', factory: MAT_PROGRESS_BAR_LOCATION_FACTORY });
    /** @docs-private */
    function MAT_PROGRESS_BAR_LOCATION_FACTORY() {
        const _document = core.inject(common.DOCUMENT);
        const _location = _document ? _document.location : null;
        return {
            // Note that this needs to be a function, rather than a property, because Angular
            // will only resolve it once, but we want the current path on each call.
            getPathname: () => _location ? (_location.pathname + _location.search) : ''
        };
    }
    /** Counter used to generate unique IDs for progress bars. */
    let progressbarId = 0;
    /**
     * `<mat-progress-bar>` component.
     */
    let MatProgressBar = /** @class */ (() => {
        class MatProgressBar extends _MatProgressBarMixinBase {
            constructor(_elementRef, _ngZone, _animationMode, 
            /**
             * @deprecated `location` parameter to be made required.
             * @breaking-change 8.0.0
             */
            location) {
                super(_elementRef);
                this._elementRef = _elementRef;
                this._ngZone = _ngZone;
                this._animationMode = _animationMode;
                /** Flag that indicates whether NoopAnimations mode is set to true. */
                this._isNoopAnimation = false;
                this._value = 0;
                this._bufferValue = 0;
                /**
                 * Event emitted when animation of the primary progress bar completes. This event will not
                 * be emitted when animations are disabled, nor will it be emitted for modes with continuous
                 * animations (indeterminate and query).
                 */
                this.animationEnd = new core.EventEmitter();
                /** Reference to animation end subscription to be unsubscribed on destroy. */
                this._animationEndSubscription = rxjs.Subscription.EMPTY;
                /**
                 * Mode of the progress bar.
                 *
                 * Input must be one of these values: determinate, indeterminate, buffer, query, defaults to
                 * 'determinate'.
                 * Mirrored to mode attribute.
                 */
                this.mode = 'determinate';
                /** ID of the progress bar. */
                this.progressbarId = `mat-progress-bar-${progressbarId++}`;
                // We need to prefix the SVG reference with the current path, otherwise they won't work
                // in Safari if the page has a `<base>` tag. Note that we need quotes inside the `url()`,
                // because named route URLs can contain parentheses (see #12338). Also we don't use since
                // we can't tell the difference between whether
                // the consumer is using the hash location strategy or not, because `Location` normalizes
                // both `/#/foo/bar` and `/foo/bar` to the same thing.
                const path = location ? location.getPathname().split('#')[0] : '';
                this._rectangleFillValue = `url('${path}#${this.progressbarId}')`;
                this._isNoopAnimation = _animationMode === 'NoopAnimations';
            }
            /** Value of the progress bar. Defaults to zero. Mirrored to aria-valuenow. */
            get value() { return this._value; }
            set value(v) {
                this._value = clamp(coerceNumberProperty(v) || 0);
            }
            /** Buffer value of the progress bar. Defaults to zero. */
            get bufferValue() { return this._bufferValue; }
            set bufferValue(v) { this._bufferValue = clamp(v || 0); }
            /** Gets the current transform value for the progress bar's primary indicator. */
            _primaryTransform() {
                const scale = this.value / 100;
                return { transform: `scaleX(${scale})` };
            }
            /**
             * Gets the current transform value for the progress bar's buffer indicator. Only used if the
             * progress mode is set to buffer, otherwise returns an undefined, causing no transformation.
             */
            _bufferTransform() {
                if (this.mode === 'buffer') {
                    const scale = this.bufferValue / 100;
                    return { transform: `scaleX(${scale})` };
                }
                return null;
            }
            ngAfterViewInit() {
                // Run outside angular so change detection didn't get triggered on every transition end
                // instead only on the animation that we care about (primary value bar's transitionend)
                this._ngZone.runOutsideAngular((() => {
                    const element = this._primaryValueBar.nativeElement;
                    this._animationEndSubscription =
                        rxjs.fromEvent(element, 'transitionend')
                            .pipe(filter(((e) => e.target === element)))
                            .subscribe(() => {
                            if (this.mode === 'determinate' || this.mode === 'buffer') {
                                this._ngZone.run(() => this.animationEnd.next({ value: this.value }));
                            }
                        });
                }));
            }
            ngOnDestroy() {
                this._animationEndSubscription.unsubscribe();
            }
        }
        MatProgressBar.decorators = [
            { type: core.Component, args: [{
                        selector: 'mat-progress-bar',
                        exportAs: 'matProgressBar',
                        host: {
                            'role': 'progressbar',
                            'aria-valuemin': '0',
                            'aria-valuemax': '100',
                            '[attr.aria-valuenow]': '(mode === "indeterminate" || mode === "query") ? null : value',
                            '[attr.mode]': 'mode',
                            'class': 'mat-progress-bar',
                            '[class._mat-animation-noopable]': '_isNoopAnimation',
                        },
                        inputs: ['color'],
                        template: "<!--\n  The background div is named as such because it appears below the other divs and is not sized based\n  on values.\n-->\n<svg width=\"100%\" height=\"4\" focusable=\"false\" class=\"mat-progress-bar-background mat-progress-bar-element\">\n  <defs>\n    <pattern [id]=\"progressbarId\" x=\"4\" y=\"0\" width=\"8\" height=\"4\" patternUnits=\"userSpaceOnUse\">\n      <circle cx=\"2\" cy=\"2\" r=\"2\"/>\n    </pattern>\n  </defs>\n  <rect [attr.fill]=\"_rectangleFillValue\" width=\"100%\" height=\"100%\"/>\n</svg>\n<div class=\"mat-progress-bar-buffer mat-progress-bar-element\" [ngStyle]=\"_bufferTransform()\"></div>\n<div class=\"mat-progress-bar-primary mat-progress-bar-fill mat-progress-bar-element\" [ngStyle]=\"_primaryTransform()\" #primaryValueBar></div>\n<div class=\"mat-progress-bar-secondary mat-progress-bar-fill mat-progress-bar-element\"></div>\n",
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        styles: [".mat-progress-bar{display:block;height:4px;overflow:hidden;position:relative;transition:opacity 250ms linear;width:100%}._mat-animation-noopable.mat-progress-bar{transition:none;animation:none}.mat-progress-bar .mat-progress-bar-element,.mat-progress-bar .mat-progress-bar-fill::after{height:100%;position:absolute;width:100%}.mat-progress-bar .mat-progress-bar-background{width:calc(100% + 10px)}.cdk-high-contrast-active .mat-progress-bar .mat-progress-bar-background{display:none}.mat-progress-bar .mat-progress-bar-buffer{transform-origin:top left;transition:transform 250ms ease}.cdk-high-contrast-active .mat-progress-bar .mat-progress-bar-buffer{border-top:solid 5px;opacity:.5}.mat-progress-bar .mat-progress-bar-secondary{display:none}.mat-progress-bar .mat-progress-bar-fill{animation:none;transform-origin:top left;transition:transform 250ms ease}.cdk-high-contrast-active .mat-progress-bar .mat-progress-bar-fill{border-top:solid 4px}.mat-progress-bar .mat-progress-bar-fill::after{animation:none;content:\"\";display:inline-block;left:0}.mat-progress-bar[dir=rtl],[dir=rtl] .mat-progress-bar{transform:rotateY(180deg)}.mat-progress-bar[mode=query]{transform:rotateZ(180deg)}.mat-progress-bar[mode=query][dir=rtl],[dir=rtl] .mat-progress-bar[mode=query]{transform:rotateZ(180deg) rotateY(180deg)}.mat-progress-bar[mode=indeterminate] .mat-progress-bar-fill,.mat-progress-bar[mode=query] .mat-progress-bar-fill{transition:none}.mat-progress-bar[mode=indeterminate] .mat-progress-bar-primary,.mat-progress-bar[mode=query] .mat-progress-bar-primary{-webkit-backface-visibility:hidden;backface-visibility:hidden;animation:mat-progress-bar-primary-indeterminate-translate 2000ms infinite linear;left:-145.166611%}.mat-progress-bar[mode=indeterminate] .mat-progress-bar-primary.mat-progress-bar-fill::after,.mat-progress-bar[mode=query] .mat-progress-bar-primary.mat-progress-bar-fill::after{-webkit-backface-visibility:hidden;backface-visibility:hidden;animation:mat-progress-bar-primary-indeterminate-scale 2000ms infinite linear}.mat-progress-bar[mode=indeterminate] .mat-progress-bar-secondary,.mat-progress-bar[mode=query] .mat-progress-bar-secondary{-webkit-backface-visibility:hidden;backface-visibility:hidden;animation:mat-progress-bar-secondary-indeterminate-translate 2000ms infinite linear;left:-54.888891%;display:block}.mat-progress-bar[mode=indeterminate] .mat-progress-bar-secondary.mat-progress-bar-fill::after,.mat-progress-bar[mode=query] .mat-progress-bar-secondary.mat-progress-bar-fill::after{-webkit-backface-visibility:hidden;backface-visibility:hidden;animation:mat-progress-bar-secondary-indeterminate-scale 2000ms infinite linear}.mat-progress-bar[mode=buffer] .mat-progress-bar-background{-webkit-backface-visibility:hidden;backface-visibility:hidden;animation:mat-progress-bar-background-scroll 250ms infinite linear;display:block}.mat-progress-bar._mat-animation-noopable .mat-progress-bar-fill,.mat-progress-bar._mat-animation-noopable .mat-progress-bar-fill::after,.mat-progress-bar._mat-animation-noopable .mat-progress-bar-buffer,.mat-progress-bar._mat-animation-noopable .mat-progress-bar-primary,.mat-progress-bar._mat-animation-noopable .mat-progress-bar-primary.mat-progress-bar-fill::after,.mat-progress-bar._mat-animation-noopable .mat-progress-bar-secondary,.mat-progress-bar._mat-animation-noopable .mat-progress-bar-secondary.mat-progress-bar-fill::after,.mat-progress-bar._mat-animation-noopable .mat-progress-bar-background{animation:none;transition-duration:1ms}@keyframes mat-progress-bar-primary-indeterminate-translate{0%{transform:translateX(0)}20%{animation-timing-function:cubic-bezier(0.5, 0, 0.701732, 0.495819);transform:translateX(0)}59.15%{animation-timing-function:cubic-bezier(0.302435, 0.381352, 0.55, 0.956352);transform:translateX(83.67142%)}100%{transform:translateX(200.611057%)}}@keyframes mat-progress-bar-primary-indeterminate-scale{0%{transform:scaleX(0.08)}36.65%{animation-timing-function:cubic-bezier(0.334731, 0.12482, 0.785844, 1);transform:scaleX(0.08)}69.15%{animation-timing-function:cubic-bezier(0.06, 0.11, 0.6, 1);transform:scaleX(0.661479)}100%{transform:scaleX(0.08)}}@keyframes mat-progress-bar-secondary-indeterminate-translate{0%{animation-timing-function:cubic-bezier(0.15, 0, 0.515058, 0.409685);transform:translateX(0)}25%{animation-timing-function:cubic-bezier(0.31033, 0.284058, 0.8, 0.733712);transform:translateX(37.651913%)}48.35%{animation-timing-function:cubic-bezier(0.4, 0.627035, 0.6, 0.902026);transform:translateX(84.386165%)}100%{transform:translateX(160.277782%)}}@keyframes mat-progress-bar-secondary-indeterminate-scale{0%{animation-timing-function:cubic-bezier(0.15, 0, 0.515058, 0.409685);transform:scaleX(0.08)}19.15%{animation-timing-function:cubic-bezier(0.31033, 0.284058, 0.8, 0.733712);transform:scaleX(0.457104)}44.15%{animation-timing-function:cubic-bezier(0.4, 0.627035, 0.6, 0.902026);transform:scaleX(0.72796)}100%{transform:scaleX(0.08)}}@keyframes mat-progress-bar-background-scroll{to{transform:translateX(-8px)}}\n"]
                    },] }
        ];
        MatProgressBar.ctorParameters = () => [
            { type: core.ElementRef },
            { type: core.NgZone },
            { type: String, decorators: [{ type: core.Optional }, { type: core.Inject, args: [ANIMATION_MODULE_TYPE,] }] },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [MAT_PROGRESS_BAR_LOCATION,] }] }
        ];
        MatProgressBar.propDecorators = {
            value: [{ type: core.Input }],
            bufferValue: [{ type: core.Input }],
            _primaryValueBar: [{ type: core.ViewChild, args: ['primaryValueBar',] }],
            animationEnd: [{ type: core.Output }],
            mode: [{ type: core.Input }]
        };
        return MatProgressBar;
    })();
    /** Clamps a value to be between two numbers, by default 0 and 100. */
    function clamp(v, min = 0, max = 100) {
        return Math.max(min, Math.min(max, v));
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let MatProgressBarModule = /** @class */ (() => {
        class MatProgressBarModule {
        }
        MatProgressBarModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [common.CommonModule, MatCommonModule],
                        exports: [MatProgressBar, MatCommonModule],
                        declarations: [MatProgressBar],
                    },] }
        ];
        return MatProgressBarModule;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Content of a card, needed as it's used as a selector in the API.
     * @docs-private
     */
    let MatCardContent = /** @class */ (() => {
        class MatCardContent {
        }
        MatCardContent.decorators = [
            { type: core.Directive, args: [{
                        selector: 'mat-card-content, [mat-card-content], [matCardContent]',
                        host: { 'class': 'mat-card-content' }
                    },] }
        ];
        return MatCardContent;
    })();
    /**
     * Title of a card, needed as it's used as a selector in the API.
     * @docs-private
     */
    let MatCardTitle = /** @class */ (() => {
        class MatCardTitle {
        }
        MatCardTitle.decorators = [
            { type: core.Directive, args: [{
                        selector: `mat-card-title, [mat-card-title], [matCardTitle]`,
                        host: {
                            'class': 'mat-card-title'
                        }
                    },] }
        ];
        return MatCardTitle;
    })();
    /**
     * Sub-title of a card, needed as it's used as a selector in the API.
     * @docs-private
     */
    let MatCardSubtitle = /** @class */ (() => {
        class MatCardSubtitle {
        }
        MatCardSubtitle.decorators = [
            { type: core.Directive, args: [{
                        selector: `mat-card-subtitle, [mat-card-subtitle], [matCardSubtitle]`,
                        host: {
                            'class': 'mat-card-subtitle'
                        }
                    },] }
        ];
        return MatCardSubtitle;
    })();
    /**
     * Action section of a card, needed as it's used as a selector in the API.
     * @docs-private
     */
    let MatCardActions = /** @class */ (() => {
        class MatCardActions {
            constructor() {
                /** Position of the actions inside the card. */
                this.align = 'start';
            }
        }
        MatCardActions.decorators = [
            { type: core.Directive, args: [{
                        selector: 'mat-card-actions',
                        exportAs: 'matCardActions',
                        host: {
                            'class': 'mat-card-actions',
                            '[class.mat-card-actions-align-end]': 'align === "end"',
                        }
                    },] }
        ];
        MatCardActions.propDecorators = {
            align: [{ type: core.Input }]
        };
        return MatCardActions;
    })();
    /**
     * Footer of a card, needed as it's used as a selector in the API.
     * @docs-private
     */
    let MatCardFooter = /** @class */ (() => {
        class MatCardFooter {
        }
        MatCardFooter.decorators = [
            { type: core.Directive, args: [{
                        selector: 'mat-card-footer',
                        host: { 'class': 'mat-card-footer' }
                    },] }
        ];
        return MatCardFooter;
    })();
    /**
     * Image used in a card, needed to add the mat- CSS styling.
     * @docs-private
     */
    let MatCardImage = /** @class */ (() => {
        class MatCardImage {
        }
        MatCardImage.decorators = [
            { type: core.Directive, args: [{
                        selector: '[mat-card-image], [matCardImage]',
                        host: { 'class': 'mat-card-image' }
                    },] }
        ];
        return MatCardImage;
    })();
    /**
     * Image used in a card, needed to add the mat- CSS styling.
     * @docs-private
     */
    let MatCardSmImage = /** @class */ (() => {
        class MatCardSmImage {
        }
        MatCardSmImage.decorators = [
            { type: core.Directive, args: [{
                        selector: '[mat-card-sm-image], [matCardImageSmall]',
                        host: { 'class': 'mat-card-sm-image' }
                    },] }
        ];
        return MatCardSmImage;
    })();
    /**
     * Image used in a card, needed to add the mat- CSS styling.
     * @docs-private
     */
    let MatCardMdImage = /** @class */ (() => {
        class MatCardMdImage {
        }
        MatCardMdImage.decorators = [
            { type: core.Directive, args: [{
                        selector: '[mat-card-md-image], [matCardImageMedium]',
                        host: { 'class': 'mat-card-md-image' }
                    },] }
        ];
        return MatCardMdImage;
    })();
    /**
     * Image used in a card, needed to add the mat- CSS styling.
     * @docs-private
     */
    let MatCardLgImage = /** @class */ (() => {
        class MatCardLgImage {
        }
        MatCardLgImage.decorators = [
            { type: core.Directive, args: [{
                        selector: '[mat-card-lg-image], [matCardImageLarge]',
                        host: { 'class': 'mat-card-lg-image' }
                    },] }
        ];
        return MatCardLgImage;
    })();
    /**
     * Large image used in a card, needed to add the mat- CSS styling.
     * @docs-private
     */
    let MatCardXlImage = /** @class */ (() => {
        class MatCardXlImage {
        }
        MatCardXlImage.decorators = [
            { type: core.Directive, args: [{
                        selector: '[mat-card-xl-image], [matCardImageXLarge]',
                        host: { 'class': 'mat-card-xl-image' }
                    },] }
        ];
        return MatCardXlImage;
    })();
    /**
     * Avatar image used in a card, needed to add the mat- CSS styling.
     * @docs-private
     */
    let MatCardAvatar = /** @class */ (() => {
        class MatCardAvatar {
        }
        MatCardAvatar.decorators = [
            { type: core.Directive, args: [{
                        selector: '[mat-card-avatar], [matCardAvatar]',
                        host: { 'class': 'mat-card-avatar' }
                    },] }
        ];
        return MatCardAvatar;
    })();
    /**
     * A basic content container component that adds the styles of a Material design card.
     *
     * While this component can be used alone, it also provides a number
     * of preset styles for common card sections, including:
     * - mat-card-title
     * - mat-card-subtitle
     * - mat-card-content
     * - mat-card-actions
     * - mat-card-footer
     */
    let MatCard = /** @class */ (() => {
        class MatCard {
            // @breaking-change 9.0.0 `_animationMode` parameter to be made required.
            constructor(_animationMode) {
                this._animationMode = _animationMode;
            }
        }
        MatCard.decorators = [
            { type: core.Component, args: [{
                        selector: 'mat-card',
                        exportAs: 'matCard',
                        template: "<ng-content></ng-content>\n<ng-content select=\"mat-card-footer\"></ng-content>\n",
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        host: {
                            'class': 'mat-card mat-focus-indicator',
                            '[class._mat-animation-noopable]': '_animationMode === "NoopAnimations"',
                        },
                        styles: [".mat-card{transition:box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);display:block;position:relative;padding:16px;border-radius:4px}._mat-animation-noopable.mat-card{transition:none;animation:none}.mat-card .mat-divider-horizontal{position:absolute;left:0;width:100%}[dir=rtl] .mat-card .mat-divider-horizontal{left:auto;right:0}.mat-card .mat-divider-horizontal.mat-divider-inset{position:static;margin:0}[dir=rtl] .mat-card .mat-divider-horizontal.mat-divider-inset{margin-right:0}.cdk-high-contrast-active .mat-card{outline:solid 1px}.mat-card-actions,.mat-card-subtitle,.mat-card-content{display:block;margin-bottom:16px}.mat-card-title{display:block;margin-bottom:8px}.mat-card-actions{margin-left:-8px;margin-right:-8px;padding:8px 0}.mat-card-actions-align-end{display:flex;justify-content:flex-end}.mat-card-image{width:calc(100% + 32px);margin:0 -16px 16px -16px}.mat-card-footer{display:block;margin:0 -16px -16px -16px}.mat-card-actions .mat-button,.mat-card-actions .mat-raised-button,.mat-card-actions .mat-stroked-button{margin:0 8px}.mat-card-header{display:flex;flex-direction:row}.mat-card-header .mat-card-title{margin-bottom:12px}.mat-card-header-text{margin:0 16px}.mat-card-avatar{height:40px;width:40px;border-radius:50%;flex-shrink:0;object-fit:cover}.mat-card-title-group{display:flex;justify-content:space-between}.mat-card-sm-image{width:80px;height:80px}.mat-card-md-image{width:112px;height:112px}.mat-card-lg-image{width:152px;height:152px}.mat-card-xl-image{width:240px;height:240px;margin:-8px}.mat-card-title-group>.mat-card-xl-image{margin:-8px 0 8px}@media(max-width: 599px){.mat-card-title-group{margin:0}.mat-card-xl-image{margin-left:0;margin-right:0}}.mat-card>:first-child,.mat-card-content>:first-child{margin-top:0}.mat-card>:last-child:not(.mat-card-footer),.mat-card-content>:last-child:not(.mat-card-footer){margin-bottom:0}.mat-card-image:first-child{margin-top:-16px;border-top-left-radius:inherit;border-top-right-radius:inherit}.mat-card>.mat-card-actions:last-child{margin-bottom:-8px;padding-bottom:0}.mat-card-actions .mat-button:first-child,.mat-card-actions .mat-raised-button:first-child,.mat-card-actions .mat-stroked-button:first-child{margin-left:0;margin-right:0}.mat-card-title:not(:first-child),.mat-card-subtitle:not(:first-child){margin-top:-4px}.mat-card-header .mat-card-subtitle:not(:first-child){margin-top:-8px}.mat-card>.mat-card-xl-image:first-child{margin-top:-8px}.mat-card>.mat-card-xl-image:last-child{margin-bottom:-8px}\n"]
                    },] }
        ];
        MatCard.ctorParameters = () => [
            { type: String, decorators: [{ type: core.Optional }, { type: core.Inject, args: [ANIMATION_MODULE_TYPE,] }] }
        ];
        return MatCard;
    })();
    /**
     * Component intended to be used within the `<mat-card>` component. It adds styles for a
     * preset header section (i.e. a title, subtitle, and avatar layout).
     * @docs-private
     */
    let MatCardHeader = /** @class */ (() => {
        class MatCardHeader {
        }
        MatCardHeader.decorators = [
            { type: core.Component, args: [{
                        selector: 'mat-card-header',
                        template: "<ng-content select=\"[mat-card-avatar], [matCardAvatar]\"></ng-content>\n<div class=\"mat-card-header-text\">\n  <ng-content\n      select=\"mat-card-title, mat-card-subtitle,\n      [mat-card-title], [mat-card-subtitle],\n      [matCardTitle], [matCardSubtitle]\"></ng-content>\n</div>\n<ng-content></ng-content>\n",
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        host: { 'class': 'mat-card-header' }
                    },] }
        ];
        return MatCardHeader;
    })();
    /**
     * Component intended to be used within the `<mat-card>` component. It adds styles for a preset
     * layout that groups an image with a title section.
     * @docs-private
     */
    let MatCardTitleGroup = /** @class */ (() => {
        class MatCardTitleGroup {
        }
        MatCardTitleGroup.decorators = [
            { type: core.Component, args: [{
                        selector: 'mat-card-title-group',
                        template: "<div>\n  <ng-content\n      select=\"mat-card-title, mat-card-subtitle,\n      [mat-card-title], [mat-card-subtitle],\n      [matCardTitle], [matCardSubtitle]\"></ng-content>\n</div>\n<ng-content select=\"img\"></ng-content>\n<ng-content></ng-content>\n",
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        host: { 'class': 'mat-card-title-group' }
                    },] }
        ];
        return MatCardTitleGroup;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let MatCardModule = /** @class */ (() => {
        class MatCardModule {
        }
        MatCardModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [MatCommonModule],
                        exports: [
                            MatCard,
                            MatCardHeader,
                            MatCardTitleGroup,
                            MatCardContent,
                            MatCardTitle,
                            MatCardSubtitle,
                            MatCardActions,
                            MatCardFooter,
                            MatCardSmImage,
                            MatCardMdImage,
                            MatCardLgImage,
                            MatCardImage,
                            MatCardXlImage,
                            MatCardAvatar,
                            MatCommonModule,
                        ],
                        declarations: [
                            MatCard, MatCardHeader, MatCardTitleGroup, MatCardContent, MatCardTitle, MatCardSubtitle,
                            MatCardActions, MatCardFooter, MatCardSmImage, MatCardMdImage, MatCardLgImage, MatCardImage,
                            MatCardXlImage, MatCardAvatar,
                        ],
                    },] }
        ];
        return MatCardModule;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /** Default color palette for round buttons (mat-fab and mat-mini-fab) */
    const DEFAULT_ROUND_BUTTON_COLOR = 'accent';
    /**
     * List of classes to add to MatButton instances based on host attributes to
     * style as different variants.
     */
    const BUTTON_HOST_ATTRIBUTES = [
        'mat-button',
        'mat-flat-button',
        'mat-icon-button',
        'mat-raised-button',
        'mat-stroked-button',
        'mat-mini-fab',
        'mat-fab',
    ];
    // Boilerplate for applying mixins to MatButton.
    /** @docs-private */
    class MatButtonBase {
        constructor(_elementRef) {
            this._elementRef = _elementRef;
        }
    }
    const _MatButtonMixinBase = mixinColor(mixinDisabled(mixinDisableRipple(MatButtonBase)));
    /**
     * Material design button.
     */
    let MatButton = /** @class */ (() => {
        class MatButton extends _MatButtonMixinBase {
            constructor(elementRef, _focusMonitor, _animationMode) {
                super(elementRef);
                this._focusMonitor = _focusMonitor;
                this._animationMode = _animationMode;
                /** Whether the button is round. */
                this.isRoundButton = this._hasHostAttributes('mat-fab', 'mat-mini-fab');
                /** Whether the button is icon button. */
                this.isIconButton = this._hasHostAttributes('mat-icon-button');
                // For each of the variant selectors that is present in the button's host
                // attributes, add the correct corresponding class.
                for (const attr of BUTTON_HOST_ATTRIBUTES) {
                    if (this._hasHostAttributes(attr)) {
                        this._getHostElement().classList.add(attr);
                    }
                }
                // Add a class that applies to all buttons. This makes it easier to target if somebody
                // wants to target all Material buttons. We do it here rather than `host` to ensure that
                // the class is applied to derived classes.
                elementRef.nativeElement.classList.add('mat-button-base');
                if (this.isRoundButton) {
                    this.color = DEFAULT_ROUND_BUTTON_COLOR;
                }
            }
            ngAfterViewInit() {
                this._focusMonitor.monitor(this._elementRef, true);
            }
            ngOnDestroy() {
                this._focusMonitor.stopMonitoring(this._elementRef);
            }
            /** Focuses the button. */
            focus(origin = 'program', options) {
                this._focusMonitor.focusVia(this._getHostElement(), origin, options);
            }
            _getHostElement() {
                return this._elementRef.nativeElement;
            }
            _isRippleDisabled() {
                return this.disableRipple || this.disabled;
            }
            /** Gets whether the button has one of the given attributes. */
            _hasHostAttributes(...attributes) {
                return attributes.some(attribute => this._getHostElement().hasAttribute(attribute));
            }
        }
        MatButton.decorators = [
            { type: core.Component, args: [{
                        selector: `button[mat-button], button[mat-raised-button], button[mat-icon-button],
             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],
             button[mat-flat-button]`,
                        exportAs: 'matButton',
                        host: {
                            '[attr.disabled]': 'disabled || null',
                            '[class._mat-animation-noopable]': '_animationMode === "NoopAnimations"',
                            'class': 'mat-focus-indicator',
                        },
                        template: "<span class=\"mat-button-wrapper\"><ng-content></ng-content></span>\n<div matRipple class=\"mat-button-ripple\"\n     [class.mat-button-ripple-round]=\"isRoundButton || isIconButton\"\n     [matRippleDisabled]=\"_isRippleDisabled()\"\n     [matRippleCentered]=\"isIconButton\"\n     [matRippleTrigger]=\"_getHostElement()\"></div>\n<div class=\"mat-button-focus-overlay\"></div>\n",
                        inputs: ['disabled', 'disableRipple', 'color'],
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        styles: [".mat-button .mat-button-focus-overlay,.mat-icon-button .mat-button-focus-overlay{opacity:0}.mat-button:hover .mat-button-focus-overlay,.mat-stroked-button:hover .mat-button-focus-overlay{opacity:.04}@media(hover: none){.mat-button:hover .mat-button-focus-overlay,.mat-stroked-button:hover .mat-button-focus-overlay{opacity:0}}.mat-button,.mat-icon-button,.mat-stroked-button,.mat-flat-button{box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:none;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;border-radius:4px;overflow:visible}.mat-button::-moz-focus-inner,.mat-icon-button::-moz-focus-inner,.mat-stroked-button::-moz-focus-inner,.mat-flat-button::-moz-focus-inner{border:0}.mat-button[disabled],.mat-icon-button[disabled],.mat-stroked-button[disabled],.mat-flat-button[disabled]{cursor:default}.mat-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-button.cdk-program-focused .mat-button-focus-overlay,.mat-icon-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-icon-button.cdk-program-focused .mat-button-focus-overlay,.mat-stroked-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-stroked-button.cdk-program-focused .mat-button-focus-overlay,.mat-flat-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-flat-button.cdk-program-focused .mat-button-focus-overlay{opacity:.12}.mat-button::-moz-focus-inner,.mat-icon-button::-moz-focus-inner,.mat-stroked-button::-moz-focus-inner,.mat-flat-button::-moz-focus-inner{border:0}.mat-raised-button{box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:none;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;border-radius:4px;overflow:visible;transform:translate3d(0, 0, 0);transition:background 400ms cubic-bezier(0.25, 0.8, 0.25, 1),box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1)}.mat-raised-button::-moz-focus-inner{border:0}.mat-raised-button[disabled]{cursor:default}.mat-raised-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-raised-button.cdk-program-focused .mat-button-focus-overlay{opacity:.12}.mat-raised-button::-moz-focus-inner{border:0}._mat-animation-noopable.mat-raised-button{transition:none;animation:none}.mat-stroked-button{border:1px solid currentColor;padding:0 15px;line-height:34px}.mat-stroked-button .mat-button-ripple.mat-ripple,.mat-stroked-button .mat-button-focus-overlay{top:-1px;left:-1px;right:-1px;bottom:-1px}.mat-fab{box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:none;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;border-radius:4px;overflow:visible;transform:translate3d(0, 0, 0);transition:background 400ms cubic-bezier(0.25, 0.8, 0.25, 1),box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);min-width:0;border-radius:50%;width:56px;height:56px;padding:0;flex-shrink:0}.mat-fab::-moz-focus-inner{border:0}.mat-fab[disabled]{cursor:default}.mat-fab.cdk-keyboard-focused .mat-button-focus-overlay,.mat-fab.cdk-program-focused .mat-button-focus-overlay{opacity:.12}.mat-fab::-moz-focus-inner{border:0}._mat-animation-noopable.mat-fab{transition:none;animation:none}.mat-fab .mat-button-wrapper{padding:16px 0;display:inline-block;line-height:24px}.mat-mini-fab{box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:none;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;border-radius:4px;overflow:visible;transform:translate3d(0, 0, 0);transition:background 400ms cubic-bezier(0.25, 0.8, 0.25, 1),box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);min-width:0;border-radius:50%;width:40px;height:40px;padding:0;flex-shrink:0}.mat-mini-fab::-moz-focus-inner{border:0}.mat-mini-fab[disabled]{cursor:default}.mat-mini-fab.cdk-keyboard-focused .mat-button-focus-overlay,.mat-mini-fab.cdk-program-focused .mat-button-focus-overlay{opacity:.12}.mat-mini-fab::-moz-focus-inner{border:0}._mat-animation-noopable.mat-mini-fab{transition:none;animation:none}.mat-mini-fab .mat-button-wrapper{padding:8px 0;display:inline-block;line-height:24px}.mat-icon-button{padding:0;min-width:0;width:40px;height:40px;flex-shrink:0;line-height:40px;border-radius:50%}.mat-icon-button i,.mat-icon-button .mat-icon{line-height:24px}.mat-button-ripple.mat-ripple,.mat-button-focus-overlay{top:0;left:0;right:0;bottom:0;position:absolute;pointer-events:none;border-radius:inherit}.mat-button-ripple.mat-ripple:not(:empty){transform:translateZ(0)}.mat-button-focus-overlay{opacity:0;transition:opacity 200ms cubic-bezier(0.35, 0, 0.25, 1),background-color 200ms cubic-bezier(0.35, 0, 0.25, 1)}._mat-animation-noopable .mat-button-focus-overlay{transition:none}.cdk-high-contrast-active .mat-button-focus-overlay{background-color:#fff}.cdk-high-contrast-black-on-white .mat-button-focus-overlay{background-color:#000}.mat-button-ripple-round{border-radius:50%;z-index:1}.mat-button .mat-button-wrapper>*,.mat-flat-button .mat-button-wrapper>*,.mat-stroked-button .mat-button-wrapper>*,.mat-raised-button .mat-button-wrapper>*,.mat-icon-button .mat-button-wrapper>*,.mat-fab .mat-button-wrapper>*,.mat-mini-fab .mat-button-wrapper>*{vertical-align:middle}.mat-form-field:not(.mat-form-field-appearance-legacy) .mat-form-field-prefix .mat-icon-button,.mat-form-field:not(.mat-form-field-appearance-legacy) .mat-form-field-suffix .mat-icon-button{display:block;font-size:inherit;width:2.5em;height:2.5em}.cdk-high-contrast-active .mat-button,.cdk-high-contrast-active .mat-flat-button,.cdk-high-contrast-active .mat-raised-button,.cdk-high-contrast-active .mat-icon-button,.cdk-high-contrast-active .mat-fab,.cdk-high-contrast-active .mat-mini-fab{outline:solid 1px}\n"]
                    },] }
        ];
        MatButton.ctorParameters = () => [
            { type: core.ElementRef },
            { type: FocusMonitor },
            { type: String, decorators: [{ type: core.Optional }, { type: core.Inject, args: [ANIMATION_MODULE_TYPE,] }] }
        ];
        MatButton.propDecorators = {
            ripple: [{ type: core.ViewChild, args: [MatRipple,] }]
        };
        return MatButton;
    })();
    /**
     * Material design anchor button.
     */
    let MatAnchor = /** @class */ (() => {
        class MatAnchor extends MatButton {
            constructor(focusMonitor, elementRef, animationMode) {
                super(elementRef, focusMonitor, animationMode);
            }
            _haltDisabledEvents(event) {
                // A disabled button shouldn't apply any actions
                if (this.disabled) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                }
            }
        }
        MatAnchor.decorators = [
            { type: core.Component, args: [{
                        selector: `a[mat-button], a[mat-raised-button], a[mat-icon-button], a[mat-fab],
             a[mat-mini-fab], a[mat-stroked-button], a[mat-flat-button]`,
                        exportAs: 'matButton, matAnchor',
                        host: {
                            // Note that we ignore the user-specified tabindex when it's disabled for
                            // consistency with the `mat-button` applied on native buttons where even
                            // though they have an index, they're not tabbable.
                            '[attr.tabindex]': 'disabled ? -1 : (tabIndex || 0)',
                            '[attr.disabled]': 'disabled || null',
                            '[attr.aria-disabled]': 'disabled.toString()',
                            '(click)': '_haltDisabledEvents($event)',
                            '[class._mat-animation-noopable]': '_animationMode === "NoopAnimations"',
                            'class': 'mat-focus-indicator',
                        },
                        inputs: ['disabled', 'disableRipple', 'color'],
                        template: "<span class=\"mat-button-wrapper\"><ng-content></ng-content></span>\n<div matRipple class=\"mat-button-ripple\"\n     [class.mat-button-ripple-round]=\"isRoundButton || isIconButton\"\n     [matRippleDisabled]=\"_isRippleDisabled()\"\n     [matRippleCentered]=\"isIconButton\"\n     [matRippleTrigger]=\"_getHostElement()\"></div>\n<div class=\"mat-button-focus-overlay\"></div>\n",
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        styles: [".mat-button .mat-button-focus-overlay,.mat-icon-button .mat-button-focus-overlay{opacity:0}.mat-button:hover .mat-button-focus-overlay,.mat-stroked-button:hover .mat-button-focus-overlay{opacity:.04}@media(hover: none){.mat-button:hover .mat-button-focus-overlay,.mat-stroked-button:hover .mat-button-focus-overlay{opacity:0}}.mat-button,.mat-icon-button,.mat-stroked-button,.mat-flat-button{box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:none;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;border-radius:4px;overflow:visible}.mat-button::-moz-focus-inner,.mat-icon-button::-moz-focus-inner,.mat-stroked-button::-moz-focus-inner,.mat-flat-button::-moz-focus-inner{border:0}.mat-button[disabled],.mat-icon-button[disabled],.mat-stroked-button[disabled],.mat-flat-button[disabled]{cursor:default}.mat-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-button.cdk-program-focused .mat-button-focus-overlay,.mat-icon-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-icon-button.cdk-program-focused .mat-button-focus-overlay,.mat-stroked-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-stroked-button.cdk-program-focused .mat-button-focus-overlay,.mat-flat-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-flat-button.cdk-program-focused .mat-button-focus-overlay{opacity:.12}.mat-button::-moz-focus-inner,.mat-icon-button::-moz-focus-inner,.mat-stroked-button::-moz-focus-inner,.mat-flat-button::-moz-focus-inner{border:0}.mat-raised-button{box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:none;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;border-radius:4px;overflow:visible;transform:translate3d(0, 0, 0);transition:background 400ms cubic-bezier(0.25, 0.8, 0.25, 1),box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1)}.mat-raised-button::-moz-focus-inner{border:0}.mat-raised-button[disabled]{cursor:default}.mat-raised-button.cdk-keyboard-focused .mat-button-focus-overlay,.mat-raised-button.cdk-program-focused .mat-button-focus-overlay{opacity:.12}.mat-raised-button::-moz-focus-inner{border:0}._mat-animation-noopable.mat-raised-button{transition:none;animation:none}.mat-stroked-button{border:1px solid currentColor;padding:0 15px;line-height:34px}.mat-stroked-button .mat-button-ripple.mat-ripple,.mat-stroked-button .mat-button-focus-overlay{top:-1px;left:-1px;right:-1px;bottom:-1px}.mat-fab{box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:none;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;border-radius:4px;overflow:visible;transform:translate3d(0, 0, 0);transition:background 400ms cubic-bezier(0.25, 0.8, 0.25, 1),box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);min-width:0;border-radius:50%;width:56px;height:56px;padding:0;flex-shrink:0}.mat-fab::-moz-focus-inner{border:0}.mat-fab[disabled]{cursor:default}.mat-fab.cdk-keyboard-focused .mat-button-focus-overlay,.mat-fab.cdk-program-focused .mat-button-focus-overlay{opacity:.12}.mat-fab::-moz-focus-inner{border:0}._mat-animation-noopable.mat-fab{transition:none;animation:none}.mat-fab .mat-button-wrapper{padding:16px 0;display:inline-block;line-height:24px}.mat-mini-fab{box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:none;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;border-radius:4px;overflow:visible;transform:translate3d(0, 0, 0);transition:background 400ms cubic-bezier(0.25, 0.8, 0.25, 1),box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);min-width:0;border-radius:50%;width:40px;height:40px;padding:0;flex-shrink:0}.mat-mini-fab::-moz-focus-inner{border:0}.mat-mini-fab[disabled]{cursor:default}.mat-mini-fab.cdk-keyboard-focused .mat-button-focus-overlay,.mat-mini-fab.cdk-program-focused .mat-button-focus-overlay{opacity:.12}.mat-mini-fab::-moz-focus-inner{border:0}._mat-animation-noopable.mat-mini-fab{transition:none;animation:none}.mat-mini-fab .mat-button-wrapper{padding:8px 0;display:inline-block;line-height:24px}.mat-icon-button{padding:0;min-width:0;width:40px;height:40px;flex-shrink:0;line-height:40px;border-radius:50%}.mat-icon-button i,.mat-icon-button .mat-icon{line-height:24px}.mat-button-ripple.mat-ripple,.mat-button-focus-overlay{top:0;left:0;right:0;bottom:0;position:absolute;pointer-events:none;border-radius:inherit}.mat-button-ripple.mat-ripple:not(:empty){transform:translateZ(0)}.mat-button-focus-overlay{opacity:0;transition:opacity 200ms cubic-bezier(0.35, 0, 0.25, 1),background-color 200ms cubic-bezier(0.35, 0, 0.25, 1)}._mat-animation-noopable .mat-button-focus-overlay{transition:none}.cdk-high-contrast-active .mat-button-focus-overlay{background-color:#fff}.cdk-high-contrast-black-on-white .mat-button-focus-overlay{background-color:#000}.mat-button-ripple-round{border-radius:50%;z-index:1}.mat-button .mat-button-wrapper>*,.mat-flat-button .mat-button-wrapper>*,.mat-stroked-button .mat-button-wrapper>*,.mat-raised-button .mat-button-wrapper>*,.mat-icon-button .mat-button-wrapper>*,.mat-fab .mat-button-wrapper>*,.mat-mini-fab .mat-button-wrapper>*{vertical-align:middle}.mat-form-field:not(.mat-form-field-appearance-legacy) .mat-form-field-prefix .mat-icon-button,.mat-form-field:not(.mat-form-field-appearance-legacy) .mat-form-field-suffix .mat-icon-button{display:block;font-size:inherit;width:2.5em;height:2.5em}.cdk-high-contrast-active .mat-button,.cdk-high-contrast-active .mat-flat-button,.cdk-high-contrast-active .mat-raised-button,.cdk-high-contrast-active .mat-icon-button,.cdk-high-contrast-active .mat-fab,.cdk-high-contrast-active .mat-mini-fab{outline:solid 1px}\n"]
                    },] }
        ];
        MatAnchor.ctorParameters = () => [
            { type: FocusMonitor },
            { type: core.ElementRef },
            { type: String, decorators: [{ type: core.Optional }, { type: core.Inject, args: [ANIMATION_MODULE_TYPE,] }] }
        ];
        MatAnchor.propDecorators = {
            tabIndex: [{ type: core.Input }]
        };
        return MatAnchor;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let MatButtonModule = /** @class */ (() => {
        class MatButtonModule {
        }
        MatButtonModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            MatRippleModule,
                            MatCommonModule,
                        ],
                        exports: [
                            MatButton,
                            MatAnchor,
                            MatCommonModule,
                        ],
                        declarations: [
                            MatButton,
                            MatAnchor,
                        ],
                    },] }
        ];
        return MatButtonModule;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Returns an exception to be thrown in the case when attempting to
     * load an icon with a name that cannot be found.
     * @docs-private
     */
    function getMatIconNameNotFoundError(iconName) {
        return Error(`Unable to find icon with the name "${iconName}"`);
    }
    /**
     * Returns an exception to be thrown when the consumer attempts to use
     * `<mat-icon>` without including @angular/common/http.
     * @docs-private
     */
    function getMatIconNoHttpProviderError() {
        return Error('Could not find HttpClient provider for use with Angular Material icons. ' +
            'Please include the HttpClientModule from @angular/common/http in your ' +
            'app imports.');
    }
    /**
     * Returns an exception to be thrown when a URL couldn't be sanitized.
     * @param url URL that was attempted to be sanitized.
     * @docs-private
     */
    function getMatIconFailedToSanitizeUrlError(url) {
        return Error(`The URL provided to MatIconRegistry was not trusted as a resource URL ` +
            `via Angular's DomSanitizer. Attempted URL was "${url}".`);
    }
    /**
     * Returns an exception to be thrown when a HTML string couldn't be sanitized.
     * @param literal HTML that was attempted to be sanitized.
     * @docs-private
     */
    function getMatIconFailedToSanitizeLiteralError(literal) {
        return Error(`The literal provided to MatIconRegistry was not trusted as safe HTML by ` +
            `Angular's DomSanitizer. Attempted literal was "${literal}".`);
    }
    /**
     * Configuration for an icon, including the URL and possibly the cached SVG element.
     * @docs-private
     */
    class SvgIconConfig {
        constructor(data, options) {
            this.options = options;
            // Note that we can't use `instanceof SVGElement` here,
            // because it'll break during server-side rendering.
            if (!!data.nodeName) {
                this.svgElement = data;
            }
            else {
                this.url = data;
            }
        }
    }
    /**
     * Service to register and display icons used by the `<mat-icon>` component.
     * - Registers icon URLs by namespace and name.
     * - Registers icon set URLs by namespace.
     * - Registers aliases for CSS classes, for use with icon fonts.
     * - Loads icons from URLs and extracts individual icons from icon sets.
     */
    let MatIconRegistry = /** @class */ (() => {
        class MatIconRegistry {
            constructor(_httpClient, _sanitizer, document, _errorHandler) {
                this._httpClient = _httpClient;
                this._sanitizer = _sanitizer;
                this._errorHandler = _errorHandler;
                /**
                 * URLs and cached SVG elements for individual icons. Keys are of the format "[namespace]:[icon]".
                 */
                this._svgIconConfigs = new Map();
                /**
                 * SvgIconConfig objects and cached SVG elements for icon sets, keyed by namespace.
                 * Multiple icon sets can be registered under the same namespace.
                 */
                this._iconSetConfigs = new Map();
                /** Cache for icons loaded by direct URLs. */
                this._cachedIconsByUrl = new Map();
                /** In-progress icon fetches. Used to coalesce multiple requests to the same URL. */
                this._inProgressUrlFetches = new Map();
                /** Map from font identifiers to their CSS class names. Used for icon fonts. */
                this._fontCssClassesByAlias = new Map();
                /**
                 * The CSS class to apply when an `<mat-icon>` component has no icon name, url, or font specified.
                 * The default 'material-icons' value assumes that the material icon font has been loaded as
                 * described at http://google.github.io/material-design-icons/#icon-font-for-the-web
                 */
                this._defaultFontSetClass = 'material-icons';
                this._document = document;
            }
            /**
             * Registers an icon by URL in the default namespace.
             * @param iconName Name under which the icon should be registered.
             * @param url
             */
            addSvgIcon(iconName, url, options) {
                return this.addSvgIconInNamespace('', iconName, url, options);
            }
            /**
             * Registers an icon using an HTML string in the default namespace.
             * @param iconName Name under which the icon should be registered.
             * @param literal SVG source of the icon.
             */
            addSvgIconLiteral(iconName, literal, options) {
                return this.addSvgIconLiteralInNamespace('', iconName, literal, options);
            }
            /**
             * Registers an icon by URL in the specified namespace.
             * @param namespace Namespace in which the icon should be registered.
             * @param iconName Name under which the icon should be registered.
             * @param url
             */
            addSvgIconInNamespace(namespace, iconName, url, options) {
                return this._addSvgIconConfig(namespace, iconName, new SvgIconConfig(url, options));
            }
            /**
             * Registers an icon using an HTML string in the specified namespace.
             * @param namespace Namespace in which the icon should be registered.
             * @param iconName Name under which the icon should be registered.
             * @param literal SVG source of the icon.
             */
            addSvgIconLiteralInNamespace(namespace, iconName, literal, options) {
                const sanitizedLiteral = this._sanitizer.sanitize(core.SecurityContext.HTML, literal);
                if (!sanitizedLiteral) {
                    throw getMatIconFailedToSanitizeLiteralError(literal);
                }
                const svgElement = this._createSvgElementForSingleIcon(sanitizedLiteral, options);
                return this._addSvgIconConfig(namespace, iconName, new SvgIconConfig(svgElement, options));
            }
            /**
             * Registers an icon set by URL in the default namespace.
             * @param url
             */
            addSvgIconSet(url, options) {
                return this.addSvgIconSetInNamespace('', url, options);
            }
            /**
             * Registers an icon set using an HTML string in the default namespace.
             * @param literal SVG source of the icon set.
             */
            addSvgIconSetLiteral(literal, options) {
                return this.addSvgIconSetLiteralInNamespace('', literal, options);
            }
            /**
             * Registers an icon set by URL in the specified namespace.
             * @param namespace Namespace in which to register the icon set.
             * @param url
             */
            addSvgIconSetInNamespace(namespace, url, options) {
                return this._addSvgIconSetConfig(namespace, new SvgIconConfig(url, options));
            }
            /**
             * Registers an icon set using an HTML string in the specified namespace.
             * @param namespace Namespace in which to register the icon set.
             * @param literal SVG source of the icon set.
             */
            addSvgIconSetLiteralInNamespace(namespace, literal, options) {
                const sanitizedLiteral = this._sanitizer.sanitize(core.SecurityContext.HTML, literal);
                if (!sanitizedLiteral) {
                    throw getMatIconFailedToSanitizeLiteralError(literal);
                }
                const svgElement = this._svgElementFromString(sanitizedLiteral);
                return this._addSvgIconSetConfig(namespace, new SvgIconConfig(svgElement, options));
            }
            /**
             * Defines an alias for a CSS class name to be used for icon fonts. Creating an matIcon
             * component with the alias as the fontSet input will cause the class name to be applied
             * to the `<mat-icon>` element.
             *
             * @param alias Alias for the font.
             * @param className Class name override to be used instead of the alias.
             */
            registerFontClassAlias(alias, className = alias) {
                this._fontCssClassesByAlias.set(alias, className);
                return this;
            }
            /**
             * Returns the CSS class name associated with the alias by a previous call to
             * registerFontClassAlias. If no CSS class has been associated, returns the alias unmodified.
             */
            classNameForFontAlias(alias) {
                return this._fontCssClassesByAlias.get(alias) || alias;
            }
            /**
             * Sets the CSS class name to be used for icon fonts when an `<mat-icon>` component does not
             * have a fontSet input value, and is not loading an icon by name or URL.
             *
             * @param className
             */
            setDefaultFontSetClass(className) {
                this._defaultFontSetClass = className;
                return this;
            }
            /**
             * Returns the CSS class name to be used for icon fonts when an `<mat-icon>` component does not
             * have a fontSet input value, and is not loading an icon by name or URL.
             */
            getDefaultFontSetClass() {
                return this._defaultFontSetClass;
            }
            /**
             * Returns an Observable that produces the icon (as an `<svg>` DOM element) from the given URL.
             * The response from the URL may be cached so this will not always cause an HTTP request, but
             * the produced element will always be a new copy of the originally fetched icon. (That is,
             * it will not contain any modifications made to elements previously returned).
             *
             * @param safeUrl URL from which to fetch the SVG icon.
             */
            getSvgIconFromUrl(safeUrl) {
                const url = this._sanitizer.sanitize(core.SecurityContext.RESOURCE_URL, safeUrl);
                if (!url) {
                    throw getMatIconFailedToSanitizeUrlError(safeUrl);
                }
                const cachedIcon = this._cachedIconsByUrl.get(url);
                if (cachedIcon) {
                    return rxjs.of(cloneSvg(cachedIcon));
                }
                return this._loadSvgIconFromConfig(new SvgIconConfig(safeUrl)).pipe(tap(svg => this._cachedIconsByUrl.set(url, svg)), map(svg => cloneSvg(svg)));
            }
            /**
             * Returns an Observable that produces the icon (as an `<svg>` DOM element) with the given name
             * and namespace. The icon must have been previously registered with addIcon or addIconSet;
             * if not, the Observable will throw an error.
             *
             * @param name Name of the icon to be retrieved.
             * @param namespace Namespace in which to look for the icon.
             */
            getNamedSvgIcon(name, namespace = '') {
                // Return (copy of) cached icon if possible.
                const key = iconKey(namespace, name);
                const config = this._svgIconConfigs.get(key);
                if (config) {
                    return this._getSvgFromConfig(config);
                }
                // See if we have any icon sets registered for the namespace.
                const iconSetConfigs = this._iconSetConfigs.get(namespace);
                if (iconSetConfigs) {
                    return this._getSvgFromIconSetConfigs(name, iconSetConfigs);
                }
                return rxjs.throwError(getMatIconNameNotFoundError(key));
            }
            ngOnDestroy() {
                this._svgIconConfigs.clear();
                this._iconSetConfigs.clear();
                this._cachedIconsByUrl.clear();
            }
            /**
             * Returns the cached icon for a SvgIconConfig if available, or fetches it from its URL if not.
             */
            _getSvgFromConfig(config) {
                if (config.svgElement) {
                    // We already have the SVG element for this icon, return a copy.
                    return rxjs.of(cloneSvg(config.svgElement));
                }
                else {
                    // Fetch the icon from the config's URL, cache it, and return a copy.
                    return this._loadSvgIconFromConfig(config).pipe(tap(svg => config.svgElement = svg), map(svg => cloneSvg(svg)));
                }
            }
            /**
             * Attempts to find an icon with the specified name in any of the SVG icon sets.
             * First searches the available cached icons for a nested element with a matching name, and
             * if found copies the element to a new `<svg>` element. If not found, fetches all icon sets
             * that have not been cached, and searches again after all fetches are completed.
             * The returned Observable produces the SVG element if possible, and throws
             * an error if no icon with the specified name can be found.
             */
            _getSvgFromIconSetConfigs(name, iconSetConfigs) {
                // For all the icon set SVG elements we've fetched, see if any contain an icon with the
                // requested name.
                const namedIcon = this._extractIconWithNameFromAnySet(name, iconSetConfigs);
                if (namedIcon) {
                    // We could cache namedIcon in _svgIconConfigs, but since we have to make a copy every
                    // time anyway, there's probably not much advantage compared to just always extracting
                    // it from the icon set.
                    return rxjs.of(namedIcon);
                }
                // Not found in any cached icon sets. If there are icon sets with URLs that we haven't
                // fetched, fetch them now and look for iconName in the results.
                const iconSetFetchRequests = iconSetConfigs
                    .filter(iconSetConfig => !iconSetConfig.svgElement)
                    .map(iconSetConfig => {
                    return this._loadSvgIconSetFromConfig(iconSetConfig).pipe(catchError((err) => {
                        const url = this._sanitizer.sanitize(core.SecurityContext.RESOURCE_URL, iconSetConfig.url);
                        // Swallow errors fetching individual URLs so the
                        // combined Observable won't necessarily fail.
                        const errorMessage = `Loading icon set URL: ${url} failed: ${err.message}`;
                        this._errorHandler.handleError(new Error(errorMessage));
                        return rxjs.of(null);
                    }));
                });
                // Fetch all the icon set URLs. When the requests complete, every IconSet should have a
                // cached SVG element (unless the request failed), and we can check again for the icon.
                return rxjs.forkJoin(iconSetFetchRequests).pipe(map(() => {
                    const foundIcon = this._extractIconWithNameFromAnySet(name, iconSetConfigs);
                    if (!foundIcon) {
                        throw getMatIconNameNotFoundError(name);
                    }
                    return foundIcon;
                }));
            }
            /**
             * Searches the cached SVG elements for the given icon sets for a nested icon element whose "id"
             * tag matches the specified name. If found, copies the nested element to a new SVG element and
             * returns it. Returns null if no matching element is found.
             */
            _extractIconWithNameFromAnySet(iconName, iconSetConfigs) {
                // Iterate backwards, so icon sets added later have precedence.
                for (let i = iconSetConfigs.length - 1; i >= 0; i--) {
                    const config = iconSetConfigs[i];
                    if (config.svgElement) {
                        const foundIcon = this._extractSvgIconFromSet(config.svgElement, iconName, config.options);
                        if (foundIcon) {
                            return foundIcon;
                        }
                    }
                }
                return null;
            }
            /**
             * Loads the content of the icon URL specified in the SvgIconConfig and creates an SVG element
             * from it.
             */
            _loadSvgIconFromConfig(config) {
                return this._fetchIcon(config)
                    .pipe(map(svgText => this._createSvgElementForSingleIcon(svgText, config.options)));
            }
            /**
             * Loads the content of the icon set URL specified in the SvgIconConfig and creates an SVG element
             * from it.
             */
            _loadSvgIconSetFromConfig(config) {
                // If the SVG for this icon set has already been parsed, do nothing.
                if (config.svgElement) {
                    return rxjs.of(config.svgElement);
                }
                return this._fetchIcon(config).pipe(map(svgText => {
                    // It is possible that the icon set was parsed and cached by an earlier request, so parsing
                    // only needs to occur if the cache is yet unset.
                    if (!config.svgElement) {
                        config.svgElement = this._svgElementFromString(svgText);
                    }
                    return config.svgElement;
                }));
            }
            /**
             * Creates a DOM element from the given SVG string, and adds default attributes.
             */
            _createSvgElementForSingleIcon(responseText, options) {
                const svg = this._svgElementFromString(responseText);
                this._setSvgAttributes(svg, options);
                return svg;
            }
            /**
             * Searches the cached element of the given SvgIconConfig for a nested icon element whose "id"
             * tag matches the specified name. If found, copies the nested element to a new SVG element and
             * returns it. Returns null if no matching element is found.
             */
            _extractSvgIconFromSet(iconSet, iconName, options) {
                // Use the `id="iconName"` syntax in order to escape special
                // characters in the ID (versus using the #iconName syntax).
                const iconSource = iconSet.querySelector(`[id="${iconName}"]`);
                if (!iconSource) {
                    return null;
                }
                // Clone the element and remove the ID to prevent multiple elements from being added
                // to the page with the same ID.
                const iconElement = iconSource.cloneNode(true);
                iconElement.removeAttribute('id');
                // If the icon node is itself an <svg> node, clone and return it directly. If not, set it as
                // the content of a new <svg> node.
                if (iconElement.nodeName.toLowerCase() === 'svg') {
                    return this._setSvgAttributes(iconElement, options);
                }
                // If the node is a <symbol>, it won't be rendered so we have to convert it into <svg>. Note
                // that the same could be achieved by referring to it via <use href="#id">, however the <use>
                // tag is problematic on Firefox, because it needs to include the current page path.
                if (iconElement.nodeName.toLowerCase() === 'symbol') {
                    return this._setSvgAttributes(this._toSvgElement(iconElement), options);
                }
                // createElement('SVG') doesn't work as expected; the DOM ends up with
                // the correct nodes, but the SVG content doesn't render. Instead we
                // have to create an empty SVG node using innerHTML and append its content.
                // Elements created using DOMParser.parseFromString have the same problem.
                // http://stackoverflow.com/questions/23003278/svg-innerhtml-in-firefox-can-not-display
                const svg = this._svgElementFromString('<svg></svg>');
                // Clone the node so we don't remove it from the parent icon set element.
                svg.appendChild(iconElement);
                return this._setSvgAttributes(svg, options);
            }
            /**
             * Creates a DOM element from the given SVG string.
             */
            _svgElementFromString(str) {
                const div = this._document.createElement('DIV');
                div.innerHTML = str;
                const svg = div.querySelector('svg');
                if (!svg) {
                    throw Error('<svg> tag not found');
                }
                return svg;
            }
            /**
             * Converts an element into an SVG node by cloning all of its children.
             */
            _toSvgElement(element) {
                const svg = this._svgElementFromString('<svg></svg>');
                const attributes = element.attributes;
                // Copy over all the attributes from the `symbol` to the new SVG, except the id.
                for (let i = 0; i < attributes.length; i++) {
                    const { name, value } = attributes[i];
                    if (name !== 'id') {
                        svg.setAttribute(name, value);
                    }
                }
                for (let i = 0; i < element.childNodes.length; i++) {
                    if (element.childNodes[i].nodeType === this._document.ELEMENT_NODE) {
                        svg.appendChild(element.childNodes[i].cloneNode(true));
                    }
                }
                return svg;
            }
            /**
             * Sets the default attributes for an SVG element to be used as an icon.
             */
            _setSvgAttributes(svg, options) {
                svg.setAttribute('fit', '');
                svg.setAttribute('height', '100%');
                svg.setAttribute('width', '100%');
                svg.setAttribute('preserveAspectRatio', 'xMidYMid meet');
                svg.setAttribute('focusable', 'false'); // Disable IE11 default behavior to make SVGs focusable.
                if (options && options.viewBox) {
                    svg.setAttribute('viewBox', options.viewBox);
                }
                return svg;
            }
            /**
             * Returns an Observable which produces the string contents of the given icon. Results may be
             * cached, so future calls with the same URL may not cause another HTTP request.
             */
            _fetchIcon(iconConfig) {
                var _a;
                const { url: safeUrl, options } = iconConfig;
                const withCredentials = (_a = options === null || options === void 0 ? void 0 : options.withCredentials) !== null && _a !== void 0 ? _a : false;
                if (!this._httpClient) {
                    throw getMatIconNoHttpProviderError();
                }
                if (safeUrl == null) {
                    throw Error(`Cannot fetch icon from URL "${safeUrl}".`);
                }
                const url = this._sanitizer.sanitize(core.SecurityContext.RESOURCE_URL, safeUrl);
                if (!url) {
                    throw getMatIconFailedToSanitizeUrlError(safeUrl);
                }
                // Store in-progress fetches to avoid sending a duplicate request for a URL when there is
                // already a request in progress for that URL. It's necessary to call share() on the
                // Observable returned by http.get() so that multiple subscribers don't cause multiple XHRs.
                const inProgressFetch = this._inProgressUrlFetches.get(url);
                if (inProgressFetch) {
                    return inProgressFetch;
                }
                // TODO(jelbourn): for some reason, the `finalize` operator "loses" the generic type on the
                // Observable. Figure out why and fix it.
                const req = this._httpClient.get(url, { responseType: 'text', withCredentials }).pipe(finalize(() => this._inProgressUrlFetches.delete(url)), share());
                this._inProgressUrlFetches.set(url, req);
                return req;
            }
            /**
             * Registers an icon config by name in the specified namespace.
             * @param namespace Namespace in which to register the icon config.
             * @param iconName Name under which to register the config.
             * @param config Config to be registered.
             */
            _addSvgIconConfig(namespace, iconName, config) {
                this._svgIconConfigs.set(iconKey(namespace, iconName), config);
                return this;
            }
            /**
             * Registers an icon set config in the specified namespace.
             * @param namespace Namespace in which to register the icon config.
             * @param config Config to be registered.
             */
            _addSvgIconSetConfig(namespace, config) {
                const configNamespace = this._iconSetConfigs.get(namespace);
                if (configNamespace) {
                    configNamespace.push(config);
                }
                else {
                    this._iconSetConfigs.set(namespace, [config]);
                }
                return this;
            }
        }
        MatIconRegistry.ɵprov = core.ɵɵdefineInjectable({ factory: function MatIconRegistry_Factory() { return new MatIconRegistry(core.ɵɵinject(HttpClient, 8), core.ɵɵinject(platformBrowser.DomSanitizer), core.ɵɵinject(common.DOCUMENT, 8), core.ɵɵinject(core.ErrorHandler)); }, token: MatIconRegistry, providedIn: "root" });
        MatIconRegistry.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        MatIconRegistry.ctorParameters = () => [
            { type: HttpClient, decorators: [{ type: core.Optional }] },
            { type: platformBrowser.DomSanitizer },
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: core.ErrorHandler }
        ];
        return MatIconRegistry;
    })();
    /** @docs-private */
    function ICON_REGISTRY_PROVIDER_FACTORY(parentRegistry, httpClient, sanitizer, errorHandler, document) {
        return parentRegistry || new MatIconRegistry(httpClient, sanitizer, document, errorHandler);
    }
    /** @docs-private */
    const ICON_REGISTRY_PROVIDER = {
        // If there is already an MatIconRegistry available, use that. Otherwise, provide a new one.
        provide: MatIconRegistry,
        deps: [
            [new core.Optional(), new core.SkipSelf(), MatIconRegistry],
            [new core.Optional(), HttpClient],
            platformBrowser.DomSanitizer,
            core.ErrorHandler,
            [new core.Optional(), common.DOCUMENT],
        ],
        useFactory: ICON_REGISTRY_PROVIDER_FACTORY,
    };
    /** Clones an SVGElement while preserving type information. */
    function cloneSvg(svg) {
        return svg.cloneNode(true);
    }
    /** Returns the cache key to use for an icon namespace and name. */
    function iconKey(namespace, name) {
        return namespace + ':' + name;
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // Boilerplate for applying mixins to MatIcon.
    /** @docs-private */
    class MatIconBase {
        constructor(_elementRef) {
            this._elementRef = _elementRef;
        }
    }
    const _MatIconMixinBase = mixinColor(MatIconBase);
    /**
     * Injection token used to provide the current location to `MatIcon`.
     * Used to handle server-side rendering and to stub out during unit tests.
     * @docs-private
     */
    const MAT_ICON_LOCATION = new core.InjectionToken('mat-icon-location', {
        providedIn: 'root',
        factory: MAT_ICON_LOCATION_FACTORY
    });
    /** @docs-private */
    function MAT_ICON_LOCATION_FACTORY() {
        const _document = core.inject(common.DOCUMENT);
        const _location = _document ? _document.location : null;
        return {
            // Note that this needs to be a function, rather than a property, because Angular
            // will only resolve it once, but we want the current path on each call.
            getPathname: () => _location ? (_location.pathname + _location.search) : ''
        };
    }
    /** SVG attributes that accept a FuncIRI (e.g. `url(<something>)`). */
    const funcIriAttributes = [
        'clip-path',
        'color-profile',
        'src',
        'cursor',
        'fill',
        'filter',
        'marker',
        'marker-start',
        'marker-mid',
        'marker-end',
        'mask',
        'stroke'
    ];
    const ɵ0$1 = attr => `[${attr}]`;
    /** Selector that can be used to find all elements that are using a `FuncIRI`. */
    const funcIriAttributeSelector = funcIriAttributes.map(ɵ0$1).join(', ');
    /** Regex that can be used to extract the id out of a FuncIRI. */
    const funcIriPattern = /^url\(['"]?#(.*?)['"]?\)$/;
    /**
     * Component to display an icon. It can be used in the following ways:
     *
     * - Specify the svgIcon input to load an SVG icon from a URL previously registered with the
     *   addSvgIcon, addSvgIconInNamespace, addSvgIconSet, or addSvgIconSetInNamespace methods of
     *   MatIconRegistry. If the svgIcon value contains a colon it is assumed to be in the format
     *   "[namespace]:[name]", if not the value will be the name of an icon in the default namespace.
     *   Examples:
     *     `<mat-icon svgIcon="left-arrow"></mat-icon>
     *     <mat-icon svgIcon="animals:cat"></mat-icon>`
     *
     * - Use a font ligature as an icon by putting the ligature text in the content of the `<mat-icon>`
     *   component. By default the Material icons font is used as described at
     *   http://google.github.io/material-design-icons/#icon-font-for-the-web. You can specify an
     *   alternate font by setting the fontSet input to either the CSS class to apply to use the
     *   desired font, or to an alias previously registered with MatIconRegistry.registerFontClassAlias.
     *   Examples:
     *     `<mat-icon>home</mat-icon>
     *     <mat-icon fontSet="myfont">sun</mat-icon>`
     *
     * - Specify a font glyph to be included via CSS rules by setting the fontSet input to specify the
     *   font, and the fontIcon input to specify the icon. Typically the fontIcon will specify a
     *   CSS class which causes the glyph to be displayed via a :before selector, as in
     *   https://fortawesome.github.io/Font-Awesome/examples/
     *   Example:
     *     `<mat-icon fontSet="fa" fontIcon="alarm"></mat-icon>`
     */
    let MatIcon = /** @class */ (() => {
        class MatIcon extends _MatIconMixinBase {
            constructor(elementRef, _iconRegistry, ariaHidden, _location, _errorHandler) {
                super(elementRef);
                this._iconRegistry = _iconRegistry;
                this._location = _location;
                this._errorHandler = _errorHandler;
                this._inline = false;
                /** Subscription to the current in-progress SVG icon request. */
                this._currentIconFetch = rxjs.Subscription.EMPTY;
                // If the user has not explicitly set aria-hidden, mark the icon as hidden, as this is
                // the right thing to do for the majority of icon use-cases.
                if (!ariaHidden) {
                    elementRef.nativeElement.setAttribute('aria-hidden', 'true');
                }
            }
            /**
             * Whether the icon should be inlined, automatically sizing the icon to match the font size of
             * the element the icon is contained in.
             */
            get inline() {
                return this._inline;
            }
            set inline(inline) {
                this._inline = coerceBooleanProperty(inline);
            }
            /** Font set that the icon is a part of. */
            get fontSet() { return this._fontSet; }
            set fontSet(value) {
                this._fontSet = this._cleanupFontValue(value);
            }
            /** Name of an icon within a font set. */
            get fontIcon() { return this._fontIcon; }
            set fontIcon(value) {
                this._fontIcon = this._cleanupFontValue(value);
            }
            /**
             * Splits an svgIcon binding value into its icon set and icon name components.
             * Returns a 2-element array of [(icon set), (icon name)].
             * The separator for the two fields is ':'. If there is no separator, an empty
             * string is returned for the icon set and the entire value is returned for
             * the icon name. If the argument is falsy, returns an array of two empty strings.
             * Throws an error if the name contains two or more ':' separators.
             * Examples:
             *   `'social:cake' -> ['social', 'cake']
             *   'penguin' -> ['', 'penguin']
             *   null -> ['', '']
             *   'a:b:c' -> (throws Error)`
             */
            _splitIconName(iconName) {
                if (!iconName) {
                    return ['', ''];
                }
                const parts = iconName.split(':');
                switch (parts.length) {
                    case 1: return ['', parts[0]]; // Use default namespace.
                    case 2: return parts;
                    default: throw Error(`Invalid icon name: "${iconName}"`);
                }
            }
            ngOnChanges(changes) {
                // Only update the inline SVG icon if the inputs changed, to avoid unnecessary DOM operations.
                const svgIconChanges = changes['svgIcon'];
                if (svgIconChanges) {
                    this._currentIconFetch.unsubscribe();
                    if (this.svgIcon) {
                        const [namespace, iconName] = this._splitIconName(this.svgIcon);
                        this._currentIconFetch = this._iconRegistry.getNamedSvgIcon(iconName, namespace)
                            .pipe(take(1))
                            .subscribe(svg => this._setSvgElement(svg), (err) => {
                            const errorMessage = `Error retrieving icon ${namespace}:${iconName}! ${err.message}`;
                            this._errorHandler.handleError(new Error(errorMessage));
                        });
                    }
                    else if (svgIconChanges.previousValue) {
                        this._clearSvgElement();
                    }
                }
                if (this._usingFontIcon()) {
                    this._updateFontIconClasses();
                }
            }
            ngOnInit() {
                // Update font classes because ngOnChanges won't be called if none of the inputs are present,
                // e.g. <mat-icon>arrow</mat-icon> In this case we need to add a CSS class for the default font.
                if (this._usingFontIcon()) {
                    this._updateFontIconClasses();
                }
            }
            ngAfterViewChecked() {
                const cachedElements = this._elementsWithExternalReferences;
                if (cachedElements && cachedElements.size) {
                    const newPath = this._location.getPathname();
                    // We need to check whether the URL has changed on each change detection since
                    // the browser doesn't have an API that will let us react on link clicks and
                    // we can't depend on the Angular router. The references need to be updated,
                    // because while most browsers don't care whether the URL is correct after
                    // the first render, Safari will break if the user navigates to a different
                    // page and the SVG isn't re-rendered.
                    if (newPath !== this._previousPath) {
                        this._previousPath = newPath;
                        this._prependPathToReferences(newPath);
                    }
                }
            }
            ngOnDestroy() {
                this._currentIconFetch.unsubscribe();
                if (this._elementsWithExternalReferences) {
                    this._elementsWithExternalReferences.clear();
                }
            }
            _usingFontIcon() {
                return !this.svgIcon;
            }
            _setSvgElement(svg) {
                this._clearSvgElement();
                // Workaround for IE11 and Edge ignoring `style` tags inside dynamically-created SVGs.
                // See: https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/10898469/
                // Do this before inserting the element into the DOM, in order to avoid a style recalculation.
                const styleTags = svg.querySelectorAll('style');
                for (let i = 0; i < styleTags.length; i++) {
                    styleTags[i].textContent += ' ';
                }
                // Note: we do this fix here, rather than the icon registry, because the
                // references have to point to the URL at the time that the icon was created.
                const path = this._location.getPathname();
                this._previousPath = path;
                this._cacheChildrenWithExternalReferences(svg);
                this._prependPathToReferences(path);
                this._elementRef.nativeElement.appendChild(svg);
            }
            _clearSvgElement() {
                const layoutElement = this._elementRef.nativeElement;
                let childCount = layoutElement.childNodes.length;
                if (this._elementsWithExternalReferences) {
                    this._elementsWithExternalReferences.clear();
                }
                // Remove existing non-element child nodes and SVGs, and add the new SVG element. Note that
                // we can't use innerHTML, because IE will throw if the element has a data binding.
                while (childCount--) {
                    const child = layoutElement.childNodes[childCount];
                    // 1 corresponds to Node.ELEMENT_NODE. We remove all non-element nodes in order to get rid
                    // of any loose text nodes, as well as any SVG elements in order to remove any old icons.
                    if (child.nodeType !== 1 || child.nodeName.toLowerCase() === 'svg') {
                        layoutElement.removeChild(child);
                    }
                }
            }
            _updateFontIconClasses() {
                if (!this._usingFontIcon()) {
                    return;
                }
                const elem = this._elementRef.nativeElement;
                const fontSetClass = this.fontSet ?
                    this._iconRegistry.classNameForFontAlias(this.fontSet) :
                    this._iconRegistry.getDefaultFontSetClass();
                if (fontSetClass != this._previousFontSetClass) {
                    if (this._previousFontSetClass) {
                        elem.classList.remove(this._previousFontSetClass);
                    }
                    if (fontSetClass) {
                        elem.classList.add(fontSetClass);
                    }
                    this._previousFontSetClass = fontSetClass;
                }
                if (this.fontIcon != this._previousFontIconClass) {
                    if (this._previousFontIconClass) {
                        elem.classList.remove(this._previousFontIconClass);
                    }
                    if (this.fontIcon) {
                        elem.classList.add(this.fontIcon);
                    }
                    this._previousFontIconClass = this.fontIcon;
                }
            }
            /**
             * Cleans up a value to be used as a fontIcon or fontSet.
             * Since the value ends up being assigned as a CSS class, we
             * have to trim the value and omit space-separated values.
             */
            _cleanupFontValue(value) {
                return typeof value === 'string' ? value.trim().split(' ')[0] : value;
            }
            /**
             * Prepends the current path to all elements that have an attribute pointing to a `FuncIRI`
             * reference. This is required because WebKit browsers require references to be prefixed with
             * the current path, if the page has a `base` tag.
             */
            _prependPathToReferences(path) {
                const elements = this._elementsWithExternalReferences;
                if (elements) {
                    elements.forEach((attrs, element) => {
                        attrs.forEach(attr => {
                            element.setAttribute(attr.name, `url('${path}#${attr.value}')`);
                        });
                    });
                }
            }
            /**
             * Caches the children of an SVG element that have `url()`
             * references that we need to prefix with the current path.
             */
            _cacheChildrenWithExternalReferences(element) {
                const elementsWithFuncIri = element.querySelectorAll(funcIriAttributeSelector);
                const elements = this._elementsWithExternalReferences =
                    this._elementsWithExternalReferences || new Map();
                for (let i = 0; i < elementsWithFuncIri.length; i++) {
                    funcIriAttributes.forEach(attr => {
                        const elementWithReference = elementsWithFuncIri[i];
                        const value = elementWithReference.getAttribute(attr);
                        const match = value ? value.match(funcIriPattern) : null;
                        if (match) {
                            let attributes = elements.get(elementWithReference);
                            if (!attributes) {
                                attributes = [];
                                elements.set(elementWithReference, attributes);
                            }
                            attributes.push({ name: attr, value: match[1] });
                        }
                    });
                }
            }
        }
        MatIcon.decorators = [
            { type: core.Component, args: [{
                        template: '<ng-content></ng-content>',
                        selector: 'mat-icon',
                        exportAs: 'matIcon',
                        inputs: ['color'],
                        host: {
                            'role': 'img',
                            'class': 'mat-icon notranslate',
                            '[class.mat-icon-inline]': 'inline',
                            '[class.mat-icon-no-color]': 'color !== "primary" && color !== "accent" && color !== "warn"',
                        },
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        styles: [".mat-icon{background-repeat:no-repeat;display:inline-block;fill:currentColor;height:24px;width:24px}.mat-icon.mat-icon-inline{font-size:inherit;height:inherit;line-height:inherit;width:inherit}[dir=rtl] .mat-icon-rtl-mirror{transform:scale(-1, 1)}.mat-form-field:not(.mat-form-field-appearance-legacy) .mat-form-field-prefix .mat-icon,.mat-form-field:not(.mat-form-field-appearance-legacy) .mat-form-field-suffix .mat-icon{display:block}.mat-form-field:not(.mat-form-field-appearance-legacy) .mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field:not(.mat-form-field-appearance-legacy) .mat-form-field-suffix .mat-icon-button .mat-icon{margin:auto}\n"]
                    },] }
        ];
        MatIcon.ctorParameters = () => [
            { type: core.ElementRef },
            { type: MatIconRegistry },
            { type: String, decorators: [{ type: core.Attribute, args: ['aria-hidden',] }] },
            { type: undefined, decorators: [{ type: core.Inject, args: [MAT_ICON_LOCATION,] }] },
            { type: core.ErrorHandler }
        ];
        MatIcon.propDecorators = {
            inline: [{ type: core.Input }],
            svgIcon: [{ type: core.Input }],
            fontSet: [{ type: core.Input }],
            fontIcon: [{ type: core.Input }]
        };
        return MatIcon;
    })();

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    let MatIconModule = /** @class */ (() => {
        class MatIconModule {
        }
        MatIconModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [MatCommonModule],
                        exports: [MatIcon, MatCommonModule],
                        declarations: [MatIcon],
                    },] }
        ];
        return MatIconModule;
    })();

    var MatFileUploadModule = /** @class */ (function () {
        function MatFileUploadModule() {
        }
        MatFileUploadModule = __decorate([
            core.NgModule({
                imports: [
                    MatButtonModule,
                    MatProgressBarModule,
                    MatIconModule,
                    MatCardModule,
                    HttpClientModule,
                    common.CommonModule
                ],
                declarations: [
                    MatFileUpload,
                    MatFileUploadQueue,
                    FileUploadInputFor,
                    BytesPipe
                ],
                exports: [
                    MatFileUpload,
                    MatFileUploadQueue,
                    FileUploadInputFor,
                    BytesPipe
                ]
            })
        ], MatFileUploadModule);
        return MatFileUploadModule;
    }());

    exports.BytesPipe = BytesPipe;
    exports.FileUploadInputFor = FileUploadInputFor;
    exports.MatFileUpload = MatFileUpload;
    exports.MatFileUploadModule = MatFileUploadModule;
    exports.MatFileUploadQueue = MatFileUploadQueue;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=matFileUpload.umd.js.map
