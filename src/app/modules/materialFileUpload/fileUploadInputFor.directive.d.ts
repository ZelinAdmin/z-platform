import { ElementRef, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * A material design file upload queue component.
 */
export declare class FileUploadInputFor {
    private element;
    private _queue;
    private _element;
    onFileSelected: EventEmitter<File[]>;
    constructor(element: ElementRef);
    set fileUploadQueue(value: any);
    onChange(): any;
    onDrop(event: any): any;
    onDropOver(event: any): any;
    static ɵfac: i0.ɵɵFactoryDef<FileUploadInputFor, never>;
    static ɵdir: i0.ɵɵDirectiveDefWithMeta<FileUploadInputFor, "input[fileUploadInputFor], div[fileUploadInputFor]", never, { "fileUploadQueue": "fileUploadInputFor"; }, { "onFileSelected": "onFileSelected"; }, never>;
}
