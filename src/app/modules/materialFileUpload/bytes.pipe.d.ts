import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class BytesPipe implements PipeTransform {
    transform(bytes: number): string;
    static ɵfac: i0.ɵɵFactoryDef<BytesPipe, never>;
    static ɵpipe: i0.ɵɵPipeDefWithMeta<BytesPipe, "bytes">;
}
