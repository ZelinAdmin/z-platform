import { Component, QueryList, Input, ContentChildren, forwardRef } from '@angular/core';
import { MatFileUpload } from './../matFileUpload/matFileUpload.component';
import { merge } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/material/button";
function MatFileUploadQueue_button_2_Template(rf, ctx) { if (rf & 1) {
    var _r3 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 1);
    i0.ɵɵlistener("click", function MatFileUploadQueue_button_2_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r3); var ctx_r2 = i0.ɵɵnextContext(); return ctx_r2.uploadAll(); });
    i0.ɵɵtext(1, "Upload All");
    i0.ɵɵelementEnd();
} }
function MatFileUploadQueue_button_3_Template(rf, ctx) { if (rf & 1) {
    var _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 1);
    i0.ɵɵlistener("click", function MatFileUploadQueue_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r5); var ctx_r4 = i0.ɵɵnextContext(); return ctx_r4.removeAll(); });
    i0.ɵɵtext(1, "Remove All");
    i0.ɵɵelementEnd();
} }
var _c0 = ["*"];
/**
 * A material design file upload queue component.
 */
var MatFileUploadQueue = /** @class */ (function () {
    function MatFileUploadQueue() {
        this.files = [];
        this.httpRequestHeaders = new HttpHeaders();
        this.httpRequestParams = new HttpParams();
        this.fileAlias = "file";
    }
    Object.defineProperty(MatFileUploadQueue.prototype, "fileUploadRemoveEvents", {
        /** Combined stream of all of the file upload remove change events. */
        get: function () {
            return merge.apply(void 0, this.fileUploads.map(function (fileUpload) { return fileUpload.removeEvent; }));
        },
        enumerable: false,
        configurable: true
    });
    MatFileUploadQueue.prototype.ngAfterViewInit = function () {
        var _this = this;
        // When the list changes, re-subscribe
        this._changeSubscription = this.fileUploads.changes.pipe(startWith(null)).subscribe(function () {
            if (_this._fileRemoveSubscription) {
                _this._fileRemoveSubscription.unsubscribe();
            }
            _this._listenTofileRemoved();
        });
    };
    MatFileUploadQueue.prototype._listenTofileRemoved = function () {
        var _this = this;
        this._fileRemoveSubscription = this.fileUploadRemoveEvents.subscribe(function (event) {
            _this.files.splice(event.id, 1);
        });
    };
    MatFileUploadQueue.prototype.add = function (file) {
        this.files.push(file);
    };
    MatFileUploadQueue.prototype.uploadAll = function () {
        this.fileUploads.forEach(function (fileUpload) { fileUpload.upload(); });
    };
    MatFileUploadQueue.prototype.removeAll = function () {
        this.files.splice(0, this.files.length);
    };
    MatFileUploadQueue.prototype.ngOnDestroy = function () {
        if (this.files) {
            this.removeAll();
        }
    };
    MatFileUploadQueue.ɵfac = function MatFileUploadQueue_Factory(t) { return new (t || MatFileUploadQueue)(); };
    MatFileUploadQueue.ɵcmp = i0.ɵɵdefineComponent({ type: MatFileUploadQueue, selectors: [["mat-file-upload-queue"]], contentQueries: function MatFileUploadQueue_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuery(dirIndex, MatFileUpload, false);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.fileUploads = _t);
        } }, inputs: { httpUrl: "httpUrl", httpRequestHeaders: "httpRequestHeaders", httpRequestParams: "httpRequestParams", fileAlias: "fileAlias" }, exportAs: ["matFileUploadQueue"], ngContentSelectors: _c0, decls: 4, vars: 2, consts: [["mat-raised-button", "", "color", "primary", 3, "click", 4, "ngIf"], ["mat-raised-button", "", "color", "primary", 3, "click"]], template: function MatFileUploadQueue_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵprojectionDef();
            i0.ɵɵprojection(0);
            i0.ɵɵelement(1, "br");
            i0.ɵɵtemplate(2, MatFileUploadQueue_button_2_Template, 2, 0, "button", 0);
            i0.ɵɵtemplate(3, MatFileUploadQueue_button_3_Template, 2, 0, "button", 0);
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.files.length > 0);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.files.length > 0);
        } }, directives: [i1.NgIf, i2.MatButton], encapsulation: 2 });
    return MatFileUploadQueue;
}());
export { MatFileUploadQueue };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MatFileUploadQueue, [{
        type: Component,
        args: [{
                selector: 'mat-file-upload-queue',
                templateUrl: "matFileUploadQueue.component.html",
                exportAs: 'matFileUploadQueue',
            }]
    }], null, { fileUploads: [{
            type: ContentChildren,
            args: [forwardRef(function () { return MatFileUpload; })]
        }], httpUrl: [{
            type: Input
        }], httpRequestHeaders: [{
            type: Input
        }], httpRequestParams: [{
            type: Input
        }], fileAlias: [{
            type: Input
        }] }); })();
//# sourceMappingURL=matFileUploadQueue.component.js.map