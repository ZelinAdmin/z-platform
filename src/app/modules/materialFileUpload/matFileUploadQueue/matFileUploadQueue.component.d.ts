import { OnDestroy, QueryList } from '@angular/core';
import { MatFileUpload } from './../matFileUpload/matFileUpload.component';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import * as i0 from "@angular/core";
/**
 * A material design file upload queue component.
 */
export declare class MatFileUploadQueue implements OnDestroy {
    fileUploads: QueryList<MatFileUpload>;
    /** Subscription to remove changes in files. */
    private _fileRemoveSubscription;
    /** Subscription to changes in the files. */
    private _changeSubscription;
    /** Combined stream of all of the file upload remove change events. */
    get fileUploadRemoveEvents(): Observable<MatFileUpload>;
    files: Array<any>;
    httpUrl: string;
    httpRequestHeaders: HttpHeaders | {
        [header: string]: string | string[];
    };
    httpRequestParams: HttpParams | {
        [param: string]: string | string[];
    };
    fileAlias: string;
    ngAfterViewInit(): void;
    private _listenTofileRemoved;
    add(file: any): void;
    uploadAll(): void;
    removeAll(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<MatFileUploadQueue, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<MatFileUploadQueue, "mat-file-upload-queue", ["matFileUploadQueue"], { "httpUrl": "httpUrl"; "httpRequestHeaders": "httpRequestHeaders"; "httpRequestParams": "httpRequestParams"; "fileAlias": "fileAlias"; }, {}, ["fileUploads"], ["*"]>;
}
