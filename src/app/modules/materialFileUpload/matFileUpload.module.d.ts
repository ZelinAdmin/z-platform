import * as i0 from "@angular/core";
import * as i1 from "./matFileUpload/matFileUpload.component";
import * as i2 from "./matFileUploadQueue/matFileUploadQueue.component";
import * as i3 from "./fileUploadInputFor.directive";
import * as i4 from "./bytes.pipe";
import * as i5 from "@angular/material/button";
import * as i6 from "@angular/material/progress-bar";
import * as i7 from "@angular/material/icon";
import * as i8 from "@angular/material/card";
import * as i9 from "@angular/common/http";
import * as i10 from "@angular/common";
export declare class MatFileUploadModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MatFileUploadModule, [typeof i1.MatFileUpload, typeof i2.MatFileUploadQueue, typeof i3.FileUploadInputFor, typeof i4.BytesPipe], [typeof i5.MatButtonModule, typeof i6.MatProgressBarModule, typeof i7.MatIconModule, typeof i8.MatCardModule, typeof i9.HttpClientModule, typeof i10.CommonModule], [typeof i1.MatFileUpload, typeof i2.MatFileUploadQueue, typeof i3.FileUploadInputFor, typeof i4.BytesPipe]>;
    static ɵinj: i0.ɵɵInjectorDef<MatFileUploadModule>;
}
