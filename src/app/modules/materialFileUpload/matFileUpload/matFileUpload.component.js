import { Component, EventEmitter, Input, Output, Inject, forwardRef } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { MatFileUploadQueue } from '../matFileUploadQueue/matFileUploadQueue.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../matFileUploadQueue/matFileUploadQueue.component";
import * as i3 from "@angular/material/card";
import * as i4 from "@angular/material/progress-bar";
import * as i5 from "@angular/common";
import * as i6 from "@angular/material/icon";
import * as i7 from "../bytes.pipe";
var _c0 = function (a0) { return { "disabled": a0 }; };
/**
 * A material design file upload component.
 */
var MatFileUpload = /** @class */ (function () {
    function MatFileUpload(HttpClient, matFileUploadQueue) {
        this.HttpClient = HttpClient;
        this.matFileUploadQueue = matFileUploadQueue;
        this.isUploading = false;
        /* Http request input bindings */
        this.httpUrl = 'http://localhost:8080';
        this.httpRequestHeaders = new HttpHeaders();
        this.httpRequestParams = new HttpParams();
        this.fileAlias = "file";
        /** Output  */
        this.removeEvent = new EventEmitter();
        this.onUpload = new EventEmitter();
        this.progressPercentage = 0;
        this.loaded = 0;
        this.total = 0;
        if (matFileUploadQueue) {
            this.httpUrl = matFileUploadQueue.httpUrl || this.httpUrl;
            this.httpRequestHeaders = matFileUploadQueue.httpRequestHeaders || this.httpRequestHeaders;
            this.httpRequestParams = matFileUploadQueue.httpRequestParams || this.httpRequestParams;
            this.fileAlias = matFileUploadQueue.fileAlias || this.fileAlias;
        }
    }
    Object.defineProperty(MatFileUpload.prototype, "file", {
        get: function () {
            return this._file;
        },
        set: function (file) {
            this._file = file;
            this.total = this._file.size;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MatFileUpload.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (id) {
            this._id = id;
        },
        enumerable: false,
        configurable: true
    });
    MatFileUpload.prototype.upload = function () {
        var _this = this;
        this.isUploading = true;
        // How to set the alias?
        var formData = new FormData();
        formData.set(this.fileAlias, this._file, this._file.name);
        this.fileUploadSubscription = this.HttpClient.post(this.httpUrl, formData, {
            headers: this.httpRequestHeaders,
            observe: "events",
            params: this.httpRequestParams,
            reportProgress: true,
            responseType: "json"
        }).subscribe(function (event) {
            if (event.type === HttpEventType.UploadProgress) {
                _this.progressPercentage = Math.floor(event.loaded * 100 / event.total);
                _this.loaded = event.loaded;
                _this.total = event.total;
            }
            _this.onUpload.emit({ file: _this._file, event: event });
        }, function (error) {
            if (_this.fileUploadSubscription) {
                _this.fileUploadSubscription.unsubscribe();
            }
            _this.isUploading = false;
            _this.onUpload.emit({ file: _this._file, event: event });
        });
    };
    MatFileUpload.prototype.remove = function () {
        if (this.fileUploadSubscription) {
            this.fileUploadSubscription.unsubscribe();
        }
        this.removeEvent.emit(this);
    };
    MatFileUpload.prototype.ngOnDestroy = function () {
        console.log('file ' + this._file.name + ' destroyed...');
    };
    MatFileUpload.ɵfac = function MatFileUpload_Factory(t) { return new (t || MatFileUpload)(i0.ɵɵdirectiveInject(i1.HttpClient), i0.ɵɵdirectiveInject(forwardRef(function () { return MatFileUploadQueue; }))); };
    MatFileUpload.ɵcmp = i0.ɵɵdefineComponent({ type: MatFileUpload, selectors: [["mat-file-upload"]], hostAttrs: [1, "mat-file-upload"], inputs: { httpUrl: "httpUrl", httpRequestHeaders: "httpRequestHeaders", httpRequestParams: "httpRequestParams", fileAlias: "fileAlias", file: "file", id: "id" }, outputs: { removeEvent: "removeEvent", onUpload: "onUpload" }, exportAs: ["matFileUpload"], decls: 17, vars: 15, consts: [[1, "file-info"], [1, "example-section"], [1, "example-margin", 3, "value"], [3, "ngClass"], [1, "action", 3, "click"]], template: function MatFileUpload_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "mat-card");
            i0.ɵɵelementStart(1, "span", 0);
            i0.ɵɵtext(2);
            i0.ɵɵpipe(3, "bytes");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(4, "section", 1);
            i0.ɵɵelement(5, "mat-progress-bar", 2);
            i0.ɵɵelementStart(6, "a", 3);
            i0.ɵɵelementStart(7, "mat-icon", 4);
            i0.ɵɵlistener("click", function MatFileUpload_Template_mat_icon_click_7_listener() { return ctx.upload(); });
            i0.ɵɵtext(8, "file_upload");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "mat-icon", 4);
            i0.ɵɵlistener("click", function MatFileUpload_Template_mat_icon_click_9_listener() { return ctx.remove(); });
            i0.ɵɵtext(10, "cancel");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(11, "span", 0);
            i0.ɵɵtext(12);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(13, "span");
            i0.ɵɵtext(14);
            i0.ɵɵpipe(15, "bytes");
            i0.ɵɵpipe(16, "bytes");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate2("", ctx.file.name, "(", i0.ɵɵpipeBind1(3, 7, ctx.file.size), ")");
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("value", ctx.progressPercentage);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(13, _c0, ctx.isUploading));
            i0.ɵɵadvance(6);
            i0.ɵɵtextInterpolate1("", ctx.progressPercentage, "%");
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate2(" ", i0.ɵɵpipeBind1(15, 9, ctx.loaded), " of ", i0.ɵɵpipeBind1(16, 11, ctx.total), "");
        } }, directives: [i3.MatCard, i4.MatProgressBar, i5.NgClass, i6.MatIcon], pipes: [i7.BytesPipe], styles: [".dropzone[_ngcontent-%COMP%] {\n    background-color: brown;\n    width: 100px;\n    height: 100px;\n}\n\n\n\n\n.example-section[_ngcontent-%COMP%] {\n  display: flex;\n  align-content: center;\n  align-items: center;\n  height: 10px;\n}\n\n.file-info[_ngcontent-%COMP%] {\n  font-size: .85rem;\n}\n\n#drop_zone[_ngcontent-%COMP%] {\n  border: 5px solid blue;\n  width:  200px;\n  height: 100px;\n}\n\n.action[_ngcontent-%COMP%] {\n  cursor: pointer;\n  outline: none;\n}\n\na.disabled[_ngcontent-%COMP%] {\n  pointer-events: none;\n}\n\n.upload-drop-zone[_ngcontent-%COMP%] {\n  height: 200px;\n  border-width: 2px;\n  margin-bottom: 20px;\n}\n\n\n.upload-drop-zone[_ngcontent-%COMP%] {\n  color: #ccc;\n  border-style: dashed;\n  border-color: #ccc;\n  line-height: 200px;\n  text-align: center\n}\n.upload-drop-zone.drop[_ngcontent-%COMP%] {\n  color: #222;\n  border-color: #222;\n}"] });
    return MatFileUpload;
}());
export { MatFileUpload };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MatFileUpload, [{
        type: Component,
        args: [{
                selector: 'mat-file-upload',
                templateUrl: "./matFileUpload.component.html",
                exportAs: 'matFileUpload',
                host: {
                    'class': 'mat-file-upload',
                },
                styleUrls: ['./../matFileUploadQueue.scss'],
            }]
    }], function () { return [{ type: i1.HttpClient }, { type: i2.MatFileUploadQueue, decorators: [{
                type: Inject,
                args: [forwardRef(function () { return MatFileUploadQueue; })]
            }] }]; }, { httpUrl: [{
            type: Input
        }], httpRequestHeaders: [{
            type: Input
        }], httpRequestParams: [{
            type: Input
        }], fileAlias: [{
            type: Input
        }], file: [{
            type: Input
        }], id: [{
            type: Input
        }], removeEvent: [{
            type: Output
        }], onUpload: [{
            type: Output
        }] }); })();
//# sourceMappingURL=matFileUpload.component.js.map