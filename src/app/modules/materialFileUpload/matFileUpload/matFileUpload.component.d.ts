import { EventEmitter, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MatFileUploadQueue } from '../matFileUploadQueue/matFileUploadQueue.component';
import * as i0 from "@angular/core";
/**
 * A material design file upload component.
 */
export declare class MatFileUpload implements OnDestroy {
    private HttpClient;
    matFileUploadQueue: MatFileUploadQueue;
    constructor(HttpClient: HttpClient, matFileUploadQueue: MatFileUploadQueue);
    isUploading: boolean;
    httpUrl: string;
    httpRequestHeaders: HttpHeaders | {
        [header: string]: string | string[];
    };
    httpRequestParams: HttpParams | {
        [param: string]: string | string[];
    };
    fileAlias: string;
    get file(): any;
    set file(file: any);
    set id(id: number);
    get id(): number;
    /** Output  */
    removeEvent: EventEmitter<MatFileUpload>;
    onUpload: EventEmitter<any>;
    progressPercentage: number;
    loaded: number;
    total: number;
    private _file;
    private _id;
    private fileUploadSubscription;
    upload(): void;
    remove(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<MatFileUpload, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<MatFileUpload, "mat-file-upload", ["matFileUpload"], { "httpUrl": "httpUrl"; "httpRequestHeaders": "httpRequestHeaders"; "httpRequestParams": "httpRequestParams"; "fileAlias": "fileAlias"; "file": "file"; "id": "id"; }, { "removeEvent": "removeEvent"; "onUpload": "onUpload"; }, never, never>;
}
