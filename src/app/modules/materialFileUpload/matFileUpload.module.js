import { NgModule } from '@angular/core';
import { MatFileUpload } from './matFileUpload/matFileUpload.component';
import { MatFileUploadQueue } from './matFileUploadQueue/matFileUploadQueue.component';
import { FileUploadInputFor } from './fileUploadInputFor.directive';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { BytesPipe } from './bytes.pipe';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
var MatFileUploadModule = /** @class */ (function () {
    function MatFileUploadModule() {
    }
    MatFileUploadModule.ɵmod = i0.ɵɵdefineNgModule({ type: MatFileUploadModule });
    MatFileUploadModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MatFileUploadModule_Factory(t) { return new (t || MatFileUploadModule)(); }, imports: [[
                MatButtonModule,
                MatProgressBarModule,
                MatIconModule,
                MatCardModule,
                HttpClientModule,
                CommonModule
            ]] });
    return MatFileUploadModule;
}());
export { MatFileUploadModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MatFileUploadModule, { declarations: [MatFileUpload,
        MatFileUploadQueue,
        FileUploadInputFor,
        BytesPipe], imports: [MatButtonModule,
        MatProgressBarModule,
        MatIconModule,
        MatCardModule,
        HttpClientModule,
        CommonModule], exports: [MatFileUpload,
        MatFileUploadQueue,
        FileUploadInputFor,
        BytesPipe] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MatFileUploadModule, [{
        type: NgModule,
        args: [{
                imports: [
                    MatButtonModule,
                    MatProgressBarModule,
                    MatIconModule,
                    MatCardModule,
                    HttpClientModule,
                    CommonModule
                ],
                declarations: [
                    MatFileUpload,
                    MatFileUploadQueue,
                    FileUploadInputFor,
                    BytesPipe
                ],
                exports: [
                    MatFileUpload,
                    MatFileUploadQueue,
                    FileUploadInputFor,
                    BytesPipe
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=matFileUpload.module.js.map