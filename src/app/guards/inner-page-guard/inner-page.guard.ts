import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  CanActivateChild,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class InnerPageGuard implements CanActivateChild, CanActivate {
  constructor(private auth: AuthService, private router: Router) {}

  /* Guard to protect login and register pages from already authenticated users.
   * Checks the login status before redirecting to home page*/

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    if (this.auth.isLogged) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    if (this.auth.isLogged) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}
