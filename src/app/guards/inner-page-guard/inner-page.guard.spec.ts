import { inject, TestBed } from '@angular/core/testing';

import { InnerPageGuard } from './inner-page.guard';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

class AuthMock {
  isLogged: boolean;
}

describe('InnerPageGuardGuard', () => {
  /* mocks definitions */
  const authServiceSpy = new AuthMock();
  const routerMock = { navigate: jasmine.createSpy('navigate') };
  const routeMock: any = { snapshot: {} };
  const routerStateMock: any = { snapshot: {}, url: '/cookies' };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        InnerPageGuard,
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerMock },
      ],
    });
  });

  it('should ...', inject([InnerPageGuard], (guard: InnerPageGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should redirect an authenticated user to the home page', inject(
    [InnerPageGuard],
    (guard: InnerPageGuard) => {
      authServiceSpy.isLogged = true;
      expect(guard.canActivate(routeMock, routerStateMock)).toEqual(false);
      expect(routerMock.navigate).toHaveBeenCalledWith(['']);
    }
  ));

  it('should return true if the user is not logged', inject(
    [InnerPageGuard],
    (guard: InnerPageGuard) => {
      authServiceSpy.isLogged = false;
      expect(guard.canActivate(routeMock, routerStateMock)).toEqual(true);
    }
  ));
});
