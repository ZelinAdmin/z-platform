import { inject, TestBed } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

class AuthMock {
  isLogged: boolean;
}

describe('AuthGuardGuard', () => {
  /* mocks definitions */
  const authServiceSpy = new AuthMock();
  const routerMock = { navigate: jasmine.createSpy('navigate') };
  const routeMock: any = { snapshot: {} };
  const routerStateMock: any = { snapshot: {}, url: '/cookies' };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerMock },
      ],
    });
  });

  it('should ...', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should return true for a logged user', inject(
    [AuthGuard],
    (guard: AuthGuard) => {
      authServiceSpy.isLogged = true;
      expect(guard.canActivate(routeMock, routerStateMock)).toEqual(true);
    }
  ));

  it('should redirect an unauthenticated user to the login page', inject(
    [AuthGuard],
    (guard: AuthGuard) => {
      authServiceSpy.isLogged = false;
      expect(guard.canActivate(routeMock, routerStateMock)).toEqual(false);
      expect(routerMock.navigate).toHaveBeenCalledWith(['login']);
    }
  ));
});
