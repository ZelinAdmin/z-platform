export interface VrmlFile {
  name?: string;
  uuid: string;
  size?: number | string;
  hascolormap?: boolean;
  status?: string;
  code?: number;
  projectId?: string;
  modified?: Date;
}

export interface VrmlFileDisplay {
  name?: string;
  uuid: string;
  size?: number | string;
  hascolormap?: boolean;
  status?: string;
  code?: number;
  projectId?: string;
  modified?: Date;
  package: string | number;
  id: string | number;
}
