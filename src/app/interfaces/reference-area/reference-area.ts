export interface ReferenceArea {
  id: string;
  name: string;
}
