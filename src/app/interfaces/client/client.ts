export interface Client {
  id: string;
  userId: string;
  service: string;
  rating: number;
}
