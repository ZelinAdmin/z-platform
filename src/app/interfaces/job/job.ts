export class Job {
  id: string;
  name: string;
  userId: string;
  functionalId: number;
  status: string;
  cpu: number;
  submissionDate: string;
  startDate: string;
  endDate: string;
  cost: number;
}
