export interface Expert {
  id?: string;
  userId?: string;
  title: string;
  rating: number;
  wage: number;
  available?: boolean;
  technicalAssistance?: boolean;
  studies?: boolean;
}
