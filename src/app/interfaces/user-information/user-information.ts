export interface UserInformation {
  email: string;
  password?: string;
  firstName: string;
  lastName: string;
  phone?: string;
  company?: string;
  address?: string;
  zipCode?: string;
  city?: string;
  country?: string;
  image?: string;
  citizenship?: string;
  isValidated?: boolean;
  id?: string;

  service?: string;
  isDisabled?: boolean;
  isAdmin?: boolean;
  hasCommunityAccess?: boolean;
  isPremiumCommunity?: boolean;
  clientId?: string;
  expertId?: string;
  softwares?: string[];
  languages?: Array<string>;
  languagesRef?: any[];
  rating?: number;
  credits?: number;
  totCredits?: number;
  creditsTimeRatio?: number;
  areas?: Array<string>;
}
