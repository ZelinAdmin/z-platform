export interface UserItem {
  email: string;
  firstName: string;
  lastName: string;
  userId: string;
  isValidated: boolean;
  isAdmin: boolean;
  hasCommunityAccess: boolean;
  isPremiumCommunity: boolean;
  userRating: number;
  coreHour: number;
}
