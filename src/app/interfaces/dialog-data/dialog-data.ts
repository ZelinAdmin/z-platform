export interface DialogData {
  users?: string;
  amount?: number;
  projectId?: string;
  userId: string;
}
