export interface DialogDataToInject {
  projectId: string;
  category: string;
  path: string;
}
