export interface DialogDataNewProject {
  projectName?: string;
  userId?: string;
}
