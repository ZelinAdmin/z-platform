import { AccessItem } from 'src/app/sub-modules/admin/interfaces/access-item';

export interface Project {
  id?: string;
  name?: string;
  progress?: string;
  globalprogress?: number;
  startDate?: Date;
  endDate?: Date;
  image?: string;
  teamsUrl?: string;
  nextMeeting?: Date;
  access?: AccessItem[];

  role?: string;

  managers?: string;
  guests?: string;
  customers?: string;
}
