import { Electromag } from '../../sub-modules/profile/interfaces/expertises/electromag';
import { Struct } from '../../sub-modules/profile/interfaces/expertises/struct';
import { Thermic } from '../../sub-modules/profile/interfaces/expertises/thermic';
import { Fluids } from '../../sub-modules/profile/interfaces/expertises/fluids';

export class Expertise {
    fluids: boolean;
    thermic: boolean;
    electroMag: boolean;
    struct: boolean;
}

export class Software {
    id: string;
    name: string;
}

export class Language {
    id: string;
    name: string;
}

export class ExpertUserInfo {
    expertId: string;
    userId: string;
    title: string;
    rating: number;
    wage: number;
    available: boolean;

    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    company: string;
    address: string;
    zipCode: string;
    city: string;
    country: string;
    citizenship: string;
    isValidated: boolean;
    isFavorite: boolean;
    availability: boolean;
    photoUrl: string;

    electro: Electromag;
    struct: Struct;
    thermic: Thermic;
    fluids: Fluids;
    expertises: Expertise;
    areas: string[];
    softwares: Software[];
    languages: Language[];
}