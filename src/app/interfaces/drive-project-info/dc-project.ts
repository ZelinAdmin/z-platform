export class DriveFile {
  id?: string;
  name?: string;
  size?: number;
  created?: Date;
  modified?: Date;
  type?: string;
  path?: string;
  fileId?: string;
  canDelete?: boolean;
  canDownload?: boolean;

  //  table only values
  displayName?: string;
  package?: number | string;
  idBeta?: number | string;
}

export class DcProject {
  path?: string;
  files: DriveFile[];
  category?: string;
}
