import { Project } from 'src/app/interfaces/project/project';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';

export interface UserProject {
  project: Project;
  role: string;
  user: UserInformation;
}
