export interface CreditsOperationRequest {
  userId: string;
  operationDate: Date;
  amount: number;
  creditsAfter: number;
}
