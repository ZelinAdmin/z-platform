import { ReferenceArea } from '../reference-area/reference-area';

export interface AreaExpert {
  id: string;
  expertId: string;
  areaId: string;
  refArea: ReferenceArea;
}
