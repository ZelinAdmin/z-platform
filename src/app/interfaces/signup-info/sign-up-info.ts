export class SignUpInfo {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  phone?: string;
  company?: string;
  address?: string;
  zipCode?: string;
  city?: string;
  country?: string;
  citizenship?: string;
  isValidated?: boolean;
}
