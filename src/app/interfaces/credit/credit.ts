export interface Credit {
  id: string;
  amount: number;
  creditsAfter: number;
  operationDate: Date;
  userId: string;

  company: string;
  email: string;
  credits: number;
}
