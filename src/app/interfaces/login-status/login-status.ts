export interface LoginStatus {
  name: string;
  refreshToken: string;
  token: string;
  userId: string;
}
