import { Credit } from '../credit/credit';

export interface CreditsUser {
  id: string;
  email: string;
  company: string;
  firstName: string;
  lastName: string;
  isDisabled: boolean;
  credits: number;
  totCredits: number;
  operations: Credit[];
}
