export interface ProjectUpdateInfo {
  globalprogress: number;
  startDate: Date;
  endDate: Date;
  nextMeeting: Date;
  teamsUrl: string;
}
