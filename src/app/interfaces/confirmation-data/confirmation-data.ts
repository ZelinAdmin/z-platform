import { DriveFile } from 'src/app/interfaces/drive-project-info/dc-project';
import { ProjectFileDisplayed } from 'src/app/sub-modules/vision/interfaces/project-file-displayed/project-file-displayed';
import { VrmlFileDisplay } from 'src/app/interfaces/vrml-file/vrml-file';

export interface ConfirmationData {
  title: string;
  message?: string;
  arrayOfFiles?: DriveFile[] | ProjectFileDisplayed[] | VrmlFileDisplay[];
}
