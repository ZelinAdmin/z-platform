import { ConnectionPositionPair } from '@angular/cdk/overlay';

export class Globals {
  static readonly USER_KEY = 'ZPlatform-currentUser';
  static readonly JWT_TOKEN = 'ZPlatform-token';
  static readonly PROJECTS = 'ZPlatform-projects';
  static readonly USERNAME = 'ZPlatform-name';
  static readonly USER_ID = 'ZPlatform-userId';
  static readonly REFRESH_TOKEN = 'ZPlatform-refresh-token';
  /* Vision variables*/
  static readonly testCaseMatrixRaw = 'Vision_TCMR';
  static readonly testCaseMatrixCol = 'Vision_TCMC';
  static readonly testCaseMatrixHeader = 'Vision_TCMH';

  /* Drive variables */
  static readonly DIRECTORY = 'dir';
  static readonly FILE = 'file';
  static readonly NAN_FILE = 'NaN';

  static readonly LIGHTNING_INTENSITY = 1.6;
  static readonly HIGHLIGHT_COLOR = 0x616161;

  static readonly top: ConnectionPositionPair = {
    originX: 'center',
    originY: 'top',
    overlayX: 'center',
    overlayY: 'bottom',
  };

  static readonly right: ConnectionPositionPair = {
    originX: 'end',
    originY: 'center',
    overlayX: 'start',
    overlayY: 'center',
  };

  static readonly bottom: ConnectionPositionPair = {
    originX: 'center',
    originY: 'bottom',
    overlayX: 'center',
    overlayY: 'top',
  };

  static readonly left: ConnectionPositionPair = {
    originX: 'start',
    originY: 'center',
    overlayX: 'end',
    overlayY: 'center',
  };

  static formatBytes(bytes, decimals = 2) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  // Copy-pasted from https://github.com/emilbayes/object-transpose/blob/master/index.js
  static transposeArray = (data) => {
    const res = {};
    // tslint:disable-next-line:one-variable-per-declaration
    let i, key;

    for (i = 0; i < data.length; i++) {
      // tslint:disable-next-line:forin
      for (key in data[i]) {
        res[key] = res[key] || [];
        res[key][i] = data[i][key];
      }
    }

    return res;
  };
}

// tslint:disable-next-line:class-name
export class _Array<T> extends Array<T> {
  static range(from: number, to: number, step: number): number[] {
    // tslint:disable-next-line:no-bitwise
    return Array.from(Array(~~((to - from) / step) + 1)).map(
      (v, k) => from + k * step
    );
  }
}
