import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MetSafeService } from 'src/app/sub-modules/admin/services/met-safe/met-safe.service';

@Component({
  selector: 'app-meteo-api',
  templateUrl: './meteo-api.component.html',
  styleUrls: ['./meteo-api.component.less'],
})
export class MeteoApiComponent implements OnInit {
  public meteoForm: FormGroup;
  public result: any;
  public sliceMet: any;

  constructor(private metSafeService: MetSafeService) {}

  ngOnInit(): void {
    this.meteoForm = new FormGroup({
      lat: new FormControl(),
      lon: new FormControl(),
      alt: new FormControl(),
    });
    this.meteoForm.controls.lat.setValue('43.566280');
    this.meteoForm.controls.lon.setValue('1.385466');
    this.meteoForm.controls.alt.setValue('4');
  }

  onSubmit() {
    this.metSafeService
      .get(
        this.meteoForm.controls.lat.value,
        this.meteoForm.controls.lon.value,
        this.meteoForm.controls.alt.value
      )
      .subscribe((data) => {
        this.result = data.points[0].data.t;
      });
  }

  submitSliceMet() {
    this.metSafeService.getSliceMet().subscribe((data) => {
      this.sliceMet = data;
    });
  }
}
