import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/interfaces/project/project';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessService } from 'src/app/services/access-service/access.service';
import { ProjectService } from 'src/app/services/project/project.service';
import { environment } from 'src/environments/environment';
import { ProjectPermissionsComponent } from 'src/app/sub-modules/admin/dialogs/project-permissions/project-permissions.component';
import { DialogData } from 'src/app/interfaces/dialog-data/dialog-data';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';

@Component({
  selector: 'app-admin-project-config',
  templateUrl: './project-config.component.html',
  styleUrls: ['./project-config.component.less'],
})
export class ProjectConfigComponent implements OnInit {
  projectId: string;
  project: Project;
  roles: UserProject[];
  public displayedColumns: string[] = ['email', 'role', 'action']; // columns to display
  public photoEditMode = false; // Used to display profile photo edit popup
  public fallbackImage = '/assets/fallbackProject.png';

  constructor(
    private route: ActivatedRoute,
    private accessService: AccessService,
    private projectService: ProjectService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(async (params) => {
      if (params.get('id')) {
        this.projectId = params.get('id');
      }
      this.projectService.getProject(this.projectId).subscribe((data) => {
        this.project = data;
      });
      this.accessService
        .getAll(this.projectId)
        .subscribe((data: UserProject[]) => {
          this.roles = data;
        });
    });
  }

  deletePermission(userId: string) {
    this.accessService.delete(this.projectId, userId).subscribe(() => {});
    location.reload();
  }

  /* TODO finish this*/
  openDialog(userId: string): void {
    const dialogData: DialogData = {
      projectId: this.projectId,
      userId,
    };
    const dialogRef = this.dialog.open(ProjectPermissionsComponent, {
      width: '',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      location.reload();
    });
  }

  generateImgUrl(element: Project) {
    if (!element) {
      return '';
    }
    return `${environment.apiUrl}files/${element.image}/download`;
  }

  navigateToProjectManagement() {
    this.router.navigate(['admin']);
  }

  // Open edit photo window
  opClEditPhoto() {
    this.photoEditMode = !this.photoEditMode;
  }
}
