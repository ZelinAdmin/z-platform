import { DialogData } from './../../../../interfaces/dialog-data/dialog-data';
import {
  Component,
  OnInit,
  ViewChild,
  ɵAPP_ID_RANDOM_PROVIDER,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { UserService } from 'src/app/services/user/user.service';
import { AccessService } from 'src/app/services/access-service/access.service';
import { ProjectService } from 'src/app/services/project/project.service';
import { Router } from '@angular/router';
import { Project } from 'src/app/interfaces/project/project';
import { MatTableDataSource } from '@angular/material/table';
import { AccessItem } from 'src/app/sub-modules/admin/interfaces/access-item';
import { ProjectUsersReduced } from 'src/app/sub-modules/vision/interfaces/project-users-reduced/project-users-reduced';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { MatSort } from '@angular/material/sort';
import { Globals } from '../../../../globals/globals';
import { DialogDataNewProject } from 'src/app/interfaces/dialog-data/dialog-data-newproject';
import { MatDialog } from '@angular/material/dialog';
import { OperationProjectComponent } from 'src/app/sub-modules/admin/dialogs/operation-project/operation-project.component';
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.less'],
})
export class ProjectComponent implements OnInit {
  projects: Project[] = []; // contains projects get from back
  public dataSource: MatTableDataSource<Project>; // users that will be displayed in a material way
  public displayedColumns: string[] = [
    'name',
    'managers',
    'guests',
    'customers',
    'progress', // change 'images' by 'progress' for more logic and to run the table sort in this column + project.ts
    'action',
  ]; // columns to display
  public pageSize = 5; // default displayed elements number
  public currentPage = 0; // default displayed page
  public totalSize = 0; // default number of element in dataSource (not set yet)
  public currentUserId = localStorage.getItem(Globals.USER_ID);
  public userConnected: any;
  public newProjectName: string;
  public newProjectId: any;
  public progress = '';
  public globalprogress = 0;
  public startDate = new Date();
  public endDate = new Date();
  public image = '';
  public nextMeeting = new Date();
  public teamsUrl = '';

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator; // paginator at the bottom of the page
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public accessService: AccessService,
    private projectService: ProjectService,
    private router: Router,
    private userService: UserService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.fetchAndMapProjects();
    this.dataSource.sort = this.sort;
  }

  // set paginator properties
  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  // set paginator page
  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.projects.slice(start, end);
    // this.dataSource = new MatTableDataSource<Project>(part); // init dataSource here create a bugg for the table's sorting
  }

  // get all projects from back
  private fetchAndMapProjects(): void {
    this.projectService.findAll().subscribe((data) => {
      this.mapProjects(data);
    });
  }

  // fill projects variable with projects and permissions
  private mapProjects(data): void {
    this.projects = data.map((element) => {
      return {
        name: element.name,
        id: element.id,
        progress: element.progress,
        globalprogress: element.globalprogress,
        endDate: element.endDate,
        startDate: element.startDate,
        nextMeeting: element.nextMeeting,
        teamsUrl: element.teamsUrl,
        access: [],
      };
    });
    // get all permissions for all projects
    this.projects.forEach((project) => {
      this.accessService
        .getAll(project.id)
        .subscribe((dataCall: UserProject[]) => {
          this.mapAccess(dataCall, project);
        });
    });
    // Sort by name before displaying the list
    this.projects.sort((project1, project2) => {
      if (project1.name < project2.name) {
        return -1;
      }
      if (project1.name > project2.name) {
        return 1;
      }
      return 0;
    });
    // setting datasource of table from projects
    this.dataSource = new MatTableDataSource<Project>(this.projects);
    this.dataSource.sort = this.sort;

    this.dataSource.paginator = this.paginator;
    this.totalSize = this.projects.length;
    this.iterator();
  }

  // create a list with all permissions of a project
  private mapAccess(data: UserProject[], project: Project): void {
    data.forEach((element) => {
      this.userService.get(element.user.id).subscribe((user) => {
        if (!user.isDisabled) {
          const ai: AccessItem = {
            userId: element.user.id,
            userEMail: element.user.email,
            role: element.role,
          };
          project.access.push(ai);
        }
      });
    });
  }

  // handle "delete" button click
  deleteProject(projectId: string): void {
    if (
      !confirm(
        'Do you really want to delete this user ?\n' +
          this.projects.find((obj) => {
            return obj.id === projectId;
          }).name
      )
    ) {
      return;
    }
    this.projectService
      .delete(projectId)
      .subscribe(() => this.fetchAndMapProjects());
  }

  // check if a project has no permission of a defined type
  isEmptyPermission(project: Project, str: string): boolean {
    if (!project) {
      return true;
    }
    for (const ac of project.access) {
      if (ac.role.toUpperCase() === str.toUpperCase()) {
        return false;
      }
    }
    return true;
  }

  editProject(projectId: string) {
    this.router.navigate(['/admin/project/' + projectId]);
  }

  /////////////////////////////////////////////////////////
  // Add a new project, set only the name + egenerate Id //
  /////////////////////////////////////////////////////////
  openDialog(): void {
    const dialogData: DialogDataNewProject = {
      userId: this.currentUserId,
      projectName: this.newProjectName,
    };

    const dialogRef = this.dialog.open(OperationProjectComponent, {
      width: '500px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.projectName) {
        this.newProjectName = result.projectName;
        this.addOperation();
      }
    });
  }

  addOperation() {
    const newProject: Project = {
      name: this.newProjectName,
      progress: this.progress,
      globalprogress: this.globalprogress,
      image: this.image,
      teamsUrl: this.teamsUrl,
      startDate: this.startDate,
      endDate: this.endDate,
      nextMeeting: this.nextMeeting,
    };
    this.projectService.createNewProject(newProject).subscribe(() => {
      console.log(
        'new project',
        newProject,
        'created! Now update it (first: managers in admin & dates in z-vision'
      );
    });
  }
}
