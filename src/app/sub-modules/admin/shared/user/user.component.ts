import { LanguageService } from './../../../../services/language/language.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import { Expert } from 'src/app/interfaces/expert/expert';
import { Client } from 'src/app/interfaces/client/client';
import { UserItem } from 'src/app/interfaces/user-item/user-item';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { ClientService } from 'src/app/services/client/client.service';
import { ExpertService } from 'src/app/services/expert/expert.service';
import { MatTableDataSource } from '@angular/material/table';
import { VpnAccessComponent } from 'src/app/sub-modules/admin/dialogs/vpn-access/vpn-access.component';
import {
  ComponentsTrslt,
  TranslationService,
} from 'src/app/services/translation/translation.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less'],
})
export class UserComponent implements OnInit {
  public translatePage: ComponentsTrslt = ComponentsTrslt.PROFILE;
  users: UserItem[] = []; // contains users get from back
  public dataSource: MatTableDataSource<UserItem>; // users that will be displayed in a material way
  public displayedColumns: string[] = [
    'email',
    'firstName',
    'lastName',
    'userRating',
    'coreHour',
    'action',
  ]; // columns to display
  public pageSize = 10; // default displayed elements number
  public currentPage = 0; // default displayed page
  public totalSize = 0; // default number of element in dataSource (not set yet)
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator; // paginator at the bottom of the page

  // sorted data
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // sortedData: UserItem[] ; TO REMOVE

  // Popups / Dialog boxes
  editModeClient = false; // client profil edition popup is opened or not
  editModeExpert = false; // expert profil edition popup is opened or not
  editModeRating = false; // Expert Rating edition popup is opened or not
  editModeCoreHour = false; // User core hour edition popup is opened or not

  // Edition data passed to forms / fields
  userInfoEdition: UserInformation; // user profil for edition
  userIdEdition: string; // user id for edition
  expertIdEdition: string; // expert id for RATING edition
  expertRatingEdition: number; // Expert Profile for edition
  expertInfoEdition: Expert; // expert profil for edition
  clientInfoEdition: Client; // client profil for edition
  userIdCoreHourEdition: string;
  userCoreHourEdition: number;

  updatingProfile = false; // Used to display spinner when updating profile after edit
  editExpertProfileForm: FormGroup; // Used to pre-fill Expert Profile edit form
  editProfileForm: FormGroup; // Used to pre-fill Client profile edit form
  editRatingForm: FormGroup; // Used to pre-fill Expert Rating Edit form
  editCoreHourForm: FormGroup; // Used to pre-fill Core Hour Edit form
  userIdSelected: string;
  // Array to display all languages in profile Edition
  public allLanguages = [];

  constructor(
    public translateService: TranslationService,
    private userService: UserService,
    private expertService: ExpertService,
    private clientService: ClientService,
    private router: Router,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    public languageService: LanguageService
  ) {
    // this.sortedData = this.users.slice(); // TO REMOVE
  }

  ngOnInit(): void {
    this.fetchAndMapUsers();
    // this.dataSource: new MatTableDataSource<UserItem>();

    this.editExpertProfileForm = this.formBuilder.group({
      lastName: '',
      firstName: '',
      email: '',
      phone: '',
      company: '',
      title: '',
      wage: '',
      citizenship: '',
      country: '',
      city: '',
      address: '',
      zipCode: '',
      rating: [{ value: null }],
    });
    this.editProfileForm = this.formBuilder.group({
      lastName: '',
      firstName: '',
      email: '',
      phone: '',
      company: '',
      service: '',
      citizenship: '',
      country: '',
      city: '',
      address: '',
      zipCode: '',
      languages: [],
    });
    this.editRatingForm = this.formBuilder.group({
      id: '',
      userId: '',
      title: '',
      rating: '',
      wage: '',
      available: '',
    });
    this.editCoreHourForm = this.formBuilder.group({
      coreHour: 0,
    });
  }

  // function called on submit button of profile edition
  public updateExpert(formData) {
    this.updatingProfile = true;
    const newUserInfo: UserInformation = {
      lastName: formData.lastName,
      firstName: formData.firstName,
      email: formData.email,
      phone: formData.phone,
      company: formData.company,
      citizenship: formData.citizenship,
      country: formData.country,
      city: formData.city,
      address: formData.address,
      zipCode: formData.zipCode,
    };

    this.userService
      .updateUser(this.userIdEdition, newUserInfo)
      .subscribe((_) => {
        const newExpertInfo: Expert = {
          title: formData.title,
          wage: formData.wage,
          rating: formData.rating,
        };

        this.expertService
          .updateUser(this.expertInfoEdition.id, newExpertInfo)
          .subscribe((__) => {
            this.editModeExpert = false;
            this.updatingProfile = false;
            this.fetchAndMapUsers();
          });
      });
  }

  // Open or close dialog for expert rating edition
  public OpClExRatingEdit(expertId: string) {
    this.expertIdEdition = expertId;

    // Get expert data from User Id
    this.expertService.getByUserId(expertId).subscribe((data) => {
      // Send data to form
      this.editRatingForm.setValue({
        id: data.id,
        userId: data.userId,
        title: '',
        rating: data.rating,
        wage: '',
        available: '',
      });

      this.expertInfoEdition = data;
      this.expertRatingEdition = data.rating;
      this.editModeRating = !this.editModeRating;
    });
  }

  // Open or close dialog for user core hour edit.
  public OpClCoreHourEdit(userId: string) {
    this.userService.get(userId).subscribe((userData) => {
      // Send data to form
      this.editCoreHourForm.setValue({
        coreHour: userData.creditsTimeRatio,
      });

      this.userIdCoreHourEdition = userData.id;
      this.userCoreHourEdition = userData.creditsTimeRatio;
      this.editModeCoreHour = !this.editModeCoreHour;
    });
  }

  // Open or Close dialog for user profil edition
  public OpClEditMode(userId: string) {
    this.userIdEdition = userId;

    this.languageService.getAllLanguages().subscribe((res) => {
      this.allLanguages = res.map((e) => e.name);
    });
    // Get user Info and pre-fill the form
    this.userService.get(userId).subscribe((data) => {
      this.userInfoEdition = data; // This pre-fills the form

      // IF user is an Expert
      if (data.expertId) {
        this.editExpertProfileForm.setValue({
          lastName: data.lastName,
          firstName: data.firstName,
          email: data.email,
          phone: data.phone,
          company: data.company,
          wage: '',
          title: '',
          citizenship: data.citizenship,
          country: data.country,
          city: data.city,
          address: data.address,
          zipCode: data.zipCode,
          rating: null,
        });

        this.expertService.getByUserId(userId).subscribe((dataExpert) => {
          // Get Expert specific info and complete the pre-filling of the form
          this.editExpertProfileForm.patchValue({
            wage: dataExpert.wage, // Specific Infos
            title: dataExpert.title,
            rating: dataExpert.rating, // ***
          });

          this.expertInfoEdition = dataExpert; // This pre-fills the form
          this.editModeExpert = !this.editModeExpert;
          this.editModeClient = false;
        });

        // IF User is a client
      } else if (data.clientId) {
        this.editProfileForm.setValue({
          lastName: data.lastName,
          firstName: data.firstName,
          email: data.email,
          phone: data.phone,
          company: data.company,
          service: '',
          citizenship: data.citizenship,
          country: data.country,
          city: data.city,
          address: data.address,
          zipCode: data.zipCode,
          languages: [''],
        });

        this.clientService.getByUserId(userId).subscribe((dataClient) => {
          this.editModeClient = !this.editModeClient;
          this.editModeExpert = false;
        });
      }
    });
  }

  // Get all users from back
  private fetchAndMapUsers(): void {
    this.userService.getAll().subscribe((data) => {
      this.mapUsers(data);
    });
  }

  // populateTable() {
  //   this.userService.getAll()
  //       .subscribe(data => {
  //               this.results = data;
  //               this.dataSource = new MatTableDataSource(this.results);
  //               this.dataSource.sort = this.sort;
  //           })}

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.users.slice(start, end);
    // this.dataSource = new MatTableDataSource<UserItem>(part); // init dataSource here create a bugg for the table's sorting
  }

  // Create the list of users from the users got by the call to back
  private mapUsers(data): void {
    // Fill the list of users from the users got by the call to back
    this.users = data.map((element) => {
      const ui: UserItem = {
        userId: element.id,
        email: element.email,
        firstName: element.firstName,
        lastName: element.lastName,
        isValidated: element.isValidated,
        isAdmin: element.isAdmin,
        isPremiumCommunity: element.isPremiumCommunity,
        hasCommunityAccess: element.hasCommunityAccess,
        userRating: null,
        coreHour: null,
      };
      this.userService.get(element.id).subscribe((userData) => {
        ui.coreHour = userData.creditsTimeRatio;
        if (userData.expertId) {
          this.expertService.get(userData.expertId).subscribe((expertData) => {
            ui.userRating = expertData.rating;
          });
        }
      });
      if (!element.isDisabled) {
        return ui;
      }
    });

    // Sort by email address before displaying the list
    this.users.sort((user1, user2) => {
      if (user1.email < user2.email) {
        return -1;
      }
      if (user1.email > user2.email) {
        return 1;
      }
      return 0;
    });
    // setting datasource of table from users
    this.dataSource = new MatTableDataSource<UserItem>(this.users);
    this.dataSource.sort = this.sort;

    // lowercaze data to facilitate the asc sorting aBc and not Bac
    this.dataSource.sortingDataAccessor = (
      element: any,
      sortHeaderId: string
    ): string => {
      if (typeof element[sortHeaderId] === 'string') {
        return element[sortHeaderId].toLocaleLowerCase();
      }
      return element[sortHeaderId];
    };
    this.dataSource.paginator = this.paginator;
    this.totalSize = this.users.length;
    this.iterator();
  }

  // handle "delete" button click
  deleteUser(userId: string): void {
    if (
      !confirm(
        'Do you really want to delete this user ?\n' +
          this.users.find((obj) => {
            return obj.userId === userId;
          }).email
      )
    ) {
      return;
    }
    this.userService
      .deleteUser(userId)
      .subscribe(() => this.fetchAndMapUsers());
  }

  // handle "manage" button click
  editUser(userId: string): void {
    this.router.navigate(['admin/user', userId]);
  }

  // Used to know the icon needed for the "Block / accept this account" button
  getLockState(validated: boolean): string {
    return validated ? 'lock_open' : 'lock';
  }

  // Used to know the text for the "Block / accept this account" button
  getToolTipValidated(validated: boolean): string {
    return validated ? 'Block this account' : 'Accept this account';
  }

  // Used to know the color needed for the "Block / accept this account" button
  getButtonStyleValidated(validated: boolean): string {
    return validated ? 'default' : 'primary';
  }

  // When clicked "Block / accept this account" button, update the whole user
  // with "isValidated" field changed
  changeValidatedState(userId: string): void {
    this.userService.blockAcceptAccount(userId).subscribe(() => {
      this.snackBar.open(`Updated`, '', {
        duration: 2500,
        verticalPosition: 'top',
      });
      this.fetchAndMapUsers();
    });
  }

  // Used to know the icon needed for the "set admin / unset admin this account" button
  getAdminState(admin: boolean): string {
    return admin ? 'account_circle' : 'admin_panel_settings';
  }

  // Used to know the text for the "set admin / unset admin this account" button
  getToolTipAdmin(admin: boolean): string {
    return !admin ? 'Set this account admin' : 'Remove admin rights';
  }

  // Used to know the color needed for the "set admin / unset admin this account" button
  getButtonStyleAdmin(admin: boolean): string {
    return !admin ? 'default' : 'primary';
  }

  // When clicked "set admin / unset admin this account" button, update the whole user
  // with "isValidated" field changed
  changeAdminState(userId: string): void {
    this.userService.changeAdminValueAccount(userId).subscribe(() => {
      this.snackBar.open(`Updated`, '', {
        duration: 2500,
        verticalPosition: 'top',
      });
      this.fetchAndMapUsers();
    });
  }

  getPremiumState(isPremiumCommunity: boolean): string {
    return isPremiumCommunity ? 'offline_bolt' : 'offline_bolt';
  }

  getTooltipPremium(isPremiumCommunity: boolean): string {
    return !isPremiumCommunity
      ? 'Set this account Premium'
      : 'Remove Premium from this account';
  }

  getButtonStylePremium(isPremiumCommunity: boolean): string {
    return !isPremiumCommunity ? 'default' : 'primary';
  }

  changePremiumState(userId: string): void {
    this.userService.changePremiumCommunityAccount(userId).subscribe((_) => {
      this.snackBar.open(`Updated`, '', {
        duration: 2500,
        verticalPosition: 'top',
      });
      this.fetchAndMapUsers();
    });
  }

  getCommunityState(hasCommunityAccess: boolean): string {
    return hasCommunityAccess ? 'done' : 'clear';
  }

  getTooltipCommunity(hasCommunityAccess: boolean): string {
    return !hasCommunityAccess
      ? 'Give access to Z - Community'
      : 'Revoke access to Z - Community';
  }

  getButtonStyleCommunity(hasCommunityAccess: boolean): string {
    return !hasCommunityAccess ? 'default' : 'primary';
  }

  changeCommunityAccess(userId: string): void {
    this.userService.changeCommunityAccess(userId).subscribe((_) => {
      this.snackBar.open('Updated', '', {
        duration: 2500,
        verticalPosition: 'top',
      });
      this.fetchAndMapUsers();
    });
  }

  // Function to update Core Hour from the "Core Hour" column.
  public updateCoreHour(formData, userId) {
    this.updatingProfile = true;

    this.userService
      .updateUserCoreHourAdmin(formData, userId)
      .subscribe((_) => {
        (this.editModeCoreHour = false), (this.updatingProfile = false);

        // Update and refresh
        this.snackBar.open(`Updated`, '', {
          duration: 2500,
          verticalPosition: 'top',
        });
        this.fetchAndMapUsers();
      });
  }

  // Function to update Expert Rating from the "Rating" column.
  public updateExpertRating(formData, expertData) {
    this.updatingProfile = true;

    const NewExpertInfo: Expert = {
      id: expertData.id,
      userId: expertData.userId,
      title: expertData.title,
      rating: formData,
      wage: expertData.wage,
      available: expertData.available,
    };

    this.expertService
      .updateUser(expertData.id, NewExpertInfo)
      .subscribe((_) => {
        (this.editModeRating = false), (this.updatingProfile = false);

        // Update and refresh
        this.snackBar.open(`Updated`, '', {
          duration: 2500,
          verticalPosition: 'top',
        });
        this.fetchAndMapUsers();
      });
  }

  public updateClient(formData) {
    this.updatingProfile = true;

    const newUserInfo: UserInformation = {
      lastName: formData.lastName,
      firstName: formData.firstNam,
      email: formData.email,
      phone: formData.phone,
      company: formData.company,
      service: formData.service,
      citizenship: formData.citizenship,
      country: formData.country,
      city: formData.city,
      address: formData.address,
      zipCode: formData.zipCode,
      softwares: [], // TMP
      languages: formData.language,
      rating: formData.rating,
    };

    this.userService
      .updateUserAdmin(this.userIdEdition, newUserInfo)
      .subscribe((_) => {
        this.clientService
          .updateUserAdmin(this.userIdEdition, newUserInfo)
          .subscribe((__) => {
            this.editModeClient = false;
            this.updatingProfile = false;
            this.fetchAndMapUsers();
          });
      });
  }

  public setVpnAccess(userId: string) {
    const vpnAccess = {
      vpnUsername: 'testUsername',
      vpnPassword: 'admin',
    };
    this.userService.setVpnAcessOfUser(vpnAccess, userId).subscribe(() => {});
  }

  openDialog(userId: string): void {
    const dialogRef = this.dialog.open(VpnAccessComponent, {
      width: '450px',
      data: { userId },
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }
}
