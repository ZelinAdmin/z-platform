import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';

@Component({
  selector: 'app-project-picture',
  templateUrl: './project-picture.component.html',
  styleUrls: ['./project-picture.component.less'],
})
export class ProjectPictureComponent implements OnInit {
  @Input() projectId: string;
  @Output() openEvent = new EventEmitter<any>();
  // public translatePage: ComponentsTrslt = ComponentsTrslt.EDIT_PROFILE_PHOTO;
  // TODO

  constructor(
    private snackbar: MatSnackBar,
    private fileService: FileManipulationsService
  ) {}

  ngOnInit(): void {}

  public openClose() {
    this.openEvent.emit();
  }

  async changeProjectPhoto(file: FileList) {
    this.fileService
      .uploadProjectPhoto(file[0], this.projectId)
      .subscribe((res) => {
        if (res == null) {
          this.snackbar.open('Error (to handle) : File already uploaded', '', {
            duration: 2500,
          });
        } else {
          location.reload();
        }
      });
  }
}
