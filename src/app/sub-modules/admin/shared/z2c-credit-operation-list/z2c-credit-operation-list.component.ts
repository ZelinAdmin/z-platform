import { Component, Input, OnInit } from '@angular/core';
import { CreditsUser } from 'src/app/interfaces/credits-user/credits-user';

@Component({
  selector: 'app-z2c-credit-operation-list',
  templateUrl: './z2c-credit-operation-list.component.html',
  styleUrls: ['./z2c-credit-operation-list.component.less'],
})
export class Z2cCreditOperationListComponent implements OnInit {
  @Input() creditsUser: CreditsUser;
  public displayedColumns: string[] = [
    'amount',
    'creditsAfter',
    'operationDate',
  ];

  constructor() {}

  ngOnInit() {}
}
