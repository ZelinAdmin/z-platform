import { Component, OnInit, ViewChild } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user/user.service';
import { MatTableDataSource } from '@angular/material/table';
import { OperationUserComponent } from 'src/app/sub-modules/admin/dialogs/operation-user/operation-user.component';
import { CreditsOperationRequest } from 'src/app/interfaces/credits-operation-request/credits-operation-request';
import { CreditService } from 'src/app/services/credit/credit.service';
import { DialogData } from 'src/app/interfaces/dialog-data/dialog-data';
import { MatSort } from '@angular/material/sort';

class CreditsUsers {}

@Component({
  selector: 'app-z2c-credits',
  templateUrl: './z2c-credits.component.html',
  styleUrls: ['./z2c-credits.component.less'],
  animations: [
    trigger('detailExpand', [
      state(
        'collapsed, void',
        style({ height: '0px', minHeight: '0', display: 'none' })
      ),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
      transition(
        'expanded <=> void',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class Z2cCreditsComponent implements OnInit {
  public dataSource: any; // users that will be displayed
  public columnsToDisplay: string[] = [
    'email',
    'company',
    'firstName',
    'lastName',
    'credits',
    'actions',
  ];
  public columnsToDisplayWithoutActions: string[] = [
    'email',
    'company',
    'firstName',
    'lastName',
    'credits',
  ];
  public userIdSelected: string;
  public amountSelected: number;
  public users: any;
  public expandedElement: CreditsUsers;
  // dataSource = ELEMENT_DATA;
  // columnsToDisplay = ['email', 'weight', 'symbol', 'position'];
  // expandedElement: PeriodicElement | null;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private creditsService: CreditService,
    private userService: UserService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.updateOperations();
    this.userService.getAll().subscribe((data) => {
      this.users = data;
      this.dataSource.sort = this.sort;
    });
    this.dataSource.sort = this.sort;
  }

  updateOperations() {
    this.creditsService.getAllUsersWithOperations().subscribe((data) => {
      this.dataSource = new MatTableDataSource<CreditsUsers>(data);
    });
  }

  openDialog(userId): void {
    if (userId != null) {
      this.userIdSelected = userId;
    }

    const dialogData: DialogData = {
      users: this.users,
      userId: this.userIdSelected,
      amount: this.amountSelected,
    };

    const dialogRef = this.dialog.open(OperationUserComponent, {
      width: '500px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result && result.userId && result.amount) {
        this.userIdSelected = result.userId;
        this.amountSelected = result.amount;
        this.addOperation();
      }
    });
  }

  addOperation() {
    const credit: CreditsOperationRequest = {
      userId: this.userIdSelected,
      amount: this.amountSelected,
      creditsAfter: 140.32,
      operationDate: new Date(),
    };
    this.creditsService.add(credit).subscribe((_) => {
      this.updateOperations();
    });
  }
}
