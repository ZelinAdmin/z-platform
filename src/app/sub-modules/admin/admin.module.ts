import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminPanelComponent } from './pages/admin-panel/admin-panel.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MeteoApiComponent } from './shared/meteo-api/meteo-api.component';
import { ProjectComponent } from './shared/project/project.component';
import { UserComponent } from './shared/user/user.component';
import { Z2cCreditsComponent } from './shared/z2c-credits/z2c-credits.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatListModule } from '@angular/material/list';
import { ImgFallbackModule } from 'ngx-img-fallback';
import { ProjectPermissionsComponent } from './dialogs/project-permissions/project-permissions.component';
import { ProjectConfigComponent } from 'src/app/sub-modules/admin/shared/project-config/project-config.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { ProjectPictureComponent } from './shared/project-picture/project-picture.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { VpnAccessComponent } from './dialogs/vpn-access/vpn-access.component';
import { OperationUserComponent } from './dialogs/operation-user/operation-user.component';
import { Z2cCreditOperationListComponent } from './shared/z2c-credit-operation-list/z2c-credit-operation-list.component';
import { MatSortModule } from '@angular/material/sort';
import { OperationProjectComponent } from './dialogs/operation-project/operation-project.component';

@NgModule({
  declarations: [
    AdminComponent,
    AdminPanelComponent,
    ProjectConfigComponent,
    MeteoApiComponent,
    ProjectComponent,
    UserComponent,
    Z2cCreditsComponent,
    ProjectPermissionsComponent,
    ProjectPictureComponent,
    VpnAccessComponent,
    OperationUserComponent,
    Z2cCreditOperationListComponent,
    OperationProjectComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule,
    MatTableModule,
    MatTabsModule,
    MatSortModule,
    MatPaginatorModule,
    MatListModule,
    ImgFallbackModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    FormsModule,
  ],
  entryComponents: [ProjectPermissionsComponent, VpnAccessComponent],
})
export class AdminModule {}
