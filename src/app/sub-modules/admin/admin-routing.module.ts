import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { NotFoundComponent } from 'src/app/shared-pages/not-found/not-found.component';
import { AdminPanelComponent } from './pages/admin-panel/admin-panel.component';
import { ProjectConfigComponent } from 'src/app/sub-modules/admin/shared/project-config/project-config.component';

const routes: Routes = [
  {
    path: '',
    component: AdminPanelComponent,
  },
  {
    path: 'project/:id',
    component: ProjectConfigComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
