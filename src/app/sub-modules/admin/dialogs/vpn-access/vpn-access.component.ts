import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-vpn-access',
  templateUrl: './vpn-access.component.html',
  styleUrls: ['./vpn-access.component.less'],
})
export class VpnAccessComponent {
  vpnForm: FormGroup;

  @Inject(MAT_DIALOG_DATA) public data: any;

  constructor(
    public dialogRef: MatDialogRef<VpnAccessComponent>,
    private formBuilder: FormBuilder,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.data = data;

    this.vpnForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      userPassword: ['', [Validators.required]],
    });
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.userService
      .setVpnAcessOfUser(
        {
          vpnUsername: this.vpnForm.controls.userName.value,
          vpnPassword: this.vpnForm.controls.userPassword.value,
        },
        this.data.userId
      )
      .subscribe(() => {});
    this.dialogRef.close();
  }
}
