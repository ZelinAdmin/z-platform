import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogData } from 'src/app/interfaces/dialog-data/dialog-data';

@Component({
  selector: 'app-operation-user',
  templateUrl: './operation-user.component.html',
  styleUrls: ['./operation-user.component.less'],
})
export class OperationUserComponent {
  constructor(
    public dialogRef: MatDialogRef<OperationUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onCancelClick(): void {
    this.dialogRef.close();
  }
}
