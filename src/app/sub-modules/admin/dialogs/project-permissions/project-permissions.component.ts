import { Component, Inject, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project/project.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AccessService } from 'src/app/services/access-service/access.service';
import { UserService } from 'src/app/services/user/user.service';
import { DialogData } from 'src/app/interfaces/dialog-data/dialog-data';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';

@Component({
  selector: 'app-project-permissions',
  templateUrl: './project-permissions.component.html',
  styleUrls: ['./project-permissions.component.less'],
})
export class ProjectPermissionsComponent implements OnInit {
  users: Array<UserInformation> = [];
  roles: any[] = [
    { display: 'Manager', value: 'MANAGER' },
    { display: 'Guest', value: 'GUEST' },
    { display: 'Customer', value: 'Customer' },
  ];
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  actualUser: UserInformation;
  selectedRole: string;
  selectedUser: string;

  constructor(
    public userService: UserService,
    public accessService: AccessService,
    public dialogRef: MatDialogRef<ProjectPermissionsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit() {
    if (!this.data.userId) {
      this.userService.getAll().subscribe((users: UserInformation[]) => {
        users.forEach((user) => {
          if (!user.isDisabled) {
            this.users.push(user);
          }
        });
      });
    } else {
      this.userService.get(this.data.userId).subscribe((user) => {
        this.actualUser = user;
      });
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isDisabled() {
    if (this.actualUser) {
      return true;
    }
    return false;
  }

  grantPermission() {
    if (!this.selectedUser && !this.data.userId) {
      return;
    }
    if (!this.selectedUser) {
      this.selectedUser = this.data.userId;
    }
    this.accessService.getAll(this.data.projectId).subscribe((elements) => {
      let found = false;

      elements.forEach((element) => {
        if (element.user.id === this.selectedUser) {
          found = true;
        }
      });
      if (found) {
        this.accessService
          .update(this.data.projectId, this.selectedUser, this.selectedRole)
          .subscribe(() => {});
      } else {
        this.accessService
          .add(this.data.projectId, this.selectedUser, this.selectedRole)
          .subscribe(() => {});
      }
    });
    this.dialogRef.close();
  }
}
