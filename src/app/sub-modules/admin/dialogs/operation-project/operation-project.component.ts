import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogDataNewProject } from 'src/app/interfaces/dialog-data/dialog-data-newproject';

@Component({
  selector: 'app-operation-project',
  templateUrl: './operation-project.component.html',
  styleUrls: ['./operation-project.component.less'],
})
export class OperationProjectComponent {
  constructor(
    public dialogRef: MatDialogRef<OperationProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogDataNewProject
  ) {}

  onCancelClick(): void {
    this.dialogRef.close();
  }
}
