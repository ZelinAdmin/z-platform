import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class MetSafeService {
  private apiKey = 'j6knb0Qy5xJPDyzChIkgN3EZPsWCJGqcvEG1sKVWz5w';

  constructor(private httpClient: HttpClient) {}

  get(lat: string, lon: string, alt: string): Observable<any> {
    return this.httpClient.get<string>(
      `${environment.apiUrl}admin/meteo/${lat}/${lon}/${alt}`
    );
  }

  getSliceMet(): Observable<any> {
    return this.httpClient.get<string>(
      `${environment.apiUrl}admin/meteo/slicemet`
    );
  }
}
