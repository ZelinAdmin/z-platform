import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Expert } from 'src/app/interfaces/expert/expert';
import {
  ComponentsTrslt,
  TranslationService,
} from 'src/app/services/translation/translation.service';
import { ExpertService } from 'src/app/services/expert/expert.service';
import { UserService } from 'src/app/services/user/user.service';
import { ProjectService } from 'src/app/services/project/project.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import { Project } from 'src/app/interfaces/project/project';
import { LanguageService } from 'src/app/services/language/language.service';
import { SoftwareService } from 'src/app/sub-modules/profile/services/software/software.service';
import { EditProfileComponent } from 'src/app/sub-modules/profile/shared/edit-profile/edit-profile.component';
import { Globals } from 'src/app/globals/globals';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { environment } from 'src/environments/environment';
import { AreaEnum } from 'src/app/sub-modules/profile/interfaces/area-enum/area-enum.enum';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { FavoritesService } from 'src/app/sub-modules/profile/services/favorites/favorites.service';
import { ExpertiseService } from 'src/app/sub-modules/profile/services/expertise/expertise.service';
import { ExpertisesEnums } from 'src/app/sub-modules/profile/interfaces/expertises/expertises.enum';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-expert-profile',
  templateUrl: './expert-profile.component.html',
  styleUrls: ['./expert-profile.component.less'],
})
export class ExpertProfileComponent implements OnInit {
  public translatePage: ComponentsTrslt = ComponentsTrslt.PROFILE;
  public languagePage: ComponentsTrslt = ComponentsTrslt.LANGUAGE;

  public fallbackImage = '/assets/fallbackProject.png';

  // Used to show 'about' informations more simply
  // Need to load data in async
  public expertDatas = [
    { label: 'company', data: '-' },
    { label: 'phone', data: '-' },
    { label: 'email', data: '-' },
    { label: 'location', data: '-' },
    { label: 'citizenship', data: '-' },
    { label: 'softwares', data: [] },
    { label: 'languages', data: [] },
  ];

  // Hardcoded tab, need to fill it with db datas
  public expertDemands = [
    { project: 'IAC_Demist', progress: 0 },
    { project: 'Test1', progress: 1 },
    { project: 'Test2', progress: 2 },
    { project: 'Tessszzzzzzzsssst3', progress: 3 },
    { project: 'Tezzzst4', progress: 4 },
  ];

  // 'Hardcoded' enum, need to fill the "data" field with db datas
  public expertises = ExpertisesEnums;

  // Array tab to display icons in profile header
  public areas = [];

  @Output() ClickUpload = new EventEmitter<void>(); // [event binding] to transmit data to parent
  @Input() profileId: string; // [ property binding ] to transmit data to child
  public ownProfile: boolean = null; // Modify it to display edit/addFav button
  public connectedIsExpert: boolean = null; // Modify it to display edit/addFav button
  public selectedMenuItem = 'about'; // Used to switch tab in content-part
  public photoUrl: string; // Used to concatenate image url without modifying original data
  public photoEditMode = false; // Used to display profile photo edit popup

  expert: Expert;
  expert$: Observable<Expert>;
  userInfos: UserInformation;
  projectRoles: Project[];
  projectRolesUpdate: Project[] = [];
  editProfileForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    public translateService: TranslationService,
    public expertiseService: ExpertiseService,
    public languageService: LanguageService,
    private softwareService: SoftwareService,
    private expertService: ExpertService,
    private userService: UserService,
    private projectService: ProjectService,
    private favService: FavoritesService,
    private formBuilder: FormBuilder,
    private fileService: FileManipulationsService,
    private snackbar: MatSnackBar,
    private router: Router
  ) {
    // keeped to update the "about"
    // TODO: use other method
    this.editProfileForm = this.formBuilder.group({
      lastName: [{ value: '', disabled: true }],
      firstName: '',
      email: '',
      phone: '',
      company: '',
      title: '',
      wage: '',
      citizenship: '',
      country: '',
      city: '',
      address: '',
      zipCode: '',
      softwares: [],
      aerospace: false,
      energy: false,
      transport: false,
      industries: false,
    });
  }

  ngOnInit() {
    this.ownProfile = this.profileId === localStorage.getItem(Globals.USER_ID);

    // Replace by request to get only if user is expert
    this.userService
      .getPartial(localStorage.getItem(Globals.USER_ID))
      .subscribe((user) => {
        this.connectedIsExpert = user.expertId != null;
      });

    this.expert$ = this.expertService.getByUserId(this.profileId);
    this.getProfileInfo();
    this.getProjectsInfo();
  }

  // -----------------------------------------------------------------------------//
  //                        E X P E R T       P R O J E C T S
  // -----------------------------------------------------------------------------//
  private getProjectsInfo() {
    this.projectService
      .getUserProjects(this.profileId)
      .subscribe((data: UserProject[]) => {
        this.projectRolesUpdate = data.map((element) => {
          return {
            image: `${environment.apiUrl}files/${element.project.image}/download`,
            id: element.project.id,
            name: element.project.name,
            progress: element.project.progress,
            role: element.role,
            globalprogress: element.project.globalprogress,
            AndDate: element.project.endDate,
            StartDate: element.project.startDate,
            NextMeeting: element.project.nextMeeting,
            TeamsUrl: element.project.teamsUrl,
          };
        });

        this.projectRoles = this.projectRolesUpdate;
      });

    this.projectService
      .getUserProjects(localStorage.getItem(Globals.USER_ID))
      .subscribe((projects: UserProject[]) => {
        projects.map((elem) => elem);
      });
  }

  // spinner color
  updateColor(progress) {
    if (progress < 100) {
      return 'warn';
    }
  }

  // "Parse" projects informations get from the back
  // getRolesFromData(data): Project[] {
  //   return data.map((element) => {
  //     return {
  //       id: element.id,
  //       name: element.name,
  //       role: element.role,
  //       progress: element.progress,
  //       globalprogress: element.globalprogress,
  //       AndDate: element.AndDate,
  //       StartDate: element.StartDate,
  //       NextMeeting: element.NextMeeting,
  //       TeamsUrl: element.TeamsUrl,
  //     };
  //   });
  // }

  /*  Generate link to project's profile  */
  public navigateToProject(projectId) {
    this.router.navigate(['vision/project/' + projectId]);
  }

  // -----------------------------------------------------------------------------//
  //                        E X P E R T       P R O F I L E     D A T A
  // -----------------------------------------------------------------------------//

  /* Fill the dataSource array with data of the profil selected */
  getProfileInfo() {
    this.userService
      .getPartial(this.profileId)
      .subscribe((user: UserInformation) => {
        this.photoUrl = `${environment.apiUrl}files/${user.image}/download`;
        this.userInfos = user;

        this.expertDatas[0].data = user.company;
        this.expertDatas[1].data = user.phone;
        this.expertDatas[2].data = user.email;
        this.expertDatas[3].data = user.address;
        this.expertDatas[4].data = user.citizenship;

        // this.editProfileForm.setValue({
        //   lastName: this.userInfos.lastName,
        //   firstName: this.userInfos.firstName,
        //   email: this.userInfos.email,
        //   phone: this.userInfos.phone,
        //   company: this.userInfos.company,
        //   citizenship: this.userInfos.citizenship,
        //   country: this.userInfos.country,
        //   city: this.userInfos.city,
        //   address: this.userInfos.address,
        //   zipCode: this.userInfos.zipCode,
        //   title: '',
        //   wage: '',
        //   softwares: '',
        //   aerospace: false,
        //   energy: false,
        //   transport: false,
        //   industries: false,
        // });

        // Get Expert Info
        this.expertService
          .getByUserId(this.profileId)
          .subscribe((expert: Expert) => {
            this.expert = expert;

            this.expertises = this.expertiseService.getProfileExpertises(
              this.expert.id
            );

            this.areas = this.getProfileAreas();

            // Get User Languages info
            this.languageService
              .getLanguagesByUserId(this.profileId)
              .subscribe((e) => {
                this.userInfos.languages = e.map((elem) => elem.name);
                this.expertDatas[6].data = this.userInfos.languages;

                // Get USer Softwares Info
                this.softwareService
                  .getSoftwaresByUserId(this.profileId)
                  .subscribe((k) => {
                    this.userInfos.softwares = k.map((elem) => elem.name);
                    this.expertDatas[5].data = this.userInfos.softwares;

                    // Get User "Areas" info.
                    // this.expertService
                    //   .getAreasNames(this.profileId)
                    //   .subscribe((userAreas) => {
                    //     // this.userInfos.areas = userAreas;

                    //     // this.editProfileForm.patchValue({
                    //     //   title: this.expert.title,
                    //     //   wage: this.expert.wage,
                    //     //   softwares: this.userInfos.softwares,
                    //     // });

                    //     // Here we prefill the form with user info, there are obviously better ways to do this, but the with the
                    //     // current implementation of user "Areas" this is easier for now. We would have to manually add areas here
                    //     // (if we increase the diversity of areas in the DB) and at several other places in this file.
                    //     // if (userAreas.includes('AEROSPACE')) {
                    //     //   this.editProfileForm.patchValue({
                    //     //     aerospace: true,
                    //     //   });
                    //     // }
                    //     // if (userAreas.includes('ENERGY')) {
                    //     //   this.editProfileForm.patchValue({
                    //     //     energy: true,
                    //     //   });
                    //     // }
                    //     // if (userAreas.includes('TRANSPORT')) {
                    //     //   this.editProfileForm.patchValue({
                    //     //     transport: true,
                    //     //   });
                    //     // }
                    //     // if (userAreas.includes('INDUSTRIES')) {
                    //     //   this.editProfileForm.patchValue({
                    //     //     industries: true,
                    //     //   });
                    //     // }
                    //   });
                  });
              });
          });
      });
  }

  // get all areas concerning the expert
  private getProfileAreas() {
    this.expertService.getAreasNames(this.profileId).subscribe((data) => {
      this.areas = AreaEnum;
      for (let cpt = 0; cpt < this.areas.length; cpt++) {
        if (data.find((e) => e === this.areas[cpt].name) === undefined) {
          this.areas.splice(cpt, 1);
          cpt = cpt - 1;
        }
      }
    });
    return this.areas;
  }

  onAvailabilityChange(newValue: boolean) {
    this.expertService
      .updateAvailability(this.expert.id, newValue)
      .subscribe((e) => {});
  }

  onFixedPriceStudiesChange(newValue: boolean) {
    this.expertService
      .updateFixedPriceStudies(this.expert.id, newValue)
      .subscribe((e) => {});
  }

  onTechAssistanceChange(newValue: boolean) {
    this.expertService
      .updateTechAssistance(this.expert.id, newValue)
      .subscribe((e) => {});
  }

  // -----------------------------------------------------------------------------//
  //                        E X P E R T I S E S
  // -----------------------------------------------------------------------------//

  // Temporary. Expert can himself update his expertises
  public onExpertiseCBClic(expertise, subExpertise) {
    const expInd: number = this.expertises.findIndex(
      (e) => e.name === expertise
    );
    const subExpInd: number = this.expertises[expInd].subExpertises.findIndex(
      (e) => e.name === subExpertise
    );
    this.expertises[expInd].subExpertises[subExpInd].done = !this.expertises[
      expInd
    ].subExpertises[subExpInd].done;
    this.expertiseService.updateExpertise(
      this.expert.id,
      expertise,
      this.expertises[expInd].subExpertises
    );
  }

  // -----------------------------------------------------------------------------//
  //                        D E M A N D S
  // -----------------------------------------------------------------------------//

  // Changing menu item selected
  public changeSection(newValue: string): void {
    if (newValue === 'demands') {
      this.snackbar.open('Coming soon', '', {
        duration: 2500,
      });
    } else {
      this.selectedMenuItem = newValue;
    }
  }

  // -----------------------------------------------------------------------------//
  //                        F A V O R I T E
  // -----------------------------------------------------------------------------//

  // Add on favorites
  onClickFavourite() {
    this.favService
      .add(localStorage.getItem('clientId'), this.expert.id)
      .subscribe(() => {});
  }

  // -----------------------------------------------------------------------------//
  //                        E D I T        P R O F I L E
  // -----------------------------------------------------------------------------//

  opClEditMode(): void {
    const dialogRef = this.dialog.open(EditProfileComponent, {
      width: '850px',
      height: '600px',
      data: { user: this.userInfos },
    });
  }

  // -----------------------------------------------------------------------------//
  //                        E D I T        A V A T A R
  // -----------------------------------------------------------------------------//

  // Open edit photo window
  opClEditPhoto() {
    this.photoEditMode = !this.photoEditMode;
  }

  // change profile photo
  async changeProfilePhoto(file: FileList) {
    this.fileService
      .uploadProfilePhoto(file[0], this.profileId)
      .subscribe((res) => {
        if (res == null) {
          this.snackbar.open('Error (to handle) : File already uploaded', '', {
            duration: 2500,
          });
        } else {
          location.reload();
        }
      });
  }
}
