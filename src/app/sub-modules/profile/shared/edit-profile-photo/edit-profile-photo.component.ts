import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {
  ComponentsTrslt,
  TranslationService,
} from 'src/app/services/translation/translation.service';
import { FileService } from 'src/app/sub-modules/vision/services/file-service/file.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';

@Component({
  selector: 'app-edit-profile-photo',
  templateUrl: './edit-profile-photo.component.html',
  styleUrls: ['./edit-profile-photo.component.less'],
})
export class EditProfilePhotoComponent implements OnInit {
  @Input() profileId: string;
  @Output() openEvent = new EventEmitter<any>();
  public translatePage: ComponentsTrslt = ComponentsTrslt.EDIT_PROFILE_PHOTO;

  constructor(
    private snackbar: MatSnackBar,
    private fileService: FileManipulationsService,
    public translateService: TranslationService
  ) {}

  ngOnInit() {}

  public openClose() {
    this.openEvent.emit();
  }

  async changeProfilePhoto(file: FileList) {
    this.fileService
      .uploadProfilePhoto(file[0], this.profileId)
      .subscribe((res) => {
        if (res == null) {
          this.snackbar.open('Error (to handle) : File already uploaded', '', {
            duration: 2500,
          });
        } else {
          location.reload();
        }
      });
  }
}
