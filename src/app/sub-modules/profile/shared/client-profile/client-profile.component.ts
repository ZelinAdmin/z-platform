// ----------------------    I M P O R T ---------------------------------- //
import {
  Component,
  ElementRef,
  Input,
  OnInit,
  Output,
  ViewChild,
  EventEmitter,
} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

// intern app import
import { Globals } from 'src/app/globals/globals';
import {
  ComponentsTrslt,
  TranslationService,
} from 'src/app/services/translation/translation.service';
import { Client } from 'src/app/interfaces/client/client';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import { Project } from 'src/app/interfaces/project/project';
import { UserService } from 'src/app/services/user/user.service';
import { ProjectService } from 'src/app/services/project/project.service';
import { ClientService } from 'src/app/services/client/client.service';
import { ExpertService } from 'src/app/services/expert/expert.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { LanguageService } from 'src/app/services/language/language.service';
import { SoftwareService } from 'src/app/sub-modules/profile/services/software/software.service';
import { CompanyService } from 'src/app/sub-modules/profile/services/company/company.service';
import { environment } from 'src/environments/environment';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { AreaEnum } from 'src/app/sub-modules/profile/interfaces/area-enum/area-enum.enum';
import { EditProfileComponent } from 'src/app/sub-modules/profile/shared/edit-profile/edit-profile.component';

// import from material
import { MatDialog } from '@angular/material/dialog';
import {
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { MatButton } from '@angular/material/button';
import { MatChipInputEvent } from '@angular/material/chips';

// ----------------------    C O M P O N E N T ---------------------------------- //
@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.less'],
})
export class ClientProfileComponent implements OnInit {
  public translatePage: ComponentsTrslt = ComponentsTrslt.PROFILE;
  public languagePage: ComponentsTrslt = ComponentsTrslt.LANGUAGE;

  public fallbackImage = '/assets/fallbackProject.png';

  // Used to show 'about' informations more simply
  // Need to load data in async
  public clientDatas = [
    { label: 'company', data: '-' },
    { label: 'service', data: '-' },
    { label: 'phone', data: '-' },
    { label: 'email', data: '-' },
    { label: 'location', data: '-' },
    { label: 'citizenship', data: '-' },
    { label: 'softwares', data: [] },
    { label: 'languages', data: [] },
  ];

  // 'Hardcoded' enum, need to fill the "data" field with db datas
  public clientDemands = [
    { project: 'IAC_Demist', progress: 0 },
    { project: 'Test1', progress: 1 },
    { project: 'Test2', progress: 2 },
    { project: 'Tessszzzzzzzsssst3', progress: 3 },
    { project: 'Tezzzst4', progress: 4 },
  ];

  // Array tab to display icons in profile header
  public areas = [];
  // Array to display all areas in profile Edition
  public allAreas = [];

  // Array to display all languages in profile Edition
  public allLanguages = [];

  // Array to display all softwares in profile Edition
  public allSoftwares: string[] = [];
  // Array to display selected softwares in profile Edition
  public currentSoftwares: string[] = [];
  // Array to display available softwares in profile Edition
  public displayedSoft: string[] = [];

  separatorKeysCodes: number[] = [ENTER, COMMA];
  softwareCtrl = new FormControl();

  private softInput: ElementRef<HTMLInputElement>;
  @ViewChild('softInput', { static: false }) set softContent(
    softContent: ElementRef<HTMLInputElement>
  ) {
    if (softContent) {
      // initially setter gets called with undefined
      this.softInput = softContent;
    }
  }
  private cancelInput: MatButton;
  @ViewChild('cancelEditButton', { static: false }) set cancelContent(
    cancelContent: MatButton
  ) {
    if (cancelContent) {
      this.cancelInput = cancelContent;
    }
  }
  private applyInput: MatButton;
  @ViewChild('applyEditButton', { static: false }) set applyContent(
    applyContent: MatButton
  ) {
    if (applyContent) {
      this.applyInput = applyContent;
    }
  }
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

  @Output() clickUpload = new EventEmitter<void>();
  @Input() profileId: string;
  public suggestions: Array<string> = null; // Used to select existing service in form when company field is changed
  public ownProfile: boolean = null; // Modify it to display edit/addFav button
  public selectedMenuItem = 'about'; // Used to switch tab in content-part
  public editMode = false; // Used to display edit popup
  public photoUrl: string; // Used to concatenate image url without modifying original data
  public photoEditMode = false; // Used to display profile photo edit popup
  public updatingProfile = false; // Used to display spinner when updating profile after edit

  userInfos: UserInformation;
  client: Client;
  projectRoles: Project[];
  projectRolesUpdate: Project[] = [];
  editProfileForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    public translateService: TranslationService,
    private expertService: ExpertService, // TEMP
    private clientService: ClientService,
    private languageService: LanguageService,
    private softwareService: SoftwareService,
    private userService: UserService,
    private projectService: ProjectService,
    public compServService: CompanyService,
    private formBuilder: FormBuilder,
    private fileService: FileManipulationsService,
    private snackbar: MatSnackBar,
    private router: Router
  ) {
    // this.editProfileForm = this.formBuilder.group({
    //   lastName: [{ value: '', disabled: true }],
    //   firstName: '',
    //   email: '',
    //   phone: '',
    //   company: '',
    //   service: '',
    //   citizenship: '',
    //   country: '',
    //   city: '',
    //   address: '',
    //   zipCode: '',
    //   softwares: [],
    //   aerospace: false,
    //   energy: false,
    //   transport: false,
    //   industries: false,
    // });
  }

  // add(event: MatChipInputEvent): void {
  //   const input = event.input;
  //   const value = event.value;

  //   // Add our software
  //   if ((value || '').trim()) {
  //     this.currentSoftwares.push(value.trim());
  //   }

  //   // Reset the input value
  //   if (input) {
  //     input.value = '';
  //   }
  //   this.softwareCtrl.setValue(null);
  //   this.noSoftDuplicate();
  // }

  // remove(software: string): void {
  //   const index = this.currentSoftwares.indexOf(software);
  //   if (index >= 0) {
  //     this.currentSoftwares.splice(index, 1);
  //   }
  //   this.noSoftDuplicate();
  // }

  // selected(event: MatAutocompleteSelectedEvent): void {
  //   this.currentSoftwares.push(event.option.viewValue);
  //   this.softInput.nativeElement.value = '';
  //   this.softwareCtrl.setValue(null);
  //   this.noSoftDuplicate();
  // }

  // private _filter(value: string): string[] {
  //   return this.allSoftwares.filter(
  //     (software) => software.toLowerCase().indexOf(value.toLowerCase()) === 0
  //   );
  // }

  // public noSoftDuplicate() {
  //   this.displayedSoft = [];
  //   this.displayedSoft = this.allSoftwares.map((software) => {
  //     if (
  //       this.currentSoftwares.find((e: string) => e === software) === undefined
  //     ) {
  //       return software;
  //     }
  //   });
  // }

  ngOnInit() {
    this.ownProfile = this.profileId === localStorage.getItem(Globals.USER_ID);

    // this.languageService.getAllLanguages().subscribe((res) => {
    //   this.allLanguages = res.map((e) => e.name);
    // });

    // this.softwareService.getAllSoftwares().subscribe((res) => {
    //   this.allSoftwares = res.map((e) => e.name);
    //   // this.noSoftDuplicate();
    // });

    // this.expertService.getAllAreas().subscribe((res) => {
    //   this.allAreas = res;
    // });

    this.getProfileInfo();
    this.getProjectsInfo();
  }

  // -----------------------------------------------------//
  //                  C L I E N T     P R O J E C T S
  // ----------------------------------------------------//
  private getProjectsInfo() {
    this.projectService
      .getUserProjects(this.profileId)
      .subscribe((data: UserProject[]) => {
        this.projectRolesUpdate = data.map((element) => {
          return {
            image: `${environment.apiUrl}files/${element.project.image}/download`,
            id: element.project.id,
            name: element.project.name,
            progress: element.project.progress,
            role: element.role,
            globalprogress: element.project.globalprogress,
            AndDate: element.project.endDate,
            StartDate: element.project.startDate,
            NextMeeting: element.project.nextMeeting,
            TeamsUrl: element.project.teamsUrl,
          };
        });
        this.projectRoles = this.projectRolesUpdate;
      });

    this.projectService
      .getUserProjects(localStorage.getItem(Globals.USER_ID))
      .subscribe((projects: UserProject[]) => {
        // this.projectRoles = this.getRolesFromData(
        //   projects.map((elem) => elem.project)
        // );
        projects.map((elem) => elem.project);
      });
  }

  // "Parse" projects informations get from the back
  // getRolesFromData(data): Project[] {
  //   return data.map((element) => {
  //     return {
  //       id: element.projectId,
  //       name: element.name,
  //       role: element.role,
  //       progress: element.progress,
  //       globalprogress: element.Globalprogress,
  //       AndDate: element.AndDate,
  //       StartDate: element.StartDate,
  //       NextMeeting: element.NextMeeting,
  //       TeamsUrl: element.TeamsUrl,
  //     };
  //   });
  // }

  // spinner color
  updateColorClass(progress) {
    if (progress < 100) {
      return 'red';
    } else {
      return 'green';
    }
  }

  /*  Generate link to project's profile  */
  public navigateToProject(projectId) {
    this.router.navigate(['vision/project/' + projectId]);
  }

  // -------------------------------------------------------------------//
  //                  C L I E N T      P R O F I L E    D A T A
  // -------------------------------------------------------------------//

  // Get and set profil info
  // TODO is it necessary to use clientData array????
  getProfileInfo() {
    this.userService.getPartial(this.profileId).subscribe((user) => {
      if (user.image != null) {
        this.photoUrl = `${environment.apiUrl}files/${user.image}/download`;
      } else {
        this.photoUrl = 'assets/fallBackUser.png';
      }

      this.userInfos = user;

      this.clientDatas[0].data = this.userInfos.company;
      this.clientDatas[2].data = this.userInfos.phone;
      this.clientDatas[3].data = this.userInfos.email;
      this.clientDatas[4].data = this.userInfos.address;
      this.clientDatas[5].data = this.userInfos.citizenship;

      // Get Client Info
      this.clientService
        .getByUserId(this.profileId)
        .subscribe((client: Client) => {
          this.clientDatas[1].data = client.service;
          // this.changeFormCompany(this.userInfos.company);
          this.client = client;
          this.areas = this.getProfileAreas();

          // Get User Languages Info
          this.languageService
            .getLanguagesByUserId(this.profileId)
            .subscribe((e) => {
              this.userInfos.languages = e.map((elem) => elem.name);
              this.clientDatas[7].data = this.userInfos.languages;

              // Get User Softwares info
              this.softwareService
                .getSoftwaresByUserId(this.profileId)
                .subscribe((k) => {
                  this.userInfos.softwares = k.map((elem) => elem.name);
                  this.clientDatas[6].data = this.userInfos.softwares;
                  this.currentSoftwares = this.userInfos.softwares;
                  // this.noSoftDuplicate();

                  // Get User "Areas" info.
                  this.expertService
                    .getAreasNames(this.profileId)
                    .subscribe((userAreas) => {
                      this.userInfos.areas = userAreas;

                      // this.editProfileForm.setValue({
                      //   lastName: this.userInfos.lastName,
                      //   firstName: this.userInfos.firstName,
                      //   email: this.userInfos.email,
                      //   phone: this.userInfos.phone,
                      //   company: this.userInfos.company,
                      //   service: this.client.service,
                      //   citizenship: this.userInfos.citizenship,
                      //   country: this.userInfos.country,
                      //   city: this.userInfos.city,
                      //   address: this.userInfos.address,
                      //   zipCode: this.userInfos.zipCode,
                      //   softwares: this.userInfos.softwares,
                      //   aerospace: false,
                      //   energy: false,
                      //   transport: false,
                      //   industries: false,
                      // });
                      // Here we prefill the form with user info, there are obviously better ways to do this, but the with the
                      // current implementation of user "Areas" this is easier for now. We would have to manually add areas here
                      // (if we increase the diversity of areas in the DB) and at several other places in this file.
                      // if (userAreas.includes('AEROSPACE')) {
                      //   this.editProfileForm.patchValue({
                      //     aerospace: true,
                      //   });
                      // }
                      // if (userAreas.includes('ENERGY')) {
                      //   this.editProfileForm.patchValue({
                      //     energy: true,
                      //   });
                      // }
                      // if (userAreas.includes('TRANSPORT')) {
                      //   this.editProfileForm.patchValue({
                      //     transport: true,
                      //   });
                      // }
                      // if (userAreas.includes('INDUSTRIES')) {
                      //   this.editProfileForm.patchValue({
                      //     industries: true,
                      //   });
                      // }
                    });
                });
            });
        });
    });
  }

  private getProfileAreas() {
    this.expertService.getAreasNames(this.profileId).subscribe((data) => {
      this.areas = AreaEnum;
      for (let cpt = 0; cpt < this.areas.length; cpt++) {
        if (data.find((e) => e === this.areas[cpt].name) === undefined) {
          this.areas.splice(cpt, 1);
          cpt = cpt - 1;
        }
      }
    });
    return this.areas;
  }
  // -------------------------------------------------------------------//
  //                 D E M A N D S
  // -------------------------------------------------------------------//
  // Changing menu item selected
  public changeSection(newValue: string): void {
    if (newValue === 'demands') {
      this.snackbar.open('Coming soon', '', {
        duration: 2500,
      });
    } else {
      this.selectedMenuItem = newValue;
    }
  }

  // public opClEditMode() {
  //   this.editMode = !this.editMode;
  //   if (!this.editMode) {
  //     this.getProfileInfo();
  //   }
  // }

  // -------------------------------------------------------------------//
  //            E D I T     P R O F I L E
  // -------------------------------------------------------------------//
  opClEditMode(): void {
    const dialogRef = this.dialog.open(EditProfileComponent, {
      width: '850px',
      height: '600px',
      data: { user: this.userInfos },
    });
  }

  public OpClPpEditMode() {
    this.photoEditMode = !this.photoEditMode;
  }

  // public changeFormCompany(company: string) {
  //   this.compServService
  //     .getAllByCompany(company.toLowerCase())
  //     .subscribe((res) => {
  //       this.suggestions = res.map((e) => e.name);
  //       for (let cpt = 0; cpt < this.suggestions.length; cpt++) {
  //         this.suggestions[cpt] =
  //           this.suggestions[cpt].charAt(0).toUpperCase() +
  //           this.suggestions[cpt].slice(1);
  //       }
  //     });
  // }

  // public updateClient(formData) {
  //   const areasArray = [];

  //   if (formData.aerospace !== false) {
  //     areasArray.push('AEROSPACE');
  //   }
  //   if (formData.energy !== false) {
  //     areasArray.push('ENERGY');
  //   }
  //   if (formData.transport !== false) {
  //     areasArray.push('TRANSPORT');
  //   }
  //   if (formData.industries !== false) {
  //     areasArray.push('INDUSTRIES');
  //   }

  //   const str = formData.phone;
  //   const matches = str.replace(/\D/g, '');

  //   const newUserInfo: UserInformation = {
  //     lastName: formData.lastName,
  //     firstName: formData.firstName,
  //     email: formData.email,
  //     phone: matches,
  //     company: formData.company,
  //     service: formData.service,
  //     citizenship: formData.citizenship,
  //     country: formData.country,
  //     city: formData.city,
  //     address: formData.address,
  //     zipCode: formData.zipCode,
  //     image: this.userInfos.image,
  //     softwares: formData.softwares,
  //     languages: this.userInfos.languages,
  //     areas: areasArray,
  //   };

  //   this.updatingProfile = true;
  //   this.applyInput.disabled = true;
  //   this.cancelInput.disabled = true;

  //   this.userService
  //     .updateUser(this.profileId, newUserInfo)
  //     .subscribe((user) => {
  //       this.clientService
  //         .updateUser(this.profileId, newUserInfo)
  //         .subscribe((client) => {
  //           this.getProfileInfo();
  //           this.opClEditMode();

  //           // Add company service
  //           this.compServService
  //             .getAllByNameAndCompany(newUserInfo.service, newUserInfo.company)
  //             .subscribe((res) => {
  //               if (res.length === 0) {
  //                 this.compServService
  //                   .add({
  //                     id: '',
  //                     name: newUserInfo.service,
  //                     company: newUserInfo.company,
  //                   })
  //                   .subscribe((_) => {});
  //               }

  //               this.updatingProfile = false;
  //               this.applyInput.disabled = false;
  //               this.cancelInput.disabled = false;
  //             });
  //         });
  //     });
  // }

  // -------------------------------------------------------------------//
  //                     P R O F I L E       A V A T A R
  // -------------------------------------------------------------------//
  async changeProfilePhoto(file: FileList) {
    this.fileService
      .uploadProfilePhoto(file[0], this.profileId)
      .subscribe((res) => {
        if (res == null) {
          this.snackbar.open('Error (to handle) : File already uploaded', '', {
            duration: 2500,
          });
        } else {
          location.reload();
        }
      });
  }
}
