import {
  Component,
  ElementRef,
  Input,
  OnInit,
  Output,
  ViewChild,
  EventEmitter,
} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';

// import from material
import {
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { MatDialogRef } from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  AbstractControl,
} from '@angular/forms';

// import from app
import { Expert } from 'src/app/interfaces/expert/expert';
import {
  ComponentsTrslt,
  TranslationService,
} from 'src/app/services/translation/translation.service';
import { ExpertService } from 'src/app/services/expert/expert.service';
import { UserService } from 'src/app/services/user/user.service';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import { LanguageService } from 'src/app/services/language/language.service';
import { SoftwareService } from 'src/app/sub-modules/profile/services/software/software.service';
import { Globals } from 'src/app/globals/globals';
import { ExpertiseService } from 'src/app/sub-modules/profile/services/expertise/expertise.service';
import { ExpertisesEnums } from 'src/app/sub-modules/profile/interfaces/expertises/expertises.enum';
import { Client } from 'src/app/interfaces/client/client';
import { ClientService } from 'src/app/services/client/client.service';
import { CompanyService } from 'src/app/sub-modules/profile/services/company/company.service';
import { HashService } from 'src/app/services/hash/hash.service';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.less'],
})
export class EditProfileComponent implements OnInit {
  // app translation management
  public translatePage: ComponentsTrslt = ComponentsTrslt.PROFILE;
  public languagePage: ComponentsTrslt = ComponentsTrslt.LANGUAGE;

  // pop up buttons
  private cancelInput: MatButton;
  @ViewChild('cancelEditButton', { static: false }) set cancelContent(
    cancelContent: MatButton
  ) {
    if (cancelContent) {
      this.cancelInput = cancelContent;
    }
  }

  private applyInput: MatButton;
  @ViewChild('applyEditButton', { static: false }) set applyContent(
    applyContent: MatButton
  ) {
    if (applyContent) {
      this.applyInput = applyContent;
    }
  }

  // EDIT POPUP WITH DB DATA
  @Input() profileId: string;
  public ownProfile: boolean = null; // Modify it to display edit/addFav button
  public editMode = false; // Used to display edit popup
  public updatingProfile = false; // Used to display spinner when updating profile after edit
  public connectedIsExpert: boolean = null; // Modify it to display edit/addFav button
  public connectedIsClient: boolean = null;
  editProfileForm: FormGroup;
  // user info
  userInfos: UserInformation;
  expert: Expert; // only expert
  client: Client; // only client
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;
  // autocomplete lists config :
  public allAreas = [];
  public allLanguages = [];
  public suggestions: Array<string> = null; // Only client :  Used to select existing service in form when company field is changed
  // software input config:
  separatorKeysCodes: number[] = [ENTER, COMMA];
  public displayedSoft: string[] = [];
  public allSoftwares: string[] = [];
  softwareCtrl = new FormControl();
  filteredSoftwares: Observable<string[]>;
  software: string[];
  public currentSoftwares: string[] = [];

  private softInput: ElementRef<HTMLInputElement>;
  @ViewChild('softInput', { static: false }) set content(
    content: ElementRef<HTMLInputElement>
  ) {
    if (content) {
      // initially setter gets called with undefined
      this.softInput = content;
    }
  }

  // [event binding] to transmit data to parent
  @Output() ClickUpload = new EventEmitter<void>();

  // Used to show 'about' informations more simply/ Need to load data in async
  public userDatas = [
    { label: 'company', data: '-' }, // [0]
    { label: 'phone', data: '-' }, // [1]
    { label: 'email', data: '-' }, // [2]
    { label: 'location', data: '-' }, // [3]
    { label: 'citizenship', data: '-' }, // [4]
    { label: 'softwares', data: [] }, // [5]
    { label: 'languages', data: [] }, // [6]
    { label: 'service', data: '-' }, // [7]
  ];

  // check data before send request
  passwordIsHidden = true;
  newPassword: string;
  newEmail: string;
  newPhone: string;
  newZipCode: string;
  emailCheckedOk = false;
  phoneCheckedOk = false;
  zipCodeCheckedOk = false;

  // 'Hardcoded' enum, need to fill the "data" field with db datas
  public expertises = ExpertisesEnums;

  constructor(
    public dialogRef: MatDialogRef<EditProfileComponent>,
    private formBuilder: FormBuilder,
    private router: Router,

    public translateService: TranslationService,
    private userService: UserService,
    private expertService: ExpertService, // only expert
    private clientService: ClientService, // only client
    public expertiseService: ExpertiseService,
    public languageService: LanguageService,
    private softwareService: SoftwareService,
    private snackbar: MatSnackBar,
    private hashService: HashService,
    public compServService: CompanyService // only client
  ) {
    // build the form
    this.editProfileForm = this.formBuilder.group({
      lastName: [{ value: '', disabled: true }],
      firstName: '',
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [
          Validators.pattern(
            '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&;,.])[A-Za-zd$@$!%*?&].{7,}'
          ),
          Validators.required,
        ],
      ],
      checkPassword: ['', [this.confirmationValidator], Validators.required],
      phone: ['', [Validators.pattern('^(0)(6|7|9)d{8}')]],
      company: '',
      title: '', // only expert
      wage: '', // only expert
      citizenship: '',
      country: '',
      city: '',
      address: '',
      zipCode: '',
      softwares: [],
      aerospace: false,
      energy: false,
      transport: false,
      industries: false,
      service: '', // only client
      languages: [],
    });

    // get all choice of softwares allready in db
    this.filteredSoftwares = this.softwareCtrl.valueChanges.pipe(
      startWith(null),
      map((software: string | null) =>
        software ? this._filter(software) : this.allSoftwares.slice()
      )
    );
  }

  /* links: select title + ctrl+D to navigate inside code
   ***  O N        I N I T
   ***  P O P U P     M A N A G E M E N T
   ***  S O F T W A R E     M A N A G E M E N T
   ***  R E F R E S H     D A T A     A N D     P A R E N T
   ***  U P D A T E     U S E R
   ***  D A T A    A B O U T    U S E R     C O N N E C T E D
   */

  /********************       O N        I N I T    ***************************/
  ngOnInit(): void {
    // open the edit popUp
    this.opClEditMode();

    // set data about user connected
    this.profileId = localStorage.getItem(Globals.USER_ID);
    this.ownProfile = true;

    // get data about user connected
    this.getProfileInfo();
    this.getClientInfo();
    this.getUserSpecificInfo();

    // set all languages and softwares choice in selected input
    this.languageService.getAllLanguages().subscribe((res) => {
      this.allLanguages = res.map((e) => e.name);
    });

    this.softwareService.getAllSoftwares().subscribe((res) => {
      this.allSoftwares = res.map((e) => e.name);
      this.noSoftDuplicate();
    });
  }

  /********************       S O F T W A R E     M A N A G E M E N T     ***************************/
  // filter to get all choice of softwares allready in db
  private _filter(value: string): string[] {
    return this.allSoftwares.filter(
      (software) => software.toLowerCase().indexOf(value.toLowerCase()) === 0
    );
  }

  public noSoftDuplicate() {
    this.displayedSoft = [];
    this.displayedSoft = this.allSoftwares.map((software) => {
      if (
        this.currentSoftwares.find((e: string) => e === software) === undefined
      ) {
        return software;
      }
    });
  }

  // checks if software is already in profile's current softwares
  public checkDuplicate(software) {
    let isDuplicate = false;
    if (this.currentSoftwares.includes(software.toString())) {
      isDuplicate = true;
    }
    return isDuplicate;
  }

  // add a new software, not in the list
  add(event: MatChipInputEvent): void {
    // prevents adding the same software twice (case sensitive)
    if (this.checkDuplicate(event.value)) {
      const message = 'Software has not been added';
      const action = 'Already exists';
      this.openSnackBar(message, action);
      return;
    }
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.currentSoftwares.push(value.trim());
    }
    // Reset the "double" input value
    if (input) {
      input.value = '';
    }
    this.softwareCtrl.patchValue(null);
    this.noSoftDuplicate();
  }

  // add a software wich is in the list
  selected(event: MatAutocompleteSelectedEvent): void {
    this.currentSoftwares.push(event.option.viewValue);
    this.softInput.nativeElement.value = '';
    this.softwareCtrl.patchValue(null);
    this.noSoftDuplicate();
  }

  remove(software: string): void {
    const index = this.currentSoftwares.indexOf(software);
    if (index >= 0) {
      this.currentSoftwares.splice(index, 1);
    }
    this.noSoftDuplicate();
  }

  /********************       P O P U P     M A N A G E M E N T     ***************************/
  CloseDialog(): void {
    this.dialogRef.close();
  }

  opClEditMode() {
    this.editMode = !this.editMode;
    if (this.editMode) {
      this.getProfileInfo();
    }
  }
  /********************       R E F R E S H     D A T A     A N D     P A R E N T      ***************************/
  refreshDataAfterUpdate() {
    this.getProfileInfo();
    this.opClEditMode();
    this.updatingProfile = false;
    this.applyInput.disabled = false;
    this.cancelInput.disabled = false;
    this.router.navigate(['/profile']);
    setTimeout(() => {
      this.getProfileInfo();
      this.getUserSpecificInfo();
      this.CloseDialog();
    }, 2);
  }

  /********************       U P D A T E     U S E R     ***************************/

  // update an expert or an user ?
  updateUser(formData) {
    const dataOk = this.checkAllPatternsBeforeSend(formData);
    if (dataOk !== false && this.connectedIsExpert === true) {
      this.updateExpert(formData);
    }
    if (dataOk !== false && this.connectedIsClient === true) {
      this.updateClient(formData);
    }
  }

  // check data before send
  get f(): { [p: string]: AbstractControl } {
    return this.editProfileForm && this.editProfileForm.controls;
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.editProfileForm.controls.password.value) {
      return { confirm: true, error: true };
    }
  };

  getMailError() {
    return this.f.email.hasError('email') ? 'Not a valid email' : '';
  }

  getPhoneError() {
    return this.f.email.hasError('phone')
      ? 'Not a valid phone number, ex: 0601020304'
      : '';
  }

  getPasswordStrengthError() {
    return this.f.password.hasError('pattern')
      ? 'At least 8 characters including lowercase, uppercase, special character and number.'
      : '';
  }

  getCheckPasswordError() {
    return this.f.checkPassword.hasError('confirm')
      ? 'Please, enter the same password'
      : '';
  }

  checkAllPatternsBeforeSend(formData) {
    // email
    if (
      this.getMailError() === '' &&
      this.f.email.value.match(/^\S+@\S+\.\S+$/)
    ) {
      this.emailCheckedOk = true;
      this.newEmail = formData.email;
    } else {
      this.emailCheckedOk = false;
      const message = 'Email has not been changed';
      const action = 'Format was not valid';
      this.openSnackBar(message, action);
      return false;
    }

    // phone
    if (this.getPhoneError() === '' && this.f.phone.value.match(/^\d{10}$/)) {
      this.phoneCheckedOk = true;
      this.newPhone = formData.phone;
    } else {
      this.phoneCheckedOk = false;
      const message = 'Phone number has not been changed';
      const action = 'Format was not valid';
      this.openSnackBar(message, action);
      return false;
    }

    // zipcode
    if (this.f.zipCode.value.match(/^\d{5}$/)) {
      this.zipCodeCheckedOk = true;
      this.newZipCode = formData.zipCode;
    } else {
      this.zipCodeCheckedOk = false;
      const message = 'Zip code has not been changed';
      const action = 'Format was not valid';
      this.openSnackBar(message, action);
      return false;
    }

    // password
    if (formData.password !== null && formData.checkPassword === null) {
      return false;
    }
    if (
      this.getPasswordStrengthError() === '' &&
      this.getCheckPasswordError() === ''
    ) {
      this.newPassword = formData.password;
    } else {
      const message = 'Password has not been changed';
      const action = 'Format or confirmation was not valid';
      this.openSnackBar(message, action);
      return false;
    }
  }

  // set pop up messages for user
  openSnackBar(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000,
    });
  }

  setNewUserInfo(areasArray, formData) {
    if (formData.aerospace !== false) {
      areasArray.push('AEROSPACE');
    }
    if (formData.energy !== false) {
      areasArray.push('ENERGY');
    }
    if (formData.transport !== false) {
      areasArray.push('TRANSPORT');
    }
    if (formData.industries !== false) {
      areasArray.push('INDUSTRIES');
    }
    const str = formData.phone;
    const matches = str.replace(/\D/g, '');

    const newUserInfo: UserInformation = {
      lastName: formData.lastName,
      firstName: formData.firstName,
      email: formData.email,
      password: this.newPassword
        ? this.hashService.hashPwd(this.newPassword)
        : '',
      phone: matches,
      company: formData.company,
      citizenship: formData.citizenship,
      country: formData.country,
      city: formData.city,
      address: formData.address,
      zipCode: formData.zipCode,
      image: this.userInfos.image,
      softwares: this.currentSoftwares,
      languages: formData.languages,
      areas: areasArray,
      service: formData.service,
    };
    return newUserInfo;
  }
  // -------------------------------------------- //
  //     update data about EXPERT connected      //
  // -------------------------------------------//

  public updateExpert(formData) {
    const areasArray = [];
    const newUserInfo = this.setNewUserInfo(areasArray, formData);

    this.updatingProfile = true;
    this.applyInput.disabled = true;
    this.cancelInput.disabled = true;

    this.userService.updateUser(this.profileId, newUserInfo).subscribe((_) => {
      const newExpertInfo: Expert = {
        title: formData.title,
        wage: formData.wage,
        rating: null,
      };

      this.expertService
        .updateUser(this.expert.id, newExpertInfo)
        .subscribe((expert) => {
          this.refreshDataAfterUpdate();
        });
    });
  }
  // -------------------------------------------- //
  //     update data about CLIENT connected      //
  // -------------------------------------------//

  public updateClient(formData) {
    // check activity area
    const areasArray = [];
    const newClientInfo = this.setNewUserInfo(areasArray, formData);

    this.updatingProfile = true;
    this.applyInput.disabled = true;
    this.cancelInput.disabled = true;

    this.userService
      .updateUser(this.profileId, newClientInfo)
      .subscribe((user) => {
        this.clientService
          .updateUser(this.profileId, newClientInfo)
          .subscribe((client) => {
            this.refreshDataAfterUpdate();
          });
      });
  }

  /********************       D A T A    A B O U T    U S E R     C O N N E C T E D     ***************************/

  // all methods needed to instantiate data about expert or client
  getExpertExpertises() {
    this.expertService
      .getByUserId(this.profileId)
      .subscribe((expert: Expert) => {
        this.expert = expert;
        this.expertises = this.expertiseService.getProfileExpertises(
          this.expert.id
        );
      });
  }

  getUserSpecificInfo() {
    // Get languages info
    this.languageService.getLanguagesByUserId(this.profileId).subscribe((e) => {
      this.userInfos.languages = e.map((elem) => elem.name);
      this.userDatas[6].data = this.userInfos.languages;
      this.editProfileForm.controls.languages.setValue(
        this.userInfos.languages
      );

      // Get user Softwares Info
      this.softwareService
        .getSoftwaresByUserId(this.profileId)
        .subscribe((k) => {
          this.userInfos.softwares = k.map((elem) => elem.name);
          this.userDatas[5].data = this.userInfos.softwares;
          this.currentSoftwares = this.userInfos.softwares;
          this.noSoftDuplicate();

          // Get User "Areas" info.
          this.expertService
            .getAreasNames(this.profileId)
            .subscribe((userAreas) => {
              this.userInfos.areas = userAreas;

              // prefill title,wage and softwares
              if (this.connectedIsExpert) {
                this.prefillSpecificExpertForm(this.expert);
              }

              // prefill activities area
              this.prefillAreasIntoTheForm(userAreas);
            });
        });
    });
  }

  prefillSpecificExpertForm(expert) {
    if (this.connectedIsExpert) {
      this.editProfileForm.controls.title.setValue(this.expert.title);
      this.editProfileForm.controls.wage.setValue(this.expert.wage);
      this.editProfileForm.controls.softwares.setValue(
        this.userInfos.softwares
      );
    } else {
      this.editProfileForm.controls.softwares.setValue(
        this.userInfos.softwares
      );
    }
  }

  prefillAreasIntoTheForm(userAreas) {
    if (userAreas.includes('AEROSPACE')) {
      this.editProfileForm.patchValue({
        aerospace: true,
      });
    }
    if (userAreas.includes('ENERGY')) {
      this.editProfileForm.patchValue({
        energy: true,
      });
    }
    if (userAreas.includes('TRANSPORT')) {
      this.editProfileForm.patchValue({
        transport: true,
      });
    }
    if (userAreas.includes('INDUSTRIES')) {
      this.editProfileForm.patchValue({
        industries: true,
      });
    }
  }

  // get data about client
  getClientInfo() {
    this.clientService
      .getByUserId(this.profileId)
      .subscribe((client: Client) => {
        this.userDatas[7].data = client.service;
        this.changeFormCompany(this.userInfos.company);
        this.client = client;
      });
  }

  // edit CLIENT profile form
  editAllClientProfileForm() {
    this.editProfileForm.controls.lastName.setValue(this.userInfos.lastName);
    this.editProfileForm.controls.firstName.setValue(this.userInfos.firstName);
    this.editProfileForm.controls.email.setValue(this.userInfos.email);
    this.editProfileForm.controls.phone.setValue(this.userInfos.phone);
    this.editProfileForm.controls.company.setValue(this.userInfos.company);
    this.editProfileForm.controls.service.setValue(this.userDatas[7].data);
    this.editProfileForm.controls.citizenship.setValue(
      this.userInfos.citizenship
    );
    this.editProfileForm.controls.country.setValue(this.userInfos.country);
    this.editProfileForm.controls.city.setValue(this.userInfos.city);
    this.editProfileForm.controls.address.setValue(this.userInfos.address);
    this.editProfileForm.controls.zipCode.setValue(this.userInfos.zipCode);
  }

  // edit EXPERT profile form
  editAllExpertProfileForm() {
    this.editProfileForm.patchValue({
      lastName: this.userInfos.lastName,
      firstName: this.userInfos.firstName,
      email: this.userInfos.email,
      phone: this.userInfos.phone,
      company: this.userInfos.company,
      citizenship: this.userInfos.citizenship,
      country: this.userInfos.country,
      city: this.userInfos.city,
      address: this.userInfos.address,
      zipCode: this.userInfos.zipCode,
      title: '',
      wage: '',
      softwares: '',
      aerospace: false,
      energy: false,
      transport: false,
      industries: false,
    });
  }

  // get all companies already in db
  public changeFormCompany(company: string) {
    this.compServService
      .getAllByCompany(company.toLowerCase())
      .subscribe((res) => {
        this.suggestions = res.map((e) => e.name);
        for (let cpt = 0; cpt < this.suggestions.length; cpt++) {
          this.suggestions[cpt] =
            this.suggestions[cpt].charAt(0).toUpperCase() +
            this.suggestions[cpt].slice(1);
        }
      });
  }

  // To fill input with data allready registered in db
  getProfileInfo() {
    this.userService
      .getPartial(this.profileId)
      .subscribe((user: UserInformation) => {
        // user connected is an expert or a client ?
        this.connectedIsExpert = user.expertId != null;
        this.connectedIsClient = user.clientId != null;
        // get all datas and set array
        this.userInfos = user;
        this.userDatas[0].data = user.company;
        this.userDatas[1].data = user.phone;
        this.userDatas[2].data = user.email;
        this.userDatas[3].data = user.address;
        this.userDatas[4].data = user.citizenship;

        // -----------------------------------    GET DATA IF EXPERT    -----------------------------------------//

        if (this.connectedIsExpert === true) {
          this.editAllExpertProfileForm();
          // Get Expert Info
          this.getExpertExpertises();

          // Get User Languages, softwares and (if needed) expertservice info
          this.getUserSpecificInfo();
        }

        // if (this.connectedIsExpert === true) {
        //   // Get Expert Info
        //   this.getExpertExpertises();

        //   // Get User Languages, softwares and (if needed) expertservice info
        //   this.getUserSpecificInfo();
        // }

        // -----------------------------------    GET DATA IF CLIENT   -----------------------------------------//
        if (this.connectedIsClient === true) {
          this.editAllClientProfileForm();
          this.clientService
            .getByUserId(this.profileId)
            .subscribe((client: Client) => {
              this.userDatas[7].data = client.service;
              this.changeFormCompany(this.userInfos.company);
              this.client = client;
            });

          this.getUserSpecificInfo();
        }
      });

    //   if(this.connectedIsClient === true) {
    //   this.clientService
    //     .getByUserId(this.profileId)
    //     .subscribe((client: Client) => {
    //       this.userDatas[7].data = client.service;
    //       this.changeFormCompany(this.userInfos.company);
    //       this.client = client;
    //     });

    //   this.getUserSpecificInfo();
    // }
    //       });
  } // end of getProfileInfo()
}
