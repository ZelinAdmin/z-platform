import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-main-profile',
  templateUrl: './main-profile.component.html',
  styleUrls: ['./main-profile.component.less'],
})
export class MainProfileComponent implements OnInit {
  public isExpert: boolean = null;
  public profileId: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) {}

  async ngOnInit() {
    this.route.paramMap.subscribe(async (params) => {
      if (params.get('id')) {
        this.profileId = params.get('id');
        await this.userIsExpert();
      } else {
        this.router.navigate(['profile/' + localStorage.getItem('userId')]);
      }
    });
  }

  private userIsExpert() {
    this.userService.getPartial(this.profileId).subscribe((user) => {
      this.isExpert = user.expertId != null;
    });
  }
}
