import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from 'src/app/shared-pages/not-found/not-found.component';
import { ProfilePageComponent } from 'src/app/sub-modules/profile/pages/profile/profile-page.component';

const routes: Routes = [
  { path: '', component: ProfilePageComponent },
  { path: ':id', component: ProfilePageComponent },
  {
    path: '**',
    component: NotFoundComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
