export const AreaEnum = [
  {
    name: 'AEROSPACE',
    img: 'assets/areas/aerospace.png',
  },
  {
    name: 'ENERGY',
    img: 'assets/areas/energy.png',
  },
  {
    name: 'TRANSPORT',
    img: 'assets/areas/transport.png',
  },
  {
    name: 'INDUSTRIES',
    img: 'assets/areas/industries.png',
  },
];
