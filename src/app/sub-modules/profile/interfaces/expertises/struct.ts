export interface Struct {
  expertId: string;

  kinematics: boolean;
  deformation: boolean;
  design: boolean;
  dynamics: boolean;
  hydraulic: boolean;
  resistance: boolean;
  statics: boolean;
  vibration: boolean;
}
