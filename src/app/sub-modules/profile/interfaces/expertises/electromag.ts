export interface Electromag {
  expertId: string;

  arc: boolean;
  elec: boolean;
  electrodynamics: boolean;
  electronics: boolean;
  electrostatics: boolean;
  magnetism: boolean;
  magnetostatics: boolean;
}
