export interface Fluids {
  expertId: string;

  aeroacoustics: boolean;
  aerolics: boolean;
  aerodynamics: boolean;
  fluidDynamics: boolean;
  hydraulics: boolean;
  hydrocarbon: boolean;
  hydrodynamics: boolean;
  neutronPhysics: boolean;
  plasma: boolean;
  rheology: boolean;
}
