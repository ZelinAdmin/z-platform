export interface Thermic {
  expertId: string;

  aerothermal: boolean;
  combustion: boolean;
  conduction: boolean;
  convection: boolean;
  hydrothermal: boolean;
  isolation: boolean;
  energyMngmt: boolean;
  radiation: boolean;
  thermodynamics: boolean;
  transfer: boolean;
}
