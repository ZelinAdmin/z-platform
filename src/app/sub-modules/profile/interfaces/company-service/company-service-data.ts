export interface CompanyServiceData {
  id: string;
  name: string;
  company: string;
}
