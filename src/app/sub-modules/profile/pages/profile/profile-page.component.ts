import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from 'src/app/services/user/user.service';
import {Globals} from 'src/app/globals/globals';

@Component({
  selector: 'app-profile',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.less'],
})
export class ProfilePageComponent implements OnInit {
  public isExpert: boolean = null;
  public profileId: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(async params => {
      if (params.get('id')) {
        this.profileId = params.get('id');
        await this.userIsExpert();
      } else {
        this.router.navigate(['profile/' + localStorage.getItem(Globals.USER_ID)]);
      }
    });
  }

  private userIsExpert() {
    this.userService.getPartial(this.profileId)
      .subscribe(user => {
        this.isExpert = user.expertId != null;
      });
  }

}
