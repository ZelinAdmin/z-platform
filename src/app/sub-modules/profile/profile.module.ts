import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ProfilePageComponent } from 'src/app/sub-modules/profile/pages/profile/profile-page.component';
import { ClientProfileComponent } from './shared/client-profile/client-profile.component';
import { EditProfilePhotoComponent } from './shared/edit-profile-photo/edit-profile-photo.component';
import { ExpertProfileComponent } from './shared/expert-profile/expert-profile.component';
import { MainProfileComponent } from './shared/main-profile/main-profile.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { ImgFallbackModule } from 'ngx-img-fallback';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EditProfileComponent } from './shared/edit-profile/edit-profile.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    ProfileComponent,
    ProfilePageComponent,
    ClientProfileComponent,
    EditProfilePhotoComponent,
    ExpertProfileComponent,
    MainProfileComponent,
    EditProfileComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatSelectModule,
    FormsModule,
    MatAutocompleteModule,
    MatChipsModule,
    ImgFallbackModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatDialogModule,
  ],
})
export class ProfileModule {}
