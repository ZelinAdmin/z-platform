import { Injectable } from '@angular/core';
import { ComponentsTrslt } from 'src/app/services/translation/translation.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Fluids } from 'src/app/sub-modules/profile/interfaces/expertises/fluids';
import { Thermic } from 'src/app/sub-modules/profile/interfaces/expertises/thermic';
import { Struct } from 'src/app/sub-modules/profile/interfaces/expertises/struct';
import { Electromag } from 'src/app/sub-modules/profile/interfaces/expertises/electromag';
import { ExpertisesEnums } from 'src/app/sub-modules/profile/interfaces/expertises/expertises.enum';

@Injectable({
  providedIn: 'root',
})
export class ExpertiseService {
  constructor(private httpClient: HttpClient) {}

  getAll<T>(
    params: HttpParams | null = null,
    urlSuffix: UrlSuffix
  ): Observable<T> {
    return this.httpClient.get<T>(`${environment.apiUrl}${urlSuffix.getAll}`, {
      params,
    });
  }

  post<T>(model: T, urlSuffix: UrlSuffix): Observable<T> {
    return this.httpClient.post<T>(`${environment.apiUrl}${urlSuffix.post}`, {
      ElectromagExpertise: model,
    });
  }

  getByExpertId<T>(expertId: string, urlSuffix: UrlSuffix): Observable<T> {
    return this.httpClient.get<T>(
      `${environment.apiUrl}${urlSuffix.getByExpertId}${expertId}`
    );
  }

  deleteElectromagExpertise<T>(
    expertiseId: string,
    urlSuffix: UrlSuffix
  ): Observable<T> {
    return this.httpClient.delete<T>(
      `${environment.apiUrl}${urlSuffix.delete}${expertiseId}`
    );
  }

  update<T>(expertise: T, urlSuffix: UrlSuffix) {
    return this.httpClient.put<T>(
      `${environment.apiUrl}${urlSuffix.update}`,
      expertise
    );
  }

  // Check if an expertise is validated
  public expertiseIsValidated(expertise): boolean {
    for (const subExpertise of expertise.subExpertises) {
      if (subExpertise.done === true) {
        return true;
      }
    }
    return false;
  }

  public getProfileExpertises(expertId: string) {
    if (expertId) {
      const expertises = JSON.parse(JSON.stringify(ExpertisesEnums));
      this.getFluidExp(expertises, expertId);
      this.getThermExp(expertises, expertId);
      this.getStructExp(expertises, expertId);
      this.getElectromagExp(expertises, expertId);
      return expertises;
    }
    return null;
  }

  private getFluidExp(expertises, expertId: string) {
    this.getByExpertId<Fluids>(
      expertId,
      expertUrl[ExpertiseSelector.Fluid]
    ).subscribe((newExp) => {
      const ind = expertises.findIndex(
        (elem) => elem.translateEnum === ComponentsTrslt.FLUIDS
      );
      expertises[ind].subExpertises[0].done = newExp.aeroacoustics;
      expertises[ind].subExpertises[1].done = newExp.aerolics;
      expertises[ind].subExpertises[2].done = newExp.aerodynamics;
      expertises[ind].subExpertises[3].done = newExp.fluidDynamics;
      expertises[ind].subExpertises[4].done = newExp.hydraulics;
      expertises[ind].subExpertises[5].done = newExp.hydrocarbon;
      expertises[ind].subExpertises[6].done = newExp.hydrodynamics;
      expertises[ind].subExpertises[7].done = newExp.neutronPhysics;
      expertises[ind].subExpertises[8].done = newExp.plasma;
      expertises[ind].subExpertises[9].done = newExp.rheology;
    });
  }

  private getThermExp(expertises, expertId: string) {
    this.getByExpertId<Thermic>(
      expertId,
      expertUrl[ExpertiseSelector.Thermic]
    ).subscribe((newExp) => {
      const ind = expertises.findIndex(
        (elem) => elem.translateEnum === ComponentsTrslt.THERMIC
      );
      expertises[ind].subExpertises[0].done = newExp.aerothermal;
      expertises[ind].subExpertises[1].done = newExp.combustion;
      expertises[ind].subExpertises[2].done = newExp.conduction;
      expertises[ind].subExpertises[3].done = newExp.convection;
      expertises[ind].subExpertises[4].done = newExp.energyMngmt;
      expertises[ind].subExpertises[5].done = newExp.hydrothermal;
      expertises[ind].subExpertises[6].done = newExp.isolation;
      expertises[ind].subExpertises[7].done = newExp.radiation;
      expertises[ind].subExpertises[8].done = newExp.thermodynamics;
      expertises[ind].subExpertises[9].done = newExp.transfer;
    });
  }

  private getStructExp(expertises, expertId: string) {
    this.getByExpertId<Struct>(
      expertId,
      expertUrl[ExpertiseSelector.Struct]
    ).subscribe((newExp) => {
      const ind = expertises.findIndex(
        (elem) => elem.translateEnum === ComponentsTrslt.STRUCTURE
      );
      expertises[ind].subExpertises[0].done = newExp.kinematics;
      expertises[ind].subExpertises[1].done = newExp.deformation;
      expertises[ind].subExpertises[2].done = newExp.design;
      expertises[ind].subExpertises[3].done = newExp.dynamics;
      expertises[ind].subExpertises[4].done = newExp.hydraulic;
      expertises[ind].subExpertises[5].done = newExp.resistance;
      expertises[ind].subExpertises[6].done = newExp.statics;
      expertises[ind].subExpertises[7].done = newExp.vibration;
    });
  }

  private getElectromagExp(expertises, expertId: string) {
    this.getByExpertId<Electromag>(
      expertId,
      expertUrl[ExpertiseSelector.Electromag]
    ).subscribe((newExp) => {
      const ind = expertises.findIndex(
        (elem) => elem.translateEnum === ComponentsTrslt.ELECTROMAG
      );
      expertises[ind].subExpertises[0].done = newExp.arc;
      expertises[ind].subExpertises[1].done = newExp.elec;
      expertises[ind].subExpertises[2].done = newExp.electrodynamics;
      expertises[ind].subExpertises[3].done = newExp.electronics;
      expertises[ind].subExpertises[4].done = newExp.electrostatics;
      expertises[ind].subExpertises[5].done = newExp.magnetism;
      expertises[ind].subExpertises[6].done = newExp.magnetostatics;
    });
  }

  public updateExpertise(
    expertId: string,
    expertise: string,
    subExpertises: Array<any>
  ) {
    if (expertise === 'fluidMeca') {
      this.updateFluidMeca(expertId, subExpertises);
    } else if (expertise === 'thermic') {
      this.updateThermal(expertId, subExpertises);
    } else if (expertise === 'structure') {
      this.updateStructure(expertId, subExpertises);
    } else if (expertise === 'electroMag') {
      this.updateElectroMag(expertId, subExpertises);
    }
  }

  public updateFluidMeca(expertId: string, subExpertises: Array<any>) {
    const exp: Fluids = {
      aeroacoustics: subExpertises.find((e) => e.name === 'aeroacoustics').done,
      aerolics: subExpertises.find((e) => e.name === 'aerolics').done,
      aerodynamics: subExpertises.find((e) => e.name === 'aerodynamics').done,
      fluidDynamics: subExpertises.find((e) => e.name === 'fluidDynamics').done,
      hydraulics: subExpertises.find((e) => e.name === 'hydraulics').done,
      hydrocarbon: subExpertises.find((e) => e.name === 'hydrocarbon').done,
      hydrodynamics: subExpertises.find((e) => e.name === 'hydrodynamics').done,
      neutronPhysics: subExpertises.find((e) => e.name === 'neutronPhysics')
        .done,
      plasma: subExpertises.find((e) => e.name === 'plasma').done,
      rheology: subExpertises.find((e) => e.name === 'rheology').done,
      expertId,
    };
    this.update<Fluids>(
      exp,
      expertUrl[ExpertiseSelector.Fluid]
    ).subscribe((res) => {});
  }

  public updateThermal(expertId: string, subExpertises: Array<any>) {
    const exp: Thermic = {
      aerothermal: subExpertises.find((e) => e.name === 'aerothermal').done,
      combustion: subExpertises.find((e) => e.name === 'combustion').done,
      conduction: subExpertises.find((e) => e.name === 'conduction').done,
      convection: subExpertises.find((e) => e.name === 'convection').done,
      hydrothermal: subExpertises.find((e) => e.name === 'hydrothermal').done,
      isolation: subExpertises.find((e) => e.name === 'thermalIns').done,
      energyMngmt: subExpertises.find((e) => e.name === 'energyMngmt').done,
      radiation: subExpertises.find((e) => e.name === 'radiation').done,
      thermodynamics: subExpertises.find((e) => e.name === 'thermodynamics')
        .done,
      transfer: subExpertises.find((e) => e.name === 'heatTrsf').done,
      expertId,
    };
    this.update<Thermic>(
      exp,
      expertUrl[ExpertiseSelector.Thermic]
    ).subscribe((res) => {});
  }

  public updateStructure(expertId: string, subExpertises: Array<any>) {
    const exp: Struct = {
      kinematics: subExpertises.find((e) => e.name === 'kinematics').done,
      deformation: subExpertises.find((e) => e.name === 'deformation').done,
      design: subExpertises.find((e) => e.name === 'design').done,
      dynamics: subExpertises.find((e) => e.name === 'dynamics').done,
      hydraulic: subExpertises.find((e) => e.name === 'hydraulic').done,
      resistance: subExpertises.find((e) => e.name === 'resistance').done,
      statics: subExpertises.find((e) => e.name === 'statics').done,
      vibration: subExpertises.find((e) => e.name === 'vibration').done,
      expertId,
    };
    this.update<Struct>(
      exp,
      expertUrl[ExpertiseSelector.Struct]
    ).subscribe((res) => {});
  }

  public updateElectroMag(expertId: string, subExpertises: Array<any>) {
    const exp: Electromag = {
      arc: subExpertises.find((e) => e.name === 'arc').done,
      elec: subExpertises.find((e) => e.name === 'elec').done,
      electrodynamics: subExpertises.find((e) => e.name === 'electrodynamics')
        .done,
      electronics: subExpertises.find((e) => e.name === 'electronics').done,
      electrostatics: subExpertises.find((e) => e.name === 'electrostatics')
        .done,
      magnetism: subExpertises.find((e) => e.name === 'magnetism').done,
      magnetostatics: subExpertises.find((e) => e.name === 'magnetostatics')
        .done,
      expertId,
    };
    this.update<Electromag>(
      exp,
      expertUrl[ExpertiseSelector.Electromag]
    ).subscribe((res) => {});
  }
}

export enum ExpertiseSelector {
  Electromag,
  Fluid,
  Struct,
  Thermic,
}

export interface UrlSuffix {
  getAll: string;
  post: string;
  getByExpertId: string;
  delete: string;
  update: string;
}

export let expertUrl: UrlSuffix[] = [
  {
    getAll: 'electromagexpertise',
    post: '/admin/electromagexpertise',
    getByExpertId: 'CompoElectroMagExperts/expertId/',
    delete: 'admin/electromagexpertise/',
    update: 'CompoElectroMagExperts',
  },
  {
    getAll: 'CompoFluidsExperts',
    post: '/admin/CompoFluidsExperts',
    getByExpertId: 'CompoFluidsExperts/expertId/',
    delete: 'admin/CompoFluidsExperts/',
    update: 'CompoFluidsExperts',
  },
  {
    getAll: 'CompoStructExperts',
    post: '/admin/CompoStructExperts',
    getByExpertId: 'CompoStructExperts/expertId/',
    delete: 'admin/CompoStructExperts/',
    update: 'CompoStructExperts',
  },
  {
    getAll: 'thermicexpertise',
    post: '/admin/thermicexpertise',
    getByExpertId: 'CompoThermicExperts/expertId/',
    delete: 'admin/thermicexpertise/',
    update: 'CompoThermicExperts',
  },
];
