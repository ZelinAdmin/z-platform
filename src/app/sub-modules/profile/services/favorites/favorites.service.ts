import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FavoritesService {
  constructor(private httpClient: HttpClient) {}

  add(clientId: string, expertId: string): Observable<any> {
    return this.httpClient.post(`${environment.apiUrl}favorite/add`, {
      id_client: clientId,
      id_expert: expertId,
    });
  }
}
