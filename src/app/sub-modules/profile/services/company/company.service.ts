import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CompanyServiceData } from 'src/app/sub-modules/profile/interfaces/company-service/company-service-data';

@Injectable({
  providedIn: 'root',
})
export class CompanyService {
  constructor(private httpClient: HttpClient) {}

  getAll(): Observable<Array<CompanyServiceData>> {
    return this.httpClient.get<Array<CompanyServiceData>>(
      `${environment.apiUrl}compaServices`
    );
  }

  getById(serviceId: string): Observable<CompanyServiceData> {
    return this.httpClient.get<CompanyServiceData>(
      `${environment.apiUrl}compaServices/${serviceId}`
    );
  }

  getAllByName(name: string): Observable<Array<CompanyServiceData>> {
    return this.httpClient.get<Array<CompanyServiceData>>(
      `${environment.apiUrl}compaServices/name/${name}`
    );
  }

  getAllByCompany(company: string): Observable<Array<CompanyServiceData>> {
    return this.httpClient.get<Array<CompanyServiceData>>(
      `${environment.apiUrl}compaServices/company/${company}`
    );
  }

  add(model: CompanyServiceData): Observable<any> {
    return this.httpClient.post(`${environment.apiUrl}compaServices`, model);
  }

  delete(id: string): Observable<any> {
    return this.httpClient.delete(
      `${environment.apiUrl}admin/compaServices/${id}`
    );
  }

  getAllByNameAndCompany(
    service: string,
    company: string
  ): Observable<Array<CompanyServiceData>> {
    return this.httpClient.get<Array<CompanyServiceData>>(
      `${
        environment.apiUrl
      }compaServices/service/${service.toLowerCase()}/company/${company.toLowerCase()}`
    );
  }
}
