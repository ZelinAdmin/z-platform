import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SoftwareService {

  constructor(private httpClient: HttpClient) {}

  // WARN: this route is in RefSoftwaresController in the back, the other one is in CompoExpertSoftwaresController
  public getAllSoftwares(): Observable<Array<any>> {
    return this.httpClient.get<Array<any>>(
      `${environment.apiUrl}software`
    );
  }

  public getSoftwaresByUserId(id: string): Observable<Array<any>> {
    return this.httpClient.get<Array<any>>(
      `${environment.apiUrl}softwares/user/${id}`
    );
  }

}
