import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { FormControl } from '@angular/forms';
import { DisabledTimeFn } from 'ng-zorro-antd';
import { _Array } from 'src/app/globals/globals';
import { ProjectUpdateInfo } from 'src/app/interfaces/project-update-info/project-update-info';

@Component({
  selector: 'app-project-settings',
  templateUrl: './project-settings.component.html',
  styleUrls: ['./project-settings.component.less'],
})
export class ProjectSettingsComponent {
  progress = 0;
  startDateForm: FormControl;
  endDateForm: FormControl;
  teamsUrlForm = new FormControl('');
  nextMeetingDate = new Date();
  teamsUrl = '';

  constructor(
    public dialogRef: MatDialogRef<ProjectSettingsComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData: UserProject
  ) {
    this.startDateForm = new FormControl(
      new Date(injectedData.project.startDate)
    );
    this.endDateForm = new FormControl(new Date(injectedData.project.endDate));
    this.progress = injectedData.project.globalprogress;
    this.nextMeetingDate = injectedData.project.nextMeeting;
    this.teamsUrl = injectedData.project.teamsUrl;
  }

  onDismiss() {
    this.dialogRef.close(null);
  }

  onConfirm() {
    const information: ProjectUpdateInfo = {
      globalprogress: this.progress,
      startDate: this.startDateForm.value,
      endDate: this.endDateForm.value,
      nextMeeting: this.nextMeetingDate,
      teamsUrl: this.teamsUrlForm.value,
    };
    this.dialogRef.close(information);
  }

  onOk(result: Date): void {
    this.nextMeetingDate = result;
  }

  disabledTime: DisabledTimeFn = () => {
    return {
      nzDisabledHours: () =>
        _Array.range(0, 8, 1).concat([13]).concat(_Array.range(19, 24, 1)),
      nzDisabledMinutes: () => [],
      nzDisabledSeconds: () => [],
    };
  }
}
