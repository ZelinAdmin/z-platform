import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PlotConfigurationData } from '../../interfaces/plot-configuration-data/plot-configuration-data';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-plot-configuration',
  templateUrl: './plot-configuration.component.html',
  styleUrls: ['./plot-configuration.component.less'],
})
export class PlotConfigurationComponent implements OnInit {
  returnData: PlotConfigurationData;
  titleForm: FormControl;
  logScale = false;
  showLegends = false;
  xAxisTitleForm: FormControl;
  yAxisTitleForm: FormControl;
  matcher = new MyErrorStateMatcher();
  validatorOps: ValidatorFn[] = [
    Validators.required,
    Validators.pattern('^(?!.*?(,|;)).*'),
  ];

  curvesForms: FormControl[];

  constructor(
    public dialogRef: MatDialogRef<PlotConfigurationComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData: PlotConfigurationData
  ) {
    this.returnData = injectedData;
    this.titleForm = new FormControl(
      injectedData.layout.title.text,
      this.validatorOps
    );
    this.xAxisTitleForm = new FormControl(
      injectedData.layout.xaxis.title.text,
      this.validatorOps
    );
    this.yAxisTitleForm = new FormControl(
      injectedData.layout.yaxis.title.text,
      this.validatorOps
    );

    this.curvesForms = injectedData.plot.map(
      (e) => new FormControl(e.name, this.validatorOps)
    );

    /*
    Not mine (og repository)
    */
    this.logScale = !!localStorage.getItem(
      injectedData.file.internalId + 'LayoutType'
    );
    this.showLegends =
      localStorage.getItem(injectedData.file.id + 'ShowLegend') === 'true';
  }

  ngOnInit(): void {}

  onDismiss() {
    this.dialogRef.close(null);
  }

  onConfirm() {
    this.formatStuff();
    this.dialogRef.close(this.returnData);
  }

  private formatStuff() {
    localStorage.setItem(
      this.returnData.file.internalId + 'LayoutType',
      this.logScale ? 'log' : 'scatter'
    );
    localStorage.setItem(
      this.returnData.file.internalId + 'ShowLegend',
      String(this.showLegends)
    );

    let line =
      '"' +
      this.xAxisTitleForm.value +
      ';' +
      this.yAxisTitleForm.value +
      ';' +
      this.titleForm.value +
      '"';

    this.returnData.layout.title.text = this.titleForm.value;
    this.returnData.layout.xaxis.title.text = this.xAxisTitleForm.value;
    this.returnData.layout.yaxis.title.text = this.yAxisTitleForm.value;
    this.returnData.layout.yaxis.type = localStorage.getItem(
      this.returnData.file.internalId + 'LayoutType'
    );
    this.returnData.layout.showlegend =
      localStorage.getItem(this.returnData.file.internalId + 'ShowLegend') !==
      'false';
    this.returnData.layout.width = this.injectedData.card.nativeElement.offsetWidth;
    this.returnData.layout.height =
      this.injectedData.card.nativeElement.offsetParent.clientHeight - 30;
    this.returnData.plot = this.returnData.plot.map((e, index) => {
      e.name = this.curvesForms[index].value;
      line += ',"' + e.name + '"';
      return e;
    });
    this.returnData.line = line;
  }

  get generateErrorMessage(): string {
    return 'No \',\' or \',\' in the title';
  }
  get generateRequiredErrorMessage(): string {
    return 'This field can not be empty';
  }

  get submitDisabled(): boolean {
    let state = false;
    this.curvesForms.forEach((e) => {
      if (e.hasError('pattern') || e.hasError('required')) {
        state = true;
      }
    });
    return (
      state ||
      this.titleForm.hasError('pattern') ||
      this.titleForm.hasError('required') ||
      this.xAxisTitleForm.hasError('pattern') ||
      this.xAxisTitleForm.hasError('required') ||
      this.yAxisTitleForm.hasError('pattern') ||
      this.yAxisTitleForm.hasError('required')
    );
  }
}
