import { Component } from '@angular/core';
import { InfoPopoverRef } from '../../classes/info-popover-ref';
import { DisabledTimeFn } from 'ng-zorro-antd';
import { _Array } from 'src/app/globals/globals';

@Component({
  selector: 'app-next-meeting-creation',
  templateUrl: './next-meeting-creation.component.html',
  styleUrls: ['./next-meeting-creation.component.scss'],
})
export class NextMeetingCreationComponent {
  date: Date;
  constructor(private infoPopoverRef: InfoPopoverRef) {}

  close(data?: any) {
    if (data === null) {
      this.infoPopoverRef.close();
    } else {
      this.infoPopoverRef.close(this.date);
    }
  }

  onOk(result: Date): void {
    this.date = result;
  }

  disabledTime: DisabledTimeFn = () => {
    return {
      nzDisabledHours: () =>
        _Array.range(0, 8, 1).concat([13]).concat(_Array.range(19, 24, 1)),
      nzDisabledMinutes: () => [],
      nzDisabledSeconds: () => [],
    };
  }
}
