import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { InfoPopoverRef } from '../../classes/info-popover-ref';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';
import { environment } from 'src/environments/environment';
import { FileComment } from '../../interfaces/file-comment/file-comment';
import { CommentService } from '../../services/comment/comment.service';
import { DataHolderService } from 'src/app/services/data-holder/data-holder.service';
import { FormControl } from '@angular/forms';
import { Globals } from 'src/app/globals/globals';
import { PlotlyLine } from '../../interfaces/plotly-line/plotly-line';
import Plotly from 'plotly.js-dist';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { PlotsFormattingService } from '../../services/plots-formatting/plots-formatting.service';
import { PlotConfigurationData } from 'src/app/sub-modules/vision/interfaces/plot-configuration-data/plot-configuration-data';
import { PlotConfigurationComponent } from 'src/app/sub-modules/vision/dialogs/plot-configuration/plot-configuration.component';
import { MatDialog } from '@angular/material/dialog';

export interface DataType {
  files: ProjectFileDisplayed[];
  index: number;
  category: string;
}

@Component({
  selector: 'app-element-insight',
  templateUrl: './element-insight.component.html',
  styleUrls: ['./element-insight.component.less'],
})
export class ElementInsightComponent implements OnInit {
  data: DataType;
  commentControl: FormControl;
  sendingComment = false;

  plots$: Observable<PlotlyLine[]> = new Observable<PlotlyLine[]>();
  layout$: Observable<Plotly.Layout> = new Observable<Plotly.Layout>();
  plotBehaviourSubject = new BehaviorSubject<PlotlyLine[]>([]);
  layoutBehaviourSubject = new BehaviorSubject<Plotly.Layout>(null);

  @ViewChild('card') card: ElementRef;

  constructor(
    private infoPopoverRef: InfoPopoverRef,
    public fileCommentsService: CommentService,
    private dataHolder: DataHolderService,
    private fileService: FileManipulationsService,
    private plotsFormatting: PlotsFormattingService,
    public dialog: MatDialog
  ) {
    this.commentControl = new FormControl('');
  }

  ngOnInit(): void {
    this.data = this.infoPopoverRef.data;
    this.getData();
  }

  /*
  a group of function to get comments
  and recalculate layout for plots
  */
  getData() {
    this.fetchComments(this.data.files[this.data.index]);
    if (this.data.category === 'plot') {
      this.plots$ = this.fetchPlot(this.data.files[this.data.index]);
      this.layout$ = this.fetchLayout(this.data.files[this.data.index]);

      /* attach behaviour subject to observables */
      this.plots$.subscribe(this.plotBehaviourSubject);
      this.layout$.subscribe(this.layoutBehaviourSubject);
    }
  }

  /*
  check if there's no out of bounds on array
  otherwise + 1 (- 1) the index to cycle
  */
  changeIndex(num: number) {
    if (
      this.data.index + num < 0 ||
      this.data.index + num > this.data.files.length - 1
    ) {
      return;
    }
    this.data.index += num;
    this.getData();
  }

  fetchPlot(file: ProjectFileDisplayed): Observable<PlotlyLine[]> {
    return this.fileService
      .download(file.internalId)
      .pipe(map((txt) => this.plotsFormatting.plotFormatting(txt, file)));
  }

  fetchLayout(file: ProjectFileDisplayed): Observable<Plotly.Layout> {
    return this.fileService
      .download(file.internalId)
      .pipe(map((txt) => this.plotsFormatting.fetchPlotLayout$(txt, file)));
  }

  public openEditDialog(
    layout: Plotly.Layout,
    file: ProjectFileDisplayed,
    ploty: PlotlyLine[]
  ) {
    const data: PlotConfigurationData = {
      layout,
      file,
      plot: ploty,
      card: this.card,
    };
    const dialogRef = this.dialog.open(PlotConfigurationComponent, {
      minWidth: '450px',
      data,
    });

    dialogRef.afterClosed().subscribe((dialogResult: PlotConfigurationData) => {
      if (dialogResult) {
        this.fileService.edit(
          file,
          this.dataHolder.projectId,
          dialogResult.line
        );
        Plotly.react(
          document.getElementById('plot'),
          dialogResult.plot,
          dialogResult.layout
        );
      }
    });
  }

  fetchComments(file: ProjectFileDisplayed) {
    this.fileCommentsService
      .getAllComments(this.dataHolder.projectId, file.internalId)
      .subscribe((data: FileComment[]) => {
        file.comment = data;
      });
  }

  sendComment() {
    this.sendingComment = true;
    const data: FileComment = {
      content: this.commentControl.value,
      fileId: this.data.files[this.data.index].internalId,
      projectId: this.dataHolder.projectId,
      // @ts-ignore
      user: { id: localStorage.getItem(Globals.USER_ID) },
    };
    this.commentControl = new FormControl('');
    this.fileCommentsService.post(data).subscribe(
      (e) => {
        this.data.files[this.data.index].comment.push(e);
        this.sendingComment = false;
      },
      (error) => {
        // tslint:disable-next-line:no-console
        console.info(error);
        this.sendingComment = false;
      }
    );
  }

  close() {
    this.infoPopoverRef.close();
  }

  getUrl(f: ProjectFileDisplayed): string {
    return `${environment.apiUrl}files/${f.internalId}/download`;
  }

  /*  Generate a url for manager img  */
  generateImgUrl(image?: any) {
    let url = '';
    if (image) {
      const hostname = environment.apiUrl;
      const path = 'files/';
      const imgName = image;
      const extension = '/download';
      url = hostname + path + imgName + extension;
    } else {
      url = 'assets/flaticon/userRed.png';
    }
    return url;
  }
}
