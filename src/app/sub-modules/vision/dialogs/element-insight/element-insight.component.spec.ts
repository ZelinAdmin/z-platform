import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementInsightComponent } from './element-insight.component';
import { InfoPopoverRef } from 'src/app/sub-modules/vision/classes/info-popover-ref';
import { CommentService } from 'src/app/sub-modules/vision/services/comment/comment.service';
import { DataHolderService } from 'src/app/services/data-holder/data-holder.service';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { PlotsFormattingService } from 'src/app/sub-modules/vision/services/plots-formatting/plots-formatting.service';
import { MatDialog } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import Plotly from 'plotly.js-dist';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Observable } from 'rxjs';
import { PlotlyLine } from 'src/app/sub-modules/vision/interfaces/plotly-line/plotly-line';
import { FileComment } from 'src/app/sub-modules/vision/interfaces/file-comment/file-comment';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const INTERNAL_ID = '310238_3213';
// tslint:disable-next-line:variable-name
const mockedFile_one = {
  package: 'pa',
  id: 2,
  filename: 'filename',
  internalId: INTERNAL_ID,
  commentNbr: 3,
};

const mockedComment: FileComment = {
  content: 'dazeda',
  fileId: 'randomId',
  projectId: 'projectId',
  user: {
    email: '',
    firstName: '',
    lastName: '',
  },
};

describe('ElementInsightComponent', () => {
  let component: ElementInsightComponent;
  let fixture: ComponentFixture<ElementInsightComponent>;
  const fileserviceMock = {
    getAllComments: (_, __) => ({
      subscribe: (func: (_) => {}) => {
        func([mockedComment]);
      },
      // subscribe: () => [
      //   mockedComment
      // ],
    }),
    post: (_) => ({
      subscribe: (func: (_) => {}) => {
        func(mockedComment);
      },
    }),
  };

  const fileManipulationMock = {
    download: () => ({
      pipe: () => true,
    }),
  };

  const plotsFormattingMock = {
    plotFormatting: (_, __) => {},
    fetchPlotLayout$: (_, __) => {},
  };

  const mockedData = {
    data: {
      files: [mockedFile_one],
      index: 0,
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ElementInsightComponent],
      providers: [
        { provide: InfoPopoverRef, useValue: mockedData },
        { provide: CommentService, useValue: fileserviceMock },
        { provide: DataHolderService, useValue: {} },
        { provide: FileManipulationsService, useValue: fileManipulationMock },
        { provide: PlotsFormattingService, useValue: plotsFormattingMock },
        { provide: MatDialog, useValue: {} },
      ],
      imports: [
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatChipsModule,
        MatListModule,
        MatDividerModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatSnackBarModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementInsightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(component, 'getData');

    spyOn(localStorage, 'getItem').and.returnValue('id');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should fetch plotly plot and layout if the file is a plot', () => {
  //   /* setup*/
  //   component.data.category = 'plot';
  //   component.plots$ = new Observable<PlotlyLine[]>();
  //   component.layout$ = new Observable<Plotly.Layout>();
  //
  //   spyOn(component, 'fetchPlot').and.returnValue(new Observable<PlotlyLine[]>());
  //   spyOn(component, 'fetchLayout').and.returnValue(new Observable<Plotly.Layout>());
  //
  //   component.getData();
  //
  //   expect(component.fetchPlot).toHaveBeenCalledTimes(1);
  //   expect(component.fetchPlot).toHaveBeenCalledWith(mockedFile_one);
  //
  //   expect(component.fetchLayout).toHaveBeenCalledTimes(1);
  //   expect(component.fetchLayout).toHaveBeenCalledWith(mockedFile_one);
  // });

  it('should change index and with edge cases', () => {
    component.changeIndex(1);
    expect(component.data.index).toEqual(0);
    expect(component.getData).toHaveBeenCalledTimes(0);

    component.changeIndex(-1);
    expect(component.data.index).toEqual(0);
    expect(component.getData).toHaveBeenCalledTimes(0);

    component.data.files = [mockedFile_one, mockedFile_one];

    component.changeIndex(1);
    expect(component.data.index).toEqual(1);
    expect(component.getData).toHaveBeenCalledTimes(1);
  });

  it('should fetch comments for a file', () => {
    expect(component.data.files[0].comment).toEqual([mockedComment]);
  });

  it('should send a comment and update accordingly', () => {
    const commentMessage = 'Lorem ipsilum';
    component.commentControl = new FormControl(commentMessage);
    component.sendComment();
    expect(component.data.files[0].comment).toEqual([
      mockedComment,
      mockedComment,
    ]);
  });
});
