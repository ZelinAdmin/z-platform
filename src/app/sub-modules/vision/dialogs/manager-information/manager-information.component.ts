import { UserService } from './../../../../services/user/user.service';
import { UserInformation } from './../../../../interfaces/user-information/user-information';
import { Component, OnInit } from '@angular/core';
import {
  InfoPopoverContent,
  InfoPopoverRef,
} from '../../classes/info-popover-ref';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Globals } from 'src/app/globals/globals';
import { MatSnackBar } from '@angular/material/snack-bar';
export interface Section {
  name: string;
  updated: Date;
}

@Component({
  selector: 'app-manager-information',
  templateUrl: './manager-information.component.html',
  styleUrls: ['./manager-information.component.scss'],
})
export class ManagerInformationComponent implements OnInit {
  // content: InfoPopoverContent;
  // context;
  arrowOffset: number;

  // for managers : get type of user (client or expert?) to give access to client profile or not
  public managers: UserInformation[];
  public managersId: [];
  public managerIsClient: boolean;

  // about user connected : has he access to clients profiles?
  public userConnected: UserInformation;
  public userConnectedisExpert: boolean;
  public data: UserInformation[] = [];
  public profileId: string;

  constructor(
    private infoPopoverRef: InfoPopoverRef,
    private router: Router, // private expertService: ExpertService,
    private userService: UserService,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    /* user connected is expert ? Manager is client? so user has he got access to client data?*/
    this.profileId = localStorage.getItem(Globals.USER_ID);
    this.userConnectedisExpert = this.UserIsExpert(this.profileId);

    /* data content about project's managers + position of popOver's arrow*/
    // this.content = this.infoPopoverRef.content;
    this.data = this.infoPopoverRef.data;
    this.arrowOffset = (6 * this.infoPopoverRef.offset) / 5 - 19;
    this.fetchManagerMoreData(this.data);
  }

  /**********************************************************************************
  GET MORE DATA ABOUT MANAGERS
  ***********************************************************************************/

  /*
    data miss some infos about manager profile:
    1- get all managers id
    2- get more data for all managers
    */

  fetchManagerMoreData(data): void {
    this.managers = [];
    this.managersId = data.map((element) => {
      this.userService.getPartial(element.user.id).subscribe((managerData) => {
        this.managers.push(managerData);
      });
    });
  }

  /**********************************************************************************
  CONTROL ACCESS TO CLIENT PROFILE : access denied if user connected is an expert
  ***********************************************************************************/

  /* user connected is expert or client ? */
  UserIsExpert(userId) {
    userId = this.profileId;
    this.userService.getPartial(userId).subscribe((user) => {
      this.userConnected = user;
      if (this.userConnected.expertId === null) {
        this.userConnectedisExpert = false;
      } else {
        this.userConnectedisExpert = true;
      }
    });
    return this.userConnectedisExpert;
  }

  /*
  manager clicked is expert or client ?
  if user connected is an expert and the manager is a client
  -> ACCESS TO MANAGER' S PROFILE IS DENIED
  */
  AccessProfile(managerId) {
    this.userService.getPartial(managerId).subscribe((manager) => {
      if (this.userConnectedisExpert === true && manager.clientId !== null) {
        this.managerIsClient = true;
        this.snackbar.open(
          'Access Denied',
          'You cannot access a client\'s profile as an Expert',
          {
            duration: 2500,
          }
        );
      } else {
        this.navigateToManager(manager.id);
      }
    });
    return this.managerIsClient;
  }

  /*  Generate link to manager's profile  */
  navigateToManager(managerId) {
    this.router.navigate(['profile/' + managerId]);
    this.close('success');
  }

  /**********************************************************************************
  POP UP CONTENT & CONTROL
  ***********************************************************************************/

  close(data?: any) {
    this.infoPopoverRef.close(data);
  }

  /*  Generate a url for manager img  */
  generateImgUrl(image?: any) {
    let url = '';
    if (image) {
      const hostname = environment.apiUrl;
      const path = 'files/';
      const imgName = image;
      const extension = '/download';
      url = hostname + path + imgName + extension;
    } else {
      url = 'assets/flaticon/userRed.png';
    }
    return url;
  }
}
