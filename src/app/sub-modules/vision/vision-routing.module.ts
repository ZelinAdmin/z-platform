import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectChoiceComponent } from './pages/project-choice/project-choice.component';
import { NotFoundComponent } from 'src/app/shared-pages/not-found/not-found.component';
import { ProjectComponent } from './pages/project/project.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectChoiceComponent,
  },
  {
    path: 'project/:id',
    component: ProjectComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisionRoutingModule {}
