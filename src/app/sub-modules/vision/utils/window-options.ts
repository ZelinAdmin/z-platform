export interface WindowOptions {
  width?: number;
  height?: number;
  fov?: number;
  aspectRatio?: number;
  zNear?: number;
  zFar?: number;
}
