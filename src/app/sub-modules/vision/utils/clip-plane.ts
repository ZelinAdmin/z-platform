import * as THREE from "./three";

export class ClipPlane extends THREE.Plane {
  public render = false;
  public renderHelper = false;
  private helper: THREE.Mesh = null;
  private step: number;
  private speed: number;
  private target: THREE.Object3D | THREE.Group | THREE.Mesh;
  private default: {
    normal: THREE.Vector3;
    constant: number;
  };

  constructor(normal: THREE.Vector3, constant: number = 0) {
    super(normal, constant);
    this.default = {
      normal: normal.clone(),
      constant: constant
    };
    this.speed = 0;
    this.step = 0;
  }

  public setHelper(
    width: number,
    height: number,
    color: number | THREE.Material | THREE.Material[],
    scene: THREE.Scene
  ) {
    if (scene) {
      const texture = new THREE.TextureLoader().load(
        `/assets/clip.png`
      );
      const geo = new THREE.PlaneGeometry(width, height, 1, 1);
      let material;
      if (typeof color === "number") {
        material = new THREE.MeshPhongMaterial({
          color: color,
          side: THREE.DoubleSide,
          map: texture,
          transparent: true
        });
      } else {
        material = color;
      }
      this.helper = new THREE.Mesh(geo, material);
      this.helper.visible = false;
      scene.add(this.helper);
    }
  }

  public reset(): void {
    this.set(this.default.normal, this.default.constant);
    this.step = 0;
  }

  public translateAlongNormal(direction: number = 0): void {
    const normal = this.normal.clone();
    0;
    if (direction > 0) {
      normal.negate();
      this.step--;
    } else {
      this.step++;
    }
    const offset = normal.multiplyScalar(this.speed);
    this.translate(offset);
  }

  public setTarget(
    target: THREE.Object3D | THREE.Group | THREE.Mesh,
    scene: THREE.Scene = null
  ): void {
    this.target = target;
    this.speed = target["radius"] / 500;
    this.translate(this.target.position);
    this.translate(
      this.target["center"]
        .clone()
        .applyQuaternion(this.target.quaternion)
        .negate()
    );
    if (scene && this.helper) {
      const size = target["radius"] * 2;
      this.setHelper(size, size, this.helper.material, scene);
    }
  }

  public update(): void {
    if (this.helper) {
      this.helper.visible = this.renderHelper && this.render;
    }
    if (!this.render) {
      this.set(new THREE.Vector3(0, 0, 0), 0);
      return;
    }
    this.constant = this.default.constant;
    this.normal = this.default.normal.clone();
    this.normal.applyQuaternion(this.target.quaternion);
    this.translate(this.target.position);
    this.translate(
      this.target["center"]
        .clone()
        .applyQuaternion(this.target.quaternion)
        .negate()
    );
    const normal = this.normal.clone();
    const offset = normal.multiplyScalar(this.speed).multiplyScalar(this.step);
    this.translate(offset);
    if (this.renderHelper) {
      const dir = this.normal.clone().divideScalar(100);
      const position = this.position;
      this.helper.position.set(
        position.x + dir.x,
        position.y + dir.y,
        position.z + dir.z
      );
      this.helper.lookAt(position.add(this.normal));
    }
  }

  public flip() {
    this.default.normal.negate();
  }

  get position() {
    return this.target.position
      .clone()
      .add(
        this.target["center"]
          .clone()
          .applyQuaternion(this.target.quaternion)
          .negate()
      )
      .add(
        this.normal
          .clone()
          .multiplyScalar(this.speed)
          .multiplyScalar(this.step)
      );
  }
}
