import { BehaviorSubject } from "rxjs";

export interface ClipGuiConfig {
  onTrue?: {
    css?: object;
    state: any; // state value to emit when the button is ON
  };
  onFalse?: {
    css?: object;
    state: any; // state value to emit when the button is OFF
  };
  min?: number;
  max?: number;
  default?: number;
  triggerChangeOnObject?: boolean;
}

export class ClipGui {
  public selector: any;
  public type: string;
  public state: boolean;
  public event: BehaviorSubject<any>;

  constructor(selector: string, type: string) {
    this.selector = document.querySelector(selector);
    this.type = type;
    this.state = false;
    this.event = new BehaviorSubject(this.state);
    if (this.type === "input") {
      let that = this;
      this.selector.oninput = function() {
        that.event.next(this.value);
      };
    } else {
      this.selector.addEventListener("click", () => {
        this.state = !this.state;
        this.event.next(this.state);
      });
    }
  }

  private pipeButton(object: any, value: string, config: ClipGuiConfig) {
    this.event.subscribe(state => {
      if (typeof object[value] === "function") {
        object[value]();
      } else {
        object[value] = state ? config.onTrue.state : config.onFalse.state;
        const css = state ? config.onTrue.css : config.onFalse.css;
        if (typeof css == "object") {
          const keys = Object.keys(css);
          keys.forEach(key => {
            this.selector.style[key] = css[key];
          });
        }
      }
    });
  }

  private pipeInput(object: any, value: string, config: ClipGuiConfig) {
    this.event.subscribe(slideValue => {
      object[value] = slideValue;
      if (config && config.triggerChangeOnObject) {
        object.dispatchEvent(new Event("change"));
      }
    });
    if (config) {
      if (config.min) {
        this.selector.min = config.min;
      }
      if (config.max) {
        this.selector.max = config.max;
      }
      if (config.default) {
        this.selector.value = config.default;
        this.event.next(this.selector.value);
      }
    }
  }

  public pipe(object: any, value: string, config: ClipGuiConfig = null) {
    switch (this.type) {
      case "button":
        this.pipeButton(object, value, config);
        break;
      case "input":
        this.pipeInput(object, value, config);
        break;
      default:
        break;
    }
  }
}
