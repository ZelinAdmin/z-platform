export interface VrmlFile {
  name?: string;
  uuid: string;
  size?: number | string;
  hascolormap?: boolean;
  status?: string;
  code?: number;
  projectId?: string;
}
