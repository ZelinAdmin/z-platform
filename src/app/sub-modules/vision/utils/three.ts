import * as THREE from "three";

export function computeMaxRadius(THREEObject3D: any): void {
  const bbox = new THREE.Box3().setFromObject(THREEObject3D);
  const size = new THREE.Vector3();
  bbox.getSize(size);
  THREEObject3D.radius = Math.max(size.x, size.y, size.z) / 2;
}

/**
 * Estimate the new center and recenter the object too
 */
export function estimateCenter(THREEObject3D: any): void {
  new THREE.Box3()
    .setFromObject(THREEObject3D)
    .getCenter(THREEObject3D.position)
    .multiplyScalar(-1);
  THREEObject3D.center = THREEObject3D.position.clone();
}

export * from "three";
