import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisionRoutingModule } from './vision-routing.module';
import { VisionComponent } from './vision.component';
import { ProjectChoiceComponent } from './pages/project-choice/project-choice.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { ProjectComponent } from './pages/project/project.component';
import { TableComponent } from './shared/table/table.component';
import { MainFrameComponent } from './shared/main-frame/main-frame.component';
import { ManagerInformationComponent } from './dialogs/manager-information/manager-information.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { NextMeetingCreationComponent } from './dialogs/next-meeting-creation/next-meeting-creation.component';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatOptionModule } from '@angular/material/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzDatePickerModule } from 'ng-zorro-antd';
import { TestCaseMatrixComponent } from './shared/test-case-matrix/test-case-matrix.component';
import { MatSliderModule } from '@angular/material/slider';
import { ModelSelectionComponent } from './shared/model-selection/model-selection.component';
import { ImageOverviewComponent } from './shared/image-multiview/image-overview.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatExpansionModule } from '@angular/material/expansion';
import { ProjectSettingsComponent } from './dialogs/project-settings/project-settings.component';
import { MatSortModule } from '@angular/material/sort';
import { ViewerComponent } from './shared/viewer/viewer.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ModelTreeViewerComponent } from './shared/model-tree-viewer/model-tree-viewer.component';
import { MatTreeModule } from '@angular/material/tree';
import { PlotOverviewComponent } from './shared/plot-multiview/plot-overview.component';
// import { MatSnackBar } from '@angular/material/snack-bar';

import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import { PlotlyModule } from 'angular-plotly.js';
import { MatSelectModule } from '@angular/material/select';
import { PlotConfigurationComponent } from './dialogs/plot-configuration/plot-configuration.component';
import { ElementInsightComponent } from './dialogs/element-insight/element-insight.component';
import { ColormapComponent } from './shared/colormap/colormap.component';
import { ImgFallbackModule } from 'ngx-img-fallback';
import { MatChipsModule } from '@angular/material/chips';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FlexLayoutModule } from '@angular/flex-layout';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [
    VisionComponent,
    ProjectChoiceComponent,
    ProjectComponent,
    TableComponent,
    MainFrameComponent,
    ManagerInformationComponent,
    NextMeetingCreationComponent,
    TestCaseMatrixComponent,
    ModelSelectionComponent,
    ImageOverviewComponent,
    ProjectSettingsComponent,
    ViewerComponent,
    ModelTreeViewerComponent,
    PlotOverviewComponent,
    PlotConfigurationComponent,
    ElementInsightComponent,
    ColormapComponent,
  ],
  imports: [
    CommonModule,
    VisionRoutingModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    FormsModule,
    MatTableModule,
    MatDialogModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FlexModule,
    ReactiveFormsModule,
    NzDatePickerModule,
    MatOptionModule,
    MatSliderModule,
    MatCheckboxModule,
    MatBadgeModule,
    MatTooltipModule,
    MatExpansionModule,
    MatSortModule,
    MatProgressBarModule,
    MatTreeModule,
    PlotlyModule,
    MatSelectModule,
    ImgFallbackModule,
    MatChipsModule,
    MatSlideToggleModule,
    FlexLayoutModule,
    // MatSnackBar,
  ],
  entryComponents: [
    ManagerInformationComponent,
    NextMeetingCreationComponent,
    ProjectSettingsComponent,
    PlotConfigurationComponent,
  ],
})
export class VisionModule {}
