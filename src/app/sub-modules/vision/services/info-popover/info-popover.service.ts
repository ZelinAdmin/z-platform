import { Injectable, Injector } from '@angular/core';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { Globals } from 'src/app/globals/globals';
import {
  InfoPopoverContent,
  InfoPopoverRef,
} from '../../classes/info-popover-ref';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';

export type InfoPopoverParams<T> = {
  origin: HTMLElement;
  content: InfoPopoverContent;
  data?: T;
  width?: string | number;
  height?: string | number;
  offset?: number;
};

@Injectable({
  providedIn: 'root',
})
export class InfoPopoverService {
  constructor(private overlay: Overlay, private injector: Injector) {}

  open<T>(
    { origin, content, data, width, height, offset = 0 }: InfoPopoverParams<T>,
    backdrop = true
  ) {
    const overlayRef = this.overlay.create(
      this.getConfig({ origin, width, height, offset }, backdrop)
    );
    const infoPopoverRef = new InfoPopoverRef<T>(
      overlayRef,
      content,
      data,
      offset
    );
    const injector = this.createInjector(infoPopoverRef, this.injector);
    // @ts-ignore
    overlayRef.attach(new ComponentPortal(content, null, injector));
    return infoPopoverRef;
  }

  createInjector(infoPopoverRef: InfoPopoverRef, injector: Injector) {
    const tokens = new WeakMap([[InfoPopoverRef, infoPopoverRef]]);
    return new PortalInjector(injector, tokens);
  }

  private getConfig(
    { origin, width, height, offset },
    backdropState
  ): OverlayConfig {
    return new OverlayConfig({
      width,
      height,
      hasBackdrop: backdropState,
      positionStrategy: this.overlay
        .position()
        .flexibleConnectedTo(origin)
        .withPositions([Globals.right])
        .withDefaultOffsetY(offset)
        .withDefaultOffsetX(40),
      scrollStrategy: this.overlay.scrollStrategies.reposition(),
    });
  }
}
