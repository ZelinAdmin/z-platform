import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ModelHolderService {
  model = new BehaviorSubject<string[]>(null);

  constructor() {}

  addModel(miniObjects: string[]) {
    this.model.next(miniObjects);
  }

  get getModel() {
    return this.model.asObservable();
  }

  clearModelGroup() {
    this.model.next(null);
  }
}
