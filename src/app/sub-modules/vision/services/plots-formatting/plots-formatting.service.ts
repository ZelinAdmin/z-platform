import { ElementRef, Injectable } from '@angular/core';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';
import { Globals } from 'src/app/globals/globals';
import { Papa } from 'ngx-papaparse';
import { PlotlyLine } from '../../interfaces/plotly-line/plotly-line';
import { Plotly } from 'angular-plotly.js/src/app/shared/plotly.interface';
import { Observable } from 'rxjs';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';

@Injectable({
  providedIn: 'root',
})
export class PlotsFormattingService {
  constructor(
    private papa: Papa,
    private fileService: FileManipulationsService
  ) {}

  public plotFormatting(txt, file: ProjectFileDisplayed) {
    // Formating to avoid errors
    txt = txt.split(';').join(',');
    const lastQuote = txt.indexOf('"', 1);
    for (let cpt = 0; cpt < lastQuote; cpt++) {
      if (txt[cpt] === ',') {
        txt = txt.replace(',', ';');
      }
    }

    /**
     * This function helps me get only a specified number of lines (total)
     * starting from the given line (start).
     */
    // tslint:disable-next-line:no-shadowed-variable
    const skipFirstLine = (txt: string): string =>
      txt.substring(txt.indexOf('\n') + 1);

    if (
      txt.substring(0, txt.indexOf('\n') - 1) ===
      '"Title","HorizontalAxis","VerticalAxis"'
    ) {
      txt = skipFirstLine(skipFirstLine(txt));
    }
    /**
     * This function returns true when an error code is present in the given
     * list of errors.
     */
    const noErrOf = (code, err): boolean =>
      // tslint:disable-next-line:no-shadowed-variable
      err.findIndex((err) => err.code === code) === -1;

    // Either papaparse can guess the delimiter OR it cannot, and in this
    // case we hint him ' '.
    const { options } = this.buildOptions(txt, skipFirstLine, noErrOf);

    // The actual parsing.
    const papa = this.papa.parse(
      options.skipFirstLine ? skipFirstLine(txt) : txt,
      options
    );

    // A row has only numbers in it.
    const isRow = (row: any[]) => row.every((c) => !isNaN(c));

    if (papa.errors && papa.errors.length > 0) {
      papa.errors.forEach((e) => {
        if (e.code === 'UndetectableDelimiter') {
          console.warn(file.filename + ':' + e.row + ': ' + e.message);
        }
      });
    }

    // We must transpose the papaparse result into a plotly-friendly format.
    // Papaparse gives us:	[{x: 1, y:1}, {x: 2, y:2}]
    // But plotly needs:	{x: [1,2], y: [1,2]}.
    const transposed = Globals.transposeArray(papa.data);
    const fieldX = papa.meta.fields[0];

    // For the 2nd, 3rd, 4th, 5th... dimension, create a new PlotlyLine. The
    // 1st dimension is always 'x'.
    let i = 0;
    return papa.meta.fields.slice(1).map((fieldY) => {
      const res: PlotlyLine = {
        x: transposed[fieldX],
        y: transposed[fieldY],
        name: papa.meta.fields[i + 1]
          ? papa.meta.fields[i + 1]
          : 'Curve ' + (i + 1),
      };
      i++;
      return res;
    });
  }

  private buildOptions(
    txt,
    skipFirstLine: (_: string) => {},
    noErrOf: (_, __) => {}
  ) {
    return (
      [
        { header: true },
        { preview: 10, delimiter: ' ', header: true },
        { preview: 10, delimiter: ' ', header: true, skipFirstLine: true }, // sometimes, first line = graph title.
      ]
        // tslint:disable-next-line:no-shadowed-variable
        .map((options) => ({
          options,
          preview: this.papa.parse(
            options.skipFirstLine ? skipFirstLine(txt) : txt,
            { ...options, preview: 10 }
          ),
        }))
        .find(
          ({ preview }) =>
            noErrOf('UndetectableDelimiter', preview.errors) &&
            noErrOf('TooManyFields', preview.errors)
        )
    );
  }

  public fetchPlotLayout$(
    txt,
    file: ProjectFileDisplayed
  ): Observable<Plotly.Layout> {
    txt = txt.substring(0, txt.indexOf('\n') - 1);

    const papa = this.papa.parse(txt, { header: true });
    const titles = papa.meta.fields[0].split(';');
    const res: Plotly.Layout = {
      title: { text: titles[2] ? titles[2] : 'Title' },
      xaxis: { title: { text: titles[0] } },
      yaxis: {
        title: { text: titles[1] ? titles[1] : 'Y Axis' },
        type: localStorage.getItem(file.internalId + 'LayoutType') || 'scatter',
      },
      showlegend:
        localStorage.getItem(file.internalId + 'ShowLegend') === 'true',
      autosize: true,
    };
    return res;
  }
}
