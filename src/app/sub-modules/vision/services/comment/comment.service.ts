import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FileComment } from '../../interfaces/file-comment/file-comment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommentService {
  constructor(private http: HttpClient) {}

  getAllComments(projectId: string, fileId: string) {
    const url = `${environment.apiUrl}projects/${projectId}/file/${fileId}/comments`;
    return this.http.get<FileComment[]>(url);
  }
  getComment(projectId: string, fileId: string, commentId: string) {
    const url = `${environment.apiUrl}projects/${projectId}/file/${fileId}/comments/${commentId}`;
    return this.http.get<FileComment>(url);
  }

  // Returns the id of the created comment. id not required, will be set by
  // the backend.
  post(comment: FileComment): Observable<FileComment> {
    const url = `${environment.apiUrl}projects/${comment.projectId}/file/${comment.fileId}/comments`;
    return this.http.post<FileComment>(url, comment);
  }

  put(comment: FileComment) {
    const url = `${environment.apiUrl}projects/${comment.projectId}/file/${comment.fileId}/comments/${comment.id}`;
    return this.http.put<FileComment>(url, comment);
  }

  delete(projectId: string, fileId: string, commentId: string) {
    const url = `${environment.apiUrl}projects/${projectId}/file/${fileId}/comments/${commentId}`;
    return this.http.delete<{}>(url);
  }
}
