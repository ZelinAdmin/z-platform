import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EmailService {
  constructor(private http: HttpClient) {}

  sendRequestMeetingEmail(nextDate: Date, projectId: string, userId: string) {
    return this.http.post<any>(
      `${environment.apiUrl}projects/send-email/${projectId}`,
      {
        role: 'CUSTOMER',
        date: nextDate,
        user: userId,
      }
    );
  }

  public sendVPNAccess(userId: string) {
    return this.http.post<any>(
      `${environment.apiUrl}mail/send-vpn/${userId}`,
      null
    );
  }
}
