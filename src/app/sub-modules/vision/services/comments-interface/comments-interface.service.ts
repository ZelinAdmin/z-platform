import { BehaviorSubject, Observable } from 'rxjs';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';

export abstract class CommentsInterfaceService {
  protected selectedFiles: BehaviorSubject<
    ProjectFileDisplayed[]
  > = new BehaviorSubject<ProjectFileDisplayed[]>(null);

  protected constructor() {}

  abstract setupFiles(files: ProjectFileDisplayed[]);

  abstract getFilesObservable(): Observable<ProjectFileDisplayed[]>;

  abstract getType(): string;
}
