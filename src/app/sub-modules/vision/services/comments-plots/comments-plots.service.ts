import { Injectable } from '@angular/core';
import { CommentsInterfaceService } from '../comments-interface/comments-interface.service';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommentsPlotsService extends CommentsInterfaceService {
  constructor() {
    super();
  }

  getFilesObservable(): Observable<ProjectFileDisplayed[]> {
    return this.selectedFiles.asObservable();
  }

  setupFiles(files: ProjectFileDisplayed[]) {
    this.selectedFiles.next(files);
  }

  getType(): string {
    return 'plot';
  }
}
