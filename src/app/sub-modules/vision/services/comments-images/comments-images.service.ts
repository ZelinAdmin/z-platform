import { Injectable } from '@angular/core';
import { CommentsInterfaceService } from '../comments-interface/comments-interface.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';

@Injectable({
  providedIn: 'root',
})
export class CommentsImagesService extends CommentsInterfaceService {
  constructor() {
    super();
  }

  getFilesObservable(): Observable<ProjectFileDisplayed[]> {
    return this.selectedFiles.asObservable();
  }

  setupFiles(files: ProjectFileDisplayed[]) {
    this.selectedFiles.next(files);
  }

  getType(): string {
    return 'image';
  }
}
