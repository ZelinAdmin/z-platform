import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ProjectFile } from '../../interfaces/project-file/project-file';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  constructor(private http: HttpClient) {}

  getAllProjectFiles(
    projectId: string,
    param: string = null,
    paramValue: string = null
  ) {
    const url = `${environment.apiUrl}projects/${projectId}/file`;

    if (param && paramValue) {
      return this.http.get<ProjectFile[]>(url, {
        params: { param: paramValue },
      });
    } else {
      return this.http.get<ProjectFile[]>(url);
    }
  }

  downloadFile(fileId: string, options = {}) {
    const url = `${environment.apiUrl}files/${fileId}/download`;
    return this.http.get(url, { responseType: 'text', ...options });
  }
}
