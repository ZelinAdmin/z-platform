import { Component, OnInit, VERSION } from '@angular/core';
import { ProjectService } from 'src/app/services/project/project.service';
import { Globals } from 'src/app/globals/globals';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
// import {LayoutModule} from '@angular/cdk/layout';

@Component({
  selector: 'app-project-choice',
  templateUrl: './project-choice.component.html',
  styleUrls: ['./project-choice.component.scss'],
})
export class ProjectChoiceComponent implements OnInit {
  public fallbackImage = 'assets/fallbackProject.png';
  public projectList: UserProject[] = [];

  // make the content responsive
  ngVersion: string = VERSION.full;
  matVersion = '5.1.0';
  breakpoint: number;
  color: string;
  // inprogress: string;
  // terminate: string;

  constructor(private projectService: ProjectService, public router: Router) {}

  ngOnInit(): void {
    this.fetchProjectList();
    this.breakpoint = 6;
    // this.inprogress='red';
    // this.terminate='green';
  }

  fetchProjectList() {
    this.projectService
      .getUserProjects(localStorage.getItem(Globals.USER_ID))
      .subscribe((projects: UserProject[]) => {
        this.projectList = projects;
      });
  }

  generateImgUrl(element: UserProject) {
    return `${environment.apiUrl}files/${element.project.image}/download`;
  }

  // make the content responsive
  onResize(event) {
    switch (true) {
      case event.target.innerWidth <= 400:
        this.breakpoint = 1;
        break;
      case event.target.innerWidth <= 600:
        this.breakpoint = 2;
        break;
      case event.target.innerWidth <= 800:
        this.breakpoint = 3;
        break;
      case event.target.innerWidth <= 950:
        this.breakpoint = 4;
        break;
      case event.target.innerWidth <= 1200:
        this.breakpoint = 5;
        break;
      default:
        this.breakpoint = 6;
        break;
    }
  }

  updateColor(progress) {
    if (progress < 100) {
      return 'warn';
    }
  }
}
