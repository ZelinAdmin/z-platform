import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project/project.service';
import { Globals } from 'src/app/globals/globals';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { SnackBarService } from 'src/app/services/snackbar-service/snack-bar.service';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { ManagerInformationComponent } from '../../dialogs/manager-information/manager-information.component';
import { AccessService } from 'src/app/services/access-service/access.service';
import { ProjectUsersReduced } from '../../interfaces/project-users-reduced/project-users-reduced';
import { NextMeetingCreationComponent } from '../../dialogs/next-meeting-creation/next-meeting-creation.component';
import { InfoPopoverService } from '../../services/info-popover/info-popover.service';
import { EmailService } from '../../services/email/email.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProjectSettingsComponent } from '../../dialogs/project-settings/project-settings.component';
import { ProjectUpdateInfo } from 'src/app/interfaces/project-update-info/project-update-info';
import { DataHolderService } from 'src/app/services/data-holder/data-holder.service';
import { CommentsImagesService } from 'src/app/sub-modules/vision/services/comments-images/comments-images.service';
import { CommentsPlotsService } from 'src/app/sub-modules/vision/services/comments-plots/comments-plots.service';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
})
export class ProjectComponent implements OnInit, AfterViewInit {
  private managerInformation: ProjectUsersReduced[];
  public loadingStatus = true;
  public projectId: string;
  public breakpoint: number; // responsive

  public fallbackImage = '/assets/fallbackProject.png';
  currentProject: UserProject = this.createPlaceholderProjectInformation();

  @ViewChild('managerInfo') managerInfoButtonRef: HTMLButtonElement;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    private projectService: ProjectService,
    private accessService: AccessService,
    private snackBarService: SnackBarService,
    private infoPopoverService: InfoPopoverService,
    private emailService: EmailService,
    public dialog: MatDialog,
    private snackbar: MatSnackBar,
    private dataService: DataHolderService,

    private commentIService: CommentsImagesService,
    private commentPService: CommentsPlotsService
  ) {}

  ngOnInit(): void {
    /* -----------------------------------
     Fetch initial info about the project
    ------------------------------------ */
    this.fetchProjectInformation();
    this.fetchManagers();

    /* -----------------------------------
     set selected files in multiview to 0
     ----------------------------------- */
    this.commentIService.setupFiles(null);
    this.commentPService.setupFiles(null);

    /*------------------------------------
    responsive
    --------------------------------------*/
    this.breakpoint = 200;
  }

  ngAfterViewInit(): void {}

  /* -----------------------------------
     1) Get project id from the route
     2) save the id to data holder service
     so it can easily be accessible by children
     and other components
     3) fetch info about project from back
    ------------------------------------ */
  fetchProjectInformation() {
    this.projectId = this.route.snapshot.paramMap.get('id');
    this.dataService.setProjectId(this.projectId);

    this.projectService
      .getProjectInformation(
        this.projectId,
        localStorage.getItem(Globals.USER_ID)
      )
      .subscribe(
        (info: UserProject) => {
          this.currentProject = info;
          this.loadingStatus = false;
        },
        (error) => {
          this.loadingStatus = false;
          this.snackBarService.unauthorized(
            'You don\'t have permission to view this project',
            '/vision'
          );
        }
      );
  }

  /* -----------------------------------
     Get all manager assigned to this project
     to displayed them in sidebar
    ------------------------------------ */
  fetchManagers() {
    this.accessService
      .getAllProjectUsers(this.projectId)
      .subscribe((projectUsers: ProjectUsersReduced[]) => {
        this.managerInformation = projectUsers.filter(
          (e) => e.role === 'MANAGER'
        );
      });
  }

  /* -----------------------------------
     Open the overlay about managers
    ------------------------------------ */
  openManagerInformationDialog(): void {
    const ref = this.infoPopoverService.open({
      origin: this.managerInfoButtonRef,
      data: this.managerInformation,
      content: ManagerInformationComponent,
      width: '400px',
      offset: -(this.managerInformation.length * 20),
    });

    // ref.afterClosedObservable.subscribe((data) => console.log(data));
  }

  /* -----------------------------------
     Open project settings dialog,
     if the data after dismiss isn't null,
     update project information on the sever
    ------------------------------------ */
  openProjectSettingDialog(): void {
    const dialogRef = this.dialog.open(ProjectSettingsComponent, {
      minWidth: '450px',
      data: this.currentProject,
    });

    dialogRef.afterClosed().subscribe((dialogResult: ProjectUpdateInfo) => {
      if (dialogResult) {
        this.projectService.update(this.projectId, dialogResult).subscribe(
          (ok) => this.updateProjectDisplayedInfo(dialogResult),
          (error) => {}
        );
      } else {
        console.log('closed dialog');
      }
    });
  }

  /* -----------------------------------
     Open request a meeting dialog
     (accessible only for customers)
     If the data after dismiss is date
     update 'next meeting' date as well as
     send email to managers
    ------------------------------------ */
  openRequestMeetingDialog(): void {
    const ref = this.infoPopoverService.open({
      origin: this.managerInfoButtonRef,
      content: NextMeetingCreationComponent,
      width: '500px',
      offset: -70,
    });

    ref.afterClosedObservable.subscribe((data) => {
      if (data instanceof Date) {
        this.emailService
          .sendRequestMeetingEmail(
            data,
            this.projectId,
            localStorage.getItem(Globals.USER_ID)
          )
          .subscribe(
            (responseData) => {
              this.snackbar.open('Your email has been successfully sent', '', {
                duration: 2500,
                verticalPosition: 'top',
              });
            },
            (error) => {
              this.snackbar.open(
                'There was a problem while sending your email',
                '',
                {
                  duration: 2500,
                  verticalPosition: 'top',
                }
              );
              console.error(error);
            }
          );
      }
    });
  }

  /* -----------------------------------
     Update currentProject variable
     with new data
    ------------------------------------ */
  updateProjectDisplayedInfo(newData: ProjectUpdateInfo) {
    this.currentProject.project.teamsUrl = newData.teamsUrl;
    this.currentProject.project.nextMeeting = newData.nextMeeting;
    this.currentProject.project.startDate = newData.startDate;
    this.currentProject.project.endDate = newData.endDate;
    this.currentProject.project.globalprogress = newData.globalprogress;
  }

  /* -----------------------------------
     Generate a url for project img
    ------------------------------------ */
  generateImgUrl(element: UserProject) {
    return `${environment.apiUrl}files/${element.project.image}/download`;
  }

  public navigateToTeamsLink(link: string) {
    window.open(link, '_blank');
  }

  /* -----------------------------------
     Init currentProject variable with
     blank data to avoid 'null' errors
    ------------------------------------ */
  private createPlaceholderProjectInformation(): UserProject {
    return {
      project: {
        progress: '100',
        name: 'Lorem',
        image: '',
      },
      role: 'Lorem',
      user: {
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        expertId: '',
      },
    };
  }

  /* -----------------------------------
    Make the content responsive
    ------------------------------------ */
  onResize(event) {
    switch (true) {
      case event.target.innerWidth <= 700:
        this.breakpoint = 80;
        break;
      case event.target.innerWidth <= 850:
        this.breakpoint = 100;
        break;
      case event.target.innerWidth <= 1000:
        this.breakpoint = 120;
        break;
      case event.target.innerWidth <= 1150:
        this.breakpoint = 150;
        break;
      case event.target.innerWidth <= 1300:
        this.breakpoint = 180;
        break;
      default:
        this.breakpoint = 200;
        break;
    }
  }

  /* -----------------------------------
     Change the spinne color when progress = 100%
    ------------------------------------ */
  updateColor(progress) {
    if (progress < 100) {
      return 'warn';
    }
  }
}
