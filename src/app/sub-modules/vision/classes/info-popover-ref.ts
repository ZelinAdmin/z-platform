import { TemplateRef, Type } from '@angular/core';
import { Subject } from 'rxjs';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

export type InfoPopoverCloseEvt<T> = {
  type: 'backdropClick' | 'close';
  data: T;
};

export type InfoPopoverContent = TemplateRef<any> | Type<any> | string;
export class InfoPopoverRef<T = any> {
  private afterClosedSubject = new Subject<InfoPopoverCloseEvt<T>>();
  afterClosedObservable = this.afterClosedSubject.asObservable();

  constructor(
    public overlayRef: OverlayRef,
    public content: InfoPopoverContent,
    public data: T,
    public offset: number
  ) {
    overlayRef
      .backdropClick()
      .subscribe(() => this.cclose('backdropClick', data));
  }

  /*close popUp*/
  close(data?: T) {
    this.cclose('close', data);
  }

  private cclose(type, data) {
    this.overlayRef.dispose();
    this.afterClosedSubject.next({
      type,
      data,
    });
    this.afterClosedSubject.complete();
  }
}
