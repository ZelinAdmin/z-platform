import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import * as THREE from '../../utils/three';
import * as EZTB from 'eztoolbar';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Globals } from 'src/app/globals/globals';
import { ClipPlane } from '../../utils/clip-plane';
import { ClipGui, ClipGuiConfig } from '../../utils/clip-gui';
import { isFullScreen, setFullScreen } from '../../utils/fullscreen';
import { ColorMapData } from '../../utils/colormap';
import { HttpClient } from '@angular/common/http';
import { WindowOptions } from '../../utils/window-options';
import { ModelHolderService } from '../../services/model-holder/model-holder.service';
import { Object3D } from 'three/src/core/Object3D';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.less'],
})
export class ViewerComponent implements OnInit, OnDestroy {
  @Output() treeViewer = new BehaviorSubject<any>(null);
  @Output() scrollTo = new BehaviorSubject<string>(null);
  // tslint:disable-next-line:no-output-native
  @Output() show = new BehaviorSubject<any>(null);
  colormapData = new BehaviorSubject<ColorMapData>(new ColorMapData(null));

  public camera: THREE.PerspectiveCamera = null;
  public isFullscreen = false;
  public options: any = null;

  private container: HTMLElement;
  private scene: THREE.Scene = null;
  private renderer: THREE.WebGLRenderer = null;
  private object: any = null;
  private objectDirRef: any = null;
  private axis: Object3D = null;
  private toolbar: EZTB = null;
  private wireframe = false;
  private enableHighlight = false;
  private clipGui = false;
  private model: Subscription;
  private highlightColor = new THREE.Color(Globals.HIGHLIGHT_COLOR);
  private highLightMaterial = new THREE.MeshPhongMaterial({
    color: 0xffff66,
    // side: THREE.DoubleSide
  });
  private regulator: any = {
    currentTime: 0,
    lastTime: 0,
    dt: 0,
  };

  constructor(
    private http: HttpClient,
    private modelHolder: ModelHolderService
  ) {}

  ngOnInit() {
    this.container = document.getElementById('canvas');
    this.injectDefaultOptions();
    this.scene = new THREE.Scene();
    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      powerPreference: 'high-performance',
    });
    this.renderer.setSize(this.options.width, this.options.height);
    this.renderer.setClearColor(0xf4f3f1, 1);
    this.renderer.domElement.style.position = 'absolute';
    this.container.appendChild(this.renderer.domElement);
    this.keyInputs();
    this.load3DModel();
    this.axesDisplayer();
    // this.addGUI();
    this.run();
    this.onWindowResize();
  }

  ngOnDestroy(): void {
    this.clearScene();
    this.model.unsubscribe();
    this.modelHolder.clearModelGroup();
  }

  private load3DModel(): void {
    this.model = this.modelHolder.getModel.subscribe((res: string[]) => {
      if (!res) {
        return;
      }
      this.clearScene();
      this.colormapData.next(new ColorMapData(null));
      const loader = new THREE.ObjectLoader();
      const group = new THREE.Group();
      res.forEach((element) => {
        if (typeof element === 'string') {
          this.colormapData.next(new ColorMapData(element));
        } else {
          group.add(loader.parse(element));
        }
      });
      group.updateMatrixWorld();
      this.object = group;
      THREE.computeMaxRadius(this.object);
      if (this.object.radius > 600) {
        const scalePercentage = 600 / this.object.radius;
        this.object.scale.set(
          scalePercentage,
          scalePercentage,
          scalePercentage
        );
        THREE.computeMaxRadius(this.object);
      }
      // myobject.center is a vector to use for translate to center (in translate/rotate method), also recenter automatically
      THREE.estimateCenter(this.object);
      this.scene.add(this.object);
      this.object.traverse((child: any) => {
        if (child instanceof THREE.Mesh) {
          this.objectDirRef.push(child);
        }
        if (child.material instanceof THREE.MeshPhongMaterial) {
          child.material.wireframe = this.wireframe;
        }
      });
      this.treeViewer.next(this.object);
      // this.colorMapdata.next(new ColorMapData(res.colormap));
      this.addGUI();
      this.addCamera();
      this.addLighting();
    });
  }

  private restoreView(resetPosition = true) {
    this.axis.rotation.set(0, 0, 0);
    this.object.rotation.set(0, 0, 0);
    if (resetPosition) {
      this.object.position.set(0, 0, 0);
    }
    this.object.updateMatrix();
    if (resetPosition) {
      THREE.estimateCenter(this.object);
    }
    this.addCamera();
  }

  private fullscreenResize() {
    setTimeout(() => {
      this.renderer.setSize(window.innerWidth, window.innerHeight);
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
    }, 200);
  }

  private minimizeResize() {
    setTimeout(() => {
      const width = this.container.getBoundingClientRect().width;
      this.renderer.setSize(width, 720);
      this.camera.aspect = width / 720;
      this.camera.updateProjectionMatrix();
    }, 200);
  }

  private switchFullscreen() {
    if (!this.isFullscreen) {
      this.isFullscreen = true;
      setFullScreen(this.isFullscreen);
      this.fullscreenResize();
    } else {
      this.isFullscreen = false;
      setFullScreen(this.isFullscreen); // remove the browser fullscreen mode
      this.minimizeResize();
    }
  }

  private addGUI() {
    if (this.toolbar === null) {
      this._toolbarNull();
    }
    if (!this.clipGui && this.object) {
      this.tempName();
    } else {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.renderer.clippingPlanes.length; i++) {
        this.renderer.clippingPlanes[i].setTarget(this.object, this.scene);
      }
    }
  }

  private _toolbarNull() {
    const showWireframe = function() {
      this.object.traverse((child) => {
        if (child.material instanceof THREE.MeshPhongMaterial) {
          this.wireframe = !child.material.wireframe;
          child.material.wireframe = !child.material.wireframe;
        }
      });
    }.bind(this);

    const rotateXneg = () =>
      this.rotate(new THREE.Vector3(0, Math.PI / 2, 0), false);
    const rotateXpos = () =>
      this.rotate(new THREE.Vector3(0, -Math.PI / 2, 0), false);
    const rotateYneg = () =>
      this.rotate(new THREE.Vector3(-Math.PI / 2, 0, 0), false);
    const rotateYpos = () =>
      this.rotate(new THREE.Vector3(-Math.PI / 2, Math.PI, 0), false);
    const rotateZneg = () =>
      this.rotate(new THREE.Vector3(0, Math.PI, 0), false);
    const rotateZpos = () => this.rotate(new THREE.Vector3(0, 0, 0), false);
    const enableHighlight = () =>
      (this.enableHighlight = !this.enableHighlight);
    const showClipPlanePanelControl = () => {
      const ct = document.querySelector('#clip-container') as any;
      ct.style.display = ct.style.display === 'none' ? 'block' : 'none';
    };
    const showTreeViewer = () => this.show.next(null);

    this.toolbar = new EZTB({ position: 'top-left' });

    this.toolbar.add({
      faclass: 'fas fa-list',
      hovertext: 'Display assembly tree',
      event: showTreeViewer,
    });
    this.toolbar.add({
      faclass: ['fas fa-border-all', 'fas fa-cube'],
      hovertext: 'Toggle mesh',
      event: showWireframe,
      toggle: true,
    });
    this.toolbar.add({
      faclass: ['far fa-lightbulb', 'fas fa-lightbulb'],
      event: enableHighlight,
      hovertext: 'Toggle highlight',
    });
    this.toolbar.add({
      faclass: 'fas fa-undo-alt',
      hovertext: 'Restore original scene',
      event: this.restoreView.bind(this),
    });
    this.toolbar.add({ text: '-X', event: rotateXneg });
    this.toolbar.add({ text: '+X', event: rotateXpos });
    this.toolbar.add({ text: '-Y', event: rotateYneg });
    this.toolbar.add({ text: '+Y', event: rotateYpos });
    this.toolbar.add({ text: '-Z', event: rotateZneg });
    this.toolbar.add({ text: '+Z', event: rotateZpos });
    const clipPlaneButton = this.toolbar.add({
      faclass: ['fas fa-crop-alt', 'fas fa-crop'],
      hovertext: 'Show clip plane panel control',
      event: showClipPlanePanelControl,
    });
    document
      .querySelector('#switch-fullscreen')
      .addEventListener('click', this.switchFullscreen.bind(this));
    document
      .querySelector('#close-clip-panel')
      .addEventListener('click', () =>
        clipPlaneButton.dispatchEvent(new Event('click'))
      );
    this.container.appendChild(this.toolbar.domElement);
  }

  private tempName() {
    const xclipPlane = new ClipPlane(new THREE.Vector3(-1, 0, 0), 0);
    const yclipPlane = new ClipPlane(new THREE.Vector3(0, -1, 0), 0);
    const zclipPlane = new ClipPlane(new THREE.Vector3(0, 0, -1), 0);

    const size = this.object.radius * 2;
    xclipPlane.setHelper(size, size, 0x00ff00, this.scene);
    yclipPlane.setHelper(size, size, 0xff0000, this.scene);
    zclipPlane.setHelper(size, size, 0x0000ff, this.scene);

    xclipPlane.setTarget(this.object);
    yclipPlane.setTarget(this.object);
    zclipPlane.setTarget(this.object);

    this.renderer.clippingPlanes = [xclipPlane, yclipPlane, zclipPlane];

    for (let i = 120; i <= 122; i++) {
      const axis = String.fromCharCode(i);
      let clipPlane = null;
      let bgColor = null;
      switch (axis) {
        case 'x':
          clipPlane = xclipPlane;
          bgColor = '#99D066';
          break;
        case 'y':
          clipPlane = yclipPlane;
          bgColor = '#E57373';
          break;
        case 'z':
          clipPlane = zclipPlane;
          bgColor = '#63A4FF';
          break;
      }

      const flipButton = new ClipGui(`.${axis}clip .flip-plane`, 'button');
      const labelButton = new ClipGui(`.${axis}clip .clip-label`, 'button');
      const renderButton = new ClipGui(`.${axis}clip .render-plane`, 'button');
      const sliderButton = new ClipGui(`.${axis}clip .slider`, 'input');
      const sliderInput = new ClipGui(`.${axis}clip .slider-input`, 'input');

      const sliderConf1: ClipGuiConfig = {
        min: -550,
        max: 550,
        triggerChangeOnObject: true,
      };
      const sliderConf2: ClipGuiConfig = {
        min: -550,
        max: 550,
        triggerChangeOnObject: false,
      };
      const buttonConf: ClipGuiConfig = {
        onTrue: {
          css: {
            opacity: 1,
            background: bgColor,
          },
          state: true,
        },
        onFalse: {
          css: {
            opacity: null,
            background: null,
          },
          state: false,
        },
      };

      sliderInput.pipe(sliderButton.selector, 'value', sliderConf1);
      sliderInput.pipe(clipPlane, 'step');

      sliderButton.pipe(clipPlane, 'step', sliderConf2);
      sliderButton.pipe(sliderInput.selector, 'value');

      flipButton.pipe(clipPlane, 'flip');
      labelButton.pipe(clipPlane, 'render', buttonConf);
      renderButton.pipe(clipPlane, 'renderHelper', buttonConf);
    }
  }

  private addLighting() {
    const ambientLight = new THREE.AmbientLight(
      0xf0f0f0,
      Globals.LIGHTNING_INTENSITY
    ); // soft white light
    const light1 = new THREE.PointLight(0xf0f0f0, 1.1, 200000);
    light1.position.set(0, 0, 100000);
    this.scene.add(light1);
    this.scene.add(ambientLight);
  }

  private injectDefaultOptions(options: WindowOptions = null) {
    // Default window and camera settings
    const width = this.container.getBoundingClientRect().width;
    const defaultOptions: WindowOptions = {
      width,
      height: 720,
      fov: 56.25,
      aspectRatio: width / 720,
      zNear: 0.01,
      zFar: 10000,
    };
    // Replace default options by the one specified in arguments
    if (typeof options === 'object' && options) {
      this.options = Object.assign(defaultOptions, options);
    } else {
      this.options = defaultOptions;
    }
    this.addCamera();
  }

  public clearScene() {
    if (this.scene !== null) {
      this.objectDirRef = [];
      this.axis.rotation.set(0, 0, 0);
      this.scene.traverse((child: any) => {
        if (child.geometry) {
          child.geometry.dispose();
        }
        if (child.material) {
          child.material.dispose();
        }
      });
      while (this.scene.children.length) {
        this.scene.remove(this.scene.children[0]);
      }
      this.renderer.renderLists.dispose();
    }
  }

  private run() {
    setInterval(() => {
      this.regulator.currentTime = performance.now();
      this.regulator.dt = this.regulator.lastTime
        ? (this.regulator.currentTime - this.regulator.lastTime) / 100
        : 0.166;
      this.regulator.lastTime = this.regulator.currentTime;

      if (this.renderer.clippingPlanes.length) {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.renderer.clippingPlanes.length; i++) {
          this.renderer.clippingPlanes[i].update();
        }
      }

      if (this.scene) {
        this.renderer.render(this.scene, this.camera);
      }
      if (this.regulator.dt >= 0.25) {
        this.regulator.dt = 0.166;
      }
    }, 16);
  }

  private addCamera() {
    let cameraDistance = 120;

    if (this.object) {
      cameraDistance =
        this.object.radius / Math.tan(((this.camera.fov / 2) * Math.PI) / 180) +
        1;
    } // calculate the distance according to the object radius, the object will fill the screen entirely
    if (this.camera === null) {
      this.camera = new THREE.PerspectiveCamera(
        this.options.fov,
        this.options.aspectRatio,
        this.options.zNear,
        this.options.zFar
      );
    }
    this.camera.position.set(0, 0, cameraDistance);
    this.camera.lookAt(0, 0, 0);
  }

  private rotate(dir: THREE.Vector3, onMouseRotate: boolean = true) {
    const quaty = new THREE.Quaternion();
    const quatx = new THREE.Quaternion();
    const quatz = new THREE.Quaternion();
    const pos = this.object.center;

    this.object.translateX(-pos.x);
    this.object.translateY(-pos.y);
    this.object.translateZ(-pos.z);
    if (onMouseRotate) {
      if (dir.y) {
        quaty.setFromAxisAngle(
          new THREE.Vector3(1, 0, 0),
          dir.y * 0.35 * this.regulator.dt
        );
      }
      if (dir.x) {
        quatx.setFromAxisAngle(
          new THREE.Vector3(0, 1, 0),
          dir.x * 0.35 * this.regulator.dt
        );
      }
    } else {
      // for preset view
      this.restoreView(false);
      if (dir.x) {
        quatx.setFromAxisAngle(new THREE.Vector3(1, 0, 0), dir.x);
      }
      if (dir.y) {
        quaty.setFromAxisAngle(new THREE.Vector3(0, 1, 0), dir.y);
      }
      if (dir.z) {
        quatz.setFromAxisAngle(new THREE.Vector3(0, 0, 1), dir.z);
      }
    }
    this.object.applyQuaternion(quatx);
    this.object.applyQuaternion(quaty);
    this.object.applyQuaternion(quatz);
    this.axis.applyQuaternion(quatx);
    this.axis.applyQuaternion(quaty);
    this.axis.applyQuaternion(quatz);
    this.object.translateX(pos.x);
    this.object.translateY(pos.y);
    this.object.translateZ(pos.z);
  }

  /**
   * This function return the nearest intersection, take into account the 3 clip planes axes
   * ex: it mean it will test the intersection point found on X axis against Y and Z clip plane
   * @param intersects An array of intersection.
   */
  private highlightNearestObject(intersects: THREE.Intersection[]) {
    if (intersects.length) {
      const cp = this.renderer.clippingPlanes;
      if (!cp[0].render && !cp[1].render && !cp[2].render) {
        // if no clip planes are enabled
        return intersects[0].object;
      }

      const testPoint = (point: THREE.Vector3, clippingPlane: ClipPlane) => {
        const planeToPoint = point
          .clone()
          .sub(clippingPlane.position)
          .normalize();
        const angle = clippingPlane.normal.clone().dot(planeToPoint);
        return angle > 0;
      };

      const getNearestIntersection = function(
        intersections: THREE.Intersection[],
        clippingPlane: ClipPlane
      ) {
        let nearestIntersection = null;
        let distanceFromCam = 9999999;
        for (let i = 0; i < intersects.length; i++) {
          if (testPoint(intersections[i].point, clippingPlane)) {
            const dst = intersections[i].point.distanceTo(this.camera.position);
            if (dst < distanceFromCam) {
              distanceFromCam = dst;
              nearestIntersection = intersections[i];
            }
          }
          if (intersections.length - 1 === i) {
            return nearestIntersection;
          }
        }
      }.bind(this);

      // tslint:disable-next-line:only-arrow-functions
      const whoIsNearestObject = function() {
        const nearest = {
          x:
            (cp[0].render ? getNearestIntersection(intersects, cp[0]) : null) ||
            null,
          y:
            (cp[1].render ? getNearestIntersection(intersects, cp[1]) : null) ||
            null,
          z:
            (cp[2].render ? getNearestIntersection(intersects, cp[2]) : null) ||
            null,
        };
        let object = null;
        if (nearest.x !== null) {
          if (nearest.y !== null && nearest.z !== null) {
            if (
              testPoint(nearest.x.point, cp[1]) &&
              testPoint(nearest.x.point, cp[2])
            ) {
              object = nearest.x.object;
            }
          } else if (nearest.y !== null && nearest.z === null) {
            if (testPoint(nearest.x.point, cp[1])) {
              object = nearest.x.object;
            }
          } else if (nearest.z !== null && nearest.y === null) {
            if (testPoint(nearest.x.point, cp[2])) {
              object = nearest.x.object;
            }
          } else {
            object = nearest.x.object;
          }
        }
        if (nearest.y !== null) {
          if (nearest.x !== null && nearest.z !== null) {
            if (
              testPoint(nearest.y.point, cp[0]) &&
              testPoint(nearest.y.point, cp[2])
            ) {
              object = nearest.y.object;
            }
          } else if (nearest.x !== null && nearest.z === null) {
            if (testPoint(nearest.y.point, cp[0])) {
              object = nearest.y.object;
            }
          } else if (nearest.z !== null && nearest.x === null) {
            if (testPoint(nearest.y.point, cp[2])) {
              object = nearest.y.object;
            }
          } else {
            object = nearest.y.object;
          }
        }
        if (nearest.z !== null) {
          if (nearest.x !== null && nearest.y !== null) {
            if (
              testPoint(nearest.z.point, cp[0]) &&
              testPoint(nearest.z.point, cp[1])
            ) {
              object = nearest.z.object;
            }
          } else if (nearest.x !== null && nearest.y === null) {
            if (testPoint(nearest.z.point, cp[0])) {
              object = nearest.z.object;
            }
          } else if (nearest.y !== null && nearest.x === null) {
            if (testPoint(nearest.z.point, cp[1])) {
              object = nearest.z.object;
            }
          } else {
            object = nearest.z.object;
          }
        }
        return object;
      }.bind(this);

      return whoIsNearestObject();
    }
  }

  private keyInputs() {
    let hoverCanvas = false;
    let leftClick = false;
    let rightClick = false;
    let ctrlPressed = false;
    let isZooming = false;
    let zoomEvent = null;
    let oldpos = null;
    const mouse = new THREE.Vector2();
    let highlighted = null;
    const raycaster = new THREE.Raycaster();

    const preventDefault = (prevDefaultElement: any) => {
      if (prevDefaultElement.preventDefault) {
        prevDefaultElement.preventDefault();
      }
      prevDefaultElement.returnValue = false;
    };

    const zoom = function(zoomElement: any) {
      if (this.object !== null) {
        const dist = this.camera.position.distanceTo(this.object.position);
        const wheelDelta =
          zoomElement.wheelDelta ||
          zoomElement.deltaY * -1 ||
          zoomElement.originalEvent.deltaY * -1 ||
          zoomElement.originalEvent.wheelDelta ||
          zoomElement.originalEvent.detail * -1;
        this.camera.position.z +=
          (wheelDelta > 0 ? 1 : -1) * (dist / 5) * this.regulator.dt;
        isZooming = true;
        if (zoomEvent) {
          clearTimeout(zoomEvent);
          zoomEvent = null;
          oldpos = null;
        }
      }
      if (zoomEvent === null) {
        zoomEvent = setTimeout(() => {
          isZooming = false;
        }, 250);
      }
    }.bind(this);

    const mouseUp = (mouseUpElement: any) => {
      if (!(mouseUpElement.buttons && 1)) {
        leftClick = false;
      }
      if (!(mouseUpElement.buttons && 2)) {
        rightClick = false;
      }
    };

    const mouseDown = (mouseDownElement: any) => {
      if (mouseDownElement.buttons && 1) {
        leftClick = true;
      }
      if (mouseDownElement.buttons && 2) {
        rightClick = true;
      }
    };

    const ctrlState = (ctrlStateElement: any) => {
      ctrlPressed = ctrlStateElement.ctrlKey;
    };

    const onMouseMove = function(onMouseDownElement: any) {
      const rect = this.renderer.domElement.getBoundingClientRect();
      mouse.x =
        ((onMouseDownElement.clientX - rect.x) / this.options.width) * 2 - 1;
      mouse.y =
        -((onMouseDownElement.clientY - rect.y) / this.options.height) * 2 + 1;
    }.bind(this);

    const get3DMousePosition = (_: any) => {
      // get the position (fullscreenResize) of your cursor movement position in the 3D World (on z = 0) (new position -old position)
      const vec = new THREE.Vector3();
      let tomove = new THREE.Vector3();
      let newpos = new THREE.Vector3();
      let distance = 0;

      vec.set(mouse.x, mouse.y, 0);
      vec.unproject(this.camera);
      vec.sub(this.camera.position).normalize();
      distance = -this.camera.position.z / vec.z;
      if (oldpos === null) {
        oldpos = this.camera.position.clone().add(vec.multiplyScalar(distance));
      } else {
        newpos = this.camera.position.clone().add(vec.multiplyScalar(distance));
        tomove = newpos.clone().sub(oldpos);
        oldpos = newpos;
      }
      return tomove;
    };

    /* prevent scroll from bubbling on the renderer */
    this.renderer.domElement.onwheel = preventDefault;
    // this.renderer.domElement.onmousewheel = preventDefault;

    /* Watch left and right click state (press / down) */
    document.onmouseup = mouseUp;
    document.onmousedown = mouseDown;

    /* Prevent opening right click menu on renderer */
    this.renderer.domElement.addEventListener('contextmenu', (e: any) => {
      preventDefault(e);
    });

    this.renderer.domElement.addEventListener(
      'mouseover',
      (e: MouseEvent) => {
        hoverCanvas = true;
      },
      false
    );

    this.renderer.domElement.addEventListener(
      'mouseleave',
      (e: MouseEvent) => {
        hoverCanvas = false;
      },
      false
    );

    window.addEventListener(
      'wheel',
      (e: any) => {
        if (hoverCanvas) {
          zoom(e);
        }
      },
      false
    );

    window.addEventListener(
      'mousemove',
      (e: MouseEvent) => {
        if (this.object && !isZooming) {
          onMouseMove(e); // GET CANVAS MOUSE POSITION
          const tomove = get3DMousePosition(e); // GET XYZ position in scene 3d coordinate
          // ROTATE OBJECT WITH LEFT CLICK
          if (hoverCanvas && leftClick && !ctrlPressed) {
            const screenLength =
              Math.abs(this.camera.position.z * Math.tan(this.camera.fov / 2)) *
              2;
            const rotatePercentage = new THREE.Vector3(
              (tomove.x / screenLength) *
                100 *
                this.regulator.dt *
                (this.isFullscreen ? 2 : 1),
              -((tomove.y / screenLength) * 100) *
                this.regulator.dt *
                (this.isFullscreen ? 2 : 1),
              0
            );
            this.rotate(rotatePercentage);
          }
          // TRANSLATE OBJECT WITH RIGHT CLICK
          if (hoverCanvas && rightClick) {
            this.object.position.x +=
              this.camera.position.z > 0 ? tomove.x : -tomove.x;
            this.object.position.y +=
              this.camera.position.z > 0 ? tomove.y : -tomove.y;
          }

          // RAYCAST TO HIGHLIGHT OVERED MESH
          if (this.enableHighlight) {
            if (highlighted) {
              highlighted.material.color = highlighted.materialRef;
              highlighted = null;
            }
            if (hoverCanvas && ((!leftClick && !rightClick) || ctrlPressed)) {
              raycaster.setFromCamera(mouse, this.camera);
              const intersects = raycaster.intersectObjects(
                this.objectDirRef,
                true
              );
              highlighted = this.highlightNearestObject(intersects);
              if (highlighted === null && intersects.length) {
                highlighted = intersects[0].object;
              }
              if (highlighted) {
                highlighted.materialRef = highlighted.material.color;
                highlighted.material.color = this.highlightColor;
                if (
                  highlighted.materialRef.transparent &&
                  highlighted.materialRef.opacity < 1
                ) {
                  highlighted.material.transparent = true;
                  highlighted.material.opacity = 0.9;
                }
              }
            }
          }
        }
      },
      false
    );

    window.addEventListener('fullscreenchange', (e: any) => {
      // exit fullscreen on ESC/F11 pressed
      if (!isFullScreen() && this.isFullscreen) {
        this.switchFullscreen();
      }
    });

    window.addEventListener('keydown', ctrlState);
    window.addEventListener('keyup', ctrlState);

    let clearScrollTo = null;
    const treeViewerScrollTo = function() {
      if (ctrlPressed && leftClick) {
        clearTimeout(clearScrollTo);
        clearScrollTo = setTimeout(() => {
          this.scrollTo.next(highlighted);
        }, 50);
      }
      requestAnimationFrame(treeViewerScrollTo.bind(this));
    }.bind(this);

    treeViewerScrollTo();
  }

  private onWindowResize() {
    window.addEventListener('resize', () => {
      if (!this.isFullscreen) {
        this.minimizeResize();
      } else {
        this.fullscreenResize();
      }
    });
  }

  private axesDisplayer() {
    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      alpha: true,
      antialias: true,
      powerPreference: 'high-performance',
    });
    renderer.setSize(93, 93);

    this.http
      .get(`app/sub-modules/vision/utils/axismodel.wrl`)
      .subscribe((res: any) => {
        const loader = new THREE.ObjectLoader();
        loader.parse(res, (object: Object3D) => {
          const camera = new THREE.OrthographicCamera(
            93 / -55,
            93 / 55,
            93 / 55,
            93 / -55,
            -2,
            2
          );
          camera.position.y = 0.5;
          this.axis = object;
          scene.add(this.axis);
          scene.add(new THREE.AmbientLight(0xffffff, 1));
          renderer.domElement.style.position = 'absolute';
          renderer.domElement.style.left = '0';
          renderer.domElement.style.bottom = '6px';
          const canvas = this.container;
          if (canvas) {
            canvas.appendChild(renderer.domElement);
          }
          const animate = () => {
            renderer.render(scene, camera);
            requestAnimationFrame(animate);
          };
          requestAnimationFrame(animate);
        });
      });
  }
}
