import { Component, Input, OnInit } from '@angular/core';
import { ProjectFile } from '../../interfaces/project-file/project-file';
import { Observable } from 'rxjs';
import { FileService } from '../../services/file-service/file.service';
import { take } from 'rxjs/operators';
import { Papa, ParseResult } from 'ngx-papaparse';
import { Globals } from 'src/app/globals/globals';
import { MatTableDataSource } from '@angular/material/table';
import { FileUploadComponent } from 'src/app/shared-components/dialogs/file-upload/file-upload.component';
import { MatDialog } from '@angular/material/dialog';
import { DialogNotificationService } from 'src/app/services/dialog-notification/dialog-notification.service';
import { DialogDataToInject } from 'src/app/interfaces/dialog-data/dialog-data-to-inject';

@Component({
  selector: 'app-test-case-matrix',
  templateUrl: './test-case-matrix.component.html',
  styleUrls: ['./test-case-matrix.component.less'],
})
export class TestCaseMatrixComponent implements OnInit {
  @Input() file: Observable<ProjectFile>;
  @Input() projectId: string;
  displayedColumns: string[] = [];
  tableDataSource: MatTableDataSource<any>;
  tableWidth = 1400;

  constructor(
    private fileService: FileService,
    private papa: Papa,
    private dialog: MatDialog,
    private dialogNotification: DialogNotificationService
  ) {}

  ngOnInit(): void {
    this.subscribeToFile();
  }

  subscribeToFile() {
    this.file.subscribe((data) => {
      if (!data) {
        return;
      }
      this.preDisplay();
      this.fileService
        .downloadFile(data.id)
        .pipe(take(1))
        .subscribe((csv) => this.preparation(csv));
    });
  }

  public openFileUploadDialog() {
    const dialogData: DialogDataToInject = {
      projectId: this.projectId,
      category: 'progress',
      path: '',
    };
    const dialogRef = this.dialog.open(FileUploadComponent, {
      minWidth: '450px',
      data: dialogData,
    });

    this.dialogNotification.getFileNotif().subscribe((data: ProjectFile) => {
      if (!data) {
        return;
      }
      this.fileService
        .downloadFile(data.id)
        .pipe(take(1))
        .subscribe((csv) => this.preparation(csv));
    });
  }

  /* check if there's data in local storage
   * if so, display it while fetching data from server*/
  private preDisplay() {
    const header = JSON.parse(
      localStorage.getItem(Globals.testCaseMatrixHeader)
    );
    const data = JSON.parse(localStorage.getItem(Globals.testCaseMatrixCol));
    if (header && data) {
      this.displayedColumns = header;
      this.tableDataSource = new MatTableDataSource(data);
    }
  }

  /* compare local storage data and fetched from server
   * update displayed if there'es difference*/
  private preparation(csv: string) {
    if (localStorage.getItem(Globals.testCaseMatrixRaw) !== csv) {
      localStorage.setItem(Globals.testCaseMatrixRaw, csv);
      this.readData(csv);
    } else {
      this.displayedColumns = JSON.parse(
        localStorage.getItem(Globals.testCaseMatrixHeader)
      );

      this.tableDataSource = new MatTableDataSource(
        JSON.parse(localStorage.getItem(Globals.testCaseMatrixCol))
      );
    }
  }

  private readData(csv: string) {
    const papa = this.papa.parse(csv);
    if (papa.data[0][papa.data[0].length - 1] === 'Progress') {
      this.parseData(papa);
    } else {
      this.showError();
    }
  }

  private parseData(papa: ParseResult) {
    const rowspanCsvData = [];
    for (let i = 1; i < papa.data.length; i++) {
      const rowspanArray: number[] = [];
      for (let y = 0; y < papa.data[i].length; y++) {
        if (!papa.data[i][y]) {
          rowspanArray.push(0);
        } else {
          let rowspan = 1;
          for (
            rowspan;
            rowspan + i < papa.data.length - 1 && !papa.data[i + rowspan][y];
            rowspan++
          ) {}
          rowspanArray.push(rowspan);
        }
      }
      rowspanCsvData.push(rowspanArray);
    }
    const csvDataHeader = papa.data[0];
    const csvData = papa.data;
    csvData.shift();

    localStorage.setItem(
      Globals.testCaseMatrixHeader,
      JSON.stringify(csvDataHeader)
    );
    localStorage.setItem(Globals.testCaseMatrixCol, JSON.stringify(csvData));

    this.displayedColumns = csvDataHeader;
    this.tableDataSource = new MatTableDataSource(csvData);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();

    if (this.tableDataSource.paginator) {
      this.tableDataSource.paginator.firstPage();
    }
  }
  showError() {}

  public parseAXER(element: Array<any>, i: number): number {
    if (element[i]) {
      return parseInt(element[i].replace('%', ''), 10);
    }
    return 0;
  }

  public comparator(element, low, high): boolean {
    const num = parseInt(element.replace('%', ''), 10);
    return element.includes('%') && low <= num && num <= high;
  }
}
