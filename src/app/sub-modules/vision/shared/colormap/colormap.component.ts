import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { colorMap, ColorMapData } from '../../utils/colormap';

@Component({
  selector: 'app-colormap',
  templateUrl: './colormap.component.html',
  styleUrls: ['./colormap.component.less'],
})

/* IT'S NOT MY CODE
 * i literally copy pasted from the source code of the old version
 * */
export class ColormapComponent implements OnInit {
  public labels: HTMLElement;
  public title: HTMLElement;
  public colors: HTMLElement;

  @ViewChild('colormap') container;
  @Input() colorMapdata: Observable<ColorMapData>;

  constructor(private hostRef: ElementRef) {}

  ngOnInit() {
    this.colorMapdata.subscribe((cMapInfo: ColorMapData) => {
      if (this.container) {
        this.container.nativeElement.innerHTML = '';
      }
      if (cMapInfo.valid) {
        this.buildHTML(cMapInfo);
      }
    });
  }

  public buildTitle(cmap: ColorMapData) {
    this.title = document.createElement('div');
    this.title.id = 'colormap-title';
    this.title.setAttribute(this.getContentAttr(), '');
    this.title.appendChild(document.createTextNode(cmap.title));
  }

  public buildLabels(cmap: ColorMapData) {
    this.labels = document.createElement('ul');
    this.labels.id = 'colormap-labels';
    this.labels.setAttribute(this.getContentAttr(), '');
    const min = cmap.min;
    const max = cmap.max;
    const gap = (max - min) / 5;
    let decimal = 1;

    if (Math.floor(min.valueOf()) !== min.valueOf()) {
      decimal += min.toString().split('.')[1].length;
    }
    if (
      max.toString().split('.').length > 1 &&
      max.toString().split('.')[1].length > decimal
    ) {
      decimal = max.toString().split('.')[1].length;
    }

    // if (Math.floor(min.valueOf()) !== min.valueOf()) {
    //   decimal += min.toString().split(".")[1].length;
    // }

    for (let i = 0; i < 6; i++) {
      const label = document.createElement('li');
      if (i === 0) {
        label.appendChild(document.createTextNode(min.toFixed(decimal)));
      } else if (i === 6) {
        label.appendChild(document.createTextNode(max.toFixed(decimal)));
      } else {
        label.appendChild(
          document.createTextNode((min + i * gap).toFixed(decimal))
        );
      }
      label.setAttribute(this.getContentAttr(), '');
      this.labels.appendChild(label);
    }
  }

  public buildColors(cmap: ColorMapData) {
    this.colors = document.createElement('div');
    this.colors.setAttribute(this.getContentAttr(), '');
    this.colors.id = 'colormap-colors';
    for (let i = 0; i < cmap.level; i++) {
      const color = document.createElement('div');
      color.style.backgroundColor = colorMap[cmap.level - 1][i];
      color.style.width = 100 / cmap.level + '%';
      color.className = 'color-cell';
      color.setAttribute(this.getContentAttr(), '');
      this.colors.appendChild(color);
    }
  }

  public buildHTML(cmap: ColorMapData) {
    this.buildTitle(cmap);
    this.buildLabels(cmap);
    this.buildColors(cmap);

    this.container.nativeElement.appendChild(this.title);
    this.container.nativeElement.appendChild(this.labels);
    this.container.nativeElement.appendChild(this.colors);
  }

  private getContentAttr(): string {
    const attrs = this.hostRef.nativeElement.attributes;
    for (let i = 0, l = attrs.length; i < l; i++) {
      if (attrs[i].name.startsWith('_nghost-')) {
        return `_ngcontent-${attrs[i].name.substring(8)}`;
      }
    }
  }
}
