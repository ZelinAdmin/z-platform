import { Component, Input, OnInit } from '@angular/core';
import { CommentsInterfaceService } from '../../services/comments-interface/comments-interface.service';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';
import { environment } from 'src/environments/environment';
import { ElementInsightComponent } from '../../dialogs/element-insight/element-insight.component';
import { InfoPopoverService } from '../../services/info-popover/info-popover.service';
import { FileService } from 'src/app/sub-modules/vision/services/file-service/file.service';
import { DataHolderService } from 'src/app/services/data-holder/data-holder.service';

@Component({
  selector: 'app-image-comments',
  templateUrl: './image-overview.component.html',
  styleUrls: ['./image-overview.component.less'],
})
export class ImageOverviewComponent implements OnInit {
  /* abstract service to pass files data to comment section*/
  @Input() commentService: CommentsInterfaceService;
  @Input() projectId: string;
  public files: ProjectFileDisplayed[] = [];
  public colonWidth = 2;

  constructor(
    private infoPopoverService: InfoPopoverService,
    private filesService: FileService,
    private dataHolder: DataHolderService
  ) {}

  ngOnInit(): void {
    this.commentService
      .getFilesObservable()
      .subscribe((files: ProjectFileDisplayed[]) => {
        if (!files) {
          return;
        }
        this.files = files;
      });
  }

  getUrl(f: ProjectFileDisplayed): string {
    return `${environment.apiUrl}files/${f.internalId}/download`;
  }

  public reRender(value) {
    this.colonWidth = value;
  }

  public openElementInsight(
    files: ProjectFileDisplayed[],
    currentIndex: number
  ) {
    const ref = this.infoPopoverService.open({
      origin: null,
      content: ElementInsightComponent,
      data: {
        files,
        index: currentIndex,
        category: 'image',
      },
    });

    ref.afterClosedObservable.subscribe(() => {
      /* update information about files in case comments were updated*/
      this.filesService
        .getAllProjectFiles(this.dataHolder.projectId, 'category', 'image')
        .subscribe((fetchedFiles) => {
          const trunked = fetchedFiles.filter((e) => e.category === 'image');
          this.files = this.files.map((file) => {
            trunked.forEach((trunk) => {
              if (trunk.id === file.internalId) {
                file.commentNbr = trunk.commentNrb;
              }
            });
            return file;
          });
        });
    });
  }
}
