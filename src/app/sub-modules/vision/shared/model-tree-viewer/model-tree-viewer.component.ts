import { Component, Input, OnInit } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import * as THREE from '../../utils/three';
import { SelectionModel } from '@angular/cdk/collections';
import { Globals } from 'src/app/globals/globals';

interface TreeNode {
  object?: THREE.Object3D;
  name: string;
  children?: TreeNode[];
}

class TreeFlatNode {
  object?: THREE.Object3D;
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-model-tree-viewer',
  templateUrl: './model-tree-viewer.component.html',
  styleUrls: ['./model-tree-viewer.component.less'],
})
export class ModelTreeViewerComponent implements OnInit {
  @Input() treeViewer: BehaviorSubject<any>;
  @Input() scrollTo: BehaviorSubject<string>;
  @Input() show: BehaviorSubject<any>;

  treeData: TreeNode[] = [];
  private highlightColor = new THREE.Color(Globals.HIGHLIGHT_COLOR);

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<TreeFlatNode, TreeNode>();
  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<TreeNode, TreeFlatNode>();
  treeControl: FlatTreeControl<TreeFlatNode>;
  treeFlattener: MatTreeFlattener<TreeNode, TreeFlatNode>;
  dataSource: MatTreeFlatDataSource<TreeNode, TreeFlatNode>;
  /** The selection for checklist */
  checklistSelection = new SelectionModel<TreeFlatNode>(true /* multiple */);

  constructor() {
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this.getLevel,
      this.isExpandable,
      this.getChildren
    );
    this.treeControl = new FlatTreeControl<TreeFlatNode>(
      this.getLevel,
      this.isExpandable
    );
    this.dataSource = new MatTreeFlatDataSource(
      this.treeControl,
      this.treeFlattener
    );
    this.dataSource.data = this.treeData;
  }

  getLevel = (node: TreeFlatNode) => node.level;

  isExpandable = (node: TreeFlatNode) => node.expandable;

  getChildren = (node: TreeNode): TreeNode[] => node.children;

  hasChild = (_: number, nodeData: TreeFlatNode) => nodeData.expandable;

  ngOnInit(): void {
    this.treeViewer.subscribe((completedObject: THREE.Object3D) => {
      if (!completedObject) {
        return;
      }
      const masterNode = this.populateTreeData(completedObject);
      this.treeData = masterNode.children;
      this.dataSource.data = this.treeData;
      this.treeData.forEach((e) => {
        this.todoItemSelectionToggle(this.transformer(e, 0));
        // this.todoLeafItemSelectionToggle(this.transformer(e, 0), true);
      });
    });
  }

  private populateTreeData(obj: THREE.Object3D): TreeNode {
    const data: TreeNode = {
      name: this.generateName(obj),
      object: obj,
    };
    const childrenArray = [];
    obj.children.forEach((object: THREE.Object3D) => {
      childrenArray.push(this.populateTreeData(object));
    });
    if (childrenArray.length > 0) {
      data.children = childrenArray;
    }
    return data;
  }

  private generateName(obj: any): string {
    if (obj instanceof THREE.Mesh) {
      return 'Mesh';
    } else if (obj instanceof THREE.LineSegments) {
      return 'LineSegments';
    } else if (obj instanceof THREE.Group) {
      return 'Group';
    } else if (obj instanceof THREE.Object3D) {
      return 'Object3D';
    }
  }

  /* highlight mesh*/
  highlightOn(object: any) {
    if (object instanceof THREE.Object3D) {
      object.children.forEach((e) => this.highlightOn(e));
      /* recursion to highlight all nested meshes*/
    }

    if (object instanceof THREE.Mesh) {
      object.material.initialColor = object.material.color;
      object.material.color = this.highlightColor;
    }
  }

  highlightOff(object: any) {
    if (object instanceof THREE.Object3D) {
      object.children.forEach((e) => this.highlightOff(e));
      /* recursion to highlight all nested meshes*/
    }

    if (object instanceof THREE.Mesh) {
      object.material.color = object.material.initialColor;
    }
  }

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: TreeNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode =
      existingNode && existingNode.name === node.name
        ? existingNode
        : new TreeFlatNode();
    flatNode.name = node.name;
    flatNode.level = level;
    flatNode.object = node.object;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: TreeFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every((child) =>
      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TreeFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some((child) =>
      this.checklistSelection.isSelected(child)
    );
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TreeFlatNode): void {
    this.checklistSelection.toggle(node);
    node.object.visible = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this._select(descendants)
      : this._deselect(descendants);

    // Force update for the parent
    descendants.every((child) => this.checklistSelection.isSelected(child));
    this.checkAllParentsSelection(node);
  }

  private _deselect(descendants: TreeFlatNode[] | TreeFlatNode) {
    if (descendants instanceof Array) {
      this.checklistSelection.deselect(...descendants);
      descendants.forEach((e) => (e.object.visible = false));
    } else {
      this.checklistSelection.deselect(descendants);
      descendants.object.visible = false;
    }
  }

  private _select(descendants: TreeFlatNode[] | TreeFlatNode) {
    if (descendants instanceof Array) {
      this.checklistSelection.select(...descendants);
      descendants.forEach((e) => (e.object.visible = true));
    } else {
      this.checklistSelection.select(descendants);
      descendants.object.visible = true;
    }
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: TreeFlatNode): void {
    this.checklistSelection.toggle(node);
    node.object.visible = this.checklistSelection.isSelected(node);
    this.checkAllParentsSelection(node);
  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: TreeFlatNode): void {
    let parent: TreeFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: TreeFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every((child) =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this._deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this._select(node);
    }
  }
  /* Get the parent node of a node */
  getParentNode(node: TreeFlatNode): TreeFlatNode | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }
}
