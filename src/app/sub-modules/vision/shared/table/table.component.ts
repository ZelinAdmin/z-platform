import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TableOptions } from '../../interfaces/table-options/table-options';
import { ProjectFile } from '../../interfaces/project-file/project-file';
import { Observable } from 'rxjs';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { CommentsInterfaceService } from '../../services/comments-interface/comments-interface.service';
import { MatDialog } from '@angular/material/dialog';
import { FileUploadComponent } from 'src/app/shared-components/dialogs/file-upload/file-upload.component';
import { DialogDataToInject } from 'src/app/interfaces/dialog-data/dialog-data-to-inject';
import { DataHolderService } from 'src/app/services/data-holder/data-holder.service';
import { isNumeric } from 'rxjs/internal-compatibility';
import { DialogNotificationService } from 'src/app/services/dialog-notification/dialog-notification.service';
import { ConfirmationData } from 'src/app/interfaces/confirmation-data/confirmation-data';
import { ConfirmationComponent } from 'src/app/shared-components/dialogs/confirmation/confirmation.component';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { InfoPopoverService } from '../../services/info-popover/info-popover.service';
import { ElementInsightComponent } from '../../dialogs/element-insight/element-insight.component';
import { ProjectService } from 'src/app/services/project/project.service';
import { Globals } from 'src/app/globals/globals';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { FileService } from 'src/app/sub-modules/vision/services/file-service/file.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  /* what rows to display and how to name them*/
  @Input() tableDisplayedOption: TableOptions;
  /* observable to get file list*/
  @Input() files: Observable<ProjectFile[]>;
  /* abstract service to pass files data to comment section*/
  @Input() commentService: CommentsInterfaceService;

  @ViewChild(MatSort, { static: true }) tableSort: MatSort;

  displayedColumns: string[] = [];
  tableData: ProjectFileDisplayed[] = [];
  tableDataSource: MatTableDataSource<ProjectFileDisplayed>;
  idMaxValue: number;
  packageMaxValue: number;
  userInfo: UserProject = { project: null, role: '', user: null };

  initialSelection = [];
  allowMultipleSelection = true;
  public tableSelection = new SelectionModel<ProjectFileDisplayed>(
    this.allowMultipleSelection,
    this.initialSelection
  );

  constructor(
    private dialog: MatDialog,
    private dataService: DataHolderService,
    private dialogNotification: DialogNotificationService,
    private fileManipulation: FileManipulationsService,
    private infoPopoverService: InfoPopoverService,
    public projectService: ProjectService,
    private filesService: FileService
  ) {}

  /* -----------------------------------
     Fetch information about project
    ------------------------------------ */
  ngOnInit(): void {
    this.subscribeToInput();
    this.projectService
      .getProjectInformation(
        this.dataService.projectId,
        localStorage.getItem(Globals.USER_ID)
      )
      .subscribe((data) => (this.userInfo = data));
  }

  /* -----------------------------------
     Open a fullscreen overlay
     with the file and comments
    ------------------------------------ */
  public openElementInsight(
    files: ProjectFileDisplayed[],
    currentIndex: number
  ) {
    const selectedFiles =
      this.tableSelection.selected.length === 0
        ? this.tableDataSource.filteredData
        : this.tableSelection.selected;
    const ref = this.infoPopoverService.open({
      origin: null,
      content: ElementInsightComponent,
      data: {
        files: selectedFiles,
        index: currentIndex > selectedFiles.length - 1 ? 0 : currentIndex,
        category: this.tableDisplayedOption.category,
      },
    });

    ref.afterClosedObservable.subscribe((_) => {
      /* update information about files in case comments were updated*/
      this.filesService
        .getAllProjectFiles(
          this.dataService.projectId,
          'category',
          this.tableDisplayedOption.category
        )
        .subscribe((fetchedFiles) => {
          const trunked = fetchedFiles.filter(
            (e) => e.category === this.tableDisplayedOption.category
          );
          this.tableData = this.tableData.map((file) => {
            trunked.forEach((trunk) => {
              if (trunk.id === file.internalId) {
                file.commentNbr = trunk.commentNrb;
              }
            });
            return file;
          });
          this.tableDataSourceSetup();
        });
    });
  }

  /* -----------------------------------
     Delete element and if success,
     remove it from the table
    ------------------------------------ */
  private _del(element: ProjectFileDisplayed) {
    this.fileManipulation
      .deleteFilesWithinCategory(this.dataService.projectId, element.internalId)
      .subscribe(
        (e) => {
          this.tableData = this.tableData.filter((file) => file !== element);
          this.tableDataSourceSetup();
        },
        (error) => console.error(error)
      );
  }

  /* -----------------------------------
     Open confirmation dialog about
     deleting an element
    ------------------------------------ */
  public deleteElement(element: ProjectFileDisplayed) {
    const dialogData: ConfirmationData = {
      title: element.filename + ' Will be deleted',
      message: 'This step is irreversible. Are you sure you want to proceed?',
    };

    const dialogRef = this.dialog.open(ConfirmationComponent, {
      minWidth: '450px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
      if (dialogResult) {
        this._del(element);
      }
    });
  }

  /* -----------------------------------
     Open confirmation dialog
     about deleting multiple elements
    ------------------------------------ */
  public deleteMultiple() {
    const dialogData: ConfirmationData = {
      title: 'Do you want to delete these elements?',
      arrayOfFiles: this.tableSelection.selected,
    };

    const dialogRef = this.dialog.open(ConfirmationComponent, {
      minWidth: '450px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
      if (dialogResult) {
        this.tableSelection.selected.forEach((e) => this._del(e));
      }
      this.tableSelection.clear();
    });
  }

  /* -----------------------------------
     1) Get material table displayed columns
     from input
     2) get associated files to this project
     4) init material table
    ------------------------------------ */
  private subscribeToInput() {
    this.displayedColumns = this.tableDisplayedOption.columnsOptions.map(
      (e) => e.identifier
    );

    this.files.subscribe((files: ProjectFile[]) => {
      if (!files) {
        return;
      }
      this.parseInputFiles(files);
      this.tableDataSourceSetup();
    });
  }

  /* -----------------------------------
     Extract package and and name
     from initial filename

     Deduce max Id and Package for sorting
    ------------------------------------ */
  private parseInputFiles(files: ProjectFile[]) {
    this.tableData = files.map((e) => this.mapToProjectFileDisplayed(e));
    this.idMaxValue = Math.max.apply(
      Math,
      this.tableData.map((elem) => {
        return elem.id;
      })
    );
    this.packageMaxValue = Math.max.apply(
      Math,
      this.tableData.map((elem) => {
        return elem.package;
      })
    );
  }

  /* -----------------------------------
     Material table config
    ------------------------------------ */
  private tableDataSourceSetup() {
    this.tableDataSource = new MatTableDataSource(this.tableData);
    this.tableDataSource.sort = this.tableSort;
    this.tableDataSource.sortingDataAccessor = this.customSortRule;
    /* select all previously selected files*/
    this.commentService.getFilesObservable().subscribe((selectedFiles) => {
      if (!selectedFiles) {
        return;
      }
      selectedFiles.forEach((e) => {
        this.tableSelection.select(
          this.tableData.find(
            (tableFile) => tableFile.internalId === e.internalId
          )
        );
      });
    });
  }

  /* -----------------------------------
     Open fileupload dialog and subscribe
     to the service which will notify if
     any files were uploaded
    ------------------------------------ */
  public openFileUploadDialog() {
    const dialogData: DialogDataToInject = {
      projectId: this.dataService.projectId,
      category: this.tableDisplayedOption.category,
      path: '',
    };
    const dialogRef = this.dialog.open(FileUploadComponent, {
      minWidth: '450px',
      data: dialogData,
    });
    this.dialogNotification.getFileNotif().subscribe((data) => {
      if (!data) {
        return;
      }
      this.tableData.push(this.mapToProjectFileDisplayed(data));
      this.tableDataSourceSetup();
    });
  }

  isAllSelected() {
    const numSelected = this.tableSelection.selected.length;
    const numRows = this.tableDataSource.data.length;
    return numSelected === numRows;
  }

  public checkboxClick(event, element) {
    event.stopPropagation();
    if (event) {
      this.toggle(element);
    }
  }

  /* -----------------------------------
     At the end, send all selected files
     to the multiview
    ------------------------------------ */
  public toggle(element: ProjectFileDisplayed) {
    this.tableSelection.toggle(element);
    this.commentService.setupFiles(this.tableSelection.selected);
  }

  /* -----------------------------------
     At the end, send all selected files
     to the multiview
    ------------------------------------ */
  masterToggle() {
    this.isAllSelected()
      ? this.tableSelection.clear()
      : this.tableDataSource.data.forEach((row) => {
          this.tableSelection.select(row);
          // }
        });
    this.commentService.setupFiles(this.tableSelection.selected);
  }

  private mapToProjectFileDisplayed = (
    file: ProjectFile
  ): ProjectFileDisplayed => {
    const splitFileName = file.filename.split('_');
    let dataToInsert: ProjectFileDisplayed;
    if (
      splitFileName.length > 2 &&
      isNumeric(splitFileName[0]) &&
      isNumeric(splitFileName[1])
    ) {
      dataToInsert = {
        package: +splitFileName[0],
        id: +splitFileName[1],
        filename: splitFileName
          .slice(2)
          .join('_')
          .split('.')
          .slice(0, -1)
          .join('.'),
        internalId: file.id,
      };
    } else {
      dataToInsert = {
        package: '',
        id: '',
        filename: splitFileName.join('_').split('.').slice(0, -1).join('.'),
        internalId: file.id,
      };
    }
    dataToInsert.commentNbr = file.commentNrb;
    return dataToInsert;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();

    if (this.tableDataSource.paginator) {
      this.tableDataSource.paginator.firstPage();
    }
  }
  customSortRule = (item: ProjectFileDisplayed, property) => {
    // with: package + id_beta + filename, we get this :
    //   0126test.txt ("01" + "26" + "test.txt") > 015test.txt ("01" + "5" + "test.txt") -- FALSE
    // to avoid that, we need to write:
    //   0126test.txt ("01" + "26" + "test.txt") > 0105test.txt ("01" + "05" + "test.txt") -- TRUE
    // to do this, we need to use the largest number of id_beta and get his lenght
    let packageStr: string = item.package.toString();
    let idBetaStr: string = item.id.toString();
    for (
      let i = 0;
      i <
      this.packageMaxValue.toString(10).length -
        item.package.toString(10).length;
      i++
    ) {
      packageStr = '0' + packageStr;
    }
    for (
      let i = 0;
      i < this.idMaxValue.toString(10).length - item.id.toString(10).length;
      i++
    ) {
      idBetaStr = '0' + idBetaStr;
    }
    // each sorting method will output a different string, the most important criteria is placed behind the others
    // cases depend on what sort method is selected (arrows near the headers):
    switch (property) {
      case 'package':
        return packageStr + idBetaStr + item.filename.toLowerCase();
      case 'id':
        return idBetaStr + packageStr + item.filename.toLowerCase();
      case 'fileName':
        return item.filename.toLowerCase() + packageStr + idBetaStr;
      default: {
        return item[property];
      }
    }
  }
}
