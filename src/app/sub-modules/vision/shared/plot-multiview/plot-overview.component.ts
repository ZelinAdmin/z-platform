import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CommentsInterfaceService } from '../../services/comments-interface/comments-interface.service';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';
import { CommentService } from '../../services/comment/comment.service';
import Plotly from 'plotly.js-dist';
import { Papa } from 'ngx-papaparse';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { flatMap, map } from 'rxjs/operators';
import { PlotsFormattingService } from '../../services/plots-formatting/plots-formatting.service';
import { PlotlyLine } from '../../interfaces/plotly-line/plotly-line';
import { MatDialog } from '@angular/material/dialog';
import { PlotConfigurationComponent } from '../../dialogs/plot-configuration/plot-configuration.component';
import { PlotConfigurationData } from '../../interfaces/plot-configuration-data/plot-configuration-data';
import { DataHolderService } from 'src/app/services/data-holder/data-holder.service';
import { ElementInsightComponent } from '../../dialogs/element-insight/element-insight.component';
import { InfoPopoverService } from '../../services/info-popover/info-popover.service';
import { FileService } from 'src/app/sub-modules/vision/services/file-service/file.service';

// Each file can be linked to multiple plots (first column is x, then each
// column counts as a new plotly line)
interface MapFileIdToPlotData {
  [fileId: string]: PlotlyLine[];
}

@Component({
  selector: 'app-plot-comments',
  templateUrl: './plot-overview.component.html',
  styleUrls: ['./plot-overview.component.less'],
})
export class PlotOverviewComponent implements OnInit {
  /* abstract service to pass files data to comment section*/
  @Input() commentService: CommentsInterfaceService;
  @ViewChild('card') card: ElementRef;
  public files: ProjectFileDisplayed[] = [];
  public colonWidth = 4;

  plotsEdit: PlotlyLine[][];
  layoutsEdit: Plotly.Layout[];

  plots$: Observable<PlotlyLine[][]> = new Observable<PlotlyLine[][]>();
  layout$: Observable<Plotly.Layout[]> = new Observable<Plotly.Layout[]>();

  // Plotly.Data[] is of the form:
  // `{dimension: [0, 1, 2], dimension2: [1, 2, 3]}`

  plotsCache: MapFileIdToPlotData = {};

  constructor(
    public fileCommentsService: CommentService,
    private fileService: FileManipulationsService,
    private papa: Papa,
    public dialog: MatDialog,
    private plotsFormatting: PlotsFormattingService,
    private dataHolder: DataHolderService,
    private infoPopoverService: InfoPopoverService,
    private filesService: FileService
  ) {}

  ngOnInit(): void {
    this.commentService
      .getFilesObservable()
      .subscribe((files: ProjectFileDisplayed[]) => {
        if (!files) {
          return;
        }
        this.files = files;
        // this.fetchComments(files);

        /* combineLatest to transform Observable<T>[] into Observable<T[]>*/
        this.plots$ = combineLatest(
          this.files.map((file) => this.fetchPlots(file).pipe())
        );
        this.layout$ = combineLatest(
          this.files.map((file) => this.fetchLayout(file).pipe())
        );
        this.layoutsEdit = [];
        this.plotsEdit = [];
        this.layout$.subscribe((layouts) => (this.layoutsEdit = layouts));
        this.plots$.subscribe((plots) => (this.plotsEdit = plots));
      });
  }

  public openElementInsight(
    files: ProjectFileDisplayed[],
    currentIndex: number
  ) {
    const ref = this.infoPopoverService.open({
      origin: null,
      content: ElementInsightComponent,
      data: {
        files,
        index: currentIndex,
        category: 'plot',
      },
    });

    ref.afterClosedObservable.subscribe((_) => {
      /* update information about files in case comments were updated*/
      this.filesService
        .getAllProjectFiles(this.dataHolder.projectId, 'category', 'plot')
        .subscribe((fetchedFiles) => {
          const trunked = fetchedFiles.filter((e) => e.category === 'plot');
          this.files = this.files.map((file) => {
            trunked.forEach((trunk) => {
              if (trunk.id === file.internalId) {
                file.commentNbr = trunk.commentNrb;
              }
            });
            return file;
          });
        });
    });
  }

  public openEditDialog(
    layout: Plotly.Layout,
    file: ProjectFileDisplayed,
    ploty: PlotlyLine[],
    index: number
  ) {
    const data: PlotConfigurationData = {
      layout,
      file,
      plot: ploty,
      card: this.card,
    };
    const dialogRef = this.dialog.open(PlotConfigurationComponent, {
      minWidth: '450px',
      data,
    });

    dialogRef.afterClosed().subscribe((dialogResult: PlotConfigurationData) => {
      if (dialogResult) {
        this.fileService.edit(
          file,
          this.dataHolder.projectId,
          dialogResult.line
        );
        Plotly.react(
          document.getElementById('plot' + index),
          dialogResult.plot,
          dialogResult.layout
        );
      }
    });
  }

  // fetchComments(files: ProjectFileDisplayed[]) {
  //   files.forEach((file) => {
  //     this.fileCommentsService
  //       .getAllComments(this.dataHolder.projectId, file.internalId)
  //       .subscribe((data: FileComment[]) => {
  //         file.comment = data;
  //       });
  //   });
  // }

  private fetchPlots(file: ProjectFileDisplayed): Observable<PlotlyLine[]> {
    return this.fileService
      .download(file.internalId)
      .pipe(map((txt) => this.plotsFormatting.plotFormatting(txt, file)));
  }

  private fetchLayout(file: ProjectFileDisplayed): Observable<Plotly.Layout> {
    return this.fileService
      .download(file.internalId)
      .pipe(map((txt) => this.plotsFormatting.fetchPlotLayout$(txt, file)));
  }

  public reRender(value) {
    this.colonWidth = value;
    setTimeout(async () => {
      let i = 0;
      for (const layout of this.layoutsEdit) {
        layout.width = this.card.nativeElement.offsetWidth;
        layout.height = this.card.nativeElement.offsetParent.clientHeight - 70;
        Plotly.react(
          document.getElementById('plot' + i),
          this.plotsEdit[i],
          layout
        );
        i++;
      }
    }, 60);
  }
}
