import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { TableOptions } from '../../interfaces/table-options/table-options';
import { FileService } from '../../services/file-service/file.service';
import { ProjectFile } from '../../interfaces/project-file/project-file';
import { BehaviorSubject } from 'rxjs';
import { CommentsPlotsService } from '../../services/comments-plots/comments-plots.service';
import { CommentsImagesService } from '../../services/comments-images/comments-images.service';

@Component({
  selector: 'app-main-frame',
  templateUrl: './main-frame.component.html',
  styleUrls: ['./main-frame.component.scss'],
})
export class MainFrameComponent implements OnInit {
  @Input() projectId: string;

  plotsBlinking = false;
  imageBlinking = true;

  tableOptions: TableOptions[] = this.getOptions();
  tcmFile: BehaviorSubject<ProjectFile> = new BehaviorSubject<ProjectFile>(
    null
  );
  plotFiles: BehaviorSubject<ProjectFile[]> = new BehaviorSubject<
    ProjectFile[]
  >(null);
  imageFiles: BehaviorSubject<ProjectFile[]> = new BehaviorSubject<
    ProjectFile[]
  >(null);

  constructor(
    private filesService: FileService,
    public plotsCommentService: CommentsPlotsService,
    public imagesCommentService: CommentsImagesService
  ) {}

  ngOnInit(): void {
    this.fetchProjectFiles();

    this.plotsCommentService.getFilesObservable().subscribe((files) => {
      if (files) {
        this.plotsBlinking = true;
        setTimeout(() => {
          this.plotsBlinking = false;
        }, 4000);
      }
    });

    this.imagesCommentService.getFilesObservable().subscribe((files) => {
      if (files) {
        this.imageBlinking = true;
        setTimeout(() => {
          this.plotsBlinking = false;
        }, 4000);
      }
    });
  }

  /* get list of files associated with the project
   * and filter them according to the categories*/
  private fetchProjectFiles() {
    this.filesService
      .getAllProjectFiles(this.projectId)
      .subscribe((files: ProjectFile[]) => {
        this.tcmFile.next(files.filter((e) => e.category === 'progress')[0]);
        this.plotFiles.next(files.filter((e) => e.category === 'plot'));
        this.imageFiles.next(files.filter((e) => e.category === 'image'));
      });
  }

  getOptions(): TableOptions[] {
    return [
      /* plots*/
      {
        columnsOptions: [
          {
            identifier: 'selection',
            columnName: '',
          },
          {
            identifier: 'package',
            columnName: 'Package',
          },
          {
            identifier: 'id',
            columnName: 'Id',
          },
          {
            identifier: 'comments',
            columnName: 'Nb of comments',
          },
          {
            identifier: 'fileName',
            columnName: 'File Name',
          },
          {
            identifier: 'action',
            columnName: '',
          },
        ],
        category: 'plot',
      },
      /* images*/
      {
        columnsOptions: [
          {
            identifier: 'selection',
            columnName: '',
          },
          {
            identifier: 'package',
            columnName: 'Package',
          },
          {
            identifier: 'id',
            columnName: 'Id',
          },
          {
            identifier: 'comments',
            columnName: 'Nb of comments',
          },
          {
            identifier: 'fileName',
            columnName: 'File Name',
          },
          {
            identifier: 'action',
            columnName: '',
          },
        ],
        category: 'image',
      },
      /* 3d models*/
      {
        columnsOptions: [
          {
            identifier: 'selection',
            columnName: '',
          },
          {
            identifier: 'id',
            columnName: 'Id',
          },
          {
            identifier: 'package',
            columnName: 'Package',
          },
          {
            identifier: 'fileName',
            columnName: 'File Name',
          },
          {
            identifier: 'size',
            columnName: 'Size',
          },
          {
            identifier: 'colormap',
            columnName: 'Colormap',
          },
          {
            identifier: 'action',
            columnName: '',
          },
        ],
      },
    ];
  }
}
