import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataHolderService } from 'src/app/services/data-holder/data-holder.service';
import { MatSort } from '@angular/material/sort';
import { ProjectFileDisplayed } from '../../interfaces/project-file-displayed/project-file-displayed';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import {
  VrmlFile,
  VrmlFileDisplay,
} from 'src/app/interfaces/vrml-file/vrml-file';
import { VisualizerService } from 'src/app/services/visualize/visualizer.service';
import { isNumeric } from 'rxjs/internal-compatibility';
import { Globals } from 'src/app/globals/globals';
import { MatDialog } from '@angular/material/dialog';
import { DialogNotificationService } from 'src/app/services/dialog-notification/dialog-notification.service';
import { DialogDataToInject } from 'src/app/interfaces/dialog-data/dialog-data-to-inject';
import { FileUploadComponent } from 'src/app/shared-components/dialogs/file-upload/file-upload.component';
import { Socket } from 'ngx-socket-io';
import { ConfirmationData } from 'src/app/interfaces/confirmation-data/confirmation-data';
import { ConfirmationComponent } from 'src/app/shared-components/dialogs/confirmation/confirmation.component';
import { HttpHeaderResponse, HttpResponse } from '@angular/common/http';
import { ModelHolderService } from '../../services/model-holder/model-holder.service';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { ProjectService } from 'src/app/services/project/project.service';

@Component({
  selector: 'app-model-selection',
  templateUrl: './model-selection.component.html',
  styleUrls: ['./model-selection.component.less'],
})
export class ModelSelectionComponent implements OnInit {
  displayedColumns: string[] = [
    'selection',
    'package',
    'id',
    'name',
    'size',
    'colormap',
    'actions',
  ];
  tableData: VrmlFileDisplay[] = [];
  tableDataSource: MatTableDataSource<VrmlFileDisplay>;
  idMaxValue: number;
  packageMaxValue: number;
  public modelLoading = false;
  public modelLoadingProgress = 0;
  public currentFile = 0;
  public totalFiles = 0;

  initialSelection = [];
  allowMultipleSelection = true;
  public tableSelection = new SelectionModel<VrmlFileDisplay>(
    this.allowMultipleSelection,
    this.initialSelection
  );

  tableRowExpandable = true;
  viewerExpandable = false;

  userInfo: UserProject = { project: null, role: '', user: null };

  @ViewChild(MatSort, { static: true }) tableSort: MatSort;
  @ViewChild('parent_element') wrap: ElementRef;

  constructor(
    private dataService: DataHolderService,
    private visualizerService: VisualizerService,
    private dialog: MatDialog,
    private dialogNotification: DialogNotificationService,
    private socket: Socket,
    private modelHolder: ModelHolderService,
    public projectService: ProjectService
  ) {}

  ngOnInit(): void {
    /*
    Join socket room for 3d viewer server
    */
    this.joinRoom();

    /*
    Fetch all the 3d models assosiated with the project
    */
    this.fetchModels(this.dataService.projectId);

    /*
    Get user information
    */
    this.projectService
      .getProjectInformation(
        this.dataService.projectId,
        localStorage.getItem(Globals.USER_ID)
      )
      .subscribe((data) => (this.userInfo = data));
  }

  private fetchModels(projectId: string) {
    this.visualizerService
      .getFilesList(projectId)
      .subscribe((files: VrmlFile[]) => this.parseData(files));
  }

  /*
  1) Open upload dialog
  2) Subscribe to visuService which will emit events from socket
  Parse these output accordingly
  */
  public openUploadDialog(event) {
    event.stopPropagation();
    const dialogData: DialogDataToInject = {
      projectId: this.dataService.projectId,
      category: 'model',
      path: '',
    };
    const dialogRef = this.dialog.open(FileUploadComponent, {
      minWidth: '450px',
      data: dialogData,
    });

    this.visualizerService.getState().subscribe((data: VrmlFile) => {
      if (!data) {
        return;
      }
      /* If the conversion started, append file to the table*/
      if (data.status === 'converting') {
        this.tableData.push(this.parseFileName(data));
        this.tableDataSourceSetup();
      }
      /* When the conversion ended, update file's size */
      if (data.status === 'done') {
        const el = this.tableData.filter((e) => e.uuid === data.uuid)[0];
        this.tableData[this.tableData.indexOf(el)].size = data.size;
      }
    });
  }

  /*
  Parse filename into Id, package and filename
  and deduce max id & package
  */
  private parseData(files: VrmlFile[]) {
    if (!files) {
      return;
    }
    this.tableData = files.map((e) => this.parseFileName(e));

    this.idMaxValue = Math.max.apply(
      Math,
      this.tableData.map((elem) => {
        return elem.id;
      })
    );
    this.packageMaxValue = Math.max.apply(
      Math,
      this.tableData.map((elem) => {
        return elem.package;
      })
    );
    this.tableDataSourceSetup();
  }

  // mat table config
  private tableDataSourceSetup() {
    this.tableDataSource = new MatTableDataSource(this.tableData);
    this.tableDataSource.sort = this.tableSort;
    this.tableDataSource.sortingDataAccessor = this.customSortRule;
  }

  private parseFileName(e: VrmlFile): VrmlFileDisplay {
    let packageDef: string | number;
    let id: string | number;
    let filename: string;

    const splitFileName = e.name.split('_');
    if (
      splitFileName.length > 2 &&
      isNumeric(splitFileName[0]) &&
      isNumeric(splitFileName[1])
    ) {
      packageDef = +splitFileName[0];
      id = +splitFileName[1];
      filename = splitFileName
        .slice(2)
        .join('_')
        .split('.')
        .slice(0, -1)
        .join('.');
    } else {
      packageDef = '';
      id = '';
      filename = splitFileName.join('_').split('.').slice(0, -1).join('.');
    }

    return {
      name: filename,
      uuid: e.uuid,
      size: e.size,
      hascolormap: e.hascolormap,
      status: e.status,
      code: e.code,
      projectId: e.projectId,
      modified: e.modified,
      package: packageDef,
      id,
    };
  }

  /*
  Load file:
  First fetch the 'header' file which will contain the general information
  then, download all the chunks, and at the end push them
  to the modelHolder service for 3d viewer
  */
  public loadFile(file: VrmlFileDisplay) {
    if (this.modelLoading) {
      alert('A file is already loading.');
      return;
    }

    const objectFiles = [];
    this.visualizerService
      .fetchFilesInformation(file.uuid, this.dataService.projectId)
      .subscribe((filesArray) => {
        if (filesArray instanceof HttpResponse) {
          this.modelLoading = true;
          this.tableRowExpandable = false;
          this.viewerExpandable = true;
          setTimeout(() => {
            this.wrap.nativeElement.scrollTop = this.wrap.nativeElement.scrollHeight;
          }, 300);
          filesArray.body.files.forEach((element) =>
            this.loadObjectChunks(
              file.uuid,
              element,
              objectFiles,
              filesArray.body.files.length,
              filesArray.body.colormap
            )
          );
        }
      });
  }

  private loadObjectChunks(
    uuid: string,
    fileUUID: string,
    objectFiles: string[],
    objectsCount: number,
    colomap: string
  ) {
    this.visualizerService
      .loadFile(uuid, this.dataService.projectId, fileUUID)
      .subscribe((res: any) => {
        if (res instanceof HttpResponse) {
          objectFiles.push(res.body);
          if (objectFiles.length === objectsCount) {
            this.modelLoading = false;
            if (colomap) {
              /* TODO include and parse colormap*/
              objectFiles.push(colomap);
            }
            this.modelHolder.addModel(objectFiles);
          }
        } else if (!(res instanceof HttpHeaderResponse)) {
          this.progressLoadingAnimate(res, objectFiles.length, objectsCount);
        }
      });
  }

  /*
  Display loading status and calculate the percentage
  */
  private progressLoadingAnimate(
    res: any,
    fileNumber: number,
    totalFiles: number
  ) {
    if (fileNumber && totalFiles) {
      this.currentFile = fileNumber;
      this.totalFiles = totalFiles;
      this.modelLoadingProgress = Math.ceil((res.loaded * 100) / res.total);
    }
  }

  /* element deletion operations*/

  public deleteElement(element: VrmlFileDisplay) {
    const dialogData: ConfirmationData = {
      title: element.name + ' Will be deleted',
      message: 'This step is irreversible. Are you sure you want to proceed?',
    };

    const dialogRef = this.dialog.open(ConfirmationComponent, {
      minWidth: '450px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
      if (dialogResult) {
        this._del(element);
      }
    });
  }

  public deleteMultiple() {
    const dialogData: ConfirmationData = {
      title: 'Do you want to delete these elements?',
      arrayOfFiles: this.tableSelection.selected,
    };

    const dialogRef = this.dialog.open(ConfirmationComponent, {
      minWidth: '450px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
      if (dialogResult) {
        this.tableSelection.selected.forEach((e) => this._del(e));
      }
      this.tableSelection.clear();
    });
  }

  private _del(element: VrmlFileDisplay) {
    this.visualizerService
      .deleteFile(element.uuid, this.dataService.projectId)
      .subscribe(
        (e) => {
          this.tableData = this.tableData.filter((file) => file !== element);
          this.tableDataSourceSetup();
        },
        (error) => console.error(error)
      );
  }

  public formatBytesWrapper(bytes, decimals = 2) {
    return Globals.formatBytes(bytes, decimals);
  }

  private joinRoom() {
    this.socket.emit('auth', localStorage.getItem(Globals.JWT_TOKEN));
    this.socket.emit('joinroom', this.dataService.projectId);
  }

  public checkSizeType(type: string | number): boolean {
    return typeof type === 'number';
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.tableSelection.selected.length;
    const numRows = this.tableDataSource.data.length;
    // if (this.currentPathSplittedArray.length === 1) {
    //   /* exclude 3d model folder*/
    //   return numSelected === (numRows - 1);
    // } else {
    return numSelected === numRows;
    // }
  }

  public toggle(element: VrmlFileDisplay) {
    this.tableSelection.toggle(element);
  }

  masterToggle() {
    this.isAllSelected()
      ? this.tableSelection.clear()
      : this.tableDataSource.data.forEach((row) => {
          // if (row.canDelete || row.canDownload) {
          this.tableSelection.select(row);
          // }
        });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();

    if (this.tableDataSource.paginator) {
      this.tableDataSource.paginator.firstPage();
    }
  }

  customSortRule = (item: VrmlFileDisplay, property) => {
    // with: package + id_beta + filename, we get this :
    //   0126test.txt ("01" + "26" + "test.txt") > 015test.txt ("01" + "5" + "test.txt") -- FALSE
    // to avoid that, we need to write:
    //   0126test.txt ("01" + "26" + "test.txt") > 0105test.txt ("01" + "05" + "test.txt") -- TRUE
    // to do this, we need to use the largest number of id_beta and get his lenght
    let packageStr: string = item.package.toString();
    let idBetaStr: string = item.id.toString();
    for (
      let i = 0;
      i <
      this.packageMaxValue.toString(10).length -
        item.package.toString(10).length;
      i++
    ) {
      packageStr = '0' + packageStr;
    }
    for (
      let i = 0;
      i < this.idMaxValue.toString(10).length - item.id.toString(10).length;
      i++
    ) {
      idBetaStr = '0' + idBetaStr;
    }
    // each sorting method will output a different string, the most important criteria is placed behind the others
    // cases depend on what sort method is selected (arrows near the headers):
    switch (property) {
      case 'package':
        return packageStr + idBetaStr + item.name.toLowerCase();
      case 'id':
        return idBetaStr + packageStr + item.name.toLowerCase();
      case 'fileName':
        return item.name.toLowerCase() + packageStr + idBetaStr;
      default: {
        return item[property];
      }
    }
  }
}
