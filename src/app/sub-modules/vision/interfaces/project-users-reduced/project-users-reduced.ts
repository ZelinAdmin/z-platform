import { UniqueSelectionDispatcher } from '@angular/cdk/collections';

export interface ProjectUsersReduced {
  role: string;
  user: {
    email: string;
    firstName: string;
    lastName: string;
    expertId?: number | string;
    phone: number;
    id?: number | string;
    image?: number | string;
    clientId?: string;
  };
}
