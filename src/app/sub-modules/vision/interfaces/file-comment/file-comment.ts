import { UserInformation } from 'src/app/interfaces/user-information/user-information';

export interface FileComment {
  content: string;
  createdAt?: Date;
  fileId: string;
  id?: string;
  projectId: string;
  updatedAt?: Date;
  user: UserInformation;
}
