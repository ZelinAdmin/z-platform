export interface ColumnOption {
  identifier: string;
  columnName: string;
}

export interface TableOptions {
  columnsOptions: ColumnOption[];
  category?: string;
}
