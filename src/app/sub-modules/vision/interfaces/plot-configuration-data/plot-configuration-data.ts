import { Plotly } from 'angular-plotly.js/src/app/shared/plotly.interface';
import { ProjectFileDisplayed } from '../project-file-displayed/project-file-displayed';
import { PlotlyLine } from '../plotly-line/plotly-line';
import { ElementRef } from '@angular/core';

export interface PlotConfigurationData {
  layout: Plotly.Layout;
  file: ProjectFileDisplayed;
  plot: PlotlyLine[];
  card: ElementRef;
  line?: string;
}
