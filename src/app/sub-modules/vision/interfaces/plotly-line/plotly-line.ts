export interface PlotlyLine {
  x: number[];
  y: number[];
  name?: string;
}
