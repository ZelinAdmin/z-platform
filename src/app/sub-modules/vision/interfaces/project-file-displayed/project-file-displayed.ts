import { FileComment } from '../file-comment/file-comment';

export interface ProjectFileDisplayed {
  package: number | string;
  id: number | string;
  filename: string;
  internalId: string;
  comment?: FileComment[];
  commentNbr?: number;
}
