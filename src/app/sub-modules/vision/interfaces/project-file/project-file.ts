export interface ProjectFile {
  category: string;
  filename: string;
  id: string;
  subtype: string;
  type: string;
  commentNrb: number;
}
