import { Component, Inject, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ErrorStateMatcher } from '@angular/material/core';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { DialogDataToInject } from 'src/app/interfaces/dialog-data/dialog-data-to-inject';
import { DialogNotificationService } from 'src/app/services/dialog-notification/dialog-notification.service';

@Component({
  selector: 'app-folder-creation',
  templateUrl: './folder-creation.component.html',
  styleUrls: ['./folder-creation.component.less'],
})
export class FolderCreationComponent {
  public folderName: string;
  folderControl = new FormControl('', [Validators.required]);
  public errorState = false;
  public errorMessage: '';

  matcher = new FolderNameErrorStateMatcher();

  constructor(
    public dialogRef: MatDialogRef<FolderCreationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogDataToInject,
    public driverService: FileManipulationsService,
    private dialogNotification: DialogNotificationService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  createFolder(): void {
    this.folderName = this.folderControl.value;
    if (!this.folderName) {
      this.onNoClick();
      return;
    }
    this.driverService.createFolder(this.data.path, this.folderName).subscribe(
      (result) => {
        this.dialogRef.close();
        this.dialogNotification.triggerState(true);
      },
      (error) => {
        this.errorState = true;
        this.errorMessage = error;
        console.error(error);
      }
    );
    this.dialogRef.close();
  }
}

class FolderNameErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
