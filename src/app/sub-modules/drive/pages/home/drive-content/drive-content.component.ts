import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import * as fileSaver from 'file-saver';

import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';

import { speedDialFabAnimations } from './drive-content-fab.animations';
import { SelectionModel } from '@angular/cdk/collections';
import { isNumeric } from 'rxjs/internal-compatibility';
import { Globals } from 'src/app/globals/globals';
import {
  DcProject,
  DriveFile,
} from 'src/app/interfaces/drive-project-info/dc-project';
import { ProjectService } from 'src/app/services/project/project.service';
import { VisualizerService } from 'src/app/services/visualize/visualizer.service';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { FileUploadComponent } from 'src/app/shared-components/dialogs/file-upload/file-upload.component';
import { ProjectInfoSmall } from '../../../interfaces/projectinfo-small/project-info-small';
import { FolderCreationComponent } from '../../../dialogs/folder-creation/folder-creation.component';
import { DialogNotificationService } from 'src/app/services/dialog-notification/dialog-notification.service';
import { ProjectUser } from '../../../interfaces/project-user/project-user';
import { VrmlFile } from 'src/app/interfaces/vrml-file/vrml-file';
import { ConfirmationComponent } from 'src/app/shared-components/dialogs/confirmation/confirmation.component';
import { ConfirmationData } from 'src/app/interfaces/confirmation-data/confirmation-data';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { Project } from 'src/app/interfaces/project/project';

export interface Fabtemplate {
  icon: string;
  tooltip: string;
  openDialog: () => any;
}

@Component({
  selector: 'app-drive-content',
  templateUrl: './drive-content.component.html',
  styleUrls: ['./drive-content.component.scss'],
  animations: speedDialFabAnimations,
})
export class DriveContentComponent implements OnInit {
  //      CONSTRUCTOR
  constructor(
    private driveService: FileManipulationsService,
    private ngZone: NgZone,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public dialogNotification: DialogNotificationService,
    public visualizerService: VisualizerService,
    public projectService: ProjectService
  ) {}
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  //      DECLARATION OF VARIABLES
  driveFiles: DriveFile[];
  // save essential information such as category (PLOTS, PROGRESS OR IMAGES), proejct ID (UUID) and full path
  public currentCategory: string;
  private currentPath: string;
  private savedProjectId;
  public currentPathSplittedArray: string[];
  public userRole;
  private NAME_SPLITTER = '_';

  //  TABLE CONFIG
  public tableDataSource: MatTableDataSource<DriveFile>;
  private projectList: Array<Project> = [];
  initialSelection = [];
  allowMultipleSelection = true;
  public selection = new SelectionModel<DriveFile>(
    this.allowMultipleSelection,
    this.initialSelection
  );
  public displayedColumns: string[] = [
    'icon',
    'name',
    'package',
    'id_beta',
    'lastModified',
    'size',
    'type',
    'commands',
  ];
  packageMaxValue: number;
  idBetaMaxValue: number;

  // SPEED DIAL BUTTONS CONFIGURATION
  public fabButtons: Fabtemplate[] = [
    {
      icon: 'create_new_folder',
      tooltip: 'Create a folder',
      openDialog: () => {
        const dialogRef = this.dialog.open(FolderCreationComponent, {
          minWidth: '500px',
          data: {
            projectId: this.savedProjectId,
            category: this.currentCategory,
            path: this.currentPath,
          },
        });

        dialogRef.afterClosed().subscribe((result) => {});
      },
    },
    {
      icon: 'note_add',
      tooltip: 'Upload a file',
      openDialog: () => {
        const dialogRef = this.dialog.open(FileUploadComponent, {
          minWidth: '500px',
          data: {
            projectId: this.savedProjectId,
            category: this.currentCategory,
            path: this.currentPath,
          },
        });

        dialogRef.afterClosed().subscribe((result) => {});
      },
    },
  ];
  public buttons: Fabtemplate[] = [];
  public fabTogglerState = 'inactive';
  public allowedToShowFab = false;

  // BOOLEAN TO CONTROL LOADER
  public loadingFiles = false;

  /* Boolean for confirmation dialog status
   * as such, this will prevent from triggering onRowClick event
   * because download/delete events will resolve first*/
  private confirmationDialogOpenStatus = false;

  public formatBytes(bytes, decimals = 2): string {
    if (bytes === 0) {
      return '0 Bytes';
    }

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  ngOnInit(): void {
    // parse the url
    this.route.url.subscribe((segments: UrlSegment[]) => {
      this.selection.clear();
      this.hideSpeedDialItems();
      this.allowedToShowFab = segments.length > 1;
      this.currentPath = segments.join('/');
      this.setCurrentPath(this.currentPath);
      if (
        segments[segments.length - 1] &&
        segments[segments.length - 1].path === '04_3D_MODELS'
      ) {
        this.fetch3dModels();
        this.allowedToShowFab = false;
      } else {
        this.fetchFilesFromDirectoryFromServer();
      }

      if (segments.length === 1) {
        // get role
        this.getUserRole();
      }
    });
    // subscribe to dialog
    this.dialogNotification.getTrigger().subscribe((status) => {
      if (status) {
        this.fetchFilesFromDirectoryFromServer();
      }
    });
  }

  private getUserRole() {
    this.projectService
      .getRole(this.savedProjectId, localStorage.getItem(Globals.USER_ID))
      .subscribe((information: ProjectUser) => {
        this.userRole = information.role;
        this.checkIfUserHasPermissions();
      });
  }

  private setCurrentPath(newPath: string) {
    this.currentPath = newPath;
    if (this.currentPath === '/') {
      this.currentPathSplittedArray = [];
    } else {
      this.currentPathSplittedArray = this.currentPath.split('/');
      /* replace UUID by the actual project name*/
      if (this.currentPathSplittedArray[0] !== '') {
        this.projectService
          .getUserProjects(localStorage.getItem(Globals.USER_ID))
          .subscribe((projects: UserProject[]) => {
            this.projectList = projects.map((e) => e.project);
            this.savedProjectId = this.currentPathSplittedArray[0];
            if (this.projectList) {
              this.currentPathSplittedArray[0] = this.projectList.find(
                (element) => element.id === this.currentPathSplittedArray[0]
              ).name;
            }
          });
      }
    }
    if (this.currentPath === '') {
      this.currentPath = '/';
    }
  }

  private fetch3dModels() {
    this.visualizerService.getFilesList(this.savedProjectId).subscribe(
      (files: VrmlFile[]) => {
        this.driveFiles = [];
        if (files) {
          files.forEach((file) => {
            const variable: DriveFile = {
              size:
                typeof file.size === 'string'
                  ? parseFloat(file.size)
                  : file.size,
              name: file.name,
              type: 'file',
              modified: file.modified,
              displayName: file.name,
              canDelete: true,
              id: file.uuid,
            };
            this.driveFiles.push(variable);
          });
        }
        this.parsePackageId();
        // force refresh rendering
        this.formatFileList();
        this.tableSourceSetup();
        // force refresh
        this.loadingFiles = false;
        this.ngZone.run(() => {});
      },
      (error) => {}
    );
  }

  private fetchFilesFromDirectoryFromServer() {
    this.tableDataSource = null;
    this.loadingFiles = true;
    /* insert projects*/
    this.driveService
      .getFilesFromPath(
        this.currentPath === '/' ? '' : this.currentPath,
        localStorage.getItem(Globals.USER_ID)
      )
      .subscribe((data: DcProject) => {
        this.driveFiles = data.files;
        this.currentCategory = data.category;
        this.loadingFiles = false;
        if (this.driveFiles !== null) {
          this.parsePackageId();
          this.formatFileList();
          this.tableSourceSetup();
          // force refresh rendering
          this.loadingFiles = false;
          this.checkIfUserHasPermissions();
          this.ngZone.run(() => {});
        }
      });
  }

  private parsePackageId() {
    this.driveFiles.forEach((file) => {
      const splitName = file.name.split(this.NAME_SPLITTER);
      if (
        splitName.length > 2 &&
        isNumeric(splitName[0]) &&
        isNumeric(splitName[1])
      ) {
        file.package = +splitName[0];
        file.idBeta = +splitName[1];
        file.displayName = file.name
          .split(this.NAME_SPLITTER)
          .slice(2)
          .join(this.NAME_SPLITTER);
      } else {
        file.package = '';
        file.idBeta = '';
        file.displayName = file.name;
      }
    });
    this.idBetaMaxValue = Math.max.apply(
      Math,
      this.driveFiles.map((e) => {
        if (e.idBeta) {
          return e.idBeta;
        }
      })
    );
    this.packageMaxValue = Math.max.apply(
      Math,
      this.driveFiles.map((e) => {
        if (e.package) {
          return e.package;
        }
      })
    );
  }

  private tableSourceSetup() {
    this.tableDataSource = new MatTableDataSource(this.driveFiles);
    this.tableDataSource.sort = this.sort;
    this.tableDataSource.sortingDataAccessor = (item: DriveFile, property) => {
      // with: package + id_beta + filename, we get this :
      //   0126test.txt ("01" + "26" + "test.txt") > 015test.txt ("01" + "5" + "test.txt") -- FALSE
      // to avoid that, we need to write:
      //   0126test.txt ("01" + "26" + "test.txt") > 0105test.txt ("01" + "05" + "test.txt") -- TRUE
      // to do this, we need to use the largest number of id_beta and get his lenght
      let packageStr: string = item.package.toString();
      let idBetaStr: string = item.idBeta.toString();
      for (
        let i = 0;
        i <
        this.packageMaxValue.toString(10).length -
          item.package.toString(10).length;
        i++
      ) {
        packageStr = '0' + packageStr;
      }
      for (
        let i = 0;
        i <
        this.idBetaMaxValue.toString(10).length -
          item.idBeta.toString(10).length;
        i++
      ) {
        idBetaStr = '0' + idBetaStr;
      }
      // each sorting method will output a different string, the most important criteria is placed behind the others
      // cases depend on what sort method is selected (arrows near the headers):
      switch (property) {
        case 'package':
          return packageStr + idBetaStr + item.displayName.toLowerCase();
        case 'id_beta':
          return idBetaStr + packageStr + item.displayName.toLowerCase();
        case 'name':
          return item.displayName.toLowerCase() + packageStr + idBetaStr;
        case 'lastModified':
          return new Date(item.modified);
        default: {
          return item[property];
        }
      }
    };
  }

  private formatFileList() {
    if (this.driveFiles !== null) {
      this.driveFiles.forEach((element) => {
        const splitted = element.name.split('/');
        element.name = splitted[splitted.length - 1];
      });
    }
  }

  // Navigation in table
  public onRowClick(event: DriveFile) {
    if (
      event.type === Globals.DIRECTORY &&
      !this.confirmationDialogOpenStatus
    ) {
      this.router.navigate(['/drive/' + event.path]);
    } else if (event.type === Globals.FILE) {
      // File managment
    } else {
      /* NaN */
      // unknown
    }
  }

  public onBackArrowInBreadcrumbClick() {
    if (this.currentPath !== '/') {
      this.router.navigate([
        '/drive/' +
          this.currentPath.substr(0, this.currentPath.lastIndexOf('/')),
      ]);
    }
  }

  public goToStepInBreadcrumb(step: string) {
    let newPath: string;
    if (step === '/') {
      newPath = step;
    } else {
      if (
        this.projectList.find((element) => element.id === this.savedProjectId)
          .name === step
      ) {
        newPath =
          this.currentPath.split(this.savedProjectId)[0] + this.savedProjectId;
      } else {
        newPath = this.currentPath.split(step)[0] + step;
      }
    }
    this.router.navigate(['/drive/' + newPath]);
  }

  // single file del and download
  public deleteElement(element: DriveFile) {
    this.confirmationDialogOpenStatus = true;
    const dialogData: ConfirmationData = {
      title: 'Deletion of ' + element.name,
      message: 'This step is irreversible. Are you sure you want to proceed?',
    };

    const dialogRef = this.dialog.open(ConfirmationComponent, {
      minWidth: '450px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
      this.confirmationDialogOpenStatus = false;
      if (dialogResult) {
        /* remove the element from multiselection*/
        this.selection.clear();
        if (this.currentPathSplittedArray.includes('04_3D_MODELS')) {
          this.visualizerService
            .deleteFile(element.id, this.savedProjectId)
            .subscribe(
              (result) => this.fetch3dModels(),
              (error) => {
                console.error(error);
              }
            );
        } else if (!this.currentCategory) {
          this.delWithoutCategory(element);
        } else {
          this.delWithinCategory(element);
        }
      }
    });
  }

  public downloadElement(element: DriveFile) {
    this.confirmationDialogOpenStatus = true;
    this.driveService.downloadFile(element.path).subscribe((response) => {
      const filename = response.headers.get('filename');
      fileSaver.saveAs(response.body, filename);
    });
    setTimeout(() => {
      this.confirmationDialogOpenStatus = false;
    }, 100);
  }

  // multi file del and download

  deleteMultiple() {
    this.confirmationDialogOpenStatus = true;
    const dialogData: ConfirmationData = {
      title: 'Do you want to delete these elements?',
      arrayOfFiles: this.selection.selected,
    };

    const dialogRef = this.dialog.open(ConfirmationComponent, {
      minWidth: '450px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
      this.confirmationDialogOpenStatus = false;

      if (dialogResult) {
        if (!this.currentCategory) {
          this.selection.selected.forEach((element) =>
            this.delWithoutCategory(element)
          );
        } else {
          this.selection.selected.forEach((element) =>
            this.delWithinCategory(element)
          );
        }
      }
      this.selection.clear();
    });
  }

  downloadMultiple() {
    this.confirmationDialogOpenStatus = true;
    this.selection.selected.forEach((file) => {
      this.driveService.downloadFile(file.path).subscribe((response) => {
        const filename = response.headers.get('filename');
        fileSaver.saveAs(response.body, filename);
      });
    });
    this.selection.clear();
    setTimeout(() => {
      this.confirmationDialogOpenStatus = false;
    }, 100);
  }

  // Function to apply search in array
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
  }

  /* speed dial management section*/
  private showSpeedDialItems() {
    this.fabTogglerState = 'active';
    if (this.currentCategory) {
      this.buttons.push(this.fabButtons[1]);
    } else {
      this.buttons = this.fabButtons;
    }
  }

  private hideSpeedDialItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  public onFABPress() {
    this.buttons.length ? this.hideSpeedDialItems() : this.showSpeedDialItems();
  }
  /*end of speed dial section*/

  private checkIfUserHasPermissions() {
    if (!this.userRole && this.savedProjectId) {
      this.getUserRole();
      return;
    }
    if (this.currentCategory && this.userRole !== 'MANAGER') {
      this.allowedToShowFab = false;
    }
  }

  delWithoutCategory(element) {
    this.driveService.deleteFile(element.path).subscribe(
      (response) => {
        this.fetchFilesFromDirectoryFromServer();
      },
      (error) => {}
    );
  }

  delWithinCategory(element) {
    this.driveService
      .deleteFilesWithinCategory(this.savedProjectId, element.id)
      .subscribe(
        (response) => this.fetchFilesFromDirectoryFromServer(),
        (error) => {
          this.delWithoutCategory(element);
        }
      );
  }

  multiSelectionCanDelete() {
    return (
      !this.selection.isEmpty() &&
      this.selection.selected.every((e) => e.canDelete)
    );
  }

  multiSelectionCanDownload() {
    return (
      !this.selection.isEmpty() &&
      this.selection.selected.every((e) => e.canDownload)
    );
  }

  public getExtensionOfAFile(element: DriveFile): string {
    return element.name.split('.').length >= 2
      ? element.name.split('.').pop().toLowerCase()
      : element.type;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.tableDataSource.data.length;
    if (this.currentPathSplittedArray.length === 1) {
      /* exclude 3d model folder*/
      return numSelected === numRows - 1;
    } else {
      return numSelected === numRows;
    }
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.tableDataSource.data.forEach((row) => {
          if (row.canDelete || row.canDownload) {
            this.selection.select(row);
          }
        });
  }
}
