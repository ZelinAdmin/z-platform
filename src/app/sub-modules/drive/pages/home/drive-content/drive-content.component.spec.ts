import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriveContentComponent } from './drive-content.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgZone } from '@angular/core';

describe('DriveContentComponent', () => {
  // let component: DriveContentComponent;
  // let fixture: ComponentFixture<DriveContentComponent>;
  //
  // const driverServiceSpy = {
  //   getFilesFromPath: () => ({ subscribe: () => {} }),
  //   deleteFile: () => ({ subscribe: () => {} }),
  //   deleteFilesWithinCategory: () => ({ subscribe: () => {} }),
  //   downloadFile: () => ({ subscribe: () => {} }),
  // };
  // const ngZoneSpy = jasmine.createSpyObj('NgZone', ['run']);
  // const route = {
  //   url: {
  //     subscribe: () => {},
  //   },
  // };
  // const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  //
  // const dialogNotifcationSpy = {
  //   getTrigger: () => ({ subscribe: () => {} }),
  // };
  //
  // beforeEach(async(() => {
  //   TestBed.configureTestingModule({
  //     declarations: [DriveContentComponent],
  //     providers: [
  //       { provide: DriveService, useValue: driverServiceSpy },
  //       { provide: ActivatedRoute, useValue: route },
  //       { provide: Router, useValue: routerSpy },
  //       { provide: NgZone, useValue: ngZoneSpy },
  //       { provide: NotifyFileRefreshService, useValue: dialogNotifcationSpy },
  //     ],
  //     imports: [
  //       MatGridListModule,
  //       MatCardModule,
  //       MatIconModule,
  //       MatFormFieldModule,
  //       MatInputModule,
  //       BrowserAnimationsModule,
  //       HttpClientTestingModule,
  //       ReactiveFormsModule,
  //       MatTableModule,
  //       MatProgressSpinnerModule,
  //       MatTooltipModule,
  //     ],
  //   }).compileComponents();
  // }));
  //
  // beforeEach(() => {
  //   fixture = TestBed.createComponent(DriveContentComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });
});
