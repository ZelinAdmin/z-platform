import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from 'src/app/globals/globals';
import {
  DcProject,
  DriveFile,
} from 'src/app/interfaces/drive-project-info/dc-project';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';

@Component({
  selector: 'app-drive-menu',
  templateUrl: './drive-menu.component.html',
  styleUrls: ['./drive-menu.component.scss'],
})
export class DriveMenuComponent implements OnInit {
  projects: DriveFile[] = [];
  selectedMenuItem = 'myzdrive';

  constructor(
    private drive: FileManipulationsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.drive
      .getFilesFromPath('', localStorage.getItem(Globals.USER_ID))
      .subscribe((data: DcProject) => {
        this.projects = data.files;
      });
  }

  onRowClick(project: DriveFile) {
    this.router.navigate(['/drive/' + `${project.path}`]);
  }
}
