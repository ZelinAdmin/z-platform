import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import {
  DcProject,
  DriveFile,
} from 'src/app/interfaces/drive-project-info/dc-project';
import { Globals } from 'src/app/globals/globals';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  public projects: Array<DriveFile> = new Array<DriveFile>();
  private currentPath: string;
  tableDataSource: MatTableDataSource<DcProject>;

  constructor(
    private drive: FileManipulationsService,
    private router: Router,
    private route: ActivatedRoute,
    private ngZone: NgZone
  ) {}

  ngOnInit() {
    this.route.url.subscribe((segments) => {
      this.currentPath = segments.toString().replace(',', '/');
      // this.populateProjectArray();
    });
  }

  ngOnDestroy(): void {
    // this.owncloud.cleanProjectListSubject();
  }

  public afterOwncloudInit() {
    /* fetch projects*/
    // try {
    //   this.owncloud.projectsInformation();
    // } catch (e) {
    //   /*project info threw an error*/
    // }
  }

  public projectChoice(project: DcProject) {
    // this.router.navigate(['/project/' + project.projectName]);
  }

  test(row) {}

  populateProjectArray() {
    /* insert projects*/
    this.drive
      .getFilesFromPath(this.currentPath, localStorage.getItem(Globals.USER_ID))
      .subscribe((data: DcProject) => {
        this.projects = data.files;
      });
  }
}
