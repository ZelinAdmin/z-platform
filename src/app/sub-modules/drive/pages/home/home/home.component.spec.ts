import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DriveMenuComponent } from '../drive-menu/drive-menu.component';
import { DriveContentComponent } from '../drive-content/drive-content.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';

describe('HomeComponent', () => {
  // let component: HomeComponent;
  // let fixture: ComponentFixture<HomeComponent>;
  //
  // /*spy objects to provide for constructor*/
  // const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  // const driverServiceSpy = jasmine.createSpyObj('DriveService', [
  //   'getFilesFromPath',
  // ]);
  //
  // beforeEach(async(() => {
  //   TestBed.configureTestingModule({
  //     declarations: [HomeComponent, DriveMenuComponent, DriveContentComponent],
  //     providers: [
  //       { provide: Router, useValue: routerSpy },
  //       { provide: DriveService, useValue: driverServiceSpy },
  //     ],
  //     imports: [
  //       MatGridListModule,
  //       MatCardModule,
  //       MatIconModule,
  //       MatListModule,
  //       MatDividerModule,
  //       MatFormFieldModule,
  //       MatInputModule,
  //       BrowserAnimationsModule,
  //       HttpClientTestingModule,
  //       ReactiveFormsModule,
  //       MatTableModule,
  //       MatTooltipModule,
  //       MatProgressSpinnerModule,
  //     ],
  //   }).compileComponents();
  // }));
  //
  // beforeEach(() => {
  //   fixture = TestBed.createComponent(HomeComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });
});
