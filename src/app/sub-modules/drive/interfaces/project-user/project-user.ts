import { Project } from 'src/app/interfaces/project/project';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';

export interface ProjectUser {
  role: string;
  project: Project;
  user: UserInformation;
}
