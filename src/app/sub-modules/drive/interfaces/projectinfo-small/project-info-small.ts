export interface ProjectInfoSmall {
  projectId: string;
  name: string;
  role: string;
}
