import { Sort } from '@angular/material/sort';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ENTER, COMMA } from '@angular/cdk/keycodes';

import {
  ComponentsTrslt,
  TranslationService,
} from 'src/app/services/translation/translation.service';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { ExpertService } from 'src/app/services/expert/expert.service';
import { ExpertiseService } from '../profile/services/expertise/expertise.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { Globals } from '../../globals/globals';
import {
  MatAutocompleteSelectedEvent,
  MatAutocomplete,
} from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { ReferenceArea } from 'src/app/interfaces/reference-area/reference-area';
import { ExpertisesEnums } from '../profile/interfaces/expertises/expertises.enum';
import { SoftwareService } from '../profile/services/software/software.service';
import { LanguageService } from 'src/app/services/language/language.service';
import { environment } from 'src/environments/environment';
import { ExpertUserInfo } from 'src/app/interfaces/expert-user-info/expert-user-info';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-expert',
  templateUrl: './expert.component.html',
  styleUrls: ['./expert.component.scss'],
})
export class ExpertComponent implements OnInit {
  public hasCommunityAccess = false; // Boolean to check if user has access to community features
  public isPremium = false; // Boolean to check if user is Premium.

  isLoaded = false;
  isEmpty = true;

  emailString = 'mailto:hello@zelin.io';

  allExperts: ExpertUserInfo[] = [];
  experts: ExpertUserInfo[] = [];
  existingAreas: ReferenceArea[];
  existingSoftwares: any[];
  public languages = [];
  public softwares = [];
  public translatePage: ComponentsTrslt = ComponentsTrslt.BROWSE_EXPERT;
  public languagePage: ComponentsTrslt = ComponentsTrslt.LANGUAGE;
  filtersForm: FormGroup;
  public expertises = ExpertisesEnums;

  // Array to display selected softwares in filters
  public currentSoftwares: string[] = [];
  softwareCtrl = new FormControl();
  displayedSoft: string[] = [];
  separatorKeysCodes: number[] = [ENTER, COMMA];
  maxPrice = 0;

  private softInput: ElementRef<HTMLInputElement>;
  @ViewChild('softInput', { static: false }) set content(
    content: ElementRef<HTMLInputElement>
  ) {
    if (content) {
      // initially setter gets called with undefined
      this.softInput = content;
    }
  }
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

  constructor(
    public translateService: TranslationService,
    private expertService: ExpertService,
    public expertisesService: ExpertiseService,
    public softwareService: SoftwareService,
    public languageService: LanguageService,
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder,
    public auth: AuthService
  ) {}

  ngOnInit() {
    this.getPremiumStatus(); // Get user Premium / admin status

    // Get all languages from back.
    this.languageService.getAllLanguages().subscribe((res) => {
      this.languages = res.map((e) => e.name);
    });

    // Get all softwares from back.
    this.softwareService.getAllSoftwares().subscribe((res) => {
      this.softwares = res.map((e) => e.name);
    });

    // Building filters form
    this.filtersForm = this.formBuilder.group({
      firstName: null,
      maximumPrice: 50,
      minimumRating: null,
      fluids: null,
      thermic: null,
      struct: null,
      electro: null,
      aerospace: null,
      transport: null,
      energy: null,
      industries: null,
      isAvailable: null,
      softwares: null,
      languages: null,
    });
    this.fetchAndMapExperts();
  }

  getPremiumStatus() {
    // Check if user is either Admin or Premium, and therefore if they have access to all the Expert profiles.
    // We only use this to display a message though, since the backend already handles the number of experts a non-premium
    // user can see.
    this.auth.isLoggedIn.subscribe((state) => {
      if (!state) {
        return;
      }
      if (state) {
        this.userService
          .getPartial(localStorage.getItem(Globals.USER_ID))
          .subscribe((data) => {
            if (!data) {
              return;
            }
            if (data.hasCommunityAccess !== false) {
              this.hasCommunityAccess = true;
            }
            if (data.isAdmin !== false || data.isPremiumCommunity !== false) {
              this.isPremium = true;
            }
          });
      }
    });
  }

  onSubmitFilters(filters) {
    this.isLoaded = false;
    this.filterExperts(filters.value);
  }

  hasSoftwares(element) {
    if (this.currentSoftwares.length === 0) {
      return true;
    }
    const expertSoft = element.softwares.map((s) => s.name);
    for (const i of this.currentSoftwares) {
      if (!expertSoft.includes(this.currentSoftwares[i])) {
        return false;
      }
    }
    return true;
  }

  hasLanguages(element) {
    if (this.filtersForm.controls.language.value.length === 0) {
      return true;
    }
    if (!element.languages) {
      return false;
    }
    const expertLang = element.languages.map((s) => s.name);
    for (const i of this.filtersForm.controls.language.value) {
      if (!expertLang.includes(this.filtersForm.controls.language.value[i])) {
        return false;
      }
    }
    return true;
  }

  public delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async fetchAndMapExperts(params?) {
    this.isLoaded = false;
    this.allExperts = [];
    this.experts = [];

    // If we receive params from the form (If user clicked submit)
    if (params) {
      this.expertService.getFiltered(params).subscribe((data) => {
        // Get data from expert table
        data.forEach((element) => {
          const e: ExpertUserInfo = new ExpertUserInfo();
          e.expertId = element.id;
          e.userId = element.userId;
          e.rating = element.rating;
          e.title = element.title;
          e.wage = element.wage;
          e.available = element.availability;
          e.expertises = element.expertises;
          e.areas = element.areas;
          e.softwares = element.softwares;
          // Get data from user table
          this.userService.getPartial(element.userId).subscribe((user) => {
            e.email = user.email;
            e.firstName = user.firstName;
            e.lastName = user.lastName;
            e.phone = user.phone;
            e.company = user.company;
            e.address = user.address;
            e.zipCode = user.zipCode;
            e.city = user.city;
            e.country = user.country;
            e.citizenship = user.citizenship;
            e.isValidated = user.isValidated;
            e.languages = user.languagesRef;
            if (user.image != null) {
              e.photoUrl = `${environment.apiUrl}files/${user.image}/download`;
            } else {
              e.photoUrl = `${environment.apiUrl}files/6a2ec182-504f-4e8e-badd-b5f79365245c/download`;
            }
            if (e.isValidated) {
              this.allExperts.push(e);
              this.experts.push(e);
              if (this.maxPrice < e.wage) {
                this.getMaxWage();
              }
            }
          });
        });
      });

      // If we don't receive data from the form (When loading the page for the first time)
    } else {
      this.expertService.getFiltered().subscribe((data) => {
        // Get data from expert table
        data.forEach((element) => {
          const e: ExpertUserInfo = new ExpertUserInfo();
          e.expertId = element.id;
          e.userId = element.userId;
          e.rating = element.rating;
          e.title = element.title;
          e.wage = element.wage;
          e.available = element.availability;
          e.expertises = element.expertises;
          e.areas = element.areas;
          e.softwares = element.softwares;
          // Get data from user table
          this.userService.getPartial(element.userId).subscribe((user) => {
            e.email = user.email;
            e.firstName = user.firstName;
            e.lastName = user.lastName;
            e.phone = user.phone;
            e.company = user.company;
            e.address = user.address;
            e.zipCode = user.zipCode;
            e.city = user.city;
            e.country = user.country;
            e.citizenship = user.citizenship;
            e.isValidated = user.isValidated;
            e.languages = user.languagesRef;
            if (user.image != null) {
              e.photoUrl = `${environment.apiUrl}files/${user.image}/download`;
            } else {
              e.photoUrl = 'assets/fallBackUser.png';
            }
            if (e.isValidated) {
              this.allExperts.push(e);
              this.experts.push(e);
              if (this.maxPrice < e.wage) {
                this.getMaxWage();
                this.filtersForm.controls.maximumPrice.setValue(this.maxPrice);
              }
            }
          });
          this.experts.sort((expert1, expert2) => {
            if (expert1.rating < expert2.rating) {
              return -1;
            }
            if (expert1.rating > expert2.rating) {
              return 1;
            }
            return 0;
          });
        });
      });
    }
    await this.delay(2000);
    this.isLoaded = true;
  }

  filterExperts(filters) {
    this.isLoaded = false;
    this.experts = [];
    this.fetchAndMapExperts(filters);
  }

  capitalizeString(str: string) {
    str = str.toLowerCase();
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our software
    if ((value || '').trim()) {
      this.currentSoftwares.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.softwareCtrl.setValue(null);
    this.noSoftDuplicate();
  }

  remove(software: string): void {
    const index = this.currentSoftwares.indexOf(software);
    if (index >= 0) {
      this.currentSoftwares.splice(index, 1);
    }
    this.noSoftDuplicate();
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.currentSoftwares.push(event.option.viewValue);
    this.softInput.nativeElement.value = '';
    this.softwareCtrl.setValue(null);
    this.noSoftDuplicate();
  }

  private _filter(value: string): string[] {
    return this.existingSoftwares.filter(
      (software) => software.toLowerCase().indexOf(value.toLowerCase()) === 0
    );
  }

  public noSoftDuplicate() {
    this.displayedSoft = [];
    for (const cpt of this.existingSoftwares) {
      if (
        this.currentSoftwares.find((e) => e === this.existingSoftwares[cpt]) ===
        undefined
      ) {
        this.displayedSoft.push(this.existingSoftwares[cpt]);
      }
    }
  }

  public navigateToExpert(userId) {
    this.router.navigate(['profile/' + userId]);
  }

  public resetFilters() {
    this.filtersForm.setValue({
      firstName: null,
      maximumPrice: 50,
      minimumRating: null,
      fluids: null,
      thermic: null,
      struct: null,
      electro: null,
      aerospace: null,
      transport: null,
      energy: null,
      industries: null,
      isAvailable: null,
      softwares: null,
      languages: null,
    });
  }

  public getMaxWage() {
    this.expertService.getFiltered().subscribe((data) => {
      // Sort the experts array and return highest wage.
      data.sort((a, b) => (a.wage > b.wage ? 1 : -1));
      this.maxPrice = data[data.length - 1].wage;
      this.filtersForm.controls.maximumPrice.patchValue(this.maxPrice);
    });
  }
}
