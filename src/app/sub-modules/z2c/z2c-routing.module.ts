import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Z2cComponent } from './z2c.component';

const routes: Routes = [{ path: '', component: Z2cComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Z2cRoutingModule {}
