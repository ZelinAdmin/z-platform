import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Input,
  OnDestroy,
} from '@angular/core';
import { Subject } from 'rxjs';

import { JobService } from 'src/app/services/jobs/jobs.service';
import { MatSelect, MatSelectChange } from '@angular/material/select';
import { Job } from 'src/app/interfaces/job/job';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss'],
})
export class MonitoringComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('monitor', { static: false }) monitor: ElementRef;
  @Input() parentSubject: Subject<any>;
  @Input() userId: string;
  @Input() firstName: string;
  @Input() lastName: string;
  @ViewChild('selectJobs', { static: false }) selectJobs: MatSelect;

  private jobsList: Array<Job> = [];
  public statusList: Array<string> = [];
  public textInMonitor: Array<string> = [];

  public filteredJobsList: Array<Job> = [];
  private filteredStatus: Array<string> = [];
  private filteredBeginDate: string = undefined;
  private filteredEndDate: string = undefined;

  constructor(public jobService: JobService) {}

  ngOnInit() {
    // Get parent obj (to refresh data when tab clicked)
    this.parentSubject.subscribe((event) => {
      this.refresh();
    });
  }

  ngAfterViewInit() {
    this.onMonitorResize();
  }

  ngOnDestroy() {
    this.parentSubject.unsubscribe();
  }

  // Other methods

  public refresh() {
    this.jobService
      .updateDb(this.userId, this.firstName, this.lastName)
      .subscribe((response) => {
        // Get jobs list for the user
        this.jobService.getByUserId(this.userId).subscribe((res) => {
          this.jobsList = res;
          this.filteredJobsList = res;
          // Get list of all status
          this.statusList = [
            ...new Set(
              res.map((x) => this.jobService.getNameFromCode(x.status))
            ),
          ];
        });
      });

    // Build text of selected job
    if (
      this.selectJobs.selected === undefined ||
      this.selectJobs.value === ''
    ) {
      this.textInMonitor = ['No job selected.'];
      this.onMonitorResize();
    } else {
      const name: string = this.jobsList.find(
        (e) => e.functionalId === parseInt(this.selectJobs.value, 10)
      ).name;
      this.jobService
        .getJob(this.firstName, this.lastName, this.selectJobs.value, name)
        .subscribe((res) => {
          this.textInMonitor = res.text.split('\n');
          setTimeout((x) => {
            this.onMonitorResize();
          }, 10);
        });
    }

    // Reset filters
    this.filteredStatus = [];
    this.filteredBeginDate = undefined;
    this.filteredEndDate = undefined;
  }

  public onMonitorResize(): void {
    this.monitor.nativeElement.scrollTop = this.monitor.nativeElement.scrollHeight;
  }

  public selectJobChanged(event: Event): void {
    this.refresh();
  }

  public onStatusFilterChange(event: MatSelectChange): void {
    this.filteredStatus = event.value;
    this.filterJobs();
  }

  // mode is here to change if it's begin or end's datepicker
  public onDateFilterChange(mode: number, event: MatDatepickerInputEvent<any>) {
    // Mode === 0 : Begin date  /  Mode === 1 : End date
    if (event && event.value) {
      const tmp = event.value.toString().split(' ');
      if (mode === 0) {
        this.filteredBeginDate =
          tmp[3] + '-' + this.temporaryConvertMonth(tmp[1]) + '-' + tmp[2];
      } else {
        this.filteredEndDate =
          tmp[3] + '-' + this.temporaryConvertMonth(tmp[1]) + '-' + tmp[2];
      }
    } else {
      if (mode === 0) {
        this.filteredBeginDate = undefined;
      } else {
        this.filteredEndDate = undefined;
      }
    }
    this.filterJobs();
  }

  private filterJobs(): void {
    let resultJobList = this.jobsList;

    if (this.filteredStatus.length !== 0) {
      resultJobList = resultJobList.filter((x) =>
        this.filteredStatus.includes(this.jobService.getNameFromCode(x.status))
      );
    }

    if (this.filteredBeginDate !== undefined) {
      resultJobList = resultJobList.filter(
        (x) =>
          new Date(x.submissionDate).getTime() >=
          new Date(this.filteredBeginDate + 'T00:00:00').getTime()
      );
    }

    if (this.filteredEndDate !== undefined) {
      resultJobList = resultJobList.filter(
        (x) =>
          new Date(x.endDate).getTime() <=
            new Date(this.filteredEndDate + 'T23:59:59').getTime() &&
          x.endDate != null
      );
    }

    if (
      this.filteredStatus.length === 0 &&
      this.filteredBeginDate === undefined &&
      this.filteredEndDate === undefined
    ) {
      this.filteredJobsList = this.jobsList;
    } else {
      this.filteredJobsList = resultJobList;
    }
  }

  private temporaryConvertMonth(month: string): string {
    switch (month) {
      case 'Jan':
        return '01';
      case 'Feb':
        return '02';
      case 'Mar':
        return '03';
      case 'Apr':
        return '04';
      case 'May':
        return '05';
      case 'Jun':
        return '06';
      case 'Jul':
        return '07';
      case 'Aug':
        return '08';
      case 'Sep':
        return '09';
      case 'Oct':
        return '10';
      case 'Nov':
        return '11';
      case 'Dec':
        return '12';
    }
  }
}
