import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChange,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Job } from 'src/app/interfaces/job/job';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import { JobService } from 'src/app/services/jobs/jobs.service';
import { UserService } from 'src/app/services/user/user.service';
import { EmailService } from '../../vision/services/email/email.service';

export interface RunningJobs {
  functionalId: number;
  duration: string;
  cost: string;
}

@Component({
  selector: 'app-z2c-menu',
  templateUrl: './z2c-menu.component.html',
  styleUrls: ['./z2c-menu.component.scss'],
})
export class Z2CMenuComponent implements OnInit, OnChanges {
  @Input() public userId: string;
  @Input() public userInfos: UserInformation;

  // currentDateTime update (used to calc 'realtime' values in z2c subcomponents)
  @Input() public currentDatetime: Date;

  // Displayed values
  public nbrCPU = 100;
  public storageGo = 500;

  // Little array in bottom of Z2C menu
  public dataSource: MatTableDataSource<RunningJobs>;
  public displayedColumns: string[] = ['functionalId', 'duration', 'cost'];

  constructor(
    private emailService: EmailService,
    private jobsService: JobService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.refresh();
  }

  private refresh(): void {
    this.userService.getPartial(this.userId).subscribe((res) => {
      this.userInfos = res;
    });
    this.jobsService.getByUserId(this.userId).subscribe((res) => {
      this.dataSource = new MatTableDataSource<RunningJobs>(
        res
          .filter(
            (elem: Job) =>
              this.jobsService.getNameFromCode(elem.status) === 'Running'
          )
          .map((elem: Job) => {
            return {
              functionalId: elem.functionalId,
              duration: this.jobsService.setCurrentEndDate(
                elem,
                this.currentDatetime
              ),
              cost: parseFloat(elem.cost.toString()).toFixed(2),
            } as RunningJobs;
          })
      );
    });
  }

  public sendVPNAccess(): void {
    this.emailService.sendVPNAccess(this.userId).subscribe((e) => {});
  }

  // Update current datetime value
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    const changedProp = changes.currentDatetime;
    this.currentDatetime = new Date(changedProp.currentValue);
    this.refresh();
  }
}
