import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
} from '@angular/core';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { Globals } from 'src/app/globals/globals';
import { MatTabChangeEvent } from '@angular/material/tabs/tab-group';

@Component({
  selector: 'app-z2c',
  templateUrl: './z2c.component.html',
  styleUrls: ['./z2c.component.scss'],
})
export class Z2cComponent implements OnInit {
  public userInfos: UserInformation;
  public userId: string;
  public currentSubDatetime: Date;

  public monitorTab: Subject<any> = new Subject();

  @ViewChild('usagesTab', { static: false, read: ElementRef })
  scrollContainer: ElementRef<HTMLElement>;
  @ViewChild('scrollButton', { static: false, read: ElementRef })
  scrollButton: ElementRef;

  constructor(
    private renderer: Renderer2, // Use to set CSS property on ViewChild
    private router: Router,
    private userService: UserService
  ) {
    this.currentSubDatetime = new Date();
  }

  ngOnInit(): void {
    if (localStorage.getItem(Globals.USER_ID)) {
      this.userId = localStorage.getItem(Globals.USER_ID);
      this.userService.getPartial(this.userId).subscribe((e) => {
        this.userInfos = e;
      });
    } else {
      this.router.navigate(['login']);
    }
  }

  tabClick(event: MatTabChangeEvent) {
    // To display or not scroll down button
    if (event.index === 1 || event.index === 3) {
      this.renderer.setStyle(
        this.scrollButton.nativeElement,
        'display',
        'initial'
      );
    } else {
      this.renderer.setStyle(
        this.scrollButton.nativeElement,
        'display',
        'none'
      );
    }

    // To refresh liveMonitor display
    if (event.index === 2) {
      this.monitorTab.next();
    }
  }

  public downScroll(): void {
    this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;
  }

  public onCurrSubDatetimeUpdate() {
    this.currentSubDatetime = new Date();
  }
}
