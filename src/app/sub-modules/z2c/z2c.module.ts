import { MatDividerModule } from '@angular/material/divider';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatExpansionModule } from '@angular/material/expansion'; // z2cDoc framework
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip'; // tooltip
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

import { Z2cRoutingModule } from './z2c-routing.module';
import { Z2cComponent } from './z2c.component';
import { Z2CMenuComponent } from './z2c-menu/z2c-menu.component';
import { JobsComponent } from './jobs/jobs.component';
import { UsagesComponent } from './usages/usages.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DocumentationComponent } from './documentation/documentation.component';

@NgModule({
  declarations: [
    Z2cComponent,
    Z2CMenuComponent,
    JobsComponent,
    UsagesComponent,
    MonitoringComponent,
    DocumentationComponent,
  ],
  imports: [
    CommonModule,
    Z2cRoutingModule,
    MatCardModule,
    MatExpansionModule,
    MatDividerModule,
    MatTabsModule,
    MatIconModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule,
    MatExpansionModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatButtonModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
  ],
})
export class Z2cModule {}
