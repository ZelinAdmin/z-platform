import { Selection } from './../../../shared-components/header/header.component';
import {
  Component,
  OnInit,
  ViewChild,
  Input,
  EventEmitter,
  Output,
  SimpleChange,
  OnChanges,
} from '@angular/core';

import * as fileSaver from 'file-saver';

import { JobService } from 'src/app/services/jobs/jobs.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DispatchArg } from 'rxjs/internal/observable/SubscribeOnObservable';
import { Job } from 'src/app/interfaces/job/job';

export interface DisplayedJob {
  name: string;
  functionalId: number;
  submissionDate: string;
  startDate: string;
  endDate: string;
  cpu: number;
  duration: string;
  status: string;
  cost: string;
  actions: string;
}

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
})
export class JobsComponent implements OnInit, OnChanges {
  private userJobs: Array<DisplayedJob> = [];
  public sortedData: Array<DisplayedJob> = [];
  public dataSource: MatTableDataSource<any>;
  public displayedColumns: string[] = [
    'name',
    'functionalId',
    'submissionDate',
    'startDate',
    'endDate',
    'cpu',
    'duration',
    'status',
    'cost',
    'actions',
  ];
  public jobsLoading = false;

  @Input() userId: string;
  @Input() firstName: string;
  @Input() lastName: string;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // currentDateTime update (used to calc 'realtime' values in z2c subcomponents)
  @Input() public currentDatetime: Date;
  @Output() dateTimeEmitter = new EventEmitter<void>();

  constructor(public jobService: JobService, private snackbar: MatSnackBar) {}

  ngOnInit(): void {
    this.refresh();
  }

  public refresh(): void {
    // update current Datetime
    this.dateTimeEmitter.emit();

    this.jobsLoading = true;
    this.jobService
      .updateDb(this.userId, this.firstName, this.lastName)
      .subscribe((response) => {
        // Get jobs list for the user
        this.jobService
          .getByUserId(this.userId)
          .subscribe((res: Array<Job>) => {
            this.userJobs = res
              .map((elem: Job) => {
                return {
                  name: elem.name,
                  functionalId: elem.functionalId,
                  submissionDate: elem.submissionDate,
                  startDate: elem.startDate,
                  endDate: elem.endDate,
                  cpu: elem.cpu,
                  duration: this.jobService.setCurrentEndDate(
                    elem,
                    this.currentDatetime
                  ),
                  status: elem.status,
                  cost:
                    this.jobService.getNameFromCode(elem.status) ===
                      'Running' ||
                    this.jobService.getNameFromCode(elem.status) === 'Finished'
                      ? Number(elem.cost).toFixed(2)
                      : 0,
                  actions: '',
                } as DisplayedJob;
              })
              .sort((a: DisplayedJob, b: DisplayedJob) => {
                if (a.functionalId >= b.functionalId) {
                  return -1;
                }
                return 1;
              });

            this.dataSource = new MatTableDataSource<DisplayedJob>(
              this.userJobs
            );
            this.dataSource.sort = this.sort;

            this.dataSource.sortingDataAccessor = (item, property) => {
              switch (property) {
                case 'endDate': {
                  return new Date(item.endDate * 1000);
                }
                case 'startDate': {
                  return new Date(item.startDate * 1000);
                }
                case 'cost': {
                  return item.cost * 1000;
                }
                case 'duration': {
                  return this.getDuration(item.duration);
                }
                default:
                  return item[property];
              }
            };
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.jobsLoading = false;
          });
      });
  }

  // Function to apply search in array
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public downloadJobFile(element: DisplayedJob) {
    this.jobService
      .downloadJob(
        this.firstName,
        this.lastName,
        element.functionalId,
        element.name
      )
      .subscribe((response) => {
        try {
          const filename = response.headers.get('filename');
          fileSaver.saveAs(response.body, filename);
        } catch (e) {
          this.snackbar.open('Error : File unavailable', '', {
            duration: 2000,
            verticalPosition: 'top',
          });
        }
      });
  }

  /* This function allows to sort the DURATION column.
  We extract every number from the string, and add them together to get a sortable number.
  This seems kind of counterproductive seeing that our string was originally looking like this,
  but it's the only way I found to sort durations using the already existing system.
  Also, the function could probablly be way more optimized. */
  public getDuration(startingString: string) {
    let days = '0'; // Initating our variables as strings, because we'll use substring()
    let hours = '0'; // We initate them at 0 so our last formula doesn't crash.
    let min = '0';
    let sec = '0';
    let newString = '';

    // If the string is empty we return 0
    if (startingString === '') {
      return 0;
    }

    // We start looking for identifiers ( d, h, min, s) to isolate the numbers and append them to our string variables.
    if (startingString.includes('d')) {
      for (let i = 0; i <= startingString.length; i++) {
        // Once we find the identifier
        if (startingString.charAt(i) === 'd') {
          // We assign it to our variable and use substring to only keep the rest of the string.
          days = startingString.substring(0, i);
          newString = startingString.substring(i + 1, startingString.length);
        }
      }
    }

    // Looking for hours
    if (startingString.includes('h')) {
      if (newString !== '') {
        for (let i = 0; i <= newString.length; i++) {
          if (newString.charAt(i) === 'h') {
            hours = newString.substring(0, i);
            newString = newString.substring(i + 1, newString.length);
          }
        }
      } else {
        // We have to take into account the scenario where some durations don't have certains identifiers.
        for (let i = 0; i <= startingString.length; i++) {
          // In which case we are able tu use the original string.
          if (startingString.charAt(i) === 'h') {
            hours = startingString.substring(0, i);
            newString = startingString.substring(i + 1, startingString.length);
          }
        }
      }
    }

    // Looking for minutes.
    if (startingString.includes('min')) {
      if (newString !== '') {
        for (let i = 0; i <= newString.length; i++) {
          if (newString.charAt(i) === 'm') {
            min = newString.substring(0, i);
            newString = newString.substring(i + 3, newString.length);
          }
        }
      } else {
        for (let i = 0; i <= startingString.length; i++) {
          if (startingString.charAt(i) === 'm') {
            min = startingString.substring(0, i);
            newString = startingString.substring(i + 3, startingString.length);
          }
        }
      }
    }

    // Finally, looking for seconds.
    if (startingString.includes('s')) {
      if (newString !== '') {
        for (let i = 0; i <= newString.length; i++) {
          if (newString.charAt(i) === 's') {
            newString = newString.substring(0, i);
          }
        }
      } else {
        // In the scenario where the original string only contains seconds, nothing has to be done.
        sec = startingString;
      }
    }

    let finalNumber = 0;

    // This is where we re-construct our time in milliseconds, using each number as a string, converted to ints using parseInt().
    finalNumber =
      parseInt(days, 10) * 24 * 60 * 60 * 1000 +
      parseInt(hours, 10) * 60 * 60 * 1000 +
      parseInt(min, 10) * 60 * 1000 +
      parseInt(sec, 10) * 1000;
    return finalNumber;
  }

  // Update current datetime value
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    const changedProp = changes.currentDatetime;
    if (changedProp && changedProp.currentValue) {
      this.currentDatetime = new Date(changedProp.currentValue);
    }
  }
}
