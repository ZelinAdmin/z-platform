import { Component, ViewChild } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';

// import { UserInformation } from 'src/app/interfaces/user-information/user-information';

@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.component.html',
  styleUrls: ['./documentation.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DocumentationComponent {
  // @ViewChild(MatAccordion) accordion: MatAccordion;
  title = 'Documentation Z2C';
  step = 0;
  // panelOpenState = false;
  elem = '<job_id>';

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}
