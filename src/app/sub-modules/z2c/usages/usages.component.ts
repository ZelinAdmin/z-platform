import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

import { JobService } from 'src/app/services/jobs/jobs.service';
import { Job } from 'src/app/interfaces/job/job';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import { Credit } from 'src/app/interfaces/credit/credit';
import { CreditService } from 'src/app/services/credit/credit.service';
import { UserService } from 'src/app/services/user/user.service';

export interface ResumeUsage {
  month: string;
  cpu: number;
  credits: string;
}

@Component({
  selector: 'app-usages',
  templateUrl: './usages.component.html',
  styleUrls: ['./usages.component.scss'],
})
export class UsagesComponent implements OnInit {
  private userUsagesResume: Array<ResumeUsage> = [];
  public dataSourceResume: MatTableDataSource<ResumeUsage>;
  public displayedColumnsResume: string[] = ['month', 'cpu', 'credits'];

  private userUsagesDetails: Array<Credit> = [];
  public dataSourceDetails: MatTableDataSource<Credit>;
  public displayedColumnsDetails: string[] = [
    'date',
    'operation',
    // 'creditsAfter',
  ];

  @Input() public userId: string;
  @Input() public userInfos: UserInformation;
  public averageJobCost = '';

  @ViewChild('resumeSort', { static: true }) resumeSort: MatSort;
  @ViewChild('detailsSort', { static: true }) detailsSort: MatSort;

  // Update z2c menu when user refresh usages âge
  @Output() z2cMenuEmitter = new EventEmitter<void>();

  constructor(
    private creditsService: CreditService,
    private jobService: JobService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.refresh();
  }

  public refresh(): void {
    // update current z2cMenu
    this.z2cMenuEmitter.emit();

    // update userInfos
    this.userService.getPartial(this.userId).subscribe((res) => {
      this.userInfos = res;
    });

    // Resume table
    this.jobService.getByUserId(this.userId).subscribe((res) => {
      this.userUsagesResume = this.resumeUsages(
        res.sort((a: Job, b: Job) => {
          if (a.functionalId <= b.functionalId) {
            return -1;
          }
          return 1;
        })
      );
      this.dataSourceResume = new MatTableDataSource<ResumeUsage>(
        this.userUsagesResume
      );
      this.dataSourceResume.sort = this.resumeSort;

      this.dataSourceResume.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'month': {
            return new Date(item.month);
          }
          case 'cpu': {
            return item.cpu;
          }
          case 'credits': {
            return parseInt(item.credits, 10);
          }
          default:
            return item[property];
        }
      };

      // Average job cost
      let totalCost = 0;
      if (res.length === 0) {
        this.averageJobCost = '0';
      } else {
        res.forEach((elem) => {
          totalCost = totalCost + parseFloat(elem.cost.toString());
        });
        this.averageJobCost = parseFloat(
          (totalCost / res.length).toString()
        ).toFixed(2);
      }
    });

    // Details table
    this.creditsService.getByUserId(this.userId).subscribe((res) => {
      this.userUsagesDetails = res;

      this.dataSourceDetails = new MatTableDataSource<Credit>(
        this.userUsagesDetails
      );
      this.dataSourceDetails.sort = this.detailsSort;
      this.dataSourceDetails.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'date': {
            return new Date(item.operationDate);
          }
          case 'operation': {
            return item.amount;
          }
          case 'creditsAfter': {
            return item.creditsAfter;
          }
          default:
            return item[property];
        }
      };
    });
  }

  private resumeUsages(usages: Array<Job>): Array<ResumeUsage> {
    const resumeUsages: Array<ResumeUsage> = [];
    const currentMonth = {
      month: -1,
      year: 0,
      cpu: 0,
      amount: 0,
    };

    usages.forEach((usage: Job) => {
      if (
        new Date(usage.submissionDate).getFullYear() !== currentMonth.year ||
        new Date(usage.submissionDate).getMonth() !== currentMonth.month
      ) {
        if (currentMonth.month !== -1) {
          resumeUsages.push({
            month:
              this.monthNbrToString(currentMonth.month) +
              ', ' +
              currentMonth.year,
            cpu: currentMonth.cpu,
            credits: Number.parseFloat(currentMonth.amount.toString()).toFixed(
              2
            ),
          });
        }
        currentMonth.year = new Date(usage.submissionDate).getFullYear();
        currentMonth.month = new Date(usage.submissionDate).getMonth();
        currentMonth.cpu = 0;
        currentMonth.amount = 0;
      }
      currentMonth.cpu = currentMonth.cpu + usage.cpu;

      currentMonth.amount =
        parseFloat(currentMonth.amount.toString()) +
        parseFloat(usage.cost.toString());
    });

    resumeUsages.push({
      month:
        this.monthNbrToString(currentMonth.month) + ', ' + currentMonth.year,
      cpu: currentMonth.cpu,
      credits: Number.parseFloat(currentMonth.amount.toString()).toFixed(2),
    });

    return resumeUsages;
  }

  private monthNbrToString(idx: number): string {
    switch (idx) {
      case 0:
        return 'January';
      case 1:
        return 'February';
      case 2:
        return 'March';
      case 3:
        return 'April';
      case 4:
        return 'May';
      case 5:
        return 'June';
      case 6:
        return 'July';
      case 7:
        return 'August';
      case 8:
        return 'September';
      case 9:
        return 'October';
      case 10:
        return 'November';
      case 11:
        return 'December';
    }
  }

  public getColor(element) {
    return element >= 0 ? 'green' : 'red';
  }

  public getSymbol(element) {
    return element >= 0 ? '+' : '';
  }
}
