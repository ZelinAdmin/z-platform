import { UserInformation } from './../../interfaces/user-information/user-information';
import { UserService } from 'src/app/services/user/user.service';
import { RegisterComponent } from './../register/register.component';
import { LoginComponent } from './../login/login.component';
import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertService } from 'src/app/services/alert-service/alert.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less'],
})
export class ResetPasswordComponent implements OnInit {
  forgetPasswordForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  notfound: boolean;

  // user info with email sended
  userInfo: UserInformation;

  constructor(
    private formBuilder: FormBuilder,
    public authenticationService: AuthService,
    public router: Router,
    public userService: UserService,
    private snackbar: MatSnackBar,
    private alertService: AlertService
  ) {
    this.loading = false;
    this.submitted = false;
    this.notfound = false;
    this.forgetPasswordForm = formBuilder.group({
      email: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  get f() {
    return this.forgetPasswordForm.controls;
  }

  async sendEmail() {
    // request to found account and send it email
    this.authenticationService
      .forgotPassword(this.f.email.value)
      .subscribe((user: UserInformation) => {
        // to init user messages
        this.submitted = true;
        this.userInfo = user;
        this.loading = false;
        this.notfound = false;
        console.log('--- u s e r ----: ', user);
        return this.userInfo;
      });

    // looking for account or no account founded
    setTimeout(() => {
      console.log('--- u s e r     n o t    fo u n d----: ');
      this.notfound = true;
      this.loading = false;
    }, 5000);
  }

  public async onSubmit(form: FormGroup) {
    console.log('form: ', form.value);
    if (form.invalid) {
      return;
    }

    // re init if several asks
    this.loading = true;
    this.userInfo = null;
    this.submitted = false;
    this.notfound = false;

    // email exist? yes -> send a reset email
    this.sendEmail();
  }
}
