import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertService } from 'src/app/services/alert-service/alert.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading: boolean;
  submitted: boolean;
  returnUrl: string;
  passwordIsHidden = true;

  constructor(
    private formBuilder: FormBuilder,
    public authenticationService: AuthService,
    public router: Router,
    private snackbar: MatSnackBar,
    private alertService: AlertService
  ) {
    this.loading = false;
    this.submitted = false;
    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: [true],
    });
  }

  ngOnInit() {}

  // convenience getter for easy access to form fields
  // private f(form: FormGroup) {
  //   return form && form.controls;
  // }
  get f() {
    return this.loginForm.controls;
  }

  // Temporary fix to show error messages, this function sets a delay.
  public delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  public async onSubmit(form: FormGroup) {
    if (form.invalid) {
      return;
    }
    this.submitted = true;

    for (const controlKey of Object.keys(form.controls)) {
      form.controls[controlKey].markAsDirty();
      form.controls[controlKey].updateValueAndValidity();
    }

    this.loading = true;

    /* send the request*/
    this.authenticationService
      .login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe({
        next: (data) => {
          this.snackbar.open('Login successful', '', {
            duration: 2500,
            verticalPosition: 'top',
          });
          this.loading = false;
        },
        error: (error) => {
          this.snackbar.open('Failed to login', '', {
            duration: 2500,
            verticalPosition: 'top',
          });
          this.alertService.error(error);
          this.loading = false;
        },
      });

    // This a temporary fix, we await response from the login service and display an error if the service fails to login the user.
    await this.delay(1000);
    if (this.loading === true) {
      this.snackbar.open('Login Failed', 'Check your credentials', {
        duration: 2000,
        verticalPosition: 'top',
      });
    }
  }
}
