import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AlertService } from 'src/app/services/alert-service/alert.service';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBar } from '@angular/material/snack-bar';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let httpMock: HttpTestingController;
  let fixture: ComponentFixture<LoginComponent>;
  let http: HttpClient;

  const formBuilderSpy = jasmine.createSpyObj('FormBuilder', ['group']);
  const authServiceSpy = {
    login: (usnm: string, pwd: string) => {
      return http.post('/mockedUrl', { email: usnm, password: pwd });
    },
  };
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const snackBarSpy = jasmine.createSpyObj('MatSnackBar', ['open']);
  const alertServiceSpy = jasmine.createSpyObj('AlertService', ['error']);

  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [
        { provide: FormBuilder, useValue: formBuilderSpy },
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: MatSnackBar, useValue: snackBarSpy },
        { provide: AlertService, useValue: alertServiceSpy },
      ],
      imports: [
        MatGridListModule,
        MatCardModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    httpMock = TestBed.get(HttpTestingController);
    http = TestBed.get(HttpClient);
    component = fixture.componentInstance;
    component.loginForm = formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      remember: [true],
    });
    fixture.detectChanges();
  }));

  afterEach(() => {
    httpMock.verify();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render inputs', () => {
    const compiled = fixture.debugElement.nativeElement;
    const usernameInput = compiled.querySelector('input[id="username"]');
    const passwordInput = compiled.querySelector('input[id="password"]');

    expect(usernameInput).toBeTruthy();
    expect(passwordInput).toBeTruthy();
  });

  it('should test form validity', () => {
    const form = component.loginForm;
    expect(form.valid).toBeFalsy();

    form.controls.username.setValue('Foo');
    expect(form.valid).toBeFalsy();
    form.controls.username.setValue(null);

    form.controls.password.setValue('Bar');
    expect(form.valid).toBeFalsy();
    form.controls.password.setValue(null);

    form.controls.username.setValue('Foo');
    form.controls.password.setValue('Bar');
    expect(form.valid).toBeTruthy();
  });

  it('should test input validity', () => {
    const usernameInput = component.loginForm.controls.username;
    const passwordInput = component.loginForm.controls.password;

    expect(usernameInput.valid).toBeFalsy();
    expect(passwordInput.valid).toBeFalsy();

    usernameInput.setValue('foo');
    passwordInput.setValue('bar');
    expect(usernameInput.valid).toBeTruthy();
    expect(passwordInput.valid).toBeTruthy();
  });

  it('should test input errors', () => {
    const usernameInput = component.loginForm.controls.username;
    const passwordInput = component.loginForm.controls.password;

    expect(usernameInput.errors.required).toBeTruthy();
    expect(passwordInput.errors.required).toBeTruthy();

    usernameInput.setValue('foo');
    passwordInput.setValue('bar');
    expect(usernameInput.errors).toBeNull();
    expect(passwordInput.errors).toBeNull();
  });

  it('the form should be submitted if the credentials are right', () => {
    const form = component.loginForm;
    form.controls.username.setValue('Foo');
    form.controls.password.setValue('bar');

    component.onSubmit(form);
    const httpRequest = httpMock.expectOne('/mockedUrl');

    httpRequest.flush({ status: 'ok' });
    expect(snackBarSpy.open).toHaveBeenCalledWith('Login successful', '', {
      duration: 2500,
      verticalPosition: 'top',
    });
  });

  it('the form should be failed if the credentials are wrong', () => {
    const errorMessage = 'deliberate 404 error';
    const form = component.loginForm;
    form.controls.username.setValue('Foo');
    form.controls.password.setValue('bar');

    component.onSubmit(form);
    const httpRequest = httpMock.expectOne('/mockedUrl');

    httpRequest.flush(errorMessage, { status: 404, statusText: 'Not found' });
    expect(snackBarSpy.open).toHaveBeenCalledWith('Failed to login', '', {
      duration: 2500,
      verticalPosition: 'top',
    });
    expect(alertServiceSpy.error).toHaveBeenCalled();
  });
});
