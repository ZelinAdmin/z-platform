import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { SignUpInfo } from 'src/app/interfaces/signup-info/sign-up-info';
import {
  ComponentsTrslt,
  TranslationService,
} from 'src/app/services/translation/translation.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  public translatePage: ComponentsTrslt = ComponentsTrslt.SIGNUP;
  public userInfo = new SignUpInfo();
  public complete: boolean;
  signupForm: FormGroup;
  passwordIsHidden = true;
  seasons = [
    {
      value: true,
      text: this.translateService.getTranslation(
        this.translatePage,
        'imClient'
      ),
    },
    {
      value: false,
      text: this.translateService.getTranslation(
        this.translatePage,
        'imExpert'
      ),
    },
  ];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    public router: Router,
    public translateService: TranslationService,
    private snackBar: MatSnackBar
  ) {
    this.signupForm = this.formBuilder.group({
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
        ],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.pattern(
            '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
          ),
        ],
      ],
      checkPassword: ['', [Validators.required, this.confirmationValidator]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: [''],
      company: [''],
      address: [''],
      zipCode: [''],
      city: [''],
      country: [''],
      citizenship: [''],
      mode: [false],
    });
  }

  ngOnInit() {
    this.complete = false;
  }

  get f(): { [p: string]: AbstractControl } {
    /* This returns an undefined*/
    return this.signupForm && this.signupForm.controls;
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      this.complete = false;
      return { required: true };
    } else if (control.value !== this.signupForm.controls.password.value) {
      this.complete = false;
      return { confirm: true, error: true };
    } else {
      this.complete = true;
    }
  }

  getMailError() {
    return this.f.email.hasError('email') ? 'Not a valid email' : '';
  }

  getPasswordStrengthError() {
    this.complete = false;
    return this.f.password.hasError('pattern')
      ? 'At least 8 characters including lowercase, uppercase, special character and number.'
      : '';
  }

  getCheckPasswordError() {
    return this.f.checkPassword.hasError('confirm')
      ? 'Please, enter the same password'
      : '';
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  onSubmit(): void {
    // check if form is ok
    if (this.complete === false) {
      const message = 'Form not valid (incomplete or error)';
      const value = 'Please fill all input';
      this.openSnackBar(message, value);
    } else {
      this.authService.signUp(this.signupForm.value).subscribe(
        (result) => {
          // register worked
          this.complete = true;
          const message = 'Welcome on Zelin Plateform !';
          const value = 'Check your email';
          this.openSnackBar(message, value),
            setTimeout(() => {
              this.router.navigate(['login']);
            }, 3000);
        },
        (error) => {
          // the registry did not work because an account with this email must already exist
          const message = 'This email is allready registered';
          const value = 'Login or use an other email';
          this.openSnackBar(message, value);
        }
      );
    }
  }
}
