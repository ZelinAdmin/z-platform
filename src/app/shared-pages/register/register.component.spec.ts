import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import {
  FormBuilder,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('RegisterComponent', () => {
  // let component: RegisterComponent;
  // let fixture: ComponentFixture<RegisterComponent>;
  // let httpMock: HttpTestingController;
  // let http: HttpClient;
  // const MOCKED_EMAIL = 'aeraze@ear.com';
  // const MOCKED_PASSWORD = 'Yeet1!Yeet1!';
  // const MOCKED_DIFF_PASSWORD = 'bbqsf';
  // const MOCKED_NAME = 'Json';
  // const MOCKED_LASTNAME = 'Doe';
  // const formBuilderSpy = jasmine.createSpyObj('FormBuilder', ['group']);
  // const authServiceSpy = {
  //   signUp: (form) => {
  //     return http.post('/mockedUrl', form);
  //   },
  // };
  // const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  // const formBuilder: FormBuilder = new FormBuilder();
  // beforeEach(async(() => {
  //   TestBed.configureTestingModule({
  //     declarations: [RegisterComponent],
  //     providers: [
  //       { provide: FormBuilder, useValue: formBuilderSpy },
  //       { provide: AuthService, useValue: authServiceSpy },
  //       { provide: Router, useValue: routerSpy },
  //     ],
  //     imports: [
  //       MatGridListModule,
  //       MatCardModule,
  //       MatIconModule,
  //       MatFormFieldModule,
  //       MatInputModule,
  //       MatSnackBarModule,
  //       BrowserAnimationsModule,
  //       HttpClientTestingModule,
  //       ReactiveFormsModule,
  //       FormsModule,
  //       MatRadioModule,
  //     ],
  //   }).compileComponents();
  //   fixture = TestBed.createComponent(RegisterComponent);
  //   httpMock = TestBed.get(HttpTestingController);
  //   http = TestBed.get(HttpClient);
  //   component = fixture.componentInstance;
  //   component.signupForm = formBuilder.group({
  //     email: [
  //       '',
  //       [
  //         Validators.required,
  //         Validators.email,
  //         Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
  //       ],
  //     ],
  //     password: [
  //       '',
  //       [
  //         Validators.required,
  //         Validators.pattern(
  //           '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&;,.])[A-Za-zd$@$!%*?&].{7,}'
  //         ),
  //       ],
  //     ],
  //     checkPassword: [
  //       '',
  //       [Validators.required, component.confirmationValidator],
  //     ],
  //     firstName: ['', Validators.required],
  //     lastName: ['', Validators.required],
  //     phone: [''],
  //     company: [''],
  //     address: [''],
  //     zipCode: [''],
  //     city: [''],
  //     country: [''],
  //     citizenship: [''],
  //     mode: [false],
  //   });
  //   fixture.detectChanges();
  // }));
  // afterEach(() => {
  //   httpMock.verify();
  // });
  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
  // it('should render inputs', () => {
  //   const compiled = fixture.debugElement.nativeElement;
  //   const attrobutes = [
  //     'email',
  //     'password',
  //     'rpassword',
  //     'firstName',
  //     'lastName',
  //   ];
  //   attrobutes.forEach((attribute) => {
  //     const element = compiled.querySelector(`input[id="${attribute}"]`);
  //     expect(element).toBeTruthy();
  //     expect(element).toBeDefined();
  //   });
  // });
  // it('should test form validity', () => {
  //   const form = component.signupForm;
  //   expect(form.valid).toBeFalsy();
  //   form.controls.email.setValue(MOCKED_EMAIL);
  //   expect(form.valid).toBeFalsy();
  //   form.controls.password.setValue(MOCKED_PASSWORD);
  //   form.controls.checkPassword.setValue(MOCKED_DIFF_PASSWORD);
  //   form.controls.password.setValue(MOCKED_PASSWORD);
  //   form.controls.firstName.setValue(MOCKED_NAME);
  //   form.controls.lastName.setValue(MOCKED_LASTNAME);
  //   expect(form.valid).toBeFalsy();
  //   form.controls.checkPassword.setValue(MOCKED_PASSWORD);
  //   expect(form.valid).toBeTruthy();
  // });
  // it('should test input errors', () => {
  //   const form = component.signupForm;
  //   const listOfValidEmails = [
  //     'email@example.com',
  //     'firstname.lastname@example.com',
  //     'email@subdomain.example.com',
  //     'firstname+lastname@example.com',
  //     '1234567890@example.com',
  //     'email@example-one.com',
  //     'email@example.name',
  //     'email@example.museum',
  //     'email@example.co.jp',
  //     'firstname-lastname@example.com',
  //   ];
  //   const listOfInvalidEmails = [
  //     'plainaddress',
  //     '#@%^%#$@#$@#.com',
  //     '@example.com',
  //     'Joe Smith <email@example.com>',
  //     'email.example.com',
  //     'email@123.123.123.123',
  //     '"email"@example.com',
  //     'email@[123.123.123.123]',
  //     'email@example@example.com',
  //     '.email@example.com',
  //     'email..email@example.com',
  //     'あいうえお@example.com',
  //     'email@example.com (Joe Smith)',
  //     'email@111.222.333.44444',
  //     'email@example..com',
  //     'Abc..123@example.com',
  //   ];
  //   expect(form.controls.email.errors.required).toBeTruthy();
  //   expect(form.controls.password.errors.required).toBeTruthy();
  //   expect(form.controls.checkPassword.errors.required).toBeTruthy();
  //   expect(form.controls.firstName.errors.required).toBeTruthy();
  //   expect(form.controls.lastName.errors.required).toBeTruthy();
  //   listOfInvalidEmails.forEach((email) => {
  //     form.controls.email.setValue(email);
  //     expect(form.controls.email.valid).toBeFalsy(email);
  //   });
  //   listOfValidEmails.forEach((email) => {
  //     form.controls.email.setValue(email);
  //     expect(form.controls.email.errors).toBeNull(email);
  //   });
  // });
  // it('the form should be submitted ', () => {
  //   const form = component.signupForm;
  //   form.controls.email.setValue(MOCKED_EMAIL);
  //   form.controls.password.setValue(MOCKED_PASSWORD);
  //   form.controls.checkPassword.setValue(MOCKED_PASSWORD);
  //   form.controls.firstName.setValue(MOCKED_NAME);
  //   form.controls.lastName.setValue(MOCKED_LASTNAME);
  //   component.signupForm = form;
  //   component.onSubmit();
  //   const httpRequest = httpMock.expectOne('/mockedUrl');
  //   httpRequest.flush({ status: 'ok' });
  //   expect(routerSpy.navigate).toHaveBeenCalledWith(['login']);
  // });
});
