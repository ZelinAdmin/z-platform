import { ForgotPasswordInfo } from './../../interfaces/forgotPassword/forgot-password-info';
import { UserInformation } from './../../interfaces/user-information/user-information';
import { UserService } from 'src/app/services/user/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { HashService } from 'src/app/services/hash/hash.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less'],
})
export class ChangePasswordComponent implements OnInit {
  // to generate the form will be sended
  resetPasswordForm: FormGroup;
  // data get about user asker
  userId: string; // to get the id of the user, in the url
  userInfos: UserInformation; // to get data of the user from his id
  forgotPassword: ForgotPasswordInfo; // a class to send the newPassword

  // if password pattern is ok and confimationPwd=pwd
  dataIsOk = false;
  // password will be sended after check all validated steps
  newPassword: string;

  // UX : informations for user in the view
  passwordIsHidden = true;
  submitted: boolean; // request is sended
  cancel: boolean; // manage the appareance of the button cancel
  login: boolean; // manage the appareance of the button login
  buttonSend: string; // manage the appareance of the button send

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public authenticationService: AuthService,
    public router: Router,
    public userService: UserService,
    private hashService: HashService
  ) {
    // manage UX
    this.submitted = false;
    this.cancel = true;
    this.login = false;
    this.buttonSend = 'send';
    // manage data
    this.resetPasswordForm = this.formBuilder.group({
      password: [
        '',
        [
          Validators.pattern(
            '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&;,.])[A-Za-zd$@$!%*?&].{7,}'
          ),
        ],
      ],
      checkPassword: ['', [this.confirmationValidator]],
    });
  }

  // on init catch the userId in url and request his profile
  ngOnInit() {
    this.userId = this.route.snapshot.paramMap.get('id');
    this.userService.getUserInfo(this.userId).subscribe((user) => {
      this.userInfos = user;
    });
  }

  get f() {
    return this.resetPasswordForm.controls;
  }

  // to validate the format of the new password
  // and if the password choosen is same as the confirmation password
  // if not: no request sended
  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      this.dataIsOk = false;
      return { required: true };
    } else if (
      control.value !== this.resetPasswordForm.controls.password.value
    ) {
      this.dataIsOk = false;
      return { confirm: true, error: true };
    } else {
      this.dataIsOk = true;
    }
  }

  getPasswordStrengthError() {
    this.newPassword = null;
    this.dataIsOk = false;
    return this.f.password.hasError('pattern')
      ? 'At least 8 characters including lowercase, uppercase, special character and number.'
      : '';
  }

  getCheckPasswordError() {
    this.newPassword = null;
    this.dataIsOk = false;
    return this.f.checkPassword.hasError('confirm')
      ? 'Please, enter the same password'
      : '';
  }

  // password is validated, send the request to change it in the db + inform the user
  public async onSubmit(form: FormGroup) {
    if (form.invalid) {
      this.newPassword = null;
      this.dataIsOk = false;
      return;
    } else {
      this.newPassword = form.value.password;
    }
    // add request
    this.cancel = false;
    this.login = true;
    this.submitted = true;
    this.buttonSend = 'allreadySended';
    const newPwd = (this.forgotPassword = {
      password: this.hashService.hashPwd(this.newPassword),
    });
    this.userService
      .updateUserPwd(this.userId, newPwd)
      .subscribe((updatedUser) => {});
  }
}
