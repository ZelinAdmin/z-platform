import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  convertToParamMap,
  ParamMap,
  Params,
  Router,
} from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { ReplaySubject } from 'rxjs';

import { ChangePasswordComponent } from './change-password.component';
import { RouterTestingModule } from '@angular/router/testing';

class ActivatedRouteStub implements Partial<ActivatedRoute> {
  private paramMapp: ParamMap;
  private subject = new ReplaySubject<ParamMap>();

  paramMap = this.subject.asObservable();
  get snapshot(): ActivatedRouteSnapshot {
    const snapshot: Partial<ActivatedRouteSnapshot> = {
      paramMap: this.paramMapp,
    };

    return snapshot as ActivatedRouteSnapshot;
  }

  constructor(initialParams?: Params) {
    this.setParamMap(initialParams);
  }

  setParamMap(params?: Params) {
    const paramMap = convertToParamMap(params);
    this.paramMapp = paramMap;
    this.subject.next(paramMap);
  }
}

describe('ChangePasswordComponent', () => {
  // let component: ChangePasswordComponent;
  // let fixture: ComponentFixture<ChangePasswordComponent>;
  // let httpMock: HttpTestingController;
  // let http: HttpClient;
  // const formBuilderSpy = jasmine.createSpyObj('FormBuilder', ['group']);
  // const authServiceSpy = {
  //   signUp: (form) => {
  //     return http.post('/mockedUrl', form);
  //   },
  // };
  // const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  // const alertServiceSpy = jasmine.createSpyObj('AlertService', ['error']);
  // // function navigateByUserId(id: string) {
  // //   routeStub.setParamMap({ id });
  // // }
  // beforeEach(async(() => {
  //   TestBed.configureTestingModule({
  //     declarations: [ChangePasswordComponent],
  //     providers: [
  //       { provide: FormBuilder, useValue: formBuilderSpy },
  //       { provide: AuthService, useValue: authServiceSpy },
  //       // { provide: Router, useValue: routerSpy },
  //       { useValue: alertServiceSpy },
  //     ],
  //     imports: [
  //       ReactiveFormsModule,
  //       HttpClientTestingModule,
  //       RouterTestingModule,
  //       HttpClientTestingModule
  //     ],
  //   }).compileComponents();
  //   httpMock = TestBed.get(HttpTestingController);
  //   http = TestBed.get(HttpClient);
  // }));
  // afterEach(() => {
  //   httpMock.verify();
  // });
  // beforeEach(() => {
  //   fixture = TestBed.createComponent(ChangePasswordComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });
  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
