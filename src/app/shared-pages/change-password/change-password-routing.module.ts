import { ResetPasswordComponent } from './../reset-password/reset-password.component';
import { ChangePasswordComponent } from './change-password.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './../not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    component: ResetPasswordComponent,
  },
  {
    path: ':id',
    component: ChangePasswordComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChangePasswordRoutingModule {}
