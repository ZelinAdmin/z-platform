import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
})
export class HomeComponent implements OnInit {
  constructor(public router: Router, private snackbar: MatSnackBar) {}

  ngOnInit() {}

  onClickNewRequest(): void {
    this.router.navigate(['/requests/new']);
  }

  onClickMyRequests(): void {
    this.router.navigate(['/requests/my']);
  }

  onClickELearning(): void {
    this.snackbar.open('Coming soon', '', {
      duration: 2500,
    });
  }

  onClickESupport() {
    this.snackbar.open('Coming soon', '', {
      duration: 2500,
    });
  }

  onClickZ2C() {
    this.router.navigate(['z2c']);
  }

  onClickExperts() {
    this.router.navigate(['experts']);
  }
}
