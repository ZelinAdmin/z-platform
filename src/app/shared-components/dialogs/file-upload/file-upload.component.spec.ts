import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadComponent } from './file-upload.component';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { DialogNotificationService } from 'src/app/services/dialog-notification/dialog-notification.service';
import { VisualizerService } from 'src/app/services/visualize/visualizer.service';
import { FileTransferService } from 'src/app/services/file-transfer/file-transfer.service';
import { DialogDataToInject } from 'src/app/interfaces/dialog-data/dialog-data-to-inject';
import { HttpEventType } from '@angular/common/http';

describe('FileUploadComponent', () => {
  let component: FileUploadComponent;
  let fixture: ComponentFixture<FileUploadComponent>;
  const dialogDataMockup: DialogDataToInject = {
    projectId: '',
    category: 'plot',
    path: '',
  };
  const fileServiceMock = {
    uploadToServerWithCategory: (one, two, three, four, five) => ({
      subscribe: () => 'something',
    }),
  };
  const visualizerMock = {
    getState: () => ({ subscribe: () => '' }),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileUploadComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: dialogDataMockup },
        { provide: MatDialogRef, useValue: {} },
        { provide: FileManipulationsService, useValue: fileServiceMock },
        { provide: DialogNotificationService, useValue: {} },
        { provide: VisualizerService, useValue: visualizerMock },
        { provide: FileTransferService, useValue: {} },
      ],
      imports: [MatDialogModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(component, 'plotImageTcmUpload');
    spyOn(component, 'modelTreating');
  });

  afterEach(() => {
    fixture.destroy();
    component.data.category = 'plot';
  });

  it('should upload generic files (with category)', () => {
    component.files = [
      {
        name: 'Test.csv',
      },
    ];

    component.uploadFiles(0);
    expect(component.plotImageTcmUpload).toHaveBeenCalledTimes(1);
    expect(component.modelTreating).toHaveBeenCalledTimes(0);
  });

  it('should prepare files', () => {
    /*
    test only one file with right format
    (non model)
    */
    let testFiles = [
      {
        name: 'test.csv',
      },
    ];
    let compared = [
      {
        name: 'test.csv',
        progress: 0,
      },
    ];
    component.prepareFilesList(testFiles);
    expect(component.files).toBeTruthy();
    expect(component.files).toEqual(compared);

    /*
    test only one file with wrong format
    (non model)
    */
    component.files = [];
    testFiles = [
      {
        name: 'test.wrongformat',
      },
    ];
    component.prepareFilesList(testFiles);
    expect(component.files).toBeTruthy();
    expect(component.files).toEqual([]);

    /*
    test multi file
    (non model)
    */
    component.files = [];
    testFiles = [{ name: 'test.csv' }, { name: 'azrry.csv' }];
    compared = [
      { name: 'test.csv', progress: 0 },
      { name: 'azrry.csv', progress: 0 },
    ];
    component.prepareFilesList(testFiles);
    expect(component.files).toBeTruthy();
    expect(component.files).toEqual(compared);

    /*
    test only one file with right format
    (model)
    */
    component.files = [];
    component.data.category = 'model';
    testFiles = [
      {
        name: 'test.wrl',
      },
    ];
    compared = [
      {
        name: 'test.wrl',
        progress: 0,
      },
    ];
    component.prepareFilesList(testFiles);
    expect(component.files).toBeTruthy();
    expect(component.files).toEqual(compared);

    /*
    test only one file with wrong format
    (model)
    */
    component.files = [];
    component.data.category = 'model';
    testFiles = [
      {
        name: 'test.mlrpior',
      },
    ];
    component.prepareFilesList(testFiles);
    expect(component.files).toBeTruthy();
    expect(component.files).toEqual([]);

    /*
    test only one file with multi file
    (model)
    */
    component.files = [];
    component.data.category = 'model';
    testFiles = [
      {
        name: 'test.wrl',
      },
      {
        name: 'testiyde.wrl',
      },
    ];
    compared = [
      {
        name: 'test.wrl',
        progress: 0,
      },
      {
        name: 'testiyde.wrl',
        progress: 0,
      },
    ];
    component.prepareFilesList(testFiles);
    expect(component.files).toBeTruthy();
    expect(component.files).toEqual(compared);

    /*
    test only one file with file & colormap
    (model)
    */
    component.files = [];
    component.data.category = 'model';
    testFiles = [
      {
        name: 'test.colormap',
      },
      {
        name: 'test.wrl',
      },
    ];
    compared = [
      {
        name: 'test.wrl',
        progress: 0,
      },
      {
        name: 'test.colormap',
        progress: 0,
      },
    ];
    component.prepareFilesList(testFiles);
    expect(component.files).toBeTruthy();
    expect(component.files).toEqual(compared);

    /*
    test only one file with multi file & colormap
    (model)
    */
    component.files = [];
    component.data.category = 'model';
    testFiles = [
      {
        name: 'test.colormap',
      },
      {
        name: 'test.wrl',
      },
      {
        name: 'random.colormap',
      },
      {
        name: 'random.wrl',
      },
    ];
    compared = [
      {
        name: 'test.wrl',
        progress: 0,
      },
      {
        name: 'test.colormap',
        progress: 0,
      },
      {
        name: 'random.wrl',
        progress: 0,
      },
      {
        name: 'random.colormap',
        progress: 0,
      },
    ];
    component.prepareFilesList(testFiles);
    expect(component.files).toBeTruthy();
    expect(component.files).toEqual(compared);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should format bytes', () => {
    const bytes = 8486127321;
    const formated = component.formatBytesAbstraction(bytes);
    expect(formated).toEqual('7.9 GB');
  });

  it('should upload model files ', () => {
    component.data.category = 'model';
    component.files = [
      {
        name: 'Test.vrl',
      },
    ];

    component.uploadFiles(0);
    expect(component.plotImageTcmUpload).toHaveBeenCalledTimes(0);
    expect(component.modelTreating).toHaveBeenCalledTimes(1);
  });

  it('should treat error', () => {
    const errorMock = {
      error: {
        message: '505; deliberate error',
        status: 505,
      },
    };
    component.treatError(errorMock);
    expect(component.errorStatus).toEqual(true);
    expect(component.errorMessage).toEqual('505');
    expect(component.errorStatusCode).toEqual(505);
  });

  it('should format progress', () => {
    const eventMock = {
      type: HttpEventType.UploadProgress,
      loaded: 32,
      total: 100,
    };
    const file = {
      progress: 0,
    };
    component.formatRequestProgress(eventMock, file);
    expect(file.progress).toEqual(32);
  });
});
