import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Globals } from 'src/app/globals/globals';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { DialogDataToInject } from 'src/app/interfaces/dialog-data/dialog-data-to-inject';
import { FileManipulationsService } from 'src/app/services/file-operations/file-manipulations.service';
import { DialogNotificationService } from 'src/app/services/dialog-notification/dialog-notification.service';
import { VisualizerService } from 'src/app/services/visualize/visualizer.service';
import { FileTransferService } from 'src/app/services/file-transfer/file-transfer.service';
import { VrmlFile } from 'src/app/interfaces/vrml-file/vrml-file';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.less'],
})
export class FileUploadComponent {
  @ViewChild('fileDropRef') fileDropEl: ElementRef;
  files: any[] = [];
  errorStatus = false;
  public restriction = '';
  errorMessage = '';
  errorStatusCode: number;

  constructor(
    public dialogRef: MatDialogRef<FileUploadComponent>,
    private fileService: FileManipulationsService,
    @Inject(MAT_DIALOG_DATA) public data: DialogDataToInject,
    private dialogNotification: DialogNotificationService,
    private visualizerService: VisualizerService,
    private fileTransfer: FileTransferService
  ) {}

  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  deleteFile(index: number) {
    if (this.files[index].progress < 100) {
      return;
    }
    this.files.splice(index, 1);
  }

  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      item.progress = 0;
      if (this.data.category) {
        // Restrict files by extension
        this.getAcceptedFilesFilter()
          .split(', ')
          .forEach((extension) => {
            if (item.name.includes(extension)) {
              this.files.push(item);
            }
          });
      } else {
        // Free upload into Inputs / Outputs folder
        this.files.push(item);
      }
    }
    if (this.files instanceof Array && this.files.length > 1) {
      this.files.sort(this.sorter);
    }
    this.fileDropEl.nativeElement.value = '';
  }

  private sorter = (a: File, b: File): number => {
    const aSplit = a.name.split('.');

    const bSplit = b.name.split('.');

    if (aSplit[0] === bSplit[0] && aSplit[1] === 'colormap') {
      return 1;
    } else if (aSplit[0] === bSplit[0] && bSplit[1] === 'colormap') {
      return -1;
    } else {
      return 0;
    }
  }

  /* upload file according to the category*/
  uploadFiles(index: number) {
    if (this.data.category && this.data.category === 'model') {
      this.modelTreating();
      return;
    }
    this.files.forEach((file) => {
      if (this.data.category) {
        this.plotImageTcmUpload(file);
      } else {
        this.fileService
          .uploadToServerWithPath(this.data.projectId, this.data.path, file)
          .subscribe(
            (event: any) => this.formatRequestProgress(event, file),
            (error) => this.treatError(error)
          );
      }
    });
  }

  plotImageTcmUpload(file: any) {
    this.fileService
      .uploadToServerWithCategory(
        this.data.projectId,
        this.data.category,
        file,
        localStorage.getItem(Globals.USER_ID)
      )
      .subscribe(
        (event: any) => this.formatRequestProgress(event, file),
        (error) => this.treatError(error)
      );
  }

  treatError(error) {
    this.errorStatus = true;
    this.errorMessage = error.error.message.split(';')[0];
    this.errorStatusCode = error.error.status;
  }

  formatRequestProgress(event: any, file: any) {
    if (event.type === HttpEventType.UploadProgress) {
      file.progress = Math.round((100 * event.loaded) / event.total);
    } else if (event instanceof HttpResponse) {
      if (event.status === 201) {
        this.dialogNotification.updateFileNotif(event.body);
      }
      this.dialogNotification.triggerRefresh(true);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  /* upload file filters*/
  getAcceptedFilesFilter = (): string => {
    if (this.data.category) {
      switch (this.data.category) {
        case 'image':
          this.restriction = 'Only images';
          return '.jpg, .jpeg, .png';
        case 'plot':
          this.restriction = 'Only .csv';
          return '.csv';
        case 'progress':
          this.restriction = 'Only .csv';
          return '.csv';
        case 'model':
          this.restriction = 'Models & Colormaps';
          // TODO modify to models & colormap extensions
          return '.vrml, .colormap, .wrl';
      }
    } else {
      return '*/*';
    }
  }

  private _updateProgress(fileprogress: VrmlFile, progress: boolean = false) {
    const el = this.files.filter((e) => e.uuid === fileprogress.uuid)[0];
    if (!progress) {
      this.files[this.files.indexOf(el)].progress =
        (parseFloat(fileprogress.status.replace(' MB', '')) /
          (el.size / 1000000)) *
        100;
    } else {
      this.files[this.files.indexOf(el)].progress = 100;
    }
  }

  modelTreating() {
    this.visualizerService.getState().subscribe((fileprogress: VrmlFile) => {
      if (fileprogress !== null) {
        if (typeof fileprogress.status === 'undefined') {
          this.files[
            this.files.indexOf(
              this.files.filter((e) => e.name === fileprogress.name)[0]
            )
          ].uuid = fileprogress.uuid;
        } else if (fileprogress.status.includes('MB')) {
          this._updateProgress(fileprogress);
        } else if (fileprogress.status === 'converting') {
          this._updateProgress(fileprogress, true);
        }
      }
    });
    this.fileTransfer.modelUploads(this.files);
  }

  formatBytesAbstraction(bytes, decimals = 2) {
    return Globals.formatBytes(bytes, decimals);
  }
}
