import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationData } from 'src/app/interfaces/confirmation-data/confirmation-data';
import { DriveFile } from 'src/app/interfaces/drive-project-info/dc-project';
import { ProjectFileDisplayed } from 'src/app/sub-modules/vision/interfaces/project-file-displayed/project-file-displayed';

@Component({
  selector: 'app-deletion-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.less'],
})
export class ConfirmationComponent {
  title: string;
  message: string;
  array: DriveFile[] | ProjectFileDisplayed[];

  constructor(
    public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmationData
  ) {
    this.title = data.title;
    if (!data.arrayOfFiles) {
      this.message = data.message;
    }
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }

  onConfirm(): void {
    this.dialogRef.close(true);
  }
}
