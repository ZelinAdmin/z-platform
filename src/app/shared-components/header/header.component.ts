import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { UserService } from '../../services/user/user.service';
import { Globals } from '../../globals/globals';
import { UserInformation } from '../../interfaces/user-information/user-information';
import { Router, NavigationEnd } from '@angular/router';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface Selection {
  iconLabel: string;
  text: string;
  action: (router: Router, snackbar: MatSnackBar) => void;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  // public translatePage: ComponentsTrslt = ComponentsTrslt.HEADER;
  public isAdmin = false; // Boolean to display or not panel admin button
  public isEngineering = false; // Boolean to display or not Z-Drive button
  public name: string; // Name to display the very right of the bar
  public photoUrl: string; // Url of the profile photo
  public nbrNotif = 0;

  public Map = new Map<string, Selection>();

  public tmp: Selection[] = [
    {
      iconLabel: 'home',
      text: 'Home ',
      action: this.zcommunity,
    },
    {
      iconLabel: 'laptop_mac',
      text: 'Z-Vision',
      action: this.zvision,
    },
    {
      iconLabel: 'cloud',
      text: 'Z2C',
      action: this.z2c,
    },
    {
      iconLabel: 'group',
      text: 'Z-Community',
      action: this.experts,
    },
    {
      iconLabel: 'support',
      text: 'Z-Ticket',
      action: this.inComing,
    },
    {
      iconLabel: 'school',
      text: 'Z-Learning',
      action: this.inComing,
    },
  ];

  public selected = this.tmp[0]; // Default value for pages select

  constructor(
    // public translateService: TranslateService,
    public router: Router,
    public snackbar: MatSnackBar,
    public auth: AuthService,
    private userService: UserService
  ) {
    this.tmp.forEach((e) => this.Map.set(e.text, e));
    router.events.subscribe(
      (event) => event instanceof NavigationEnd && this.handleRouteChange()
    );
  }

  ngOnInit() {
    this.auth.isLoggedIn.subscribe((state) => {
      if (!state) {
        return;
      }
      if (state) {
        this.userService
          .getIsAdmin(localStorage.getItem(Globals.USER_ID))
          .subscribe((data) => {
            if (!data) {
              return;
            }
            this.isAdmin = data;
          });
        this.fetchUserInformation();
      }
    });
  }

  public handleRouteChange = () => {
    if (this.router.url.includes('/vision')) {
      this.isEngineering = true;
    } else {
      this.isEngineering = false;
    }
  }

  fetchUserInformation() {
    this.userService
      .getPartial(localStorage.getItem(Globals.USER_ID))
      .subscribe((data) => {
        if (!data) {
          return;
        }
        this.name = data.firstName + ' ' + data.lastName;
        if (data.image != null) {
          this.photoUrl = `${environment.apiUrl}files/${data.image}/download`;
        }else{
          this.photoUrl = 'assets/fallBackUser.png';
        }

      });
  }

  public onChangeSelectPage(value: string, debug = false) {
    this.selected = this.Map.get(value);
    if (!debug) {
      document.getElementById('selectPage').style.width =
        (8 * value.length + 65).toString() + 'px';
      document.getElementById('pageList').style.width =
        (8 * value.length + 55).toString() + 'px';
    }

    this.Map.get(value).action(this.router, this.snackbar);
  }

  zvision(router: Router, snackbar: MatSnackBar) {
    this.isEngineering = true;
    router.navigate(['/vision']);
  }

  zcommunity(router: Router, snackbar: MatSnackBar) {
    router.navigate(['home']);
  }

  z2c(router: Router, snackbar: MatSnackBar) {
    router.navigate(['z2c']);
  }

  experts(router: Router, snackbar: MatSnackBar) {
    router.navigate(['/experts']);
  }

  profile(router: Router, snackbar: MatSnackBar) {
    router.navigate(['profile']);
  }

  inComing(router: Router, snackbar: MatSnackBar) {
    snackbar.open('Coming soon', '', {
      duration: 2000,
      verticalPosition: 'top',
    });
  }

  zDrive() {
    this.router.navigate(['/drive']);
  }

  notification() {
    this.snackbar.open('Coming soon', '', {
      duration: 2000,
      verticalPosition: 'top',
    });
  }

  adminPanel() {
    this.router.navigate(['/admin']);
  }

  zcompute() {
    this.snackbar.open('Coming soon', '', {
      duration: 2000,
      verticalPosition: 'top',
    });
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['login']);
  }
}
