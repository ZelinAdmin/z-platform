import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from 'src/app/services/user/user.service';
import { environment } from 'src/environments/environment';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  const routeSpy = {
    events: {
      subscribe: () => 'true',
      unsubscribe: () => true,
    },
    navigate: (_) => {},
  };
  const snackBarSpy = jasmine.createSpyObj('MatSnackBar', ['open']);
  const authSpy = {
    isLoggedIn: {
      subscribe: () => true,
      unsubscribe: () => true,
    },
  };
  const userSpy = {
    getIsAdmin: (_) => ({
      subscribe: () => true,
      unsubscribe: () => true,
    }),
    getPartial: (_) => ({
      subscribe: () => {
        return {
          firstName: 'Jhon',
          lastName: 'Jhonny',
          image: 'random',
        };
      },
      unsubscribe: () => true,
    }),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [
        { provide: Router, useValue: routeSpy },
        { provide: MatSnackBar, useValue: snackBarSpy },
        { provide: AuthService, useValue: authSpy },
        { provide: UserService, useValue: userSpy },
      ],
      imports: [
        MatGridListModule,
        MatCardModule,
        MatIconModule,
        MatToolbarModule,
        MatDividerModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(component, 'fetchUserInformation');
    spyOn(component, 'experts');
    spyOn(component, 'zvision');
    spyOn(component, 'z2c');
    spyOn(routeSpy, 'navigate');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should get data for admin', () => {
  //   // expect(component).toBeTruthy();
  //   // setTimeout(() => {
  //   //   expect(component.isAdmin).toEqual(true);
  //   //   expect(component.name).toEqual('Jhon Jhonny');
  //   //   expect(component.photoUrl).toEqual(`${environment.apiUrl}files/random/download`);
  //   // }, 200);
  // });

  it('should navigation to zvision', () => {
    component.onChangeSelectPage('Z-Vision', true);
    expect(routeSpy.navigate).toHaveBeenCalledTimes(1);
    expect(routeSpy.navigate).toHaveBeenCalledWith(['/vision']);
  });

  it('should navigation to z2c', () => {
    component.onChangeSelectPage('Z2C', true);
    expect(routeSpy.navigate).toHaveBeenCalledTimes(1);
    expect(routeSpy.navigate).toHaveBeenCalledWith(['z2c']);
  });

  it('should navigation to community', () => {
    component.onChangeSelectPage('Z-Community', true);
    expect(routeSpy.navigate).toHaveBeenCalledTimes(1);
    expect(routeSpy.navigate).toHaveBeenCalledWith(['/experts']);
  });
});
