import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Expert } from 'src/app/interfaces/expert/expert';
import { ReferenceArea } from 'src/app/interfaces/reference-area/reference-area';
import { AreaExpert } from 'src/app/interfaces/area-expert/area-expert';

@Injectable({
  providedIn: 'root',
})
export class ExpertService {
  constructor(private http: HttpClient) {}

  getAll(params: HttpParams | null = null): Observable<any> {
    return this.http.get<Expert>(`${environment.apiUrl}experts`, { params });
  }

  get(usertId: string): Observable<Expert> {
    return this.http.get<Expert>(`${environment.apiUrl}expert/${usertId}`);
  }

  getByUserId(userId: string): Observable<Expert> {
    return this.http.get<Expert>(`${environment.apiUrl}expert/user/${userId}`);
  }

  getAllByAvailable(available: boolean): Observable<Expert[]> {
    return this.http.get<Expert[]>(
      `${environment.apiUrl}expert/available/${available}`
    );
  }

  // Get Experts, only 3 if admin / premium and ordered
  getFiltered(params?: HttpParams): Observable<any>{

    if (params != null ) {
      return this.http.post<Expert>(
        `${environment.apiUrl}users/filtered`,
        params
      );
    } else {
      return this.http.post<Expert>(
        `${environment.apiUrl}users/filtered`,
        {}
      );
    }
  }

  updateAvailability(userId: string, newValue: boolean): Observable<Expert> {
    return this.http.put<Expert>(
      `${environment.apiUrl}expert/${userId}/available/${newValue}`,
      null
    );
  }

  updateFixedPriceStudies(userId: string, newValue: boolean): Observable<Expert> {
    return this.http.put<Expert>(
      `${environment.apiUrl}expert/${userId}/fixedprice/${newValue}`,
      null
    );
  }

  updateTechAssistance(userId: string, newValue: boolean): Observable<Expert> {
    return this.http.put<Expert>(
      `${environment.apiUrl}expert/${userId}/techassistance/${newValue}`,
      null
    );
  }

  deleteExpert(usertId: string): Observable<Expert> {
    return this.http.delete<Expert>(
      `${environment.apiUrl}admin/experts/${usertId}`
    );
  }

  post(model: Expert): Observable<Expert> {
    return this.http.post<Expert>(`${environment.apiUrl}/admin/expert`, {
      User: model,
    });
  }

  updateUser(id: string, expert: Expert): Observable<Expert> {
    return this.http.put<Expert>(
      `${environment.apiUrl}expert/update/${id}`,
      expert
    );
  }

  createArea(name: string) {
    return this.http.post<ReferenceArea>(
      `${environment.apiUrl}admin/area/create/${name}`,
      {}
    );
  }

  addAreaToExpert(usertId: string, areaId: string) {
    return this.http.post<AreaExpert>(
      `${environment.apiUrl}expert/area/add/${usertId}/${areaId}`,
      {}
    );
  }

  getAreas(usertId: string) {
    return this.http.get<any>(`${environment.apiUrl}expert/area/${usertId}`);
  }

  getAreasNames(usertId: string) {
    return this.http.get<any>(
      `${environment.apiUrl}expert/area/names/${usertId}`
    );
  }

  getAllAreas() {
    return this.http.get<any>(`${environment.apiUrl}area`, {});
  }
}
