import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Credit } from 'src/app/interfaces/credit/credit';
import { CreditsUser } from 'src/app/interfaces/credits-user/credits-user';
import { CreditsOperationRequest } from 'src/app/interfaces/credits-operation-request/credits-operation-request';

@Injectable({
  providedIn: 'root',
})
export class CreditService {
  constructor(private http: HttpClient) {}
  getAll(params: HttpParams | null = null): Observable<any> {
    return this.http.get<Credit>(`${environment.apiUrl}admin/credits`, {
      params,
    });
  }

  getAllUsersWithOperations(params: HttpParams | null = null): Observable<any> {
    return this.http.get<CreditsUser>(
      `${environment.apiUrl}admin/credits/users`,
      { params }
    );
  }

  getByUserId(
    userId: string,
    params: HttpParams | null = null
  ): Observable<any> {
    return this.http.get<Credit>(
      `${environment.apiUrl}credits/user_id/${userId}`,
      { params }
    );
  }

  add(credit: CreditsOperationRequest): Observable<any> {
    return this.http.post(`${environment.apiUrl}admin/credits`, {
      userId: credit.userId,
      amount: credit.amount,
      operationDate: credit.operationDate,
      creditsAfter: credit.creditsAfter,
    });
  }
}
