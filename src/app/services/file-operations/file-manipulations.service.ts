import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { DcProject } from 'src/app/interfaces/drive-project-info/dc-project';
import { ProjectFileDisplayed } from 'src/app/sub-modules/vision/interfaces/project-file-displayed/project-file-displayed';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FileManipulationsService {
  constructor(private http: HttpClient) {}

  getFilesFromPath(path: string, userId: string) {
    return this.http.get<DcProject>(`${environment.apiUrl}drive/${path}`, {
      headers: { UserId: userId },
    });
  }

  uploadToServerWithPath(projectId: string, path: string, file: any) {
    const formData = new FormData();
    formData.append('file', file);

    const req = new HttpRequest(
      'POST',
      `${environment.apiUrl}drive/${path}`,
      formData,
      {
        reportProgress: true,
      }
    );
    return this.http.request(req);
  }

  deleteFile(pathToFile: string) {
    return this.http.delete<string>(`${environment.apiUrl}drive/${pathToFile}`);
  }

  downloadFile(pathToFile: string) {
    return this.http.get(`${environment.apiUrl}drive-download/${pathToFile}`, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  download(fileId: string, options = {}) {
    const url = `${environment.apiUrl}files/${fileId}/download`;
    return this.http.get(url, { responseType: 'text', ...options });
  }

  createFolder(path: string, folderName: string) {
    const formData = new FormData();
    formData.append('folderName', folderName);
    return this.http.put(`${environment.apiUrl}drive/${path}`, formData);
  }

  uploadToServerWithCategory(
    projectId: string,
    category: string,
    file: any,
    userId: string
  ) {
    const formData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders({ UserId: userId });
    const request = new HttpRequest(
      'POST',
      `${environment.apiUrl}projects/${projectId}/file/${category}`,
      formData,
      {
        reportProgress: true,
        headers,
      }
    );
    return this.http.request(request);
  }

  deleteFilesWithinCategory(projectId: string, fileId: string) {
    return this.http.delete<string>(
      `${environment.apiUrl}projects/${projectId}/file/${fileId}`
    );
  }

  edit(f: ProjectFileDisplayed, projectId: string, line: string) {
    const url = `${environment.apiUrl}projects/${projectId}/file/${f.internalId}/edit`;
    return this.http.post(url, line).subscribe((data) => data);
  }

  public uploadProfilePhoto(file: File, userId: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('avatar', file, file.name);

    return this.http.post<any>(
      `${environment.apiUrl}user/${userId}/avatar`,
      formData
    );
  }

  public uploadProjectPhoto(file: File, projectId: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('avatar', file, file.name);

    return this.http.post<any>(
      `${environment.apiUrl}projects/${projectId}/avatar`,
      formData
    );
  }
}
