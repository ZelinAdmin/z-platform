import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(private httpClient: HttpClient) {}

  public getAllLanguages(): Observable<Array<any>> {
    return this.httpClient.get<Array<any>>(
      `${environment.apiUrl}language`
    );
  }

  public getLanguagesByUserId(id: string): Observable<Array<any>> {
    return this.httpClient.get<Array<any>>(
      `${environment.apiUrl}languages/user/${id}`
    );
  }

}
