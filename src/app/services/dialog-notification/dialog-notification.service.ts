import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProjectFile } from 'src/app/sub-modules/vision/interfaces/project-file/project-file';

@Injectable({
  providedIn: 'root',
})
export class DialogNotificationService {
  private status = new BehaviorSubject<ProjectFile>(null);
  private refresh = new BehaviorSubject<boolean>(null);

  constructor() {}

  updateFileNotif(value: ProjectFile) {
    this.status.next(value);
  }

  getFileNotif() {
    return this.status.asObservable();
  }

  triggerState(value) {
    this.status.next(value);
  }

  getTrigger(): Observable<any> {
    return this.status.asObservable();
  }

  triggerRefresh(value: boolean) {
    this.refresh.next(value);
  }

  getRefreshStatus() {
    return this.refresh.asObservable();
  }
}
