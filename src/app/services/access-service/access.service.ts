import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ProjectUsersReduced } from 'src/app/sub-modules/vision/interfaces/project-users-reduced/project-users-reduced';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';

@Injectable({
  providedIn: 'root',
})
export class AccessService {
  constructor(private http: HttpClient) {}

  getAllProjectUsers(projectId: string): Observable<ProjectUsersReduced[]> {
    return this.http.get<ProjectUsersReduced[]>(
      `${environment.apiUrl}project/${projectId}/users`
    );
  }

  getAll(projectId: string): Observable<UserProject[]> {
    return this.http.get<UserProject[]>(
      `${environment.apiUrl}admin/projects/${projectId}/users`
    );
  }

  get(projectId: string, userId: string): Observable<UserProject> {
    return this.http.get<UserProject>(
      `${environment.apiUrl}admin/projects/${projectId}/users/${userId}`
    );
  }

  delete(projectId: string, userId: string): Observable<any> {
    return this.http.delete(
      `${environment.apiUrl}admin/projects/${projectId}/users/${userId}`
    );
  }

  add(projectId: string, userId: string, role: string): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}admin/projects/${projectId}/users/${userId}`,
      { role }
    );
  }

  update(projectId: string, userId: string, role: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put(
      `${environment.apiUrl}admin/projects/${projectId}/users/${userId}`,
      { role },
      { headers }
    );
  }
}
