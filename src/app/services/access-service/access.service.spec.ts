import { TestBed } from '@angular/core/testing';

import { AccessService } from './access.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { ProjectUsersReduced } from 'src/app/sub-modules/vision/interfaces/project-users-reduced/project-users-reduced';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';

describe('AccessService', () => {
  let service: AccessService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccessService],
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(AccessService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterAll(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all users of a projet in reduced form', () => {
    const projectId = 'randomId';
    const awaitedData: ProjectUsersReduced[] = [
      {
        role: 'Customer',
        user: {
          id: 'rerez',
          email: 'email@test.com',
          firstName: 'Jhonny',
          lastName: 'Lanny',
          phone: 98765,
        },
      },
      {
        role: 'MANAGER',
        user: {
          id: 'rerez',
          email: 'email@test43.com',
          firstName: 'Foo',
          lastName: 'Bar',
          phone: 94325,
        },
      },
    ];

    service.getAllProjectUsers(projectId).subscribe((info) => {
      expect(info).toBeTruthy();
      expect(info).toBe(awaitedData);
    });
    /* await request */
    const httpRequest = httpMock.expectOne(
      `${environment.apiUrl}project/${projectId}/users`
    );
    /* mock response to request*/
    httpRequest.flush(awaitedData);
  });

  it('should get all users of a projet', () => {
    const projectId = 'randomId';
    const awaitedData: UserProject[] = [
      {
        project: {
          progress: '44',
        },
        role: 'Customer',
        user: {
          email: 'email@test.com',
          firstName: 'Jhonny',
          lastName: 'Lanny',
        },
      },
      {
        project: {
          progress: '44',
        },
        role: 'MANAGER',
        user: {
          email: 'email@test43.com',
          firstName: 'Foo',
          lastName: 'Bar',
        },
      },
    ];

    service.getAll(projectId).subscribe((info) => {
      expect(info).toBeTruthy();
      expect(info).toBe(awaitedData);
    });
    /* await request */
    const httpRequest = httpMock.expectOne(
      `${environment.apiUrl}admin/projects/${projectId}/users`
    );
    /* mock response to request*/
    httpRequest.flush(awaitedData);
  });

  it('should get user in a project', () => {
    const projectId = 'randomId';
    const userId = 'randomUser';
    const awaitedData: UserProject = {
      project: {
        progress: '44',
      },
      role: 'Customer',
      user: {
        email: 'email@test.com',
        firstName: 'Jhonny',
        lastName: 'Lanny',
      },
    };

    service.get(projectId, userId).subscribe((info) => {
      expect(info).toBeTruthy();
      expect(info).toBe(awaitedData);
    });
    /* await request */
    const httpRequest = httpMock.expectOne(
      `${environment.apiUrl}admin/projects/${projectId}/users/${userId}`
    );
    /* mock response to request*/
    httpRequest.flush(awaitedData);
  });
});
