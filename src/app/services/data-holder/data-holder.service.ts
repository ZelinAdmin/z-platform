import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataHolderService {
  private projectIdSubject = new BehaviorSubject<string>(null);

  constructor() {}

  setProjectId(projectId: string) {
    this.projectIdSubject.next(projectId);
  }

  get projectId() {
    return this.projectIdSubject.getValue();
  }

  get projectIdAsObservable(): Observable<string> {
    return this.projectIdSubject.asObservable();
  }
}
