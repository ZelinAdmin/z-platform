import { Injectable } from '@angular/core';
import { VisualizerService } from 'src/app/services/visualize/visualizer.service';

@Injectable({
  providedIn: 'root',
})
export class FileTransferService {
  constructor(private visualizerService: VisualizerService) {}

  public modelUploads(files: Array<File>) {
    files.forEach((file) => this.visualizerService.startTransfer(file));
  }
}
