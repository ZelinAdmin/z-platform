import { TestBed } from '@angular/core/testing';

import { AlertService } from './alert.service';
import { NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs';

class MockRouter {
  public ne = new NavigationEnd(
    0,
    'http://localhost:4200/login',
    'http://localhost:4200/login'
  );
  public events = new Observable((observer) => {
    observer.next(this.ne);
    observer.complete();
  });
}

class MockRouterNoLogin {
  public ne = new NavigationEnd(
    0,
    'http://localhost:4200/dashboard',
    'http://localhost:4200/dashboard'
  );
  public events = new Observable((observer) => {
    observer.next(this.ne);
    observer.complete();
  });
}

describe('AlertServiceService', () => {
  const routerSpy = {
    events: Observable,
  };
  let alertService: AlertService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlertService, { provide: Router, useClass: MockRouter }],
    });
  });

  it('should be created', () => {
    alertService = TestBed.get(AlertService);
    expect(alertService).toBeTruthy();
  });
});
