import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  constructor() {}

  private currentLanguage: Language = Language.ENGLISH;
  private pages: Array<any> = [
    [
      ComponentsTrslt.LOGIN,
      {
        login: [
          [Language.FRENCH, 'Connexion'],
          [Language.ENGLISH, 'Login'],
        ],
        email: [
          [Language.FRENCH, 'Adresse mail'],
          [Language.ENGLISH, 'Mail address'],
        ],
        emailDesc: [
          [
            Language.FRENCH,
            "Utilisez l'adresse email renseignée lors de la création de compte",
          ],
          [Language.ENGLISH, "Use the email address you've used to register"],
        ],
        password: [
          [Language.FRENCH, 'Mot de passe'],
          [Language.ENGLISH, 'Password'],
        ],
        passwordDesc: [
          [Language.FRENCH, 'Visibilité du mot de passe'],
          [Language.ENGLISH, 'Toggle password visibility'],
        ],
        createAccount: [
          [Language.FRENCH, 'Créer un compte'],
          [Language.ENGLISH, 'Create your account'],
        ],
      },
    ],
    [
      ComponentsTrslt.SIGNUP,
      {
        login: [
          [Language.FRENCH, 'Connexion'],
          [Language.ENGLISH, 'Login'],
        ],
        email: [
          [Language.FRENCH, 'Adresse mail'],
          [Language.ENGLISH, 'Mail address'],
        ],
        emailDesc: [
          [
            Language.FRENCH,
            "Utilisez l'adresse email renseignée lors de la création de compte",
          ],
          [Language.ENGLISH, "Use the email address you've used to register"],
        ],
        password: [
          [Language.FRENCH, 'Mot de passe'],
          [Language.ENGLISH, 'Password'],
        ],
        passwordDesc: [
          [Language.FRENCH, 'Visibilité du mot de passe'],
          [Language.ENGLISH, 'Toggle password visibility'],
        ],
        passwordRepeat: [
          [Language.FRENCH, 'Retapez votre mot de passe'],
          [Language.ENGLISH, 'Repeat password'],
        ],
        createAccount: [
          [Language.FRENCH, 'Créer un compte'],
          [Language.ENGLISH, 'Create an account'],
        ],
        firstName: [
          [Language.FRENCH, 'Prénom'],
          [Language.ENGLISH, 'First Name'],
        ],
        lastName: [
          [Language.FRENCH, 'Nom de famille'],
          [Language.ENGLISH, 'Last Name'],
        ],
        return: [
          [Language.FRENCH, 'Retour'],
          [Language.ENGLISH, 'Return'],
        ],
        imClient: [
          [Language.FRENCH, "Je suis à la recherche d'experts"],
          [Language.ENGLISH, "I'm looking for experts"],
        ],
        imExpert: [
          [Language.FRENCH, "Je suis un expert à la recherche d'offres"],
          [Language.ENGLISH, "I'm an expert looking for offers"],
        ],
        signupFail: [
          [
            Language.FRENCH,
            'Création de compte échouée, verifiez vos informations.',
          ],
          [Language.ENGLISH, 'Signup failed, please verify your informations.'],
        ],
        signupSuccess: [
          [
            Language.FRENCH,
            ' - Création de compte réussie, verifiez votre addresse e-mail.',
          ],
          [
            Language.ENGLISH,
            ' - Signup successful, please verify your email address.',
          ],
        ],
      },
    ],
    [ComponentsTrslt.HOME],
    [
      ComponentsTrslt.BROWSE_EXPERT,
      {
        nameFilter: [
          [Language.FRENCH, 'Nom'],
          [Language.ENGLISH, 'Name'],
        ],
        availabilityFilter: [
          [Language.FRENCH, 'Disponible dès à présent'],
          [Language.ENGLISH, 'Available now'],
        ],
        rateMin: [
          [Language.FRENCH, 'Notation minimum'],
          [Language.ENGLISH, 'Minimum rating'],
        ],
        maxPrice: [
          [Language.FRENCH, 'Prix maximum'],
          [Language.ENGLISH, 'Maximum price'],
        ],
        profilsFound: [
          [Language.FRENCH, 'profil(s) trouvé(s)'],
          [Language.ENGLISH, 'profile(s) found'],
        ],
        filtersTitle: [
          [Language.FRENCH, 'Filtres'],
          [Language.ENGLISH, 'Filters'],
        ],
        expertAvailable: [
          [Language.FRENCH, 'Disponible'],
          [Language.ENGLISH, 'Available'],
        ],
        noProfilesFound: [
          [Language.FRENCH, 'Aucun expert trouvé pour ces filtres'],
          [Language.ENGLISH, 'No experts corresponding to filters'],
        ],
        softwaresSelection: [
          [Language.FRENCH, 'Logiciel'],
          [Language.ENGLISH, 'Software'],
        ],
        expertUnavailable: [
          [Language.FRENCH, 'Indisponible'],
          [Language.ENGLISH, 'Unavailable'],
        ],
        expertTechnicalAssistance: [
          [Language.FRENCH, 'Assistance technique'],
          [Language.ENGLISH, 'Technical assistance'],
        ],
        expertWage: [
          [Language.FRENCH, 'Tarif'],
          [Language.ENGLISH, 'Wage'],
        ],
        expertWageUnity: [
          [Language.FRENCH, 'jour'],
          [Language.ENGLISH, 'day'],
        ],
        expertRanking: [
          [Language.FRENCH, 'Notation'],
          [Language.ENGLISH, 'Rating'],
        ],
        physics: [
          [Language.FRENCH, 'Physiques'],
          [Language.ENGLISH, 'Physics'],
        ],
        domains: [
          [Language.FRENCH, 'Secteurs'],
          [Language.ENGLISH, 'Domains'],
        ],
        languages: [
          [Language.FRENCH, 'Langue'],
          [Language.ENGLISH, 'Language'],
        ],
        resetFilters: [
          [Language.FRENCH, 'Réinitialiser filtres'],
          [Language.ENGLISH, 'Reset Filters'],
        ],
        noCommunityAccessOne: [
          [Language.FRENCH, `Vous n'avez pas encore accès à Z - Community`],
          [Language.ENGLISH, `You don't seem to have access to Z - Community `],
        ],
        noCommunityAccessLink: [
          [Language.FRENCH, `Contactez-nous`],
          [Language.ENGLISH, `Contact us`],
        ],
        noCommunityAccessTwo: [
          [Language.FRENCH, ` pour en savoir plus !`],
          [Language.ENGLISH, ` to learn more !`],
        ],
        noPremiumAccessOne: [
          [Language.FRENCH, `Vous voulez pouvoir consulter plus d'experts ?`],
          [Language.ENGLISH, `Want to see more experts ? `],
        ],
        noPremiumAccessLink: [
          [Language.FRENCH, `Contactez-nous`],
          [Language.ENGLISH, `Contact us`],
        ],
        noPremiumAccessTwo: [
          [Language.FRENCH, ` et découvrez notre offre Premium Community !`],
          [
            Language.ENGLISH,
            ` and learn more about our Premium Community offer !`,
          ],
        ],
      },
    ],
    [
      ComponentsTrslt.PROFILE,
      {
        company: [
          [Language.FRENCH, 'Entreprise'],
          [Language.ENGLISH, 'Company'],
        ],
        service: [
          [Language.FRENCH, 'Service'],
          [Language.ENGLISH, 'Service'],
        ],
        rating: [
          [Language.FRENCH, 'Note'],
          [Language.ENGLISH, 'Rating'],
        ],
        expertStudies: [
          [Language.FRENCH, 'Etudes au forfait'],
          [Language.ENGLISH, 'Fixed price studies'],
        ],
        expertTechnicalAssistance: [
          [Language.FRENCH, 'Assistance technique'],
          [Language.ENGLISH, 'Technical assistance'],
        ],
        expertWage: [
          [Language.FRENCH, 'Tarif'],
          [Language.ENGLISH, 'Wage'],
        ],
        expertWageUnity: [
          [Language.FRENCH, 'jour'],
          [Language.ENGLISH, 'day'],
        ],
        expertRanking: [
          [Language.FRENCH, 'Notation'],
          [Language.ENGLISH, 'Ranking'],
        ],
        domains: [
          [Language.FRENCH, 'Secteurs'],
          [Language.ENGLISH, 'Domains'],
        ],
        physics: [
          [Language.FRENCH, 'Physiques'],
          [Language.ENGLISH, 'Physics'],
        ],
        available: [
          [Language.FRENCH, 'Disponible'],
          [Language.ENGLISH, 'Available'],
        ],
        yes: [
          [Language.FRENCH, 'Oui'],
          [Language.ENGLISH, 'Yes'],
        ],
        no: [
          [Language.FRENCH, 'Non'],
          [Language.ENGLISH, 'No'],
        ],
        editProfile: [
          [Language.FRENCH, 'Éditer mon profil'],
          [Language.ENGLISH, 'Edit profile'],
        ],
        addFavorites: [
          [Language.FRENCH, 'Ajouter aux favoris'],
          [Language.ENGLISH, 'Add to favorites'],
        ],
        about: [
          [Language.FRENCH, 'A propos'],
          [Language.ENGLISH, 'About'],
        ],
        aboutClient: [
          [Language.FRENCH, 'A propos du client'],
          [Language.ENGLISH, 'About client'],
        ],
        aboutExpert: [
          [Language.FRENCH, "A propos de l'expert"],
          [Language.ENGLISH, 'About expert'],
        ],
        demands: [
          [Language.FRENCH, 'Demandes'],
          [Language.ENGLISH, 'Demands'],
        ],
        demandsUser: [
          [Language.FRENCH, 'Demandes en cours'],
          [Language.ENGLISH, 'Demands in progress'],
        ],
        projects: [
          [Language.FRENCH, 'Projets'],
          [Language.ENGLISH, 'Projects'],
        ],
        expertises: [
          [Language.FRENCH, 'Expertises'],
          [Language.ENGLISH, 'Expertises'],
        ],
        profileEdition: [
          [Language.FRENCH, 'Edition de profil'],
          [Language.ENGLISH, 'Profile edition'],
        ],
        personalDatas: [
          [Language.FRENCH, 'Informations personnelles'],
          [Language.ENGLISH, 'Personal informations'],
        ],
        firstName: [
          [Language.FRENCH, 'Prénom'],
          [Language.ENGLISH, 'First name'],
        ],
        lastName: [
          [Language.FRENCH, 'Nom'],
          [Language.ENGLISH, 'Last name'],
        ],
        languages: [
          [Language.FRENCH, 'Langue(s)'],
          [Language.ENGLISH, 'Language(s)'],
        ],
        softwares: [
          [Language.FRENCH, 'Logiciels'],
          [Language.ENGLISH, 'Softwares'],
        ],
        softwaresSelection: [
          [
            Language.FRENCH,
            'Logiciels : Sélectionnez parmis les logiciels existants, ou ajoutez-en un (avec la touche Entrée)',
          ],
          [
            Language.ENGLISH,
            'Softwares : Select an existing software or add a new one (pressing Enter)',
          ],
        ],
        contactInfos: [
          [Language.FRENCH, 'Coordonnées'],
          [Language.ENGLISH, 'Contact information'],
        ],
        email: [
          [Language.FRENCH, 'Adresse mail'],
          [Language.ENGLISH, 'E-Mail address'],
        ],
        phone: [
          [Language.FRENCH, 'Téléphone'],
          [Language.ENGLISH, 'Phone'],
        ],
        title: [
          [Language.FRENCH, 'Titre'],
          [Language.ENGLISH, 'Title'],
        ],
        location: [
          [Language.FRENCH, 'Localisation'],
          [Language.ENGLISH, 'Location'],
        ],
        citizenship: [
          [Language.FRENCH, 'Nationalité'],
          [Language.ENGLISH, 'Citizenship'],
        ],
        country: [
          [Language.FRENCH, 'Pays'],
          [Language.ENGLISH, 'Country'],
        ],
        city: [
          [Language.FRENCH, 'Ville'],
          [Language.ENGLISH, 'City'],
        ],
        postal: [
          [Language.FRENCH, 'Adresse postale'],
          [Language.ENGLISH, 'Postal address'],
        ],
        zipCode: [
          [Language.FRENCH, 'Code postal'],
          [Language.ENGLISH, 'Zip code'],
        ],
        competencies: [
          [Language.FRENCH, "Secteurs d'activité"],
          [Language.ENGLISH, 'Activity area'],
        ],
        AEROSPACE: [
          [Language.FRENCH, 'Aéro-spatial'],
          [Language.ENGLISH, 'Aerospace'],
        ],
        ENERGY: [
          [Language.FRENCH, 'Énergie'],
          [Language.ENGLISH, 'Energy'],
        ],
        TRANSPORT: [
          [Language.FRENCH, 'Transports'],
          [Language.ENGLISH, 'Transports'],
        ],
        INDUSTRIES: [
          [Language.FRENCH, 'Industries'],
          [Language.ENGLISH, 'Industries'],
        ],
        cancel: [
          [Language.FRENCH, 'Annuler'],
          [Language.ENGLISH, 'Cancel'],
        ],
        modify: [
          [Language.FRENCH, 'Modifier'],
          [Language.ENGLISH, 'Apply'],
        ],
      },
    ],
    [
      ComponentsTrslt.EDIT_PROFILE_PHOTO,
      {
        changeTitle: [
          [Language.FRENCH, 'Changer de photo de profil'],
          [Language.ENGLISH, 'Change profile picture'],
        ],
        browse: [
          [Language.FRENCH, 'Rechercher'],
          [Language.ENGLISH, 'Browse'],
        ],
        cancel: [
          [Language.FRENCH, 'Annuler'],
          [Language.ENGLISH, 'Cancel'],
        ],
        modify: [
          [Language.FRENCH, 'Modifier'],
          [Language.ENGLISH, 'Apply'],
        ],
      },
    ],
    [
      ComponentsTrslt.HEADER,
      {
        panelAdmin: [
          [Language.FRENCH, 'Interface administrateur'],
          [Language.ENGLISH, 'Admin panel'],
        ],
        dashboard: [
          [Language.FRENCH, 'Tableau de bord'],
          [Language.ENGLISH, 'Dashboard'],
        ],
        notifications: [
          [Language.FRENCH, 'Notifications'],
          [Language.ENGLISH, 'Notifications'],
        ],
      },
    ],
    [
      ComponentsTrslt.FOOTER,
      {
        rights: [
          [Language.FRENCH, 'Tous droits réservés'],
          [Language.ENGLISH, 'All rights reserved'],
        ],
      },
    ],
    [
      ComponentsTrslt.FLUIDS,
      {
        fluidMeca: [
          [Language.FRENCH, 'Mécanique des fluides'],
          [Language.ENGLISH, 'Fluid mechanics'],
        ],
        aeroacoustics: [
          [Language.FRENCH, 'Aéroacoustique'],
          [Language.ENGLISH, 'Aeroacoustic'],
        ],
        aerolics: [
          [Language.FRENCH, 'Aéraulique'],
          [Language.ENGLISH, 'Aerolic'],
        ],
        aerodynamics: [
          [Language.FRENCH, 'Aérodynamique'],
          [Language.ENGLISH, 'Aerodynamic'],
        ],
        fluidDynamics: [
          [Language.FRENCH, 'Dynamique des fluides'],
          [Language.ENGLISH, 'Fluid dynamics'],
        ],
        hydraulics: [
          [Language.FRENCH, 'Hydraulique'],
          [Language.ENGLISH, 'Hydraulic'],
        ],
        hydrocarbon: [
          [Language.FRENCH, 'Hydrocarbure'],
          [Language.ENGLISH, 'Hydrocarbon'],
        ],
        hydrodynamics: [
          [Language.FRENCH, 'Hydrodynamique'],
          [Language.ENGLISH, 'Hydrodynamic'],
        ],
        neutronPhysics: [
          [Language.FRENCH, 'Physique des neutrons'],
          [Language.ENGLISH, 'Neutron physics'],
        ],
        plasma: [
          [Language.FRENCH, 'Plasma'],
          [Language.ENGLISH, 'Plasma'],
        ],
        rheology: [
          [Language.FRENCH, 'Rhéologie'],
          [Language.ENGLISH, 'Rheology'],
        ],
      },
    ],
    [
      ComponentsTrslt.THERMIC,
      {
        thermic: [
          [Language.FRENCH, 'Thermique'],
          [Language.ENGLISH, 'Thermic'],
        ],
        aerothermal: [
          [Language.FRENCH, 'Aérothermique'],
          [Language.ENGLISH, 'Aerothermal'],
        ],
        combustion: [
          [Language.FRENCH, 'Combustion'],
          [Language.ENGLISH, 'Combustion'],
        ],
        conduction: [
          [Language.FRENCH, 'Conduction'],
          [Language.ENGLISH, 'Conduction'],
        ],
        convection: [
          [Language.FRENCH, 'Convection'],
          [Language.ENGLISH, 'Convection'],
        ],
        energyMngmt: [
          [Language.FRENCH, 'Maîtrise énergétique'],
          [Language.ENGLISH, 'Energy management'],
        ],
        hydrothermal: [
          [Language.FRENCH, 'Hydrothermique'],
          [Language.ENGLISH, 'Hydrothermal'],
        ],
        thermalIns: [
          [Language.FRENCH, 'Insolation thermique'],
          [Language.ENGLISH, 'Thermal insulation'],
        ],
        radiation: [
          [Language.FRENCH, 'Rayonnement'],
          [Language.ENGLISH, 'Radiation'],
        ],
        thermodynamics: [
          [Language.FRENCH, 'Thermodynamique'],
          [Language.ENGLISH, 'Thermodynamic'],
        ],
        heatTrsf: [
          [Language.FRENCH, 'Transfère de chaleur'],
          [Language.ENGLISH, 'Heat transfer'],
        ],
      },
    ],
    [
      ComponentsTrslt.STRUCTURE,
      {
        structure: [
          [Language.FRENCH, 'Structure'],
          [Language.ENGLISH, 'Structure'],
        ],
        kinematics: [
          [Language.FRENCH, 'Cinématique'],
          [Language.ENGLISH, 'Kinematic'],
        ],
        deformations: [
          [Language.FRENCH, 'Déformations non linéaires'],
          [Language.ENGLISH, 'Nonlinear deformations'],
        ],
        design: [
          [Language.FRENCH, 'Design'],
          [Language.ENGLISH, 'Design'],
        ],
        dynamics: [
          [Language.FRENCH, 'Dynamique / Crash'],
          [Language.ENGLISH, 'Dynamic / Crash'],
        ],
        hydraulic: [
          [Language.FRENCH, 'Hydraulique'],
          [Language.ENGLISH, 'Hydraulic'],
        ],
        resistance: [
          [Language.FRENCH, 'Résistance des materiaux'],
          [Language.ENGLISH, 'Material strength'],
        ],
        statics: [
          [Language.FRENCH, 'Statique'],
          [Language.ENGLISH, 'Static'],
        ],
        vibration: [
          [Language.FRENCH, 'Vibration'],
          [Language.ENGLISH, 'Vibration'],
        ],
      },
    ],
    [
      ComponentsTrslt.ELECTROMAG,
      {
        electroMag: [
          [Language.FRENCH, 'Electro-magnétisme'],
          [Language.ENGLISH, 'Electro-magnetism'],
        ],
        arc: [
          [Language.FRENCH, 'Arc électrique'],
          [Language.ENGLISH, 'Electric arc'],
        ],
        elec: [
          [Language.FRENCH, 'Electricité'],
          [Language.ENGLISH, 'Electricity'],
        ],
        electrodynamics: [
          [Language.FRENCH, 'Electrodynamique'],
          [Language.ENGLISH, 'Electrodynamic'],
        ],
        electronics: [
          [Language.FRENCH, 'Electronique'],
          [Language.ENGLISH, 'Electronic'],
        ],
        electrostatics: [
          [Language.FRENCH, 'Electrostatique'],
          [Language.ENGLISH, 'Electrostatic'],
        ],
        magnetism: [
          [Language.FRENCH, 'Magnétisme'],
          [Language.ENGLISH, 'Magnetism'],
        ],
        magnetostatics: [
          [Language.FRENCH, 'Magnétostatique'],
          [Language.ENGLISH, 'Magnetostatic'],
        ],
      },
    ],
    [
      ComponentsTrslt.LANGUAGE,
      {
        french: [
          [Language.FRENCH, 'Français'],
          [Language.ENGLISH, 'French'],
        ],
        english: [
          [Language.FRENCH, 'Anglais'],
          [Language.ENGLISH, 'English'],
        ],
        spanish: [
          [Language.FRENCH, 'Espagnol'],
          [Language.ENGLISH, 'Spanish'],
        ],
        german: [
          [Language.FRENCH, 'Allemand'],
          [Language.ENGLISH, 'German'],
        ],
        italian: [
          [Language.FRENCH, 'Italien'],
          [Language.ENGLISH, 'Italian'],
        ],
        dutch: [
          [Language.FRENCH, 'Néerlandais'],
          [Language.ENGLISH, 'Dutch'],
        ],
      },
    ],
  ];

  public getLanguage(): Language {
    return this.currentLanguage;
  }

  public setLanguage(newLang: Language) {
    this.currentLanguage = newLang;
  }

  public getTranslation(compnt: ComponentsTrslt, key: string): string {
    const ind = this.pages.findIndex((elem) => elem[0] === compnt);

    if (this.pages[ind][1][key]) {
      const ind2 = this.pages[ind][1][key].findIndex(
        (elem) => elem[0] === this.currentLanguage
      );
      return this.pages[ind][1][key][ind2][1];
    }

    return key;
  }
}

export enum ComponentsTrslt {
  LOGIN = 0,
  SIGNUP = 1,
  HOME = 2,
  BROWSE_EXPERT = 3,
  PROFILE = 4,
  EDIT_PROFILE_PHOTO = 5,
  HEADER = 6,
  FOOTER = 7,
  FLUIDS = 8,
  THERMIC = 9,
  STRUCTURE = 10,
  ELECTROMAG = 11,
  LANGUAGE = 12,
}

export enum Language {
  ENGLISH = 0,
  FRENCH = 1,
}

export let LanguageTrad = ['french', 'english'];
