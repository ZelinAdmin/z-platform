import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserProject } from 'src/app/interfaces/user-projects/user-project';
import { ProjectUpdateInfo } from 'src/app/interfaces/project-update-info/project-update-info';
import { ProjectUser } from 'src/app/sub-modules/drive/interfaces/project-user/project-user';
import { Project } from 'src/app/interfaces/project/project';

@Injectable({
  providedIn: 'root',
})
export class ProjectService {
  constructor(private http: HttpClient) {}

  getUserProjects(userId: string): Observable<UserProject[]> {
    const url = `${environment.apiUrl}users/${userId}/projects`;
    return this.http.get<UserProject[]>(url);
  }

  getProjectInformation(
    projectId: string,
    userId: string
  ): Observable<UserProject> {
    const ur = `${environment.apiUrl}projects/${projectId}/users/${userId}`;
    return this.http.get<UserProject>(ur);
  }

  update(projectId: string, projectInformation: ProjectUpdateInfo) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    return this.http.put(
      `${environment.apiUrl}updateProjects/${projectId}`,
      projectInformation,
      { headers }
    );
  }

  getRole(projectId: string, userId: string) {
    return this.http.get<ProjectUser>(
      `${environment.apiUrl}admin/projects/${projectId}/users/${userId}`
    );
  }

  delete(projectId: string): Observable<Project> {
    const url = `${environment.apiUrl}admin/projects/${projectId}`;
    return this.http.delete<Project>(url);
  }

  getProject(projectId: string): Observable<Project> {
    const url = `${environment.apiUrl}admin/projects/${projectId}`;
    return this.http.get<Project>(url);
  }

  findAll(): Observable<Project[]> {
    const url = `${environment.apiUrl}admin/projects`;
    return this.http.get<any>(url);
  }

  createNewProject(newProject): Observable<any> {
    const url = `${environment.apiUrl}admin/projects`;
    return this.http.post<any>(url, newProject);
  }
}
