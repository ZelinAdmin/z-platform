import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class SnackBarService {
  constructor(private snackbar: MatSnackBar, private router: Router) {}

  unauthorized(message: string, redirection: string) {
    this.router.navigate([redirection]);
    this.snackbar.open(message, '', {
      duration: 2500,
      verticalPosition: 'top',
    });
  }
}
