import { ForgotPasswordInfo } from './../../interfaces/forgotPassword/forgot-password-info';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  get(id: string): Observable<any> {
    return this.http.get<UserInformation>(
      `${environment.apiUrl}admin/users/${id}`
    );
  }

  getAll(params: HttpParams | null = null): Observable<UserInformation[]> {
    return this.http.get<UserInformation[]>(
      `${environment.apiUrl}admin/users`,
      { params }
    );
  }

  getIsAdmin(id: string): Observable<boolean> {
    return this.http.get<boolean>(`${environment.apiUrl}user/isAdmin/${id}`);
  }

  updateUser(id: string, userInfo: UserInformation): Observable<any> {
    return this.http.put(`${environment.apiUrl}user/update/${id}`, userInfo);
  }

  updateUserAdmin(id: string, userInfo: UserInformation): Observable<any> {
    return this.http.put(`${environment.apiUrl}admin/users/${id}`, userInfo);
  }

  //  allows to get all data from a user except his password
  getPartial(id: string): Observable<UserInformation> {
    return this.http.get<UserInformation>(`${environment.apiUrl}user/${id}`);
  }

  deleteUser(id: string): Observable<any> {
    return this.http.delete(`${environment.apiUrl}admin/users/${id}`);
  }

  blockAcceptAccount(id: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}admin/users/block/${id}`, {});
  }

  changeAdminValueAccount(id: string): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}admin/users/setIsAdmin/${id}`,
      {}
    );
  }

  setVpnAcessOfUser(newAccess: any, userId: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}vpn/new-access/${userId}`, {
      vpnUsername: newAccess.vpnUsername,
      vpnPassword: newAccess.vpnPassword,
    });
  }

  updateUserCoreHourAdmin(newValue: any, userId: string): Observable<any> {
    return this.http.put(
      `${environment.apiUrl}admin/users/setCreditsTimeRatio/${userId}`,
      { creditsTimeRatio: newValue }
    );
  }

  changePremiumCommunityAccount(userId: string): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}admin/users/setIsPremiumCommunity/${userId}`,
      {}
    );
  }

  changeCommunityAccess(userId: string): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}admin/users/setHasCommunityAccess/${userId}`,
      {}
    );
  }

  //////////////////////////////////////////////
  //                FORGOT PASSWD             //
  //           All are permitted routes       //
  //////////////////////////////////////////////

  // when user forgot his forgotPwd : ...
  // ... he asks to change it, but before received an email with the link, this request check if an account allready exist
  getByEmail(email: string): Observable<UserInformation> {
    return this.http.get<UserInformation>(
      `${environment.apiUrl}checkEmailExist/${email}`
    );
  }

  // ... request to send him an email with a link to update his pwd

  // ... with the link received->new page where we get his data
  getUserInfo(id: string): Observable<UserInformation> {
    return this.http.get<UserInformation>(
      `${environment.apiUrl}forgot_password/user/${id}`
    );
  }

  // ... and will be able to change his pwd
  updateUserPwd(id: string, newPassword: ForgotPasswordInfo): Observable<any> {
    return this.http.put(
      `${environment.apiUrl}forgot_password/user/${id}`,
      newPassword
    );
  }
}
