import { InfoPopoverParams } from './../../sub-modules/vision/services/info-popover/info-popover.service';
import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { LoginStatus } from 'src//app/interfaces/login-status/login-status';
import { HashService } from 'src/app/services/hash/hash.service';
import { Globals } from 'src/app/globals/globals';
import { environment } from 'src/environments/environment';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  /* Create behaviour subject and observable so other components can subscribe
   * and know the state of log in real time (namely Guards)*/
  private loggedInBehaviourSubject = new BehaviorSubject<boolean>(false);
  private loggedInObservable = this.loggedInBehaviourSubject.asObservable();

  constructor(
    private hashService: HashService,
    private http: HttpClient,
    private ngZone: NgZone,
    public router: Router
  ) {}

  /* return the observable of logged status*/
  get isLoggedIn() {
    return this.loggedInObservable;
  }

  /* check if the user is logged
   * update behaviour subject
   * and return status as boolean*/
  get isLogged(): boolean {
    const token = localStorage.getItem(Globals.JWT_TOKEN);
    if (token !== null) {
      if (!this.loggedInBehaviourSubject.getValue()) {
        this.loggedInBehaviourSubject.next(true);
      }
      return true;
    }
    this.loggedInBehaviourSubject.next(false);
    return false;
  }

  /* send signup data to the server*/
  signUp(userInfo: UserInformation): Observable<any> {
    userInfo.password = this.hashService.hashPwd(userInfo.password);
    const url = environment.apiUrl + 'signup';
    return this.http.post<any>(url, userInfo);
  }

  /* forgotPassword*/
  // forgotPassword(email: string): Observable<UserInformation> {
  //   return this.http.get<UserInformation>(
  //     `${environment.apiUrl}forgot_password/${email}`
  //   );
  // }

  forgotPassword(email: string): Observable<any> {
    // send environment url to generate the url in the email
    const env = environment.visionUrl;
    return this.http.get<UserInformation>(
      `${environment.apiUrl}forgot_password/${email}`//,
  //    env
    );
  }

  /* login
   * store response in local storage so we can reuse token further on
   * update behaviour subject and redirect to home page*/
  login(username: string, pwd: string): Observable<any> {
    const url = environment.apiUrl + 'login';
    return this.http
      .post<any>(url, {
        email: username,
        password: this.hashService.hashPwd(pwd),
      })
      .pipe(
        map((user: LoginStatus) => {
            localStorage.setItem(Globals.JWT_TOKEN, user.token);
            localStorage.setItem(Globals.USERNAME, user.userId);
            localStorage.setItem(Globals.USER_ID, user.userId);
            localStorage.setItem(Globals.REFRESH_TOKEN, user.refreshToken);
            this.ngZone.run(() => {
            this.loggedInBehaviourSubject.next(true);
            this.router.navigate(['']);
          });
            return user;
        })
      );
  }

  refreshJWT(refreshToken: string, userId: string) {
    return this.http.get(
      `${environment.apiUrl}refresh/${userId}/${refreshToken}`,
      { responseType: 'text' }
    );
  }

  logout() {
    localStorage.clear();
    this.loggedInBehaviourSubject.next(false);
    this.router.navigate(['login']);
  }
}
