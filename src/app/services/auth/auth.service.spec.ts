import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import { Globals } from 'src/app/globals/globals';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Router } from '@angular/router';

describe('AuthService', () => {
  const routerSpy = { navigate: jasmine.createSpy('navigate') };
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  let service: AuthService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService, { provide: Router, useValue: routerSpy }],
    });
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
    service = TestBed.get(AuthService);

    /*mock local storage*/
    let store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      },
    };
    /* spy on localstorage*/
    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem').and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear').and.callFake(mockLocalStorage.clear);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(service).toBeDefined();
  });

  it('should have apiEndpoint in Enviroment', () => {
    expect(environment.apiUrl).toBeDefined();
  });

  it('should be false if the user token is missing', () => {
    expect(service.isLogged).toBe(false);
    service.isLoggedIn.subscribe((status) => {
      expect(status).toBe(false);
    });
  });

  it("should be true if the's a  token is local storage", () => {
    localStorage.setItem(Globals.JWT_TOKEN, 'randomTOken');
    expect(service.isLogged).toBe(true);
    service.isLoggedIn.subscribe((status) => {
      expect(status).toBe(true);
    });
  });

  it('should execute a proper logout, clean local storage and redirect to login page', () => {
    localStorage.setItem(Globals.USER_KEY, 'aedaz');
    localStorage.setItem(Globals.JWT_TOKEN, 'deae');
    localStorage.setItem('deze', 'aedaz');

    service.logout();
    expect(localStorage.getItem(Globals.USER_KEY)).toEqual(null);
    expect(localStorage.getItem(Globals.JWT_TOKEN)).toEqual(null);
    expect(localStorage.getItem('daze')).toEqual(null);
    expect(routerSpy.navigate).toHaveBeenCalledWith(['login']);
  });

  it('should create a user account', () => {
    const url = environment.apiUrl + 'signup';
    const userToCreate: UserInformation = {
      email: 'test@email.com',
      password: 'password',
      firstName: 'firstName',
      lastName: 'lastName',
    };
    service.signUp(userToCreate).subscribe(
      (res) => {},
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );

    const httpRequest = httpMock.expectOne(url);
    expect(httpRequest.request.method).toEqual('POST');
  });

  it('Login should no succeed if the credentials are wrong', () => {
    const email = 'poiuytreza';
    const password = 'poiuytreza';
    const emsg = 'deliberate 404 error';

    service.login(email, password).subscribe(
      (res) => {},
      (error: HttpErrorResponse) => {
        console.error(error);
        expect(error).toBeTruthy();
        expect(error.status).toBe(404);
      }
    );

    const httpRequest = httpMock.expectOne(environment.apiUrl + 'login');
    expect(httpRequest.request.method).toEqual('POST');
    /* Mock 404 response*/
    httpRequest.flush(emsg, { status: 404, statusText: 'Not found' });
  });

  it('If the credentials are OK, set localstorage, set LoggedStatus to true and navigate to /home page', () => {
    const email = 'email';
    const password = 'pwd';
    const MOCKED_TOKEN = 'RANDOMTOKEN';
    const MOCKED_USER_ID = 'USER';
    const MOCKED_NAME = 'NAME';
    const REFRESH_TOKEN = 'refresh';
    const MOCKED_PROJECTS = [
      {
        projectId: 'daeda',
        name: 'da',
        role: 'd',
      },
      {
        projectId: 'UIUI',
        name: 'II',
        role: 'III',
      },
    ];

    const successResponseBody = {
      token: MOCKED_TOKEN,
      userId: MOCKED_USER_ID,
      name: MOCKED_NAME,
      refreshToken: REFRESH_TOKEN,
      admin: false,
    };

    service.login(email, password).subscribe(
      (response) => {
        expect(localStorage.getItem(Globals.JWT_TOKEN)).toEqual(MOCKED_TOKEN);
        expect(localStorage.getItem(Globals.USERNAME)).toEqual(MOCKED_USER_ID);
        expect(localStorage.getItem(Globals.USER_ID)).toEqual(MOCKED_USER_ID);
        expect(localStorage.getItem(Globals.REFRESH_TOKEN)).toEqual(
          REFRESH_TOKEN
        );
        expect(routerSpy.navigate).toHaveBeenCalledWith(['']);
        expect(service.isLogged).toBe(true);
      },
      (error) => {}
    );

    const httpRequest = httpMock.expectOne(environment.apiUrl + 'login');
    // expect(httpRequest.request.method).toEqual('POST');

    /* Mock OK status*/
    httpRequest.flush(successResponseBody);
  });
});
