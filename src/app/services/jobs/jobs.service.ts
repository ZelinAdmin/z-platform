import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

import { Job } from 'src/app/interfaces/job/job';

const status = [
  {
    codeLetter: 'qw',
    status: 'Pending',
    description: 'Pending.',
  },
  {
    codeLetter: 'hqw',
    status: 'Pending',
    description: 'Pending, user and system hold.',
  },
  {
    codeLetter: 'hRwq',
    status: 'Pending',
    description: 'Pending, user hold, re-queue.',
  },
  {
    codeLetter: 'r',
    status: 'Running',
    description: 'Running.',
  },
  {
    codeLetter: 't',
    status: 'Running',
    description: 'Transferring.',
  },
  {
    codeLetter: 'Rr',
    status: 'Running',
    description: 'Running, re-submit.',
  },
  {
    codeLetter: 'Rt',
    status: 'Running',
    description: 'Transferring, re-submit.',
  },
  {
    codeLetter: 's',
    status: 'Suspended',
    description: 'Obsuspended.',
  },
  {
    codeLetter: 'S',
    status: 'Suspended',
    description: 'Queue suspended.',
  },
  {
    codeLetter: 'T',
    status: 'Suspended',
    description: 'Queue suspended by alarm.',
  },
  {
    codeLetter: 'Rs',
    status: 'Suspended',
    description: 'Allsuspended with re-submit.',
  },
  {
    codeLetter: 'Eqw',
    status: 'Error',
    description: 'Allpending states with error.',
  },
  {
    codeLetter: 'dr',
    status: 'Deleted',
    description: 'All running and suspended states with deletion.',
  },
  {
    codeLetter: 'f',
    status: 'Finished',
    description: 'Job completed.',
  },
];

@Injectable({
  providedIn: 'root',
})
export class JobService {
  constructor(private httpClient: HttpClient) {}

  getAll(params: HttpParams | null = null): Observable<Array<Job>> {
    return this.httpClient.get<Array<Job>>(
      `${environment.apiUrl}CompoUserJobs`,
      { params }
    );
  }

  getByUserId(
    userId: string,
    params: HttpParams | null = null
  ): Observable<Array<Job>> {
    return this.httpClient.get<Array<Job>>(
      `${environment.apiUrl}CompoUserJobs/user_id/${userId}`,
      { params }
    );
  }

  // deleteExpert(expertId: string): Observable<Expert> {
  //   return this.httpClient.delete<Expert>(
  //     `${environment.apiUrl}admin/experts/${expertId}`
  //   );
  // }
  // updateUser(id: string, expert: Expert): Observable<Expert> {
  //   return this.httpClient.put<Expert>(
  //     `${environment.apiUrl}expert/update/${id}`,
  //     expert
  //   );
  // }

  ///////////////////////////////

  public getNameFromCode(code: string): string {
    return status.find((x) => x.codeLetter === code).status;
  }

  public getDescFromCode(code: string): string {
    return status.find((x) => x.codeLetter === code).description;
  }

  public getJob(
    firstName: string,
    lastName: string,
    functionalId: number,
    jobName: string
  ) {
    const username: string =
      firstName.charAt(0).toLowerCase() +
      lastName.charAt(0).toLowerCase() +
      lastName.slice(1);
    return this.httpClient.get<any>(
      `${
        environment.apiUrl
      }UserJobs/${username}/${functionalId}/${encodeURIComponent(jobName)}`
    );
  }

  public downloadJob(
    firstName: string,
    lastName: string,
    functionalId: number,
    jobName: string
  ) {
    const username: string =
      firstName.charAt(0).toLowerCase() +
      lastName.charAt(0).toLowerCase() +
      lastName.slice(1);
    return this.httpClient.get(
      `${environment.apiUrl}UserJobs/download/${username}/${encodeURIComponent(
        jobName
      )}/${functionalId}`,
      {
        observe: 'response',
        responseType: 'blob',
      }
    );
  }

  public updateDb(
    userId: string,
    firstName: string,
    lastName: string
  ): Observable<any> {
    const username: string =
      firstName.charAt(0).toLowerCase() +
      lastName.charAt(0).toLowerCase() +
      lastName.slice(1);
    return this.httpClient.get<any>(
      `${environment.apiUrl}UserJobs/updateData/${userId}/${username}`,
      { responseType: 'blob' as 'json' }
    );
  }

  // Function to make difference between two dates in db
  public timeDifference(date1: Date, date2: Date) {
    let difference = date1.getTime() - date2.getTime();
    if (difference === 0) {
      return '0s';
    }

    const daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
    difference -= daysDifference * 1000 * 60 * 60 * 24;

    const hoursDifference = Math.floor(difference / 1000 / 60 / 60);
    difference -= hoursDifference * 1000 * 60 * 60;

    const minutesDifference = Math.floor(difference / 1000 / 60);
    difference -= minutesDifference * 1000 * 60;

    const secondsDifference = Math.floor(difference / 1000);

    let res = '';
    if (daysDifference !== 0) {
      res = res + daysDifference + 'd ';
    }
    if (hoursDifference !== 0) {
      res = res + hoursDifference + 'h ';
    }
    if (minutesDifference !== 0) {
      res = res + minutesDifference + 'min ';
    }
    if (secondsDifference !== 0) {
      res = res + secondsDifference + 's';
    }
    if (res !== '') {
      res = res.slice();
    }
    return res;
  }

  /* This function is here to choose which kind of duration the jobs has
  Running : beginDate - now
  Finished : beginDate - endDate
  Else : "" */
  public setCurrentEndDate(elem: Job, currentDatetime: Date) {
    if (this.getNameFromCode(elem.status) === 'Running') {
      return this.timeDifference(new Date(), new Date(elem.startDate));
    } else if (this.getNameFromCode(elem.status) === 'Finished') {
      return this.timeDifference(
        new Date(elem.endDate),
        new Date(elem.startDate)
      );
    } else {
      return '';
    }
  }
}
