import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { VrmlFile } from 'src/app/interfaces/vrml-file/vrml-file';
import { Socket } from 'ngx-socket-io';

interface Chunk {
  data: any;
  end: boolean;
  uuid?: string;
  filename?: string;
}

interface ConversionInformation {
  currentSize: number;
  approximativeFileSize: number;
  uuid: string;
}

@Injectable({
  providedIn: 'root',
})
export class VisualizerService {
  public fileQueue: any[];
  private status: BehaviorSubject<any>;

  /*colormap status*/
  private colormapStateBehaviourSubject = new BehaviorSubject<string>('');
  public colormapState = this.colormapStateBehaviourSubject.asObservable();

  constructor(private http: HttpClient, private socket: Socket) {
    this.status = new BehaviorSubject<any>(null);
    this.fileQueue = [];
    this.transfer();
    this.socket.on('success', () => {
      console.log('connected');
    });
    this.socket.on('notifyStatus', (fileStatus: VrmlFile) => {
      this.status.next(fileStatus);
    });
    this.socket.on('transferNotAllowed', () => {
      console.log('You cannot upload file with GUEST/CUSTOMER rights.');
    });
    this.socket.on('colormapSuccess', (name) => {
      this.colormapStateBehaviourSubject.next(name.name);
    });
    this.conversionProcess();
  }

  public startTransfer(file: File) {
    if (file.name.match(/^(.+?)\.colormap$/gim)) {
      this.transferColormap(file);
    } else {
      this.fileQueue.push(file);
      if (this.fileQueue.length) {
        this.socket.emit('startTransfer', file.name);
      }
    }
  }

  private transferColormap(file: File) {
    const reader = new FileReader();
    reader.onload = (evt: any) => {
      if (evt.target.error === null) {
        this.socket.emit('colormap', {
          data: evt.target.result,
          name: file.name,
        });
      }
    };
    reader.readAsText(file);
  }

  private transfer() {
    // tslint:disable-next-line:variable-name
    this.socket.on('transferBegin', (_uuid: string) => {
      const uuid = _uuid;
      const file = this.fileQueue[0];

      this.fileQueue.splice(0, 1);

      if (file.name.match(/^(.+?)\.(wrl|vrml)$/gim)) {
        const chunkSize = 60000;
        let offset = 0;

        const readHandler = (evt: any) => {
          if (evt.target.error === null) {
            offset += chunkSize;
            const chunk: Chunk = {
              data: evt.target.result,
              end: false,
              uuid,
              filename: file.name,
            };
            if (offset >= file.size) {
              chunk.end = true;
              chunk.filename = file.name;
            }
            this.socket.emit('chunk', chunk);
            this.status.next({
              status: `${(offset / (1024 * 1024)).toFixed(2)} MB`,
              uuid,
            } as VrmlFile);
            if (!chunk.end) {
              readChunk(file, offset);
            }
          } else {
            console.log('Error during file reading.');
          }
        };

        // tslint:disable-next-line:variable-name
        const readChunk = (_file: any, _offset: number) => {
          const reader = new FileReader();
          const blob = _file.slice(_offset, _offset + chunkSize);
          reader.onload = readHandler;
          reader.readAsText(blob);
        };
        this.status.next({
          uuid,
          size: 0,
          name: file.name,
          hascolormap: false,
        } as VrmlFile);
        readChunk(file, offset);
      }
    });
  }

  public getState() {
    return this.status.asObservable();
  }

  public getFilesList(projectId: string): Observable<VrmlFile[]> {
    return this.http.get<VrmlFile[]>(
      `${environment.modelServer}filelist/${projectId}`
    );
  }

  public deleteFile(uuid: string, projectId: string): Observable<any> {
    return this.http.delete(`${environment.modelServer}${projectId}/${uuid}`, {
      responseType: 'text',
    });
  }

  public loadFile(
    uuid: string,
    projectId: string,
    fileuuid: string
  ): Observable<any> {
    return this.http.get(
      `${environment.modelServer}import/${projectId}/${uuid}/${fileuuid}`,
      { responseType: 'json', reportProgress: true, observe: 'events' }
    );
  }

  /* Fetch array of obj files*/
  public fetchFilesInformation(
    uuid: string,
    projectId: string
  ): Observable<any> {
    return this.http.get(
      `${environment.modelServer}fetch/${projectId}/${uuid}`,
      { responseType: 'json', reportProgress: true, observe: 'events' }
    );
  }

  public getStatus(uuid: string): Observable<any> {
    return this.http.get(`${environment.modelServer}status/${uuid}`);
  }

  public conversionProcess() {
    this.socket.on('conversion', (info: ConversionInformation) => {
      this.status.next({
        uuid: info.uuid,
        status: `${Number(
          (info.currentSize / info.approximativeFileSize) * 100
        ).toFixed(2)} %`,
      });
    });
  }
}
