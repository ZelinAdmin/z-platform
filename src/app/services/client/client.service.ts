import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client } from 'src/app/interfaces/client/client';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserInformation } from 'src/app/interfaces/user-information/user-information';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get<Client>(`${environment.apiUrl}clients`);
  }

  get(clientId: string): Observable<Client> {
    return this.http.get<Client>(`${environment.apiUrl}client/${clientId}`);
  }

  getByUserId(userId: string): Observable<Client> {
    return this.http.get<Client>(`${environment.apiUrl}client/user/${userId}`);
  }

  getByRating(rating: number): Observable<Client> {
    return this.http.get<Client>(
      `${environment.apiUrl}client/rating/${rating}`
    );
  }

  updateUser(id: string, client: UserInformation): Observable<Client> {
    return this.http.put<Client>(
      `${environment.apiUrl}client/update/${id}`,
      client
    );
  }

  updateUserAdmin(id: string, client: UserInformation): Observable<Client> {
    return this.http.put<Client>(
      `${environment.apiUrl}admin/client/update/${id}`,
      client
    );
  }

  changeRatingValue(id: string, rating: number): Observable<Client> {
    return this.http.put<Client>(
      `${environment.apiUrl}admin/client/rating/${id}`,
      rating
    );
  }
}
