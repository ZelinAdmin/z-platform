import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { Globals } from 'src/app/globals/globals';
import { catchError, switchMap, filter, take } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );

  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    request = this.insertAuthToken(request);

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error && error.status === 401) {
          /* most likely JWT expired*/
          if (this.refreshTokenInProgress) {
            // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
            // which means the new token is ready and we can retry the request again
            return this.refreshTokenSubject.pipe(
              filter((result) => result !== null),
              take(1),
              switchMap(() => next.handle(this.insertAuthToken(request)))
            );
          } else {
            this.refreshTokenInProgress = true;
            // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
            this.refreshTokenSubject.next(null);

            if (localStorage.getItem(Globals.REFRESH_TOKEN)) {
              /* ask a new jwt token*/
              this.authService
                .refreshJWT(
                  localStorage.getItem(Globals.REFRESH_TOKEN),
                  localStorage.getItem(Globals.USER_ID)
                )
                .pipe(
                  switchMap((jwt: string) => {
                    this.refreshTokenSubject.next(true);
                    /* set new jwt, and rerun the request*/
                    localStorage.setItem(Globals.JWT_TOKEN, jwt);
                    // When the call to refreshToken completes we reset the refreshTokenInProgress to false
                    // for the next time the token needs to be refreshed
                    this.refreshTokenInProgress = false;
                    return next.handle(this.insertAuthToken(request));
                  })
                )
                .subscribe((value) => {});
            }
          }
        } else {
          return throwError(error);
        }
      })
    );
  }

  private insertAuthToken(request: HttpRequest<any>): HttpRequest<any> {
    const token = localStorage.getItem(Globals.JWT_TOKEN);
    if (!token) {
      return request;
    }
    if (request.url.includes('refresh')) {
      return request;
    }

    return request.clone({
      setHeaders: {
        Authorization: `Bearer ` + token,
      },
    });
  }
}
