import { async, fakeAsync, TestBed } from '@angular/core/testing';

import { JwtInterceptor } from './jwt.interceptor';
import { environment } from 'src/environments/environment';
import { Globals } from 'src/app/globals/globals';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { UserService } from 'src/app/services/user/user.service';
import { AuthService } from 'src/app/services/auth/auth.service';

describe('JwtInterceptorInterceptor', () => {
  let service: UserService;
  let http: HttpClient;

  const authServiceSpy = {
    refreshJWT: (token, userId) => {
      return http.get(`${environment.apiUrl}refresh/${userId}/${token}`, {
        responseType: 'text',
      });
    },
  };

  let httpMock: HttpTestingController;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        UserService,
        { provide: AuthService, useValue: authServiceSpy },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptor,
          multi: true,
        },
      ],
    });

    httpMock = TestBed.inject(HttpTestingController);
    http = TestBed.inject(HttpClient);
    service = TestBed.inject(UserService);

    /*mock local storage*/
    let store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      },
    };
    /* spy on localstorage*/
    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem').and.callFake(mockLocalStorage.removeItem);
  }));

  afterEach(() => {
    httpMock.verify();
  });

  it('should create and token', () => {
    localStorage.setItem(Globals.JWT_TOKEN, 'randomToken');
    expect(localStorage.getItem(Globals.JWT_TOKEN)).toEqual('randomToken');
  });

  it('should fail if there\'s no token in the local storage', () => {
    service.get('random').subscribe((response) => {
      expect(response).toBeTruthy();
    });
    const httpRequest = httpMock.expectOne(
      `${environment.apiUrl}admin/users/random`
    );

    expect(httpRequest.request.headers.has('Authorization')).toEqual(false);
  });

  it('should add an Authorization header', () => {
    localStorage.setItem(Globals.JWT_TOKEN, 'randomToken');
    service.get('random').subscribe((response) => {
      expect(response).toBeTruthy();
    });
    const httpRequest = httpMock.expectOne(
      `${environment.apiUrl}admin/users/random`
    );

    expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
  });

  // it('should get a new authorization token with refresh token', () => {
  //   const refreshToken = 'token';
  //   const userId = 'dazhda';
  //   const newJwt = '987654321';
  //   localStorage.setItem(Globals.REFRESH_TOKEN, refreshToken);
  //   localStorage.setItem(Globals.USER_ID, userId);
  //   localStorage.setItem(Globals.JWT_TOKEN, 'newtoken');
  //   localStorage.setItem(Globals.USERNAME, 'admin');
  //
  //
  //   service.get('random').toPromise().then((response) => {
  //     expect(response).toBeTruthy();
  //   });
  //
  //   httpMock.expectOne(
  //     `${environment.apiUrl}admin/users/random`
  //   ).error(new ErrorEvent('unauthorized'), {status: 401, statusText: 'Unauthorized action'});
  //
  //   const httpRefreshRequest = httpMock.expectOne(
  //     `${environment.apiUrl}refresh/dazhda/token`
  //   );
  //   httpRefreshRequest.flush(newJwt);
  // });
});
