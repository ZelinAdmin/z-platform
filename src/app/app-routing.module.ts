import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './shared-pages/not-found/not-found.component';
import { LoginComponent } from './shared-pages/login/login.component';
import { InnerPageGuard } from './guards/inner-page-guard/inner-page.guard';
import { RegisterComponent } from './shared-pages/register/register.component';
import { AuthGuard } from './guards/auth-guard/auth.guard';
import { HomeComponent } from 'src/app/shared-pages/home/home.component';
import { ResetPasswordComponent } from './shared-pages/reset-password/reset-password.component';
import { ChangePasswordComponent } from './shared-pages/change-password/change-password.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [InnerPageGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [InnerPageGuard],
  },
  {
    path: 'resetPassword',
    component: ResetPasswordComponent,
    canActivate: [InnerPageGuard],
  },
  {
    path: 'changePassword',
    loadChildren: () =>
      import('./shared-pages/change-password/change-password.module').then(
        (m) => m.ChangePasswordModule
      ),
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'vision',
    loadChildren: () =>
      import('./sub-modules/vision/vision.module').then((m) => m.VisionModule),
    canActivateChild: [AuthGuard],
  },
  {
    path: 'drive',
    loadChildren: () =>
      import('./sub-modules/drive/drive.module').then((m) => m.DriveModule),
    canActivateChild: [AuthGuard],
  },
  {
    path: 'community',
    loadChildren: () =>
      import('./sub-modules/community/community.module').then(
        (m) => m.CommunityModule
      ),
    canActivateChild: [AuthGuard],
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./sub-modules/admin/admin.module').then((m) => m.AdminModule),
    canActivateChild: [AuthGuard],
  },

  {
    path: 'profile',
    loadChildren: () =>
      import('./sub-modules/profile/profile.module').then(
        (m) => m.ProfileModule
      ),
    canActivateChild: [AuthGuard],
  },
  {
    path: 'z2c',
    loadChildren: () =>
      import('./sub-modules/z2c/z2c.module').then((m) => m.Z2cModule),
    canActivateChild: [AuthGuard],
  },
  {
    path: 'requests',
    loadChildren: () =>
      import('./sub-modules/request/request.module').then(
        (m) => m.RequestModule
      ),
  },
  {
    path: 'experts',
    loadChildren: () =>
      import('./sub-modules/expert/expert.module').then((m) => m.ExpertModule),
    canActivateChild: [AuthGuard],
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: '**',
    component: NotFoundComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
