import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

// Angular material imports
/* Angular material imports*/

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './shared-components/header/header.component';
import { FooterComponent } from './shared-components/footer/footer.component';
import { LoginComponent } from './shared-pages/login/login.component';
import { RegisterComponent } from './shared-pages/register/register.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NotFoundComponent } from './shared-pages/not-found/not-found.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JwtInterceptor } from './interceptors/JWTInterceptor/jwt.interceptor';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { CommonModule, registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FileUploadComponent } from './shared-components/dialogs/file-upload/file-upload.component';
import { DragAndDropDirective } from './directives/drag-and-drop.directive';
import { ConfirmationComponent } from './shared-components/dialogs/confirmation/confirmation.component';
import { MatListModule } from '@angular/material/list';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from '../environments/environment';
import { MatSelectModule } from '@angular/material/select';
import { HomeComponent } from './shared-pages/home/home.component';
import { MatRadioModule } from '@angular/material/radio';
import { ResetPasswordComponent } from './shared-pages/reset-password/reset-password.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ChangePasswordComponent } from './shared-pages/change-password/change-password.component';

registerLocaleData(en);

const config: SocketIoConfig = {
  url: environment.socketIo,
  options: {
    // query:
    //   "auth_token=" +
    //   localStorage.getItem("token") +
    //   "&projectid=" +
    //   localStorage.getItem("currentProject"),
    autoConnect: true,
  },
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    NotFoundComponent,
    FileUploadComponent,
    DragAndDropDirective,
    ConfirmationComponent,
    HomeComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatIconModule,
    MatToolbarModule,
    MatDividerModule,
    MatSidenavModule,
    MatButtonModule,
    MatGridListModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatTooltipModule,
    MatSnackBarModule,
    HttpClientModule,
    NzDatePickerModule,
    FormsModule,
    MatDialogModule,
    MatListModule,
    SocketIoModule.forRoot(config),
    MatSelectModule,
    MatRadioModule,
    MatProgressBarModule,
  ],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {},
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    { provide: NZ_I18N, useValue: en_US },
  ],
  bootstrap: [AppComponent],
  entryComponents: [FileUploadComponent, ConfirmationComponent],
  exports: [ResetPasswordComponent, ChangePasswordComponent],
})
export class AppModule {}
