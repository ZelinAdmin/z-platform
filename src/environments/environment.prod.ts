export const environment = {
  production: true,
  apiUrl: 'http://10.0.0.152:11000/',
  modelServer: 'http://10.0.0.152:11500',
  zvisionUrl: 'http://10.0.0.152:80',
  socketIo: 'http://10.0.0.152:11501/',
  visionUrl: 'http://10.0.0.152',
};
