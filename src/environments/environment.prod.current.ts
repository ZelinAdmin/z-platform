export const environment = {
  production: true,
  apiUrl: 'http://10.0.0.150:8081/',
  modelServer: 'http://10.0.0.150:3011',
  socketIo: 'http://10.0.0.150:3012/',
  visionUrl: 'http://10.0.0.150:4243',
};
