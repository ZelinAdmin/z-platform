FROM node:12.2.0 as builder

WORKDIR /app/zdrive

# USE CACHE TO THE MAXIMUM
COPY package*.json ./
RUN npm ci

COPY . .

RUN npm i
RUN npm run ng build -- --configuration=dev



# Nginx configured for Single page apps
FROM fairlyn/nginx-alpine

COPY --from=builder /app/zdrive/dist/vision-material /usr/share/nginx/html
