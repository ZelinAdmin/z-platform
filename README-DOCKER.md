# DOCKER CONFIG

## ACCESS

---

**Connect to the server**<br>

1. Open Git Bash<br>
2. enter the ssh dev login & password:<br>
   > $ssh zelin@10.0.0.150<br>
  > $ZelinDevServer2020\*<br>

Now you are into the server files<br>
<br>

**Quit to the server**<br>

> \$exit<br>

Now you're back in local

**Restart docker **<br>

from ~/z-plateform/Master:

> \$docker-compose restart back<br>

## Now you're back in local

## ADD A TEMPLATE

---

From the local project:<br>

> ebarbe@DESKTOP-G3NCTGE MINGW64 ~/PROJECT/zelin-back _(exemple)_<br>
> Copy paste the file to the desired server location : exemple:<br>

- fichier :../../mytemplate.html<br>
- emplacement dans le server: zelin@10.0.0.150:/home/zelin/z-plateform/Master/ressources/templateHtml/<br>

> scp ../../mytemplate.html zelin@10.0.0.150:/home/zelin/z-plateform/Master/ressources/templateHtml/

You will have to enter login and mdp, then terminal will inform you about success of the operation.

To check: connect you to the server (see above), use the command to move into the files and check if mytemplate.html is where you wanted.
